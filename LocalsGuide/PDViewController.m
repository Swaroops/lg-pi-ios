   /*
 
 http://lg.listlinkz.com/webservice/
 http://localsguide.net/webservice/
 http://listlinkz.com/localsguide/webservice/
 
 */

#import "PDAppDelegate.h"
#import "PDViewController.h"
#import "AsyncImageView.h"
#import "PDFavoriteViewController.h"
#import "PDGalleryViewController.h"
#import "Reachability.h"
#import "UIImageView+AFNetworking.h"
#import <Social/Social.h>
#import <QuartzCore/QuartzCore.h>
#import "GMDCircleLoader.h"
#import "CustomIOSAlertView.h"

#import "PDSplashViewController.h"

#import "UIImageView+WebCache.h"
#import "JMImageCache.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "PDInappHomeViewController.h"

#import "Apps.h"
#import "Inapp.h"
#import "IMAGES_TO_CACHE.h"
#import "Static.h"
#import "Categories.h"
#import "Businesses.h"
#import "Gallery.h"
#import "Deals.h"
#import "Events.h"

#import "MKStoreKit.h"
#import "NSString+MD5.h"
#import "SettingsViewController.h"

#import "DBOperations.h"

#import "AsyncImageView.h"

#import "AFNetworking.h"



#import "UIImageView+JMImageCache.h"


#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define degreesToRadian(x) (M_PI * (x) / 180.0)

@interface PDViewController () <UITableViewDataSource, UITableViewDelegate,CustomIOSAlertViewDelegate,DBOperationsDelegate,UISearchBarDelegate>
{
    BOOL x, isLocationUpdated, isHotSpotAd;
    int scrollHeight;
    int banery;
    int index;
    UIRefreshControl *refreshControl;
    UITableView *tblViewHome;
    NSArray* arrResponse1;
    NSArray*arrResponse2;
    NSArray*arrResponse3;
    NSArray*arrResponse4;
    NSArray*arrResponse5;
    NSArray*arrResponse6;
    
    AsyncImageView *ad1Image;
    
    NSInteger currentIndex;
    CustomIOSAlertView *customAlert;
    
    NSString *bannerImgURL;
    
    IBOutlet UITableView *searchListView;
    IBOutlet UIView *searchView;
    
    BOOL startOwnGuide;
    
    AsyncImageView *tblHeaderImage;
    
    NSMutableArray *arra;
    NSArray *arrPaid;
    NSString *paid_type;
}
@end

@implementation PDViewController
NSArray *arrayContents;
NSArray *arAdDetails;
CLLocation *location_updated;
NSString *adLink;
NSString *app_View;

@synthesize appName,showBkBtn,queArray;
UITabBarController *tabbar;
-(void)initwithNavigationController:(UINavigationController*)obj
{
    
    
    mainNavigationController=obj;
}
-(void)fillDataArray:(NSArray*)dataArr
{
      arrayContents=[[NSArray alloc]init];
    arrayContents=dataArr;
    queArray=[dataArr mutableCopy];
  
    ///
//    PDSubCategoryViewController *sub =[[PDSubCategoryViewController alloc]init];
//    [sub getSubArray:[dataArr valueForKey:@"subcategory"]];
    ///
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"VIEW_GRID"] isEqualToString:@"YES"]) {
        [scrollHome removeFromSuperview];
        [self setUpScroll];
    }
    else {
        [tblViewHome removeFromSuperview];
        [self setUpListView];
        
        // [self setUpScroll];
    }
}
-(IBAction)settingsAction:(id)sender
{
    SettingsViewController *tvc=[[SettingsViewController alloc]init];
    [self presentViewController:tvc animated:NO completion:nil];
}

- (IBAction)searchAction:(id)sender {
    
    tblViewHome.tableHeaderView =  nil;
    
    tblViewHome.tableHeaderView = searchBar;
    [searchBar becomeFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    
     [[NSUserDefaults standardUserDefaults]setObject:searchBar.text forKey:@"searchBar.text"];
    
    [self.view addSubview:vwLoading];
    
    if([self offlineAvailable])
    {
       /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Search not available in offline mode"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [vwLoading removeFromSuperview];
        
        return;*/
        
        NSString*string = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
        
        arra = [[NSMutableArray alloc]init];
        
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:[NSString stringWithFormat:@"forSearchOffline%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        NSMutableArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//an active internet connection is required for global search
        
      
        
        arra= retrievedDictionary;
        
       // arra =  [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"forSearchOffline%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
        NSLog(@"arra :%@",arra);
        
       
        
        if([string isEqualToString:@"skipIntermediate"])
        {
            BOOL isTheObjectThere = [arra containsObject:[NSString stringWithFormat:@"%@",searchBar.text]];
            
            NSUInteger indexOfTheObject = [arra indexOfObject:[NSString stringWithFormat:@"%@",searchBar.text]];
            
            NSLog(@"isTheObjectThere :%hhd,%lu",isTheObjectThere,(unsigned long)indexOfTheObject);
            
            [self searchOffline];
            
        }
        
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Search is only available if guide is downloaded."
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [vwLoading removeFromSuperview];
            
            return;
        
        }
        
        
    }
    else if([self reachable])
    {
      [self searchKeywrd];
        
    }
    else
    {
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Search is only available if guide is downloaded."
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [vwLoading removeFromSuperview];
            
            return;
            
        }
    
    }
}

-(void)searchOffline
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        searchArray=[[NSMutableArray alloc]init];
        searchArray=arra;
        
        if([searchArray count]<1)
        {
            [searchBar becomeFirstResponder];
            searchBar.text=@"";
            [vwLoading removeFromSuperview];
            
            tblViewHome.tableHeaderView =  nil;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[NSString stringWithFormat:@"No Results Found"]
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
           
           // [[NSUserDefaults standardUserDefaults]setObject:arra forKey:@"searchData"];
            
             NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:arra];
            [[NSUserDefaults standardUserDefaults]setObject:data2 forKey:@"searchData"];
            
            [vwLoading removeFromSuperview];
            [activityIndicator1 stopAnimating];
            
            PDListViewController *listViewController = [[PDListViewController alloc] init];
            // [listViewController fixedWebServiceResponsee:data];
            listViewController.isSomethingEnabled = YES;
            listViewController.isfromVC=YES;
            listViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:listViewController animated:YES];
            
            
                    }
        [self.view endEditing:YES];
        
    });


}
-(void)searchKeywrd
{
    NSString *keywrd;
    NSString *dist;
    
    keywrd = searchBar.text;
    
    keywrd = [keywrd stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if(keywrd.length <3)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Search term should have a length of minimum 3 characters"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [vwLoading removeFromSuperview];
        
        return;
    }

    
   NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
   
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KM"] isEqualToString:@"NO"])
    {
        dist=@"M";
    }
    else{
        dist=@"K";
    }
    
    //http://insightto.com/webservice/search_listing.php?text=
    
    NSString *lat= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
    NSString *lon= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
    NSLog(@"lat : %@ , long : %@",lat,lon);

    
    NSString *wUrl=[NSString stringWithFormat:@"http://insightto.com/webservice/search_listing.php?text=%@&id=%@&lat=%@&long=%@&distance_in=%@",keywrd,str,lat,lon,dist];
    
    NSLog(@"url = %@",wUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
      //  NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            searchArray=[[NSMutableArray alloc]init];
            searchArray=[json objectForKey:@"business"];
            
            if([searchArray count]<1)
            {
                [searchBar becomeFirstResponder];
                searchBar.text=@"";
                [vwLoading removeFromSuperview];
                
                tblViewHome.tableHeaderView =  nil;
                
                NSString *message;
                if([json objectForKey:@"message"])
                {
                    message = [json objectForKey:@"message"];
                    
                }
                else
                {
                    message=[json objectForKey:@"result"];
                }
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:[NSString stringWithFormat:@"%@",message]
                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                //[loaderVC removeFromSuperview];
            }
            else
            {
                //[searchListView reloadData];
                //[self.view addSubview:searchView];
                [[NSUserDefaults standardUserDefaults]setObject:data2 forKey:@"searchData"];
                
                [vwLoading removeFromSuperview];
                [activityIndicator1 stopAnimating];
                
                PDListViewController *listViewController = [[PDListViewController alloc] init];
               // [listViewController fixedWebServiceResponsee:data];
                listViewController.isSomethingEnabled = YES;
                listViewController.isfromVC=YES;
                listViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:listViewController animated:YES];
                
                
                
              /*  {
                    if (_isCellClicked)return;
                    
                    
                    
                    NSDictionary *dict = [searchArray objectAtIndex:0];
                    
                    NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
                    
                    if(![unlimitedPurchase isEqualToString:@"purchased"])
                    {
                        NSInteger points=[[dict objectForKey:@"points"] integerValue];
                        if([[dict objectForKey:@"paid"] isEqualToString:@"paid"]&&points>0)
                        {
                            
                            
                            
                            if(![self checkCategoryPurchased:[dict objectForKey:@"Id"]])
                            {
                                
                                [self inappPurchasePopShow:0];
                                return;
                                
                            }
                        }
                        
                    }
                    _isCellClicked=YES;
                   
                    if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
                        PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
                        [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:[dict objectForKey:@"type"] withName:[dict objectForKey:@"name"]];
                        [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
                        subCategoryViewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:subCategoryViewController animated:YES];
                    }
                    else if ([[dict objectForKey:@"type"] isEqualToString:@"url"]){
                        if([self reachable]){
                            
                            
                            
                            NSString *webString =[dict objectForKey:@"external_url"];
                            if ([webString rangeOfString:@"http://"].location == NSNotFound)
                                webString=[NSString stringWithFormat:@"http://%@",webString];
                            
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:webString]];
                            _isCellClicked=NO;
                        }
                        else{
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            return;
                        }
                    }
                    else if ([[dict objectForKey:@"type"] isEqualToString:@"static"])
                    {
                        PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
                        [detailViewController getDictionary:dict];
                        detailViewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:detailViewController animated:YES];
                    }
                    else if ([[dict objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dict objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dict objectForKey:@"type"] isEqualToString:@"normal"]){
                        PDListViewController *listViewController = [[PDListViewController alloc] init];
                        [listViewController getDictionary:dict];
                        listViewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:listViewController animated:YES];
                    }
                    
                    else if ([[dict objectForKey:@"type"] isEqualToString:@"event"]) {
                        
                        PDEventsViewController *viewController = [[PDEventsViewController alloc] init];
                        viewController.title = [dict objectForKey:@"name"];
                        
                        [viewController getDictionaryeve:dict];
                        
                        //        if (![[dict objectForKey:@"maxeventid"] isEqualToString:@"FALSE"]) {
                        //            if (![[dict objectForKey:@"maxeventid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxEventId"]]) {
                        //                viewController.tabBarItem.badgeValue = @"new";
                        //            }
                        // }
                        viewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                    
                    else if ([[dict objectForKey:@"type"] isEqualToString:@"deal"]) {
                        PDDealsViewController *viewController = [[PDDealsViewController alloc] init];
                        viewController.title = [dict objectForKey:@"name"];
                        //        if (![[dict objectForKey:@"maxdealid"] isEqualToString:@"FALSE"]){
                        //            if (![[dict objectForKey:@"maxdealid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxDealId"]]) {
                        //                viewController.tabBarItem.badgeValue = @"new";
                        //            }
                        //        }
                        viewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                    
                    else if ([[dict objectForKey:@"type"] isEqualToString:@"rss"]) {
                        if([self reachable]){
                            PDRssListViewController *rssListViewController = [[PDRssListViewController alloc] initWithNibName:@"PDRssListViewController" bundle:nil];
                            [rssListViewController getDictionary:dict];
                            rssListViewController.isFromHome = YES;
                            rssListViewController.hidesBottomBarWhenPushed = YES;
                            [self.navigationController pushViewController:rssListViewController animated:YES];
                        }
                        else{
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            return;
                        }
                    }
                    else if ([[dict objectForKey:@"type"] isEqualToString:@"gallery"]) {
                        PDGalleryViewController *galleryViewController = [[PDGalleryViewController alloc] initWithNibName:@"PDGalleryViewController" bundle:nil];
                        [galleryViewController getDictionary:dict];
                        galleryViewController.hidesBottomBarWhenPushed = YES;
                        //galleryViewController.isFromHome = YES;
                        galleryViewController.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:galleryViewController animated:YES];
                    }
                    else {
                        if ([[dict objectForKey:@"type"] isEqualToString:@"fixed"] || [[dict objectForKey:@"type"] isEqualToString:@"location"]) {
                            if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
                                PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
                                [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:@"fixed" withName:[dict objectForKey:@"name"]];
                                [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
                                subCategoryViewController.hidesBottomBarWhenPushed = YES;
                                [self.navigationController pushViewController:subCategoryViewController animated:YES];
                            }
                            else {
                                PDListViewController *listViewController = [[PDListViewController alloc] init];
                                [listViewController getDictionary:dict];
                                listViewController.hidesBottomBarWhenPushed = YES;
                                [self.navigationController pushViewController:listViewController animated:YES];
                            }
                        }
                    }
                    
                }*/
              
                
               
                
                
                
                // tableView.userInteractionEnabled = NO;
            }
            [self.view endEditing:YES];
            
        });
        
    }];
    
    [dataTask resume];
    
    
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
    tblViewHome.tableHeaderView =  nil;
     searchBar.text = @"";
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
-(IBAction)backToApplist:(id)sender
{

    if(_isFromSearch)
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
    else if([[NSUserDefaults standardUserDefaults]objectForKey:@"yThis"])
    {
        
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[PDInappHomeViewController class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
            else
            {
               

            }
        }
       // [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
        // [self.navigationController popToRootViewControllerAnimated:NO];
        
        PDInappHomeViewController *inappVC=[[PDInappHomeViewController alloc]init];
        inappVC.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:inappVC animated:NO];
      
    }
    else
    {
        
       NSString*string = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"thisForBackFromSettings"])
        {
          [self.tabBarController.navigationController popViewControllerAnimated:YES];
            
        }
        else if([string isEqualToString:@"skipIntermediate"])
        {
         //[self.tabBarController.navigationController popViewControllerAnimated:YES];
            [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
        }
        
        else
             [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
            //[self.tabBarController.navigationController popViewControllerAnimated:YES];
       
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:@"ttt" forKey:@"clasiificationLimiter"];
    
    }

#pragma mark -Life Cycle functions
- (void)viewDidLoad
{
     arrPaid = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    
    [self.tabBarController.tabBar setHidden:NO];
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSDictionary *dictionary=[inappStr objectAtIndex:0];
    [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
    [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
    [super viewDidLoad];
  
    
   // [scrollHome removeFromSuperview];
  //  [tblViewHome removeFromSuperview];
 
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.delegate = self;
    
    isLocationUpdated = NO;
    
    if (IS_IPHONE5) {
        [[NSBundle mainBundle] loadNibNamed:@"PDViewController" owner:self options:nil];
        scrollHeight = 450+400;
        banery = 470;
    }
    else {
        [[NSBundle mainBundle] loadNibNamed:@"PDViewController4" owner:self options:nil];
        scrollHeight = 365;
        banery = 382;
    }
    if (!self.showBkBtn) {
        
        
        backBtn.hidden=YES;
        _backBtnImg.hidden=YES;
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.leftBarButtonItem = nil;
    }
    
     if (IS_IPHONE5) {
    scrollHeight=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-10;
    
     }
    else
    {
    
    }
  
    
//    if ([self reachable])
//        [self startHotSpotWebService];
  
   

    
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    
    appName= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppName"];
    titlLabel.text= [NSString stringWithFormat:@"%@", appName];
    titlLabel2.text=[NSString stringWithFormat:@"%@", appName];
    
   
    if(_isFromSearch)
    {
        backBtn.hidden=NO;
        _backBtnImg.hidden=NO;
        
        self.navigationItem.hidesBackButton = NO;
        
        titlLabel.text= [NSString stringWithFormat:@"Result"];
        
    }
   
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    searchBar.placeholder=@"search";
    searchBar.delegate = self;
    searchBar.showsCancelButton=YES;
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTintColor:[UIColor whiteColor]];
    
    if(_fromOfficialGuideSearch)
    {
        backBtn.hidden=NO;
        _backBtnImg.hidden=NO;
    }
    
    //////////////////////
    
  NSInteger indexe = [[NSUserDefaults standardUserDefaults]integerForKey:@"forGettingIndexPath"];
    
    paid_type =[[arrPaid objectAtIndex:indexe]objectForKey:@"paid_type"];
    
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *stri= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"VIEW_GRID"] isEqualToString:@"YES"]) {
        [scrollHome removeFromSuperview];
        if (![self offlineAvailable])
        {
            DBOperations *dbObj=[[DBOperations alloc]init];
            [dbObj setDelegate:self];
            [dbObj startRefeshHomePage];
        }
        else
            [self setUpScroll];
    }
    else {
        [tblViewHome removeFromSuperview];
        if ([self reachable])
        {
            if(_isFromSearch)
            {
                NSDictionary *jdict = [[NSUserDefaults standardUserDefaults]objectForKey:@"isfromsearch"];
                arrayContents=[jdict objectForKey:@"business"];
                [self setUpListView];
            }
            else{
                
                
                DBOperations *dbObj=[[DBOperations alloc]init];
                [dbObj setDelegate:self];
                //
                
                
                
                
                
                NSString *stri= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
                
                NSLog(@"stri :%@",stri);
                
                
                
                if(![self offlineAvailable])
                {
                    [self.view addSubview:vwLoading];
                    
                    [dbObj startRefeshHomePage];
                    [self setUpListView];
                }
                
                else
                {
                    [vwLoading removeFromSuperview];
                    [dbObj startOfflineDataLoading];
                 //   [dbObj startRefeshHomePage];
                    [self setUpListView];
                }
                
                
            }
        }
        else
            [self setUpListView];
        
        // [self setUpScroll];
    }
    
    [standardUserDefaults setObject:stri forKey:[NSString stringWithFormat:@"%@",stri]];
    [standardUserDefaults synchronize];
    
    //////////////
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification123"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startOunGuide:)
                                                 name:@"startOunGuide"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(test:)
                                                 name:@"headerTest"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(forTableHeaderImage:)
                                                 name:@"forTableHeaderImage"
                                               object:nil];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"forTableHeaderImage" object:self];
    
    
  }

- (void) startOunGuide:(NSNotification *) notification
{
    startOwnGuide=true;
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:1 inSection:0] ;
    [self tableView:tblViewHome didSelectRowAtIndexPath:myIP];
    

}

#pragma mark - Checking for Offline Data


/*  This function check for offline data availability in the selected app. If it has, true value will return*/
-(BOOL)isTheAppHasOflineDataa:(NSString*)appid
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Categories" inManagedObjectContext:context];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",appid];
    [fetchRequest setEntity:entity];
    
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    BOOL hasData=false;
    if([fetchedObjects count]>0)
        hasData=true;// Offline Data available
    return hasData;
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    startOwnGuide=false;
    
    NSString *stri= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    NSLog(@"stri :%@",stri);
    
    if([self isTheAppHasOflineDataa:stri])
    {
        
        
        
        
        
    }

    
   else if ([self reachable])
   {
       if(![self offlineAvailable])
    [self.view addSubview:vwLoading];
   }
    
        _isCellClicked=NO;
    btnTapToRetry.hidden = YES;
      [self.tabBarController.tabBar setHidden:NO];
    
   /*  NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
   NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    if ([self reachable])
    if(![removAdStatus isEqualToString:@"purchased"])
        [self setAdBannerWithArray:arAdDetails];*/
    
    
    
    tblViewHome.tableHeaderView =  nil;
    searchBar.text=@"";
    
    
}

- (void)testRefresh:(UIRefreshControl *)refreshControlH
{
   /*NSMutableDictionary *appDetailDic= [self coreDataReadAppDetail];
    NSString   *offlineSyncStatus=[[appDetailDic objectForKey:@"settings"] objectForKey:@"offline_guide"];
    
    if([offlineSyncStatus isEqualToString:@"yes"])
        [self AllQuestWebService];
    else
        [self startWebService];
    
    */
    
    if([self reachable])
    {
        
    DBOperations *dbObj=[[DBOperations alloc]init];
    [dbObj setDelegate:self];
    [dbObj startRefeshHomePage];
    
    
    [self startSettingsWebServiceOfCurrentApp];
        
    }
    else
    {
        [refreshControlH endRefreshing];
    }
    
    
    // [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshFooterFromVC" object:self];
    
}
- (void) processCompleted:resultArray
{
   [vwLoading removeFromSuperview];
    
      [tblViewHome removeFromSuperview];
     [scrollHome removeFromSuperview];
    
    arrayContents=[[NSArray alloc]init];
    arrayContents=resultArray;
    queArray=[resultArray mutableCopy];
    [refreshControl endRefreshing];
    
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"VIEW_GRID"] isEqualToString:@"YES"]) {
       
        [self setUpScroll];
    }
    else {
      
        [self setUpListView];
        
    }
    //[vwLoading removeFromSuperview];
    //[activityIndicator1 stopAnimating];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Check internet connection
-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
	return YES;
}

-(BOOL)offlineAvailable
{
    /*
     NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
     
     NSString*stt = [[NSUserDefaults standardUserDefaults]objectForKey:ida];
     
     NSLog(@"ida :%@ ,stt :%@",ida,stt);
     
     if([stt isEqualToString:@"noo"])
     */
    
   // [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}


// Setting things up after webservice completed
-(void)setUpScroll {
    index = 0;
    
    scrollHome = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, 320, scrollHeight)];
    [self.view addSubview:scrollHome];
    scrollHome.bounces = YES;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    [scrollHome addSubview:refreshControl];
    int limit = [arrayContents count];
    int looplimit;
    if (!((limit % 3) == 0))
    {
        looplimit = ([arrayContents count]/3) + 1;
    }
    else
    {
        looplimit = [arrayContents count]/3;
    }
    [scrollHome setContentSize:CGSizeMake(320, looplimit * (120+1))];
    if (looplimit * 120 < scrollHeight) {
        [scrollHome setContentSize:CGSizeMake(320, scrollHeight + 50)];
    }
//    for(int q = 0; q < [arrayContents count]; q++){
//        NSString*string=[NSString stringWithFormat:@"img%d",q];
//        UIImageView*string=[[UIImageView alloc]init];
//    }
    
    for (int i = 0; i < looplimit; i++) {
        for (int k = 0; k < 3; k++) {
            if (index >= [arrayContents count])
                continue;
            
//            UIImageView *btnBg = [[UIImageView alloc] initWithFrame:CGRectMake((k * 100) + 16, (i * 120) + 11, 88, 88)];
//            btnBg.image = [UIImage imageNamed:@"button_bg.png"];
//            [scrollHome addSubview:btnBg];
            NSDictionary *dict = [arrayContents objectAtIndex:index];
            
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake((k * 100) + 19, (i * 120) + 14, 83, 83)];
            //img.layer.cornerRadius = 7;
            img.clipsToBounds = YES;
            
            // [img sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholderImage:nil options:SDWebImageRetryFailed];
            
            //            [img setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]]
            //                placeholder:nil];
            [self loadImageFromURL:[dict objectForKey:@"image"] image:img];
            
            //            [img setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
            //
            //                [img setImage:imageX];
            //            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            //                NSLog(@"fail with %@ and %@",[error localizedDescription],[request URL]);
            //            }    
            //            ];
            [scrollHome addSubview:img];
            
            
            
            NSInteger *points=[[dict objectForKey:@"points"] integerValue];
            
            NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            
            NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
            
            if(![unlimitedPurchase isEqualToString:@"purchased"])
                if([[dict objectForKey:@"paid"] isEqualToString:@"paid"]&&points>0)
                {
                    
                    
                    
                    if(![self checkCategoryPurchased:[dict objectForKey:@"Id"]])
                    {
                       
                        
                        
                        
                        UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(img.frame.origin.x+img.frame.size.width-25, img.frame.origin.y+img.frame.size.height-25, 30, 30)];
                        catPointsLbl.backgroundColor=[UIColor whiteColor];
                        catPointsLbl.font=[UIFont boldSystemFontOfSize:15];
                        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                        catPointsLbl.textAlignment=NSTextAlignmentCenter;
                        catPointsLbl.textColor=[UIColor grayColor];
                        catPointsLbl.text= [dict objectForKey:@"points"];
                        [scrollHome addSubview:catPointsLbl];
                        
                        catPointsLbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
                        catPointsLbl.layer.borderWidth = 2.0;
                        
                        catPointsLbl.layer.masksToBounds = YES;
                        catPointsLbl.layer.cornerRadius = 15.0;
                        
                        
                        
                    }
                    
                }
            
            
           
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake((k * 100) + 15, (i * 120) + 10, 90, 90);
            btn.tag = index;
            [btn addTarget:self action:@selector(onListButton:) forControlEvents:UIControlEventTouchUpInside];
            [scrollHome addSubview:btn];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake((k * 100) + 15, (i * 120) + 100, 90, 20)];
            [lbl setFont:[UIFont fontWithName:@"Verdana" size:13]];
            lbl.text = [dict objectForKey:@"name"];
            lbl.backgroundColor = [UIColor clearColor];
            lbl.textAlignment = 1;
            [scrollHome addSubview:lbl];
            index++;
        }
    }
}

-(void)setUpListView
{
    [tblViewHome removeFromSuperview];
    
    tblViewHome = [[UITableView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, 320, scrollHeight-11)];//454
    
    if(IS_IPHONE5)
    {
    
    }
    
    else
    {
        CGRect frame;
        frame=tblViewHome.frame;
        frame.origin.y=self.navigationController.navigationBar.frame.size.height+11;
        frame.size.height=scrollHeight+11;
        tblViewHome.frame=frame;
        
    }
    tblViewHome.delegate = self;
    tblViewHome.dataSource = self;
    tblViewHome.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tblViewHome];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    [tblViewHome addSubview:refreshControl];
}
#pragma mark - UITableView delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        
        return 0;
    }
    else{
        
        return [arrayContents count];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
// code change by Ram for adding banner header
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section==0){
        
        bannerImgURL = [[NSUserDefaults standardUserDefaults] valueForKey:@"app_home_header_img"];
        return ([bannerImgURL isEqualToString:@"NA"]) ? 0 : kTableViewHeaderHeight;
    }
    else{
        return 0;
       
    }
    
}

-(void)forTableHeaderImage:(NSNotification *) notification
{
    NSString*str= [[NSUserDefaults standardUserDefaults]objectForKey:@"forTableHeaderImageUrl"];
    
    NSLog(@"bannerurl :%@",str);
    
   // NSString*str=bannerImgURL;
    
    NSString *key = [str MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        tblHeaderImage.image = image;
    } else {
        //cell.imageView.image = [UIImage imageNamed:@"icn_default"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:str]];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                tblHeaderImage.image = image;
                
                NSLog(@"image :%@",image);
            });
        });
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if(_isFromSearch)
    {
      return nil;
    }
    else
    {
        
    
    if(section==0){
        
        tblHeaderImage = [[AsyncImageView alloc] init];
        tblHeaderImage.frame =CGRectMake(0, 0, tableView.bounds.size.width, kTableViewHeaderHeight);
        [tblHeaderImage setBackgroundColor:[UIColor clearColor]];
        
        //tblHeaderImage.imageURL=[NSURL URLWithString:bannerImgURL];
        
       // [self loadImageFromURL1:bannerImgURL image:tblHeaderImage];
        
        
       // [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:bannerImgURL] placeholderImage:nil options:SDWebImageRefreshCached];

        
        
        
        NSString*str=bannerImgURL;
        
       /* NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        
        
        tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:str];*/
        
        
        NSString *headerImgKey=[NSString stringWithFormat:@"header%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        NSString *headerImgName=[[NSUserDefaults standardUserDefaults]objectForKey:headerImgKey];
        
        
        
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:@"FTWCaches"];
        
        
        if ([self offlineAvailable])
        {

        tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:str];
            
        }
        
//        [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:str]
//                          placeholderImage:nil
//                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                     
//                                 }];

        [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
        
       
        
        
        tblHeaderImage.imageURL = [NSURL URLWithString:str];
        
        if (!tblHeaderImage.image)
        {
            
            if (![self offlineAvailable])
            {
                
                [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                
                tblHeaderImage.imageURL = [NSURL URLWithString:str];
                
            }
            
            else
                
            {
               
                
            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
            
            NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
            
            
            
            
            NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
            
            NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit);
            NSLog(@"secondBit folder :%@",secondBit2);
            
            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            NSLog(@"str url :%@",str);
            
            NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/app_header",secondBit2]];
            
            NSArray* str3 = [str componentsSeparatedByString:@"app_header/"];
            
            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit3);
            NSLog(@"secondBit folder :%@",secondBit3);
            
            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
            
            UIImage* image = [UIImage imageWithContentsOfFile:Path];
            
            tblHeaderImage.image=image;
            
          // [self loadImageFromURL1:str image:tblHeaderImage];
            
           // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];

        
            }
        }

//        [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            
//            
//        }];
//        
    /*    NSString *headerImgKey=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        NSString *headerImgName=[[NSUserDefaults standardUserDefaults]objectForKey:headerImgKey];
        
        
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:headerImgKey];
        
        
        
        tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:bannerImgURL];
        
//        NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
//        bannerImgURL=[userDefaults objectForKey:@"bannerImgURL"];
        
        if(!tblHeaderImage.image)
        {
           
            
           // [self loadImageFromURL1:bannerImgURL image:tblHeaderImage];
            
        }

        else
        {
            bannerImgURL = [[NSUserDefaults standardUserDefaults] valueForKey:@"app_home_header_img"];
            [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:bannerImgURL] placeholderImage:nil options:SDWebImageRefreshCached];
        }
        */
        return tblHeaderImage;
    }
    else{
        return nil;
        
    }
    }
    
    
}

-(void)test:(NSNotification *) notification
{
    {
        
        tblHeaderImage = [[AsyncImageView alloc] init];
        tblHeaderImage.frame =CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kTableViewHeaderHeight);
        [tblHeaderImage setBackgroundColor:[UIColor clearColor]];
        
        //tblHeaderImage.imageURL=[NSURL URLWithString:bannerImgURL];
        
        // [self loadImageFromURL1:bannerImgURL image:tblHeaderImage];
        
        
        // [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:bannerImgURL] placeholderImage:nil options:SDWebImageRefreshCached];
        
        
        
        
        NSString*str=bannerImgURL;
        
        NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        
        
        tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:str];
        
        if (!tblHeaderImage.image)
        {
            if ([self reachable]) {
                
                
                [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                
                
                
                
                tblHeaderImage.imageURL = [NSURL URLWithString:str];
            }
            else
            {
                tblHeaderImage.image=[UIImage imageNamed:@"noimage.png"];
            }
        }
        
        
        
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = [arrayContents objectAtIndex:indexPath.row];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(75,10,250,40)];
    [lbl setFont:[UIFont fontWithName:@"Verdana" size:18]];//[UIFont fontWithName:@"Verdana" size:18] //[UIFont fontWithName:@"Verdana" size:20]
    lbl.text = [dict objectForKey:@"name"];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textAlignment = NSTextAlignmentLeft;
    [cell.contentView addSubview:lbl];
    
    
            if(_isFromSearch)
            {
                
                NSArray *arr=[dict valueForKey:@"image"];
            if((arr == nil || [arr isEqual:@""]))
            {
            
            }
            else{
                AsyncImageView *imageview = [[AsyncImageView alloc] init];
                imageview.frame = CGRectMake(10, 5, 50, 50);
                [imageview sd_setImageWithURL:[NSURL URLWithString:[arr objectAtIndex:0]] placeholderImage:[UIImage imageNamed:@"placeholder_new.png"] options:SDWebImageRefreshCached];
                 }
            }
            else{
            
                if ([dict objectForKey:@"image"]) {
                    if (![[dict objectForKey:@"image"] isEqualToString:@""]) {
                        AsyncImageView *imageview = [[AsyncImageView alloc] init];
                            imageview.frame = CGRectMake(10, 5, 50, 50);
                        
                NSString*str=[dict objectForKey:@"image"];
                        
                    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:imageCacheFolder];
                        
                        if ([self offlineAvailable])
                        {
                imageview.image=[imageCache imageFromDiskCacheForKey:str];
                        }
                        
                        if (!imageview.image)
                        {
//                            if ([self reachable]) {
//                                
//                                
//                                [imageview sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
//                                
//                                
//                                
//                                
//                                imageview.imageURL = [NSURL URLWithString:str];
//                            }
//                            else
//                            {
//                                
//                                [self loadImageFromURL1:str image:imageview];
//                                
//                                [imageview sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                    
//                                    
//                                }];
//
//                                //imageview.image=[UIImage imageNamed:@"noimage.png"];
//                            }
                            
            if (![self offlineAvailable])
            {
                            
                        [imageview sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                            
                                imageview.imageURL = [NSURL URLWithString:str];
                
            }
                            
            else
                
            {
                                NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                                
                                NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                                
                                NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                                
                                
                                
                                
                                NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                                
                                NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                                
                                NSLog(@"firstBit folder :%@",firstBit);
                                NSLog(@"secondBit folder :%@",secondBit2);
                                
                                NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                                
                                NSLog(@"str url :%@",str);
                                
                                NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/cat_images",secondBit2]];
                                
                                NSArray* str3 = [str componentsSeparatedByString:@"cat_images/"];
                                
                                NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                                
                                NSLog(@"firstBit folder :%@",firstBit3);
                                NSLog(@"secondBit folder :%@",secondBit3);
                                
                                NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
                                
                                UIImage* image = [UIImage imageWithContentsOfFile:Path];
                                
                                imageview.image=image;
                                
                                // [self loadImageFromURL1:str image:tblHeaderImage];
                                
                                // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                                
                                
                            }
                        }
                        
                        imageview.layer.cornerRadius = 6.0;
                        imageview.layer.masksToBounds = YES;
                        
                        [cell.contentView addSubview:imageview];
                    }
                    
                }
            }
/*
                NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolderCategory%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:imageCacheFolder];
                
                
                imageview.image=[imageCache imageFromDiskCacheForKey:[dict objectForKey:@"image"]];
                
                if (!imageview.image)
                {
                   
                      // imageview.image=[UIImage imageNamed:@"placeholder_new.png"];
                        
                       // [imageview sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                        
                         [imageview sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholderImage:nil options:SDWebImageRefreshCached];
                        
                        
                        imageview.imageURL = [NSURL URLWithString:[dict objectForKey:@"image"]];
                    
                       // [imageview sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholderImage:nil options:SDWebImageRefreshCached];
                        
                        
                       // imageview.imageURL = [NSURL URLWithString:[dict objectForKey:@"image"]];
                        
                    
                    
                }

                
           
                
                
            }
            [cell.contentView addSubview:imageview];*/
       // });
 //   });
    
   
    
   // [cell setNeedsDisplay];
    //[cell setNeedsLayout];
    
    
     NSInteger *points=[[dict objectForKey:@"points"] integerValue];
    
          NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
    
    //paid_type = @"paid";
    
    if(![unlimitedPurchase isEqualToString:@"purchased"])
        if([[dict objectForKey:@"paid"] isEqualToString:@"paid"]&&points>0 && ![paid_type isEqualToString:@"paid"]&&![self offlineAvailable])
        {
       
        
       
        if(![self checkCategoryPurchased:[dict objectForKey:@"Id"]])
            {
               
                
                
                
                UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width-50, 10, 35, 35)];
                catPointsLbl.backgroundColor=[UIColor clearColor];
                catPointsLbl.font=[UIFont boldSystemFontOfSize:22];
                //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                catPointsLbl.textAlignment=NSTextAlignmentCenter;
                catPointsLbl.textColor=[UIColor grayColor];
                catPointsLbl.text= [dict objectForKey:@"points"];
                [[NSUserDefaults standardUserDefaults]setObject:catPointsLbl.text forKey:@"inAppPointsValue"];
                [cell addSubview:catPointsLbl];
                
                catPointsLbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
                catPointsLbl.layer.borderWidth = 2.0;
                
                catPointsLbl.layer.masksToBounds = YES;
                catPointsLbl.layer.cornerRadius = 7.0;
                
                UILabel *catPointsLbl2=[[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width-50, 45, 35, 15)];
                catPointsLbl2.backgroundColor=[UIColor clearColor];
                
                catPointsLbl2.font=[UIFont systemFontOfSize:11];
                catPointsLbl2.textAlignment=NSTextAlignmentCenter;
                catPointsLbl2.textColor=[UIColor lightGrayColor];
                
                if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                {
                    catPointsLbl2.text=@"Point";
                }
                else
                {
                    catPointsLbl2.text=@"Points";
                }

                
                // catPointsLbl2.text=@"Points";
                [cell addSubview:catPointsLbl2];
                
           
            }
                
        }
    
    NSInteger sectionsAmount = [tableView numberOfSections];
    NSInteger rowsAmount = [tableView numberOfRowsInSection:[indexPath section]];
    if ([indexPath section] == sectionsAmount - 1 && [indexPath row] == rowsAmount - 1) {
        
        
        
        NSLog(@"This is the last cell in the table");
    }

    
    int titleX;
    if (![[dict objectForKey:@"image"] isEqualToString:@"FALSE"]) {

       
        
        titleX = 75;
    }
    else
        titleX = 20;

    
   lbl.frame = CGRectMake(titleX,10,250,40);//(titleX,10,250 - titleX,40)  (75,10,250,40)
    
    
    
    return cell;
}



// code change by Ram to hide tabbar
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"arrayContents count :%lu",(unsigned long)[arrayContents count]);
    
    if (_isCellClicked)return;
    
 
    
    NSDictionary *dict = [arrayContents objectAtIndex:indexPath.row];
    
     NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
    
    if(![unlimitedPurchase isEqualToString:@"purchased"])
    {
         NSInteger points=[[dict objectForKey:@"points"] integerValue];
        if([[dict objectForKey:@"paid"] isEqualToString:@"paid"]&&points>0&&![paid_type isEqualToString:@"paid"]&&![self offlineAvailable])
        {
            
            
            
            if(![self checkCategoryPurchased:[dict objectForKey:@"Id"]])
            {
                
                [self inappPurchasePopShow:indexPath.row];
                return;
                
            }
        }
    
    }
       _isCellClicked=YES;
    if (![[dict objectForKey:@"ad"] isEqualToString:@"FALSE"]) {
        NSMutableDictionary *dicto = [[NSMutableDictionary alloc] init];
        [dicto setObject:[dict objectForKey:@"ad"] forKey:@"imageLink"];
        [dicto setObject:[dict objectForKey:@"link"] forKey:@"webLink"];
        [[NSUserDefaults standardUserDefaults] setObject:dicto forKey:@"CatAd"];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"CatAdExist"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"CatAdExist"];
    }
    
    if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
        PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
        [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"header_image"] forKey:@"bannerEURL"];
        
        [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:[dict objectForKey:@"type"] withName:[dict objectForKey:@"name"]];
        [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
        subCategoryViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:subCategoryViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"url"]){
        if([self reachable]){
            
            
       
            NSString *webString =[dict objectForKey:@"external_url"];
            
                
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:webString]];
            _isCellClicked=NO;
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
             _isCellClicked=NO;
            return;
        }
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"static"])
    {
        PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
        [detailViewController getDictionary:dict];
        detailViewController.hidesBottomBarWhenPushed = YES;
        if(startOwnGuide==true)
        {
            detailViewController.isOwnGuide=YES;
        }
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dict objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dict objectForKey:@"type"] isEqualToString:@"normal"]){
        PDListViewController *listViewController = [[PDListViewController alloc] init];
        [listViewController getDictionary:dict];
        listViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:listViewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"event"]) {
        
        PDEventsViewController *viewController = [[PDEventsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
        
        [viewController getDictionaryeve:dict];
        
               viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"deal"]) {
        PDDealsViewController *viewController = [[PDDealsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
        
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"rss"]) {
        if([self reachable]){
            PDRssListViewController *rssListViewController = [[PDRssListViewController alloc] initWithNibName:@"PDRssListViewController" bundle:nil];
            [rssListViewController getDictionary:dict];
            rssListViewController.isFromHome = YES;
            rssListViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:rssListViewController animated:YES];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             _isCellClicked=NO;
            [alert show];
            return;
        }
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"gallery"]) {
        PDGalleryViewController *galleryViewController = [[PDGalleryViewController alloc] initWithNibName:@"PDGalleryViewController" bundle:nil];
        [galleryViewController getDictionary:dict];
         galleryViewController.hidesBottomBarWhenPushed = YES;
        //galleryViewController.isFromHome = YES;
        galleryViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:galleryViewController animated:YES];
    }
    else {
        if ([[dict objectForKey:@"type"] isEqualToString:@"fixed"] || [[dict objectForKey:@"type"] isEqualToString:@"location"]) {
            if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
                PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
                [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:@"fixed" withName:[dict objectForKey:@"name"]];
                [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
                subCategoryViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:subCategoryViewController animated:YES];
            }
            else {
                PDListViewController *listViewController = [[PDListViewController alloc] init];
                [listViewController getDictionary:dict];
                listViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:listViewController animated:YES];
            }
        }
    }
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        
        NSLog(@"tableview loaded completely 2");
        
        
       
        
    }
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSLog(@"tableview loaded completely");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"LOCATION IS BEING UPDATED");
    
    
    isLocationUpdated = YES;
    location_updated = [locations lastObject];
    //NSLog(@"updated coordinate are %@",location_updated);
    isHotSpotAd = NO;
    
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            NSArray *arrTemp = [NSArray arrayWithObject:dictDetails];
            NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
            if(![removAdStatus isEqualToString:@"purchased"])
            [self setAdBannerWithArray:arrTemp];
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    if (!isHotSpotAd) {
        NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
        if ([self reachable])
        if(![removAdStatus isEqualToString:@"purchased"])
        [self setAdBannerWithArray:arAdDetails];
    }
    
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
  
    [alertView close];
}

#pragma mark -Inapp- Purchase Notification Methods
- (void) inappSuccessNotification:(NSNotification *) notification
{
    
     [self loaderStop];

    

    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    dictionary=[inappStr objectAtIndex:0];
  

    
    
    
    
    
 
    
    NSString *notificationInap=[NSString stringWithFormat:@"%@",notification.object];
    
    if([notificationInap isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
    {
        NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self  removeBanner];
    }
    else if([notificationInap isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
    {
       
        
        
        NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"VIEW_GRID"] isEqualToString:@"YES"]) {
            [scrollHome removeFromSuperview];
            [self setUpScroll];
        }
        else {
            [tblViewHome reloadData];
        }
    }
    

}
- (void) inappFailureNotification:(NSNotification *) notification
{
     [self loaderStop];
   
    
}
- (void) inappReStoreSuccessNotification:(NSNotification *) notification
{
    
     [self loaderStop];
    
    
   
    
    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    dictionary=[inappStr objectAtIndex:0];
   

    
    
    NSString *notificationInap=[NSString stringWithFormat:@"%@",notification.object];
    
    if([notificationInap isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
    {
        NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self  removeBanner];
    }
    else if([notificationInap isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
    {
        
        
        
        NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"VIEW_GRID"] isEqualToString:@"YES"]) {
            [scrollHome removeFromSuperview];
            [self setUpScroll];
        }
        else {
            [tblViewHome reloadData];
        }
    }

    
    
}
- (void) inappReStoreFailureNotification:(NSNotification *) notification
{
    
    [self loaderStop];
  
    
  
}



#pragma mark -Inapp- Purchase Purchase
-(void)inappPurchasePopShow:(NSInteger)catIndex
{
    
   NSString* purchasePointsValue = [[NSUserDefaults standardUserDefaults]objectForKey:@"inAppPointsValue"];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterSpellOutStyle];
    
    NSString *s = [f stringFromNumber:[NSNumber numberWithInt:[purchasePointsValue intValue]]];
    NSLog(@"inAppPointsValue : %@", s);
    
   
    NSDictionary *dict=[arrayContents objectAtIndex:catIndex];
    
    NSMutableArray *nonconsumableIds=[[NSMutableArray alloc]init];
    

  //  NSInteger points=[[dict objectForKey:@"points"] integerValue];
   
    NSNumber* myPoints=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
    NSNumber* number2=0;
    
    if( myPoints==nil){
        myPoints=[NSNumber numberWithInt:0];
    }
 
   
    
    
    customAlert = [[CustomIOSAlertView alloc] init];
        
    UIView *pointView;
    
        UIView *viewInAlert = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 350)];
        NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
        NSDictionary *dictionary=[inappStr objectAtIndex:0];
    
    viewInAlert.backgroundColor=[UIColor whiteColor];
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:viewInAlert.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = viewInAlert.frame;
    maskLayer.path = maskPath.CGPath;
    viewInAlert.layer.mask = maskLayer;
    
    
        
//        int btnXpos=5;
//        int btnYpos=5;
//        int btnWidth  =300-10;
//        int btnHeight =60;
//        int btnYpospadding=2;
    
    int btnXpos=5;
    int btnYpos=5;
    int btnWidth  =240;
    int btnHeight =30;
    int btnYpospadding=2;
       // if(myPoints>=points)
        {
          pointView =[[UIView alloc]initWithFrame:CGRectMake(btnXpos, btnYpos, btnWidth, 70)];
            
            pointView.backgroundColor=[UIColor whiteColor];
            [viewInAlert addSubview:pointView];
            
            
            UILabel *pointLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, btnWidth, 40)];
            pointLbl.backgroundColor=[UIColor clearColor];
            pointLbl.font=[UIFont boldSystemFontOfSize:45];
            //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
            pointLbl.textAlignment=NSTextAlignmentCenter;
            pointLbl.textColor=[UIColor blueColor];
            pointLbl.text=[NSString stringWithFormat:@"%@",myPoints];
            [pointView addSubview:pointLbl];
            
            UILabel *pointLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, btnWidth, 30)];
            pointLbl2.backgroundColor=[UIColor clearColor];
            
            pointLbl2.font=[UIFont systemFontOfSize:15];
            pointLbl2.textAlignment=NSTextAlignmentCenter;
            pointLbl2.textColor=[UIColor blueColor];
            pointLbl2.text=@"Points Available";
            [pointView addSubview:pointLbl2];
            
             btnYpos=btnYpos+70+btnYpospadding;
        }
    
      
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
        if([[dictionary objectForKey:@"remove_ads"] isEqualToString:@"yes"]&&![removAdStatus isEqualToString:@"purchased"])
        {
            [nonconsumableIds addObject:[dictionary objectForKey:@"remove_ads_id_ios"]];
            [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
            
           
        UIButton *removeAdBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        
        //removeAdBtn.backgroundColor=[UIColor colorWithRed:255/255.0 green:0 blue:0 alpha:1];
            removeAdBtn.backgroundColor=[UIColor colorWithRed:200/255.0 green:215/255.0 blue:117/255.0 alpha:1];

        [removeAdBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSString *msgStr=[NSString stringWithFormat:@"Remove Ads                             $%@",[dictionary objectForKey:@"inapp_currency"]];
            
            
        [removeAdBtn setTitle:msgStr forState:UIControlStateNormal];
        removeAdBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
       // [viewInAlert addSubview:removeAdBtn];
            removeAdBtn.tag=0;
            removeAdBtn.titleLabel.font=[UIFont systemFontOfSize:14];
            
            [removeAdBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
            removeAdBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            removeAdBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            removeAdBtn.layer.cornerRadius=5.0;
            
             btnYpos=btnYpos+btnHeight+btnYpospadding;
          
        
        }
        
        if([[dictionary objectForKey:@"one_category"] isEqualToString:@"yes"])
        {
         
            
            
            
            
            UIButton *oneCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            //oneCatBtn.backgroundColor=[UIColor colorWithRed:204/255.0 green:102/255.0 blue:102/255.0 alpha:1];
            oneCatBtn.backgroundColor=UIColorFromRGB(0xededed);
            [oneCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            
            NSString *price=[NSString stringWithFormat:@"One Point                                  $%@",[dictionary objectForKey:@"one_category_price"]];
            [oneCatBtn setTitle:price forState:UIControlStateNormal];
            oneCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
            oneCatBtn.tag=1;
            
            oneCatBtn.titleLabel.font=[UIFont systemFontOfSize:14];
            
            [oneCatBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
            oneCatBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            oneCatBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            oneCatBtn.layer.cornerRadius=5.0;
            
            if([s isEqualToString:@"one"])
            {
                if ([myPoints doubleValue] <= [number2 doubleValue])
                //[viewInAlert addSubview:oneCatBtn];
                {
                    
                    {
                        UIView *unlockView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50)];
                        unlockView.backgroundColor=UIColorFromRGB(0x4bb549);//[UIColor colorWithRed:35/255.0 green:207/255.0 blue:52/255.0 alpha:1];  // 28bbf2
                        
                        //if ([myPoints doubleValue] >= [purchasePointsValue intValue])
                        [viewInAlert addSubview:unlockView];
                        
                        unlockView.layer.cornerRadius=5.0;
                        
                        UILabel *catPointsLbl22=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
                        catPointsLbl22.backgroundColor=[UIColor clearColor];
                        
                        catPointsLbl22.font=[UIFont systemFontOfSize:13];
                        catPointsLbl22.textAlignment=NSTextAlignmentCenter;
                        catPointsLbl22.textColor=[UIColor blueColor];
                        
                        if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                        {
                            catPointsLbl22.text=@"Point";
                        }
                        else
                        {
                            catPointsLbl22.text=@"Points";
                        }
                        
                        
                        
                        UILabel *unlockLbl=[[UILabel alloc]initWithFrame:CGRectMake(50, 5, btnWidth-70, 20)];
                        unlockLbl.backgroundColor=[UIColor clearColor];
                        unlockLbl.font=[UIFont boldSystemFontOfSize:13];
                        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                        unlockLbl.textAlignment=NSTextAlignmentLeft;
                        unlockLbl.textColor=[UIColor whiteColor];
                        unlockLbl.text= [NSString stringWithFormat:@"Buy %@ %@",s,catPointsLbl22.text];
                        [unlockView addSubview:unlockLbl];
                        unlockLbl.backgroundColor=[UIColor clearColor];
                        
                        UILabel *unlockLbl2=[[UILabel alloc]initWithFrame:CGRectMake(20, 25, btnWidth-70, 20)];
                        unlockLbl2.backgroundColor=[UIColor clearColor];
                        unlockLbl2.backgroundColor=[UIColor clearColor];
                        unlockLbl2.font=[UIFont boldSystemFontOfSize:15];
                        unlockLbl2.textAlignment=NSTextAlignmentCenter;
                        unlockLbl2.textColor=[UIColor whiteColor];
                        unlockLbl2.text=@"Unlock Category";
                        [unlockView addSubview:unlockLbl2];
                        
                        UIView *pointsView=[[UIView alloc]initWithFrame:CGRectMake(btnWidth-48, 2, 46, 46)];
                        pointsView.backgroundColor=[UIColor whiteColor];
                        pointsView.layer.cornerRadius=6.0;
                        [unlockView addSubview:pointsView];
                        
                        
                        UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 46, 46)];
                        catPointsLbl.backgroundColor=[UIColor clearColor];
                        catPointsLbl.font=[UIFont boldSystemFontOfSize:15];
                        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                        catPointsLbl.textAlignment=NSTextAlignmentCenter;
                        catPointsLbl.textColor=[UIColor blueColor];
                        catPointsLbl.text= [NSString stringWithFormat:@"$%@",[dictionary objectForKey:@"one_category_price"]];
                        [pointsView addSubview:catPointsLbl];
                        
                        UILabel *catPointsLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
                        catPointsLbl2.backgroundColor=[UIColor clearColor];
                        
                        catPointsLbl2.font=[UIFont systemFontOfSize:13];
                        catPointsLbl2.textAlignment=NSTextAlignmentCenter;
                        catPointsLbl2.textColor=[UIColor blueColor];
                        
                        if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                        {
                            catPointsLbl2.text=@"Point";
                        }
                        else
                        {
                            catPointsLbl2.text=@"Points";
                        }
                        
                        
                        
                        // catPointsLbl2.text=@"Points";
                        //[pointsView addSubview:catPointsLbl2];
                        
                        
                        
                        
                        
                        UIButton * unLockCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                        unLockCatBtn.backgroundColor=[UIColor clearColor];
                        [unLockCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                        //  [unLockCatBtn setTitle:@"Unlock Category" forState:UIControlStateNormal];
                        unLockCatBtn.frame=CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50);
                        unLockCatBtn.tag=1;
                        
                        currentIndex=catIndex;
                        [viewInAlert addSubview:unLockCatBtn];
                        btnYpos=btnYpos+btnHeight+btnYpospadding;
                        
                        
                    }
                }
            }
            
            btnYpos=btnYpos+btnHeight+btnYpospadding;

                  }
        if([[dictionary objectForKey:@"three_category"] isEqualToString:@"yes"])
        {
            
            UIButton * threeCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
           // threeCatBtn.backgroundColor=[UIColor colorWithRed:255/255.0 green:204/255.0 blue:51/155.0 alpha:1];
            
            threeCatBtn.backgroundColor=UIColorFromRGB(0xededed);
            [threeCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            NSString *price=[NSString stringWithFormat:@"Three Points                             $%@",[dictionary objectForKey:@"three_category_price"]];
            [threeCatBtn setTitle:price forState:UIControlStateNormal];
            threeCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
            threeCatBtn.tag=3;
             threeCatBtn.titleLabel.font=[UIFont systemFontOfSize:14];//systemFontOfSize
            
            [threeCatBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
            threeCatBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            threeCatBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            threeCatBtn.layer.cornerRadius=5.0;
            
            if([s isEqualToString:@"three"]||[s isEqualToString:@"two"])
            {
                
                
                if ([myPoints doubleValue] <= 2)
              // [viewInAlert addSubview:threeCatBtn];
                {
                    if ([myPoints doubleValue] <= 3)
                        //[viewInAlert addSubview:fiveCatBtn];
                    {
                        
                        {
                            UIView *unlockView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50)];
                            unlockView.backgroundColor=UIColorFromRGB(0x4bb549);//[UIColor colorWithRed:35/255.0 green:207/255.0 blue:52/255.0 alpha:1];  // 28bbf2
                            
                            //if ([myPoints doubleValue] >= [purchasePointsValue intValue])
                            [viewInAlert addSubview:unlockView];
                            
                            unlockView.layer.cornerRadius=5.0;
                            
                            UILabel *catPointsLbl22=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
                            catPointsLbl22.backgroundColor=[UIColor clearColor];
                            
                            catPointsLbl22.font=[UIFont systemFontOfSize:13];
                            catPointsLbl22.textAlignment=NSTextAlignmentCenter;
                            catPointsLbl22.textColor=[UIColor blueColor];
                            
                            if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                            {
                                catPointsLbl22.text=@"Point";
                            }
                            else
                            {
                                catPointsLbl22.text=@"Points";
                            }
                            
                            
                            
                            UILabel *unlockLbl=[[UILabel alloc]initWithFrame:CGRectMake(50, 5, btnWidth-70, 20)];
                            unlockLbl.backgroundColor=[UIColor clearColor];
                            unlockLbl.font=[UIFont boldSystemFontOfSize:13];
                            //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                            unlockLbl.textAlignment=NSTextAlignmentLeft;
                            unlockLbl.textColor=[UIColor whiteColor];
                            unlockLbl.text= [NSString stringWithFormat:@"Buy %@ %@",s,catPointsLbl22.text];
                            [unlockView addSubview:unlockLbl];
                            unlockLbl.backgroundColor=[UIColor clearColor];
                            
                            UILabel *unlockLbl2=[[UILabel alloc]initWithFrame:CGRectMake(20, 25, btnWidth-70, 20)];
                            unlockLbl2.backgroundColor=[UIColor clearColor];
                            unlockLbl2.backgroundColor=[UIColor clearColor];
                            unlockLbl2.font=[UIFont boldSystemFontOfSize:15];
                            unlockLbl2.textAlignment=NSTextAlignmentCenter;
                            unlockLbl2.textColor=[UIColor whiteColor];
                            unlockLbl2.text=@"Unlock Category";
                            [unlockView addSubview:unlockLbl2];
                            
                            UIView *pointsView=[[UIView alloc]initWithFrame:CGRectMake(btnWidth-48, 2, 46, 46)];
                            pointsView.backgroundColor=[UIColor whiteColor];
                            pointsView.layer.cornerRadius=6.0;
                            [unlockView addSubview:pointsView];
                            
                            
                            UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 46, 46)];
                            catPointsLbl.backgroundColor=[UIColor clearColor];
                            catPointsLbl.font=[UIFont boldSystemFontOfSize:15];
                            //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                            catPointsLbl.textAlignment=NSTextAlignmentCenter;
                            catPointsLbl.textColor=[UIColor blueColor];
                            catPointsLbl.text= [NSString stringWithFormat:@"$%@",[dictionary objectForKey:@"three_category_price"]];
                            [pointsView addSubview:catPointsLbl];
                            
                            UILabel *catPointsLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
                            catPointsLbl2.backgroundColor=[UIColor clearColor];
                            
                            catPointsLbl2.font=[UIFont systemFontOfSize:13];
                            catPointsLbl2.textAlignment=NSTextAlignmentCenter;
                            catPointsLbl2.textColor=[UIColor blueColor];
                            
                            if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                            {
                                catPointsLbl2.text=@"Point";
                            }
                            else
                            {
                                catPointsLbl2.text=@"Points";
                            }
                            
                            
                            
                            // catPointsLbl2.text=@"Points";
                            //[pointsView addSubview:catPointsLbl2];
                            
                            
                            
                            
                            
                            UIButton * unLockCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                            unLockCatBtn.backgroundColor=[UIColor clearColor];
                            [unLockCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                            //  [unLockCatBtn setTitle:@"Unlock Category" forState:UIControlStateNormal];
                            unLockCatBtn.frame=CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50);
                            unLockCatBtn.tag=3;
                            
                            currentIndex=catIndex;
                            [viewInAlert addSubview:unLockCatBtn];
                            btnYpos=btnYpos+btnHeight+btnYpospadding;
                            
                            
                        }
                    }
                }
                
            }
            
            btnYpos=btnYpos+btnHeight+btnYpospadding;
            
        }
    
    
        if([[dictionary objectForKey:@"five_category"] isEqualToString:@"yes"])
        {
            
            
            UIButton * fiveCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
           // fiveCatBtn.backgroundColor=[UIColor colorWithRed:102/255.0 green:153/255.0 blue:204/255.0 alpha:1];
            
            fiveCatBtn.backgroundColor=UIColorFromRGB(0xededed);
            [fiveCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
             NSString *price=[NSString stringWithFormat:@"Five Points                               $%@",[dictionary objectForKey:@"five_category_price"]];
            [fiveCatBtn setTitle:price forState:UIControlStateNormal];
            fiveCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
            fiveCatBtn.tag=5;
             fiveCatBtn.titleLabel.font=[UIFont systemFontOfSize:14];
            
            [fiveCatBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
            fiveCatBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            fiveCatBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            fiveCatBtn.layer.cornerRadius=5.0;
            
            if([s isEqualToString:@"five"]||[s isEqualToString:@"six"]||[s isEqualToString:@"seven"])
            {
                if ([myPoints doubleValue] <= 5)
                   //[viewInAlert addSubview:fiveCatBtn];
                {
                
                    {
                        UIView *unlockView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50)];
                        unlockView.backgroundColor=UIColorFromRGB(0x4bb549);//[UIColor colorWithRed:35/255.0 green:207/255.0 blue:52/255.0 alpha:1];  // 28bbf2
                        
                        //if ([myPoints doubleValue] >= [purchasePointsValue intValue])
                            [viewInAlert addSubview:unlockView];
                        
                        unlockView.layer.cornerRadius=5.0;
                        
                        UILabel *catPointsLbl22=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
                        catPointsLbl22.backgroundColor=[UIColor clearColor];
                        
                        catPointsLbl22.font=[UIFont systemFontOfSize:13];
                        catPointsLbl22.textAlignment=NSTextAlignmentCenter;
                        catPointsLbl22.textColor=[UIColor blueColor];
                        
                        if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                        {
                            catPointsLbl22.text=@"Point";
                        }
                        else
                        {
                            catPointsLbl22.text=@"Points";
                        }

                        
                        
                        UILabel *unlockLbl=[[UILabel alloc]initWithFrame:CGRectMake(50, 5, btnWidth-70, 20)];
                        unlockLbl.backgroundColor=[UIColor clearColor];
                        unlockLbl.font=[UIFont boldSystemFontOfSize:13];
                        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                        unlockLbl.textAlignment=NSTextAlignmentLeft;
                        unlockLbl.textColor=[UIColor whiteColor];
                        unlockLbl.text= [NSString stringWithFormat:@"Buy %@ %@",s,catPointsLbl22.text];
                        [unlockView addSubview:unlockLbl];
                        unlockLbl.backgroundColor=[UIColor clearColor];
                        
                        UILabel *unlockLbl2=[[UILabel alloc]initWithFrame:CGRectMake(20, 25, btnWidth-70, 20)];
                        unlockLbl2.backgroundColor=[UIColor clearColor];
                        unlockLbl2.backgroundColor=[UIColor clearColor];
                        unlockLbl2.font=[UIFont boldSystemFontOfSize:15];
                        unlockLbl2.textAlignment=NSTextAlignmentCenter;
                        unlockLbl2.textColor=[UIColor whiteColor];
                        unlockLbl2.text=@"Unlock Category";
                        [unlockView addSubview:unlockLbl2];
                        
                        UIView *pointsView=[[UIView alloc]initWithFrame:CGRectMake(btnWidth-48, 2, 46, 46)];
                        pointsView.backgroundColor=[UIColor whiteColor];
                        pointsView.layer.cornerRadius=6.0;
                        [unlockView addSubview:pointsView];
                        
                        
                        UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 46, 46)];
                        catPointsLbl.backgroundColor=[UIColor clearColor];
                        catPointsLbl.font=[UIFont boldSystemFontOfSize:15];
                        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                        catPointsLbl.textAlignment=NSTextAlignmentCenter;
                        catPointsLbl.textColor=[UIColor blueColor];
                        catPointsLbl.text= [NSString stringWithFormat:@"$%@",[dictionary objectForKey:@"five_category_price"]];
                        [pointsView addSubview:catPointsLbl];
                        
                        UILabel *catPointsLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
                        catPointsLbl2.backgroundColor=[UIColor clearColor];
                        
                        catPointsLbl2.font=[UIFont systemFontOfSize:13];
                        catPointsLbl2.textAlignment=NSTextAlignmentCenter;
                        catPointsLbl2.textColor=[UIColor blueColor];
                        
                        if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                        {
                            catPointsLbl2.text=@"Point";
                        }
                        else
                        {
                            catPointsLbl2.text=@"Points";
                        }
                        
                        
                        
                        // catPointsLbl2.text=@"Points";
                        //[pointsView addSubview:catPointsLbl2];
                        
                        
                        
                        
                        
                        UIButton * unLockCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                        unLockCatBtn.backgroundColor=[UIColor clearColor];
                        [unLockCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                        //  [unLockCatBtn setTitle:@"Unlock Category" forState:UIControlStateNormal];
                        unLockCatBtn.frame=CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50);
                        unLockCatBtn.tag=5;
                        
                        currentIndex=catIndex;
                        [viewInAlert addSubview:unLockCatBtn];
                        btnYpos=btnYpos+btnHeight+btnYpospadding;
                        
                        
                    }
                }
            }
            
            btnYpos=btnYpos+btnHeight+btnYpospadding;
            
            
        }
        
        
        if([[dictionary objectForKey:@"unlimited_category"] isEqualToString:@"yes"])
        {
            btnHeight=btnHeight+20;
            
            UIView *fullAccessView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight)];
           // fullAccessView.backgroundColor=[UIColor colorWithRed:251/255.0 green:175/255.0 blue:93/255.0 alpha:1];
            fullAccessView.backgroundColor=[UIColor colorWithRed:200/255.0 green:215/255.0 blue:117/255.0 alpha:1];
             fullAccessView.layer.cornerRadius=5.0;
            
             if([s isEqualToString:@"full"])
            [viewInAlert addSubview:fullAccessView];
            
            UILabel *fullAccessLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, btnWidth, 25)];
            fullAccessLbl.backgroundColor=[UIColor clearColor];
            fullAccessLbl.font=[UIFont systemFontOfSize:15];
            //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
            fullAccessLbl.textAlignment=NSTextAlignmentCenter;
            fullAccessLbl.textColor=[UIColor whiteColor];
             NSString *price=[NSString stringWithFormat:@"   Full Access                           $%@",[dictionary objectForKey:@"unlimited_category_price"]];
            fullAccessLbl.text= price;
            fullAccessLbl.textColor=UIColorFromRGB(0x535353);
            fullAccessLbl.textAlignment=NSTextAlignmentLeft;
            [fullAccessView addSubview:fullAccessLbl];
            
            UILabel *fullAccessLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 25, btnWidth, 20)];
            fullAccessLbl2.backgroundColor=[UIColor clearColor];
            
            fullAccessLbl2.font=[UIFont systemFontOfSize:13];
            fullAccessLbl2.textAlignment=NSTextAlignmentLeft;
            fullAccessLbl2.textColor=UIColorFromRGB(0x535353);
            
            fullAccessLbl2.text=[NSString stringWithFormat:@"   %@",appName];
            
           
            [fullAccessView addSubview:fullAccessLbl2];
            
            
            
            UIButton * unlimitedCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            unlimitedCatBtn.backgroundColor=[UIColor clearColor];
            [unlimitedCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //[unlimitedCatBtn setTitle:@"Full Access - $9.99" forState:UIControlStateNormal];
            unlimitedCatBtn.frame=CGRectMake(0, 0, btnWidth, btnHeight);
            unlimitedCatBtn.tag=10;
            [fullAccessView addSubview:unlimitedCatBtn];
            
             btnYpos=btnYpos+btnHeight+btnYpospadding;
            
            [nonconsumableIds addObject:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
            [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
           
      //  }
        
        
       // if(myPoints>=points)
     //   {
            UIView *unlockView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50)];
                                //(btnXpos, btnYpos, btnWidth, 50)];
            unlockView.backgroundColor=UIColorFromRGB(0x4bb549);//[UIColor colorWithRed:35/255.0 green:207/255.0 blue:52/255.0 alpha:1]; //  28bbf2
            
            
            if ([myPoints doubleValue] >= [purchasePointsValue intValue]) //purchasePointsValue
            [viewInAlert addSubview:unlockView];
            
            unlockView.layer.cornerRadius=5.0;
            
            UILabel *unlockLbl=[[UILabel alloc]initWithFrame:CGRectMake(50, 5, btnWidth-70, 20)];
            unlockLbl.backgroundColor=[UIColor clearColor];
            unlockLbl.font=[UIFont systemFontOfSize:13];
            //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
            unlockLbl.textAlignment=NSTextAlignmentLeft;
            unlockLbl.textColor=[UIColor whiteColor];
            unlockLbl.text= @"Unlock Category";
            [unlockView addSubview:unlockLbl];
            unlockLbl.backgroundColor=[UIColor clearColor];
            
            UILabel *unlockLbl2=[[UILabel alloc]initWithFrame:CGRectMake(20, 25, btnWidth-70, 20)];
            unlockLbl2.backgroundColor=[UIColor clearColor];
             unlockLbl2.backgroundColor=[UIColor clearColor];
            unlockLbl2.font=[UIFont boldSystemFontOfSize:15];
            unlockLbl2.textAlignment=NSTextAlignmentCenter;
            unlockLbl2.textColor=[UIColor whiteColor];
            unlockLbl2.text=[dict objectForKey:@"name"];
            [unlockView addSubview:unlockLbl2];
            
            UIView *pointsView=[[UIView alloc]initWithFrame:CGRectMake(btnWidth-48, 2, 46, 46)];
            pointsView.backgroundColor=[UIColor whiteColor];
            pointsView.layer.cornerRadius=6.0;
            [unlockView addSubview:pointsView];
            
            
            UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 46, 30)];
            catPointsLbl.backgroundColor=[UIColor clearColor];
            catPointsLbl.font=[UIFont boldSystemFontOfSize:22];
            //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
            catPointsLbl.textAlignment=NSTextAlignmentCenter;
            catPointsLbl.textColor=[UIColor blueColor];
            catPointsLbl.text= [dict objectForKey:@"points"];
            [pointsView addSubview:catPointsLbl];
            
            UILabel *catPointsLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
            catPointsLbl2.backgroundColor=[UIColor clearColor];
            
            catPointsLbl2.font=[UIFont systemFontOfSize:13];
            catPointsLbl2.textAlignment=NSTextAlignmentCenter;
            catPointsLbl2.textColor=[UIColor blueColor];
            
            if([[dict objectForKey:@"points"] isEqualToString:@"1"])
            {
                catPointsLbl2.text=@"Point";
            }
            else
            {
                catPointsLbl2.text=@"Points";
            }
            

            
           // catPointsLbl2.text=@"Points";
            [pointsView addSubview:catPointsLbl2];
            
            
            
           
        
            UIButton * unLockCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            unLockCatBtn.backgroundColor=[UIColor clearColor];
            [unLockCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
          //  [unLockCatBtn setTitle:@"Unlock Category" forState:UIControlStateNormal];
            unLockCatBtn.frame=CGRectMake(btnXpos, pointView.frame.origin.y+pointView.frame.size.height+10, btnWidth, 50);
            
            unLockCatBtn.tag=11;
         
            currentIndex=catIndex;
             if ([myPoints doubleValue] >= [purchasePointsValue intValue])
            [viewInAlert addSubview:unLockCatBtn];
             btnYpos=btnYpos+btnHeight+btnYpospadding;
           
            
        }
        
        
        
    viewInAlert.frame=CGRectMake(0, 0, 250, btnYpos);
    
        
        
      
viewInAlert.frame=CGRectMake(0, 0, 250, 150);
        
        
        
       
        
         [customAlert setContainerView:viewInAlert];
        
        [customAlert setDelegate:self];
        
        // You may use a Block, rather than a delegate.
        [customAlert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
            //[alertView close];
        }];
        
        [customAlert setUseMotionEffects:true];
        
        // And launch the dialog
        [customAlert show];
        
    
      //  NSArray *myArray = [NSArray arrayWithArray:nonconsumableIds];
         // [alert show];
   // [[MKStoreKit sharedKit] startProductRequest:myArray];
    
}

-(void)PurchaseBtnAction:(id)sender
{
    
    
    NSInteger tag=  ((UIButton*)sender).tag;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(inappSuccessNotification:)
                                                 name:@"inappSuccessNotif"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(inappFailureNotification:)
                                                 name:@"inappFailureNotif"
                                               object:nil];
    
    
    if(tag==0)
    {
        //RemoveAds
     
  if ([self reachable])
  {
        [self loaderStartWithText:@"Purchasing"];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappSuccessNotification:)
                                                     name:@"inappSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappFailureNotification:)
                                                     name:@"inappFailureNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreSuccessNotification:)
                                                     name:@"inappReStoreSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreFailureNotification:)
                                                     name:@"inappReStoreFailureNotif"
                                                   object:nil];
        
        NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        dictionary=[inappStr objectAtIndex:0];
       
       
        
        
       
                                                   
                                                   
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
  }
  else
  {
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                      message:@"Could not connect to server"
                                                     delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert show];
  }
      
    }
    else if(tag==1)
    {
        //One category Point purchase
        if ([self reachable]) {
            
        
        
        NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( unlockables==nil){
            unlockables=[NSNumber numberWithInt:0];
        }
        
            
            NSString *OnePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"OnePoint"];
           [self loaderStartWithText:@"Purchasing"];
           
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:OnePoint];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }


    }
    else if(tag==3)
    {
        if([self reachable])
        {
        NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( unlockables==nil){
            unlockables=[NSNumber numberWithInt:0];
        }
    [self loaderStartWithText:@"Purchasing"];
              NSString *ThreePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"ThreePoint"];
         [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:ThreePoint];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }


    }
    else if(tag==5)
    {
        //Five point purchase
        
       if([self reachable])
       {
        NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( unlockables==nil){
            unlockables=[NSNumber numberWithInt:0];
        }
           [self loaderStartWithText:@"Purchasing"];
        
           NSString *FivePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"FivePoint"];
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:FivePoint];
       }
       else
       {
           
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                           message:@"Could not connect to server"
                                                          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
           [alert show];
       }

  
    }
    else if(tag==10)
    {
        //Unlimited category purchase
        if([self reachable])
        {
         [self loaderStartWithText:@"Purchasing"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappSuccessNotification:)
                                                     name:@"inappSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappFailureNotification:)
                                                     name:@"inappFailureNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreSuccessNotification:)
                                                     name:@"inappReStoreSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreFailureNotification:)
                                                     name:@"inappReStoreFailureNotif"
                                                   object:nil];
        
        NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        dictionary=[inappStr objectAtIndex:0];
     
        
        
        
        
        

        
                
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(unlimitedCategoryPurchaseSuccess:)
                                                     name:@"unlimitedCategoryPurchaseNotif"
                                                   object:nil];
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
       
        }else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }

       
        
        

    }
    else if(tag==11)
    {
        //Unlock the selected category
        
        NSDictionary *dict=[arrayContents objectAtIndex:currentIndex];
        
        
      //  NSNumber *points=[[dict objectForKey:@"points"] integerValue];
        
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *points = [f numberFromString:[dict objectForKey:@"points"]];
        
        
        NSNumber* myPoints=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( myPoints==nil){
            myPoints=[NSNumber numberWithInt:0];
        }

        if([myPoints intValue]>=[points intValue])
        {
        NSString *catIdToPurchase=[dict objectForKey:@"Id"];
        BOOL status=     [self coreDataWriteInappPurchased:catIdToPurchase];
        
        if(status)
        {
          //  NSInteger newPoint=myPoints-points;
            [[MKStoreKit sharedKit]consumeCredits:points identifiedByConsumableIdentifier:@"UnlockPoints"];

            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Category unlocked successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"VIEW_GRID"] isEqualToString:@"YES"]) {
                [scrollHome removeFromSuperview];
                [self setUpScroll];
            }
            else {
                [tblViewHome reloadData];
                
                
                
                
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Category unlocking failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
            
        }
        else{
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please purchase points to unlock this category" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    [customAlert close];
}

#pragma mark - InApp Purchase CoreData Methods

-(BOOL)checkCategoryPurchased:(NSString*)catid
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Inapp" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    NSString *currentAppid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@ AND categoryid=%@ AND categoryType=%@",currentAppid,catid,@"maincategory"];
    
    
    
    [fetchRequest setPredicate:predicate];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
    
    NSArray *fetchedObjects = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    [fetchRequest setEntity:entity];
    
    
   
    
    
    
    NSString *purchaseStatus;
   
    
    //NSDate *lastSyncDate;
    @try {
        Inapp *appsObj=[fetchedObjects objectAtIndex:0];
        
        purchaseStatus=appsObj.paidStatus;
        
        
        
        
    }
    @catch (NSException * e) {
     
        purchaseStatus=@"No";
        
    }
    
    
    if([purchaseStatus isEqualToString:@"Yes"])
        return true;
    else
        return false;
    
    
}
-(BOOL)coreDataWriteInappPurchased:(NSString*)catid
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
   

    
    @try {
      
             NSError *error;
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Inapp"
                                    inManagedObjectContext:context];
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            [Que setValue:catid forKey:@"categoryid"];
        
            [Que setValue:@"maincategory" forKey:@"categoryType"];
        
            [Que setValue:@"Yes" forKey:@"paidStatus"];
        
        if (![context save:&error]) {
        return false;
        }
        else
            return true;
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        return false;
        
        
    }
  }

- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"TestNotification123"])
        NSLog (@"Successfully received the test notification!");
    
    [self loaderStop];
}

#pragma mark - PurchaseAction Loader
UIView *overlaay;
-(void)loaderStartWithText :(NSString *)loaderText
{
    
    
    overlaay =[[UIView alloc]initWithFrame:self.view.window.rootViewController.view.bounds];
    overlaay.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    overlaay.tag = 1001;
    [overlaay setUserInteractionEnabled:YES];
    [GMDCircleLoader setOnView:overlaay withTitle:loaderText animated:YES];
    [self.view.window.rootViewController.view addSubview:overlaay];
   // [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self  selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
}


-(void)loaderStop
{
    
    
    [GMDCircleLoader hideFromView:overlaay animated:YES];
    [overlaay removeFromSuperview];
}
#pragma mark - ad
// AdMob initializing

-(void)removeBanner
{
     scrollHome.frame = CGRectMake(0, 65, 320,  self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-20);
    tblViewHome.frame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height-2, 320,  self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-10);
    
    [bannerView_ removeFromSuperview];
    [ad1Image removeFromSuperview];
    
}
-(void)setAdBannerWithArray:(NSArray *) arrAdDetails
{
    //if ([arrAdDetails count] == 0) {
      //  return;
    //}
   // NSDictionary *dict = [arrAdDetails objectAtIndex:0];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"admob_status"] isEqualToString:@"Yes"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"CustomAd"];
      //  [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"];
        [bannerView_ removeFromSuperview];
        bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
       
        bannerView_.adUnitID = [[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"];
        bannerView_.rootViewController = self;
        bannerView_.delegate = self;
        [bannerView_ loadRequest:[GADRequest request]];
        bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
        [self.view addSubview:bannerView_];
        
        
        
        
        [bannerView_ setDelegate:self];
        int scrollHeightMain;
        if (IS_IPHONE5)
            scrollHeightMain = 402;//415
        else
            scrollHeightMain = 315;
        scrollHeight = scrollHeightMain;
        
        scrollHeight=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-20-bannerView_.frame.size.height;
    }
    else {
        [ad1Image removeFromSuperview];
        
        
        NSArray *arr=[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotArray"];
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotArray"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image"] isEqualToString:@"FALSE"])) {
             NSDictionary *dict= [[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"];
            if (![[dict objectForKey:@"image"] isEqualToString:@"NA"]) {
                
                banery= self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-50;
                ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                ad1Btn.backgroundColor=[UIColor clearColor];
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];

                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image"]];
                adLink = [dict objectForKey:@"link"];
                int scrollHeightMain;
                if (IS_IPHONE5)
                    scrollHeightMain = 400;
                else
                    scrollHeightMain = 315;
                
                 scrollHeightMain=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-20-50;
                
                scrollHeight = scrollHeightMain;
                scrollHome.frame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, 320, scrollHeightMain);
            }
        }
        else {
            
            NSDictionary *dict=[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAdDict"];
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"CustomAd"];
           // [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"CustomAdDict"];
            if (![[dict objectForKey:@"ad_1"] isEqualToString:@"NA"]) {
                banery= self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-50;
                ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                ad1Btn.backgroundColor=[UIColor clearColor];
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];

                
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_1"]];
                adLink = [dict objectForKey:@"link_1"];
                int scrollHeightMain;
                if (IS_IPHONE5)
                    scrollHeightMain = 400;
                else
                    scrollHeightMain = 315;
                
                scrollHeightMain=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-20-50;
                
                scrollHeight = scrollHeightMain;
                scrollHeight = scrollHeightMain;
                
                 scrollHome.frame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, 320, scrollHeightMain);
                
                
                
            }
            else
                [ad1Image removeFromSuperview];
        }
    }
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    
    int scrollHeightMain;
    if (IS_IPHONE5)
        scrollHeightMain = 402;
    else
        scrollHeightMain = 315;
scrollHeightMain=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-20-50;
    
    scrollHeight = scrollHeightMain;
    
    scrollHome.frame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, 320, scrollHeightMain);
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}

#pragma mark - Settings Web service call
-(void)startSettingsWebServiceOfCurrentApp
{
    [self.view addSubview:vwLoading];
    [activityIndicator1 startAnimating];
    
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.withOutRetry=YES;
    webService.respondToMethod = @selector(settingsServiceResponseOfCurrentApp:);
    
    
    BOOL isSingleApp=[DBOperations isSingleApp];
    NSString *str;
    if (isSingleApp) {
        str=[NSString stringWithFormat:@"%@%@&multi=no",SETTINGS_WEBSERVICE_SUB,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    }
    else{
        NSString *ismulti=@"yes";
       /* if ([[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"])
            ismulti=  [[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"];*/
        
        str=[NSString stringWithFormat:@"%@%@&multi=%@",SETTINGS_WEBSERVICE_SUB,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"],ismulti];
    }
    [webService startParsing:str];
}
/*Webservice response method of selected app settings */
-(void)settingsServiceResponseOfCurrentApp:(NSData *) responseData
{
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSArray *settingsArray=[dict objectForKey:@"settings"];
    NSDictionary *inappDict = [dict objectForKey:@"inap_settings"];
    
    [vwLoading removeFromSuperview];
    [activityIndicator1 stopAnimating];
    
    if([settingsArray count]>0)
        
    {
        NSDictionary *settingsDict=[settingsArray objectAtIndex:0];
        
        [self saveCurrentAppSettings:settingsDict];
        [self saveCurrentInappSetting:inappDict];
        
        [self coreDataWriteAppsSettings:settingsDict forInappDic:inappDict];
        
       
        
    }
    
    
}
-(void)saveCurrentInappSetting:(NSDictionary*)inappDict
{
    [[NSUserDefaults standardUserDefaults]setObject:inappDict forKey:@"inappSettings"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}
/*This function save selected app details to nsuserdefault for accessing the settings while control is inside the selected app. This storage is for only accessing it from anywhere of the app. When a new app is selected this replaces the data with settings data of new app*/
-(void)saveCurrentAppSettings:(NSDictionary*)dict
{
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"app_title"] forKey:@"AppTitle"];
    
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
    
    
    [tempDict setValue:[dict objectForKey:@"ad_1"] forKey:@"ad_1"];
    [tempDict setValue:[dict objectForKey:@"ad_2"] forKey:@"ad_2"];
    [tempDict setValue:[dict objectForKey:@"ad_3"] forKey:@"ad_3"];
    
    [tempDict setValue:[dict objectForKey:@"link_1"] forKey:@"link_1"];
    [tempDict setValue:[dict objectForKey:@"link_2"] forKey:@"link_2"];
    [tempDict setValue:[dict objectForKey:@"link_3"] forKey:@"link_3"];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempDict forKey:@"CustomAdDict"];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"admob_status"] forKey:@"admob_status"];
    if ([[dict objectForKey:@"admob_status"] isEqualToString:@"Yes"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"CustomAd"];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"admob_id"] forKey:@"adMobId"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"CustomAd"];
    }
    
    if ([[dict objectForKey:@"distance_in"] isEqualToString:@"km"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"KM"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"KM"];
    }
    if ([[dict objectForKey:@"app_view"] isEqualToString:@"grid"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"VIEW_GRID"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"VIEW_GRID"];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"]) {
        //[self.view addSubview:vwLoading];
        //[self startWebService];
    }
    if([[dict objectForKey:@"offline_guide"] isEqualToString:@"yes"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"offline_guide"];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"refresh_interval"] forKey:@"refresh_interval"];
    }
    else
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"offline_guide"];
    
        
}
/*This writes selected app settings to coredata for offline use*/
-(void)coreDataWriteAppsSettings:(NSDictionary*)dict forInappDic:(NSDictionary*)inappDict{
    
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    
    [fetchRequest setPredicate:predicate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *results = [context1 executeFetchRequest:fetchRequest error:&error];
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:dict forKey:@"settings"];
    [favoritsGrabbed setValue:inappDict forKey:@"inappsettings"];
    
    
    
    //  NSError *error;
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else{
        //  [self coreDataReadAppsSettings];
        
    }
    
    
}

#pragma mark - Web service call




// Start Web service here



// Get web service response here






-(void)startHotSpotWebService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(hotSpotResponse:);
    webService.withOutRetry=YES;
    [webService startParsing: [NSString stringWithFormat:@"%@%@",HOT_SPOT_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}
-(void)hotSpotResponse:(NSData *) responseData
{
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSDictionary *arrHotSpots = [dict objectForKey:@"hotspots"];
    [[NSUserDefaults standardUserDefaults] setObject:arrHotSpots forKey:@"HotSpotArray"];
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
}


# pragma mark - Button Actions
//Click action for the buttons in scrollview
-(IBAction)onListButton:(id)sender {
    
    
     if (_isCellClicked)return;
    
   
    
    UIButton *btn = (UIButton *) sender;
    
     NSDictionary *dict = [arrayContents objectAtIndex:btn.tag];
    
    
    NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];

  if(![unlimitedPurchase isEqualToString:@"purchased"])
    {
        NSInteger points=[[dict objectForKey:@"points"] integerValue];
        if([[dict objectForKey:@"paid"] isEqualToString:@"paid"]&&points>0)
        {
            
            
            
            if(![self checkCategoryPurchased:[dict objectForKey:@"Id"]])
            {
                
               [self inappPurchasePopShow:btn.tag];
                return;
                
              
               
                
            }
        }
        
    }
   _isCellClicked=YES;
    

    
    
   
    
    
    
    
    
    
    if (![[dict objectForKey:@"ad"] isEqualToString:@"FALSE"]) {
        NSMutableDictionary *dicto = [[NSMutableDictionary alloc] init];
        [dicto setObject:[dict objectForKey:@"ad"] forKey:@"imageLink"];
        [dicto setObject:[dict objectForKey:@"link"] forKey:@"webLink"];
        [[NSUserDefaults standardUserDefaults] setObject:dicto forKey:@"CatAd"];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"CatAdExist"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"CatAdExist"];
    }
    
   
    if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
        PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
        [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:[dict objectForKey:@"type"] withName:[dict objectForKey:@"name"]];
        [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
        [self.navigationController pushViewController:subCategoryViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"url"]){
        if([self reachable]){
    
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dict objectForKey:@"external_url"]]];
    }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
             _isCellClicked=NO;
            return;
        }
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"static"])
    {
        PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
        [detailViewController getDictionary:dict];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dict objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dict objectForKey:@"type"] isEqualToString:@"normal"]){
        PDListViewController *listViewController = [[PDListViewController alloc] init];
        [listViewController getDictionary:dict];
        [self.navigationController pushViewController:listViewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"event"]) {
      
        PDEventsViewController *viewController = [[PDEventsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
//        if (![[dict objectForKey:@"maxeventid"] isEqualToString:@"FALSE"]) {
//            if (![[dict objectForKey:@"maxeventid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxEventId"]]) {
//                viewController.tabBarItem.badgeValue = @"new";
//            }
       // }
         [self.navigationController pushViewController:viewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"deal"]) {
        PDDealsViewController *viewController = [[PDDealsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
//        if (![[dict objectForKey:@"maxdealid"] isEqualToString:@"FALSE"]){
//            if (![[dict objectForKey:@"maxdealid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxDealId"]]) {
//                viewController.tabBarItem.badgeValue = @"new";
//            }
//        }
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"rss"]) {
        if([self reachable]){
        PDRssListViewController *rssListViewController = [[PDRssListViewController alloc] initWithNibName:@"PDRssListViewController" bundle:nil];
        [rssListViewController getDictionary:dict];
        rssListViewController.isFromHome = YES;
        [self.navigationController pushViewController:rssListViewController animated:YES];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                   [alert show];
             _isCellClicked=NO;
            return;
        }
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"gallery"]) {
        PDGalleryViewController *galleryViewController = [[PDGalleryViewController alloc] initWithNibName:@"PDGalleryViewController" bundle:nil];
        [galleryViewController getDictionary:dict];
        galleryViewController.hidesBottomBarWhenPushed = YES;
        //galleryViewController.isFromHome = YES;
        [self.navigationController pushViewController:galleryViewController animated:YES];
    }
    else {
        if ([[dict objectForKey:@"type"] isEqualToString:@"fixed"] || [[dict objectForKey:@"type"] isEqualToString:@"location"]) {
            if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
                PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
                [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:@"fixed" withName:[dict objectForKey:@"name"]];
                [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
                [self.navigationController pushViewController:subCategoryViewController animated:YES];
            }
            else {
                PDListViewController *listViewController = [[PDListViewController alloc] init];
                [listViewController getDictionary:dict];
                [self.navigationController pushViewController:listViewController animated:YES];
            }
        }
    }
}
-(IBAction)onTapToRetry:(id) sender
{
//    if ([self reachable]) {
//        [self startSettingsWebService];
//    }
//    else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"App needs a working internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
}
-(void)fcloseAction
{
    [facebookPage removeFromSuperview];
}
-(void)tcloseAction
{
    [twitterPage removeFromSuperview];
}
-(void)aboutAction 
{
    viewAbout.frame = CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:viewAbout];
    [UIView animateWithDuration:0.2 delay:0.01 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewAbout.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:nil];
}
-(IBAction)onBack:(id)sender
{
    [UIView animateWithDuration:0.2 delay:0.05 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewAbout.frame = CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         [viewAbout removeFromSuperview];
                     }];
}
-(void)fPageAction
{
    [self.view addSubview:facebookPage];
}
-(void)tPageAction
{
    [self.view addSubview:twitterPage];
}
-(void)shareAction
{
    NSString *texttoshare = @"Local City Guide   Check out in your iPhone for a cool and useful app      https://itunes.apple.com/us/app/local-city-guide/id721102929?ls=1&mt=8"; //this is your text string to share
    UIImage *imagetoshare = [UIImage imageNamed:@"icon.png"]; //this is your image to share
    NSArray *activityItems = @[texttoshare, imagetoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

/*-(void)AllquestFailed
{
    
[self coreDataCodeRead];
}*/




-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==346)
    {
        
       
    }
}
-(void)coreDataWriteAppsDetail:(NSDate*)lastSynchDate{
    
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
  
    NSManagedObjectContext *updateContext= [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    
    [fetchRequest setPredicate:predicate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:updateContext];
    [fetchRequest setEntity:entity];
    @try {
    
    NSError *error;
    NSArray *results = [updateContext executeFetchRequest:fetchRequest error:&error];
        
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:lastSynchDate forKey:@"lastSync"];

    if (![updateContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else{
      //  [self coreDataReadAppDetail];
        
    }
        
    }
    @catch (NSException *exception) {
        NSLog(@"error:%@",exception);
    }
    
}
-(NSMutableDictionary*)coreDataReadAppDetail
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    
    [fetchRequest setPredicate:predicate];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
    
    NSArray *fetchedObjects = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    
  
    [fetchRequest setEntity:entity];
    
    
   
    
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    NSMutableDictionary *dicObject;
    
     //NSDate *lastSyncDate;
    @try {
        Apps *appsObj=[fetchedObjects objectAtIndex:0];
        dicObject=[[NSMutableDictionary alloc]init];
   
       [dicObject setObject:appsObj.settings forKey:@"settings"];
        [dicObject setObject:appsObj.lastSync forKey:@"lastSync"];
        
        
        
    

        [refreshControl endRefreshing];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
       /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Could not connect to server"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];*/
        
        
    }
    
    
    return dicObject;
    
    
}






-(void)readListOfImagesToDownload
{
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:readContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    for(IMAGES_TO_CACHE *object in results)
    {
        
        NSString *imageUrl=object.imageurl;
     
       
        
        [self sdImageCaching:imageUrl];
    }
}
-(void)insertImagesToDownload:(NSString*)imgstr
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
 
 
            
    @try{
        
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"IMAGES_TO_CACHE"
                                    inManagedObjectContext:context];
            
            
          
            
            [Que setValue:imgstr forKey:@"imageurl"];
            [Que setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"] forKey:@"appid"];
    
    
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                //            [self performSelectorInBackground:@selector(updateProgress:) withObject:[NSNumber numberWithFloat:i]];
                // [self stopCircleLoader4];
            }
            
    
    }
    @catch (NSException * e)
    {
        NSLog(@"Exception: %@", e);
        
        
    }
  
    
}
-(void)deleteImageDownloaded:(NSString*)imgstr
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
  
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"imageurl=%@",imgstr];
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    IMAGES_TO_CACHE *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
}
-(void)sdImageCaching:(NSString*)imageUrlKey;
{
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    NSURL *urlimg=[NSURL URLWithString:imageUrlKey];
    UIImageView*imagevw=[[UIImageView alloc]init];
    
    
    
    

    
    
    imagevw.image=[imageCache imageFromDiskCacheForKey:imageUrlKey];
    if (!imagevw.image)
    {
        
      
     [imagevw sd_setImageWithURL:urlimg placeholderImage:nil options:SDWebImageRefreshCached];
    
       /* [imagevw sd_setImageWithURL:urlimg
               placeholderImage:nil
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          
                          
                      }];*/
    }
    else
    {
        [self deleteImageDownloaded:imageUrlKey];
    }
    
}

- (void) loadImageFromURL:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        im.image = [UIImage imageNamed:@"img_def"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                im.image = image;
            });
        });
    }
}
- (void) loadImageFromURL_bus:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        im.image = [UIImage imageNamed:@"img_def"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
//            UIImage *image = [UIImage imageWithData:data];
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                im.image = image;
//            });
        });
    }
}

#pragma mark - Image caching

- (void) loadImageFromURL1:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        im.image = [UIImage imageNamed:@"img_def"];
    
        //        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        //        dispatch_async(queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:imageURL];
        [FTWCache setObject:data forKey:key];
        UIImage *image = [UIImage imageWithData:data];
        // dispatch_sync(dispatch_get_main_queue(), ^{
        im.image = image;
        //   });
        // });
        
        
       /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                im.image = image;
                
            });
        });*/

    }
}


@end
