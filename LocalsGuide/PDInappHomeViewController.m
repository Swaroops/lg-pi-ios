//
//  PDInappHomeViewController.m
//  Visiting Ashland
//
//  Created by swaroop on 01/10/15.
//  Copyright © 2015 iDeveloper. All rights reserved.
//

#import "PDInappHomeViewController.h"
#import "PDAppDelegate.h"
#import "PDBuyViewController.h"
#import "PDViewController.h"
#import "Reachability.h"
#import "PDWebViewController.h"
//#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "PDSplashViewController.h"
#import "UIImageView+AFNetworking.h"
#import "MKStoreKit.h"

#import "PDInappCell.h"

#import "Apps.h"
#import "Categories.h"
#import "SettingsViewController.h"

#import "MMLabel.h"
#import "MMPopLabel.h"
#import "MMPopLabelDelegate.h"

#import "JDFTooltipView.h"
#import "JDFSequentialTooltipManager.h"
#import "JDFTooltips.h"

#import "AsyncImageView.h"

#import "UIImageView+JMImageCache.h"

#import "ACPDownloadView.h"
#import "ACPStaticImagesAlternative.h"
#import "ACPIndeterminateGoogleLayer.h"

#import "QuartzCore/QuartzCore.h"
#import "SDWebImagePrefetcher.h"

#import "PDLoader.h"

#import "SSZipArchive.h"
#import "PDIntermediatePageViewController.h"


#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

//<UITabBarControllerDelegate,DBOperationsDelegate,NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>
@interface PDInappHomeViewController ()<UISearchBarDelegate,NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>
{
    BOOL isLocationUpdated;
    int scrollHeight;
    int banery;
    NSMutableArray*mainArray;
    UIRefreshControl *refreshControl;
    UITableView *tblView;
    
    PDSplashViewController *splashController;
    
    PDViewController *homeVC;
    
    AsyncImageView *tblHeaderImage;
    
    NSString *bannerImgURL;
    
    NSDictionary *settingDict;
    
    NSDictionary *splashDict;
    NSInteger indexe;
    int o;
    //int k;
    UIImage * resultImg;
    BOOL once;
    NSMutableArray *mutAry;
    NSMutableArray *mutStrng;
    int j;
    UIView *loaderView;
    UIActivityIndicatorView *spinner;
    
    NSMutableArray *strid;
    
    BOOL fi;
    
    NSTimer *_timer;
    
     BOOL isDownloaded;
    
    NSMutableArray* myMutableArrayAgain;
    
    NSMutableArray *headerArray;
    
    BOOL onlyOnce;
    BOOL tblVieeOnce;
    CGFloat progresss;
    
    UICircle* m_testView;
    NSTimer* m_timer;
    NSTimer *timerForSize;
    
    NSMutableArray *classifIdsArr;
    NSMutableArray *saArra;
    
}
@property (nonatomic, assign) float progress;
@property (weak, nonatomic) IBOutlet ACPDownloadView *downloadView;

@property (nonatomic, retain) NSMutableData *dataToDownload;
@property (nonatomic) float downloadSize;
@end

@implementation PDInappHomeViewController
NSMutableArray *addAppId;
NSArray *arrayContents;
NSArray *arAdDetails;
NSArray *arrFooters;
UITabBarController *tabbar;

@synthesize delegate;

//- (id)initWithStyle:(UITableViewStyle)style
//{
//  
//    if (self) {
//        NSArray *group1 = [[NSArray alloc] initWithObjects:@"Napoli", @"Juventus", @"Inter", @"Milan", @"Lazio", nil];
//        NSArray *group2 = [[NSArray alloc] initWithObjects:@"Real Madrid", @"Barcelona", @"Villareal", @"Valencia", @"Deportivo", nil];
//        NSArray *group3 = [[NSArray alloc] initWithObjects:@"Manchester City", @"Manchester United", @"Chelsea", @"Arsenal", @"Liverpool", nil];
//        
//        originalData = [[NSArray alloc] initWithObjects:group1, group2, group3, nil];
//       
//    }
//    return self;
//}
-(IBAction)settingsAction:(id)sender
{
  
    [backgrounViewForToolTip removeFromSuperview];
    if ([self reachable]) {
        SettingsViewController *tvc=[[SettingsViewController alloc]init];
        [self presentViewController:tvc animated:NO completion:nil];
    }
}

- (IBAction)searchAction:(id)sender {
    
//     if(![self reachable])
//     {
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
//                                                    message:@"An active internet connection is required for global search"
//                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
//    [loaderView removeFromSuperview];
//    
//    return;
//
//     }
    
    tblView.tableHeaderView =  nil;
    
    tblView.tableHeaderView = searchBar;
    
    [searchBar becomeFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    [self.view addSubview:loaderView];
    [loaderView addSubview:spinner];
    [spinner startAnimating];
    
    if(![self reachable])
    {
        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Search not available in offline mode"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [loaderView removeFromSuperview];
        
        return;*/
        
        saArra = [[NSMutableArray alloc]init];
        
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:[NSString stringWithFormat:@"forGlobalSearchoffline"]];
        NSMutableArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        //an active internet connection is required for global search
        
        
        
        saArra= retrievedDictionary;
        
        // arra =  [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"forSearchOffline%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
        NSLog(@"saarra :%@",saArra);
        
        
       
        [self offlinSearch];
        
    }
    else
    {
        [self searchKeywrd];
    }
}

-(void)offlinSearch
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        searchArray=[[NSMutableArray alloc]init];
        searchArray=saArra;
        
        if([searchArray count]<1)
        {
            [searchBar becomeFirstResponder];
            searchBar.text=@"";
            [vwLoading removeFromSuperview];
            
            tblView.tableHeaderView =  nil;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[NSString stringWithFormat:@"No Results Found"]
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
            
            // [[NSUserDefaults standardUserDefaults]setObject:arra forKey:@"searchData"];
            
            NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:saArra];
            [[NSUserDefaults standardUserDefaults]setObject:data2 forKey:@"searchData"];
            
            [vwLoading removeFromSuperview];
            [activityIndicator1 stopAnimating];
            
            PDListViewController *listViewController = [[PDListViewController alloc] init];
            // [listViewController fixedWebServiceResponsee:data];
            listViewController.isSomethingEnabled = YES;
            listViewController.isfromVC=YES;
            listViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:listViewController animated:YES];
            
            
        }
        [self.view endEditing:YES];
        
    });
    
    
}
-(void)searchKeywrd
{
    NSString *keywrd;
    NSString *dist;
    
    keywrd = searchBar.text ;
    //sorry
    //search text must have minimum of three characters.
    
    [[NSUserDefaults standardUserDefaults]setObject:searchBar.text forKey:@"keywrd"];
    
    keywrd = [keywrd stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    
    if(keywrd.length <3)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Search term should have a length of minimum 3 characters"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [loaderView removeFromSuperview];

        return;
    }
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
    {
       dist=@"M";
    }
    else{
    dist=@"K";
    }
    //http://insightto.com/webservice/search_listing.php?text=
    
    
    NSString *guideids;
    guideids = [strid componentsJoinedByString:@","];
    NSLog(@"guideids :%@",guideids);
    
   NSString *lat= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
    NSString *lon= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
    NSLog(@"lat : %@ , long : %@",lat,lon);
    NSString *wUrl=[NSString stringWithFormat:@"http://insightto.com/webservice/search_listing.php?text=%@&lat=%@&long=%@&distance_in=%@&guide_ids=%@",keywrd,lat,lon,dist,guideids];
    
    NSLog(@"url = %@",wUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
       // NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
       NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            
           searchArray=[[NSMutableArray alloc]init];
            searchArray=[json objectForKey:@"business"];
            
            if([searchArray count]<1)
            {
                
                NSString *keySer =[[NSUserDefaults standardUserDefaults]objectForKey:@"keywrd"];
                    int i=0;
                    NSMutableDictionary *strA=[NSMutableDictionary dictionary];
                    
                    NSMutableArray *arrMain = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
                    
                    
                    for(NSString *str in arrMain)
                    {
                        BOOL isTheObjectThere;
                        
                        isTheObjectThere=false;
                        
                        NSArray *myArray = [[arrMain objectAtIndex:i] valueForKey:@"keywords"];
                        
                        if(![myArray isEqual:@""])
                        {
                            
                            for(NSString *strArr in myArray)
                            {
                                
                                NSString *strArr2 =   [strArr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                
                                
                                
                                NSLog(@"strArr :%@ keySer :%@",strArr2,keySer);
                                
                                if([strArr2 caseInsensitiveCompare:keySer] == NSOrderedSame)
                                {
                                    NSLog(@"Matches");
                                    isTheObjectThere=true;
                                    break;
                                }
                            }
                            //isTheObjectThere = [myArray containsObject:keySer];
                        }
                        
                        NSString *strAppname = [[arrMain objectAtIndex:i] valueForKey:@"app_name"];
                        NSString *keywords = [[arrMain objectAtIndex:i] valueForKey:@"keywords"];
                        
                        if([strAppname isEqualToString:@"Mount Shasta"])
                            NSLog(@"strAppname :%@",strAppname);
                        
                        //if(![keywords isEqualToString:@""])
                        NSLog(@"keywords :%@",keywords);
                        
                        
                        if(([keySer caseInsensitiveCompare:strAppname]== NSOrderedSame)||isTheObjectThere)//ignores casesensitive
                        {
                            strA =[arrMain objectAtIndex:i];
                            NSLog(@"strA :%@",strA);
                            
                            
                            break;
                        }
                        i++;
                    }
                    
                
                    
if([strA count]>1)
                    {
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexe]valueForKey:@"id"] forKey:@"currentAppId"];
                            [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexe]valueForKey:@"name"] forKey:@"currentAppName"];
                            [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
                            
                            
                            NSLog(@"indexe : %ld",(long)indexe);
                            
                            
                            [[NSUserDefaults standardUserDefaults]setObject:data2 forKey:@"searchData"];
                            
                            
                            [loaderView removeFromSuperview];
                            
                            //[spinner stopAnimating];
                            
                            PDListViewController *listViewController = [[PDListViewController alloc] init];
                            // [listViewController fixedWebServiceResponsee:data];
                            listViewController.isSomethingEnabled = YES;
                            listViewController.hidesBottomBarWhenPushed = YES;
                            [self.navigationController pushViewController:listViewController animated:YES];
                            
                        }
                   
            }

        else{
                        
              [loaderView removeFromSuperview];

                //[spinner stopAnimating];
                [searchBar becomeFirstResponder];
                searchBar.text=@"";
                tblView.tableHeaderView =  nil;
               [self.view endEditing:YES];
            
            
            NSString *message;
            if([json objectForKey:@"message"])
            {
                message = [json objectForKey:@"message"];
                
            }
            else
            {
                message=[json objectForKey:@"result"];
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[NSString stringWithFormat:@"%@",message]
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            //[loaderVC removeFromSuperview];
            
            }
                
        }
           
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexe]valueForKey:@"id"] forKey:@"currentAppId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexe]valueForKey:@"name"] forKey:@"currentAppName"];
                [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
                //selectedCell=indexPath.row;
                
                //indexe = indexPath.row;
                
                NSLog(@"indexe : %ld",(long)indexe);
                
           
                [[NSUserDefaults standardUserDefaults]setObject:data2 forKey:@"searchData"];
                
                
                [loaderView removeFromSuperview];
                
                //[spinner stopAnimating];
                
                PDListViewController *listViewController = [[PDListViewController alloc] init];
                // [listViewController fixedWebServiceResponsee:data];
                listViewController.isSomethingEnabled = YES;
                listViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:listViewController animated:YES];
               /* PDViewController *vc = [[PDViewController alloc]init];
                vc.isFromSearch = YES;
                [self.navigationController pushViewController:vc animated:NO];
                */
                
                
               // tableView.userInteractionEnabled = NO;
            }
                [self.view endEditing:YES];
            
        });
        
    }];
    
    [dataTask resume];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
  [self->searchController setActive:FALSE];
    tblView.tableHeaderView =  nil;
    
    searchBar.text = @"";
}
#pragma mark - Table & Refresh Control Functions

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch ;
    touch = [[event allTouches] anyObject];
    
    
 /*   if ([touch view] == vwLoading)
    {
        [vwLoading removeFromSuperview];
        return;
    }
    else */if ([touch view] == backgrounViewForToolTip)
      [backgrounViewForToolTip removeFromSuperview];
}
-(void)showToolTip
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"showToolTip"])
    {
    NSString *numberOfTimes=[[NSUserDefaults standardUserDefaults] objectForKey:@"showToolTip"];
    int noOfTimes=[numberOfTimes intValue];
    noOfTimes++;
    numberOfTimes =[NSString stringWithFormat:@"%d",noOfTimes];
    [[NSUserDefaults standardUserDefaults]setObject:numberOfTimes forKey:@"showToolTip"];
    }
    else
    {
          [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"showToolTip"];
    }
    
    
   
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    backgrounViewForToolTip=[[UIView alloc]initWithFrame:backgroundImgv.frame];
    backgrounViewForToolTip.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    [self.view insertSubview:backgrounViewForToolTip belowSubview:vwLoading];
   
    
    JDFTooltipView *tooltip = [[JDFTooltipView alloc] initWithTargetView:settingsBtn hostView:backgrounViewForToolTip tooltipText:@"Click Here To Manage Settings" arrowDirection:JDFTooltipViewArrowDirectionUp width:270.0f];
    tooltip.font=[UIFont boldSystemFontOfSize:13];
   // tooltip.backgroundColor=[UIColor colorWithRed:244.0f/255.0f green:157.0f/255.0f blue:20.0f/255.0f alpha:0.4];
    //[self .view bringSubviewToFront:tooltip];
    tooltip.tooltipBackgroundColour=[UIColor colorWithRed:244.0f/255.0f green:157.0f/255.0f blue:20.0f/255.0f alpha:1.0f];
    //tooltip.frame=CGRectMake(40, 60, 100, 30);
    [tooltip drawRect:CGRectMake(40, 60, 100, 30)];
    [tooltip show];
    
 
}

-(BOOL)shouldAutorotate {
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    // Use this to allow upside down as well
    //return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}
-(NSUInteger) supportedInterfaceOrientations {
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}
-(void)setUpListView
{
    if(!tblVieeOnce)
    {
        tblVieeOnce=YES;
        
    [tblView removeFromSuperview];
    tblView = [[UITableView alloc] initWithFrame:CGRectMake(0, 65, 320, scrollHeight-30)style:UITableViewStyleGrouped];
    tblView.delegate = self;
    tblView.dataSource = self;
    tblView.backgroundColor = [UIColor whiteColor];
    tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view insertSubview:tblView belowSubview:vwLoading];
    
    }
    UILabel *linlbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 64, 320,1)];
    linlbl.backgroundColor=[UIColor grayColor];
    [self.view addSubview:linlbl];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    [tblView addSubview:refreshControl];
    
    [tblView setShowsVerticalScrollIndicator:NO];
    
   // [self.view bringSubviewToFront:backgrounViewForToolTip];
   
  //  [vwLoading removeFromSuperview];
    
    [refreshControl endRefreshing];
    
   /* dispatch_async(dispatch_get_main_queue(), ^{
        [sel.tblView reloadData];
    });*/

    

}

- (void)testRefresh:(UIRefreshControl *)refreshControlH
{
    callMainService=NO;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    
    
    
    splashImgv.image=nil;
    NSLog(@"mainarray count :%lu",(unsigned long)[mainArray count]-1);
    int r=(int)[mainArray count]-1;
    
    if(o==r)
    {
        
        [_locationManager startUpdatingLocation];
        
       // once=NO;
        if ([self reachable])
        {
            [self startAppsDetailWebService];
        }
    }
    else
    {
        int64_t delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        
        [refreshControl endRefreshing];
             [_locationManager startUpdatingLocation];
            
        [self startAppsDetailWebService];
            
             });
    }
    
    
  /*  NSString *distance=@"&distance_in=M";
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"])
    {
        distance= [NSString stringWithFormat:@"&distance_in=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"]];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"M" forKey:@"showDistanceIn"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    distUnit= [[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"];
    NSString *nearby=@"&nearby=on";
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"])
    {
        nearby= [NSString stringWithFormat:@"&nearby=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"]];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [tblView reloadData];*/
}
#pragma mark -AlertView Delegate method
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==346)
    {
        
        
        if(buttonIndex!=0)
        {
      
           
          
            
            DBOperations *obj=[[DBOperations alloc]init];
            obj.delegate=self;
            [obj readSelectedGuideClassification];
            
            return;

        }
        [refreshControl endRefreshing];
       
    }
}
#pragma mark - Webservice Functions inner Apps list
-(void)startAppsDetailWebService
{
    
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(startAppsDetailWebServiceResponse:);
    
    
    NSString *appid=[NSString stringWithFormat:@"&app_id=%@",[DBOperations getMainAppid]];
    NSString *distance=@"&distance_in=M";
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"])
    {
        distance= [NSString stringWithFormat:@"&distance_in=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"]];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"M" forKey:@"showDistanceIn"];
        [[NSUserDefaults standardUserDefaults]synchronize];

    }
      distUnit= [[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"];
    NSString *nearby=@"&nearby=on";
   
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"])
    {
        nearby= [NSString stringWithFormat:@"&nearby=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"]];
        
        if([classIds isEqualToString:@""]||[classIds isEqualToString:@"&clid=()"])
        {
          [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
        }
        
        else
        {
            tblVieeOnce = NO;
            
         [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"nearby"];
        }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if([classIds isEqualToString:@""])
        {
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
        }
        
        else
        {
            tblVieeOnce = NO;
            
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"nearby"];
        }
    }
    
    
    /*DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readSelectedGuideClassification];*/
    
    mainWebServiceParam=[NSString stringWithFormat:@"%@%@%@%@%@%@",APP_LIST_DETAILS,mainWebServiceParamloc,appid,distance,classIds,nearby];
    
    if([classIds isEqualToString:@"&clid=(null)"])
    {
        NSLog(@"classIds :%@",classIds);
        
        [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
    }

    
    if(!mainWebServiceParamloc || [mainWebServiceParamloc isEqualToString:@""] ||(mainWebServiceParamloc==nil) )
    
    {
        NSLog(@"location is nil :%@",mainWebServiceParamloc);
        
       
        
    }
    
    
    isSingleApp=[DBOperations isSingleApp];
    
    
    [webService startParsing:mainWebServiceParam];
    
    
    NSLog(@"mainWebServiceParam url :%@",mainWebServiceParam);
    
}
-(void) classificaionAccessComplete:(NSArray*)resultArray
{
   // if(![[NSUserDefaults standardUserDefaults]objectForKey:@"clasiificationLimiter"])
    
   // tblVieeOnce = NO;
    
    {
        classIds=@"";
/*if ([resultArray count]==1) {
        NSDictionary *dict=[resultArray objectAtIndex:0];
        classIds=classIds=[NSString stringWithFormat:@"&clid=%@",[dict objectForKey:@"classid"]];;
    if ([self reachable]) {
        
       // if(!(once==YES))
        
        
        
                [self startAppsDetailWebService];
    }
    else {
        [self coreDataCodeReadAppDetails];
    }
       return;
}*/
 
    BOOL classbool=YES;
    
    for (NSDictionary *dict in resultArray) {
        
        if (classbool) {
            NSDictionary *dict=[resultArray objectAtIndex:0];
            classIds=[dict objectForKey:@"classid"];
            classbool=NO;
        }
        else
        classIds=[NSString stringWithFormat:@"%@,%@",classIds,[dict objectForKey:@"classid"]];
        
    }
    if (classIds.length>0) {
        classIds=[NSString stringWithFormat:@"&clid=%@",classIds]; //classIds
    }
    
        NSArray *fromDb;
        NSString *classsids;
        classsids = [classifIdsArr componentsJoinedByString:@","];
        NSLog(@"classsids :%@",classsids);
        
       // classIds =[NSString stringWithFormat:@"&clid=%@",classsids];
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"classifIdsArr"])
       fromDb = [[NSUserDefaults standardUserDefaults]objectForKey:@"classifIdsArr"];
        
        NSString *strng1;
        strng1 = [fromDb componentsJoinedByString:@","];
        NSLog(@"strngclass :%@",strng1);
        
       classIds =[NSString stringWithFormat:@"&clid=%@",strng1];
        
    if ([self reachable]) {
     
       // if(!(once==YES))
            
        [self startAppsDetailWebService];
    }
    else {
        [self coreDataCodeReadAppDetails];
    }
}
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"clasiificationLimiter"];
}
// Get web service response here
-(void)startAppsDetailWebServiceResponse:(NSData *) responseData
{
    [noAppAvailableLbl removeFromSuperview];
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    mainArray=[[NSMutableArray alloc]init];
    
    ////////
   // mutAry = nil;
    ////////
    
    mutStrng= [[NSMutableArray alloc]init];
    
    mainArray = [dict objectForKey:@"guides"];
    [[NSUserDefaults standardUserDefaults]setObject:mainArray forKey:@"guidesName"];
    
     [[NSUserDefaults standardUserDefaults]setObject:dict forKey:@"guidesNameSearch"];
    
    NSArray *classArr=[[NSArray alloc]init];
    
    classArr =[dict objectForKey:@"classification"];
    
    
    
    NSDictionary *dictoo;
    NSString *ndccode;
    NSMutableArray *outputArray=[NSMutableArray new];
    for (int i=0; i<[mainArray count]; i++) {
        dictoo=[mainArray objectAtIndex:i];
        ndccode=[dictoo objectForKey:@"id"];
        if(ndccode)
        [outputArray addObject:ndccode];
    }
    strid=[outputArray copy];
    NSLog(@"strid :%@",strid);
    
    
    classifIdsArr=[[NSMutableArray alloc]init];
    
    NSMutableArray *classifiIds=[NSMutableArray new];
    NSString*classifiIdd;
    NSDictionary *classifiDict;
    for (int i=0; i<[classArr count]; i++) {
        classifiDict=[classArr objectAtIndex:i];
        classifiIdd=[classifiDict objectForKey:@"id"];
        if(classifiIdd)
            [classifiIds addObject:classifiIdd];
    }
    classifIdsArr=[classifiIds copy];
    NSLog(@"classifIdsArr :%@",classifIdsArr);
    
    if([classIds isEqualToString:@""]||[classIds isEqualToString:@"&clid=(null)"])
        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"classifIdsArr"])
    [[NSUserDefaults standardUserDefaults]setObject:classifIdsArr forKey:@"classifIdsArr"];
  
    
    
    
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj writeClassificationId:classArr];
    
    [self coreDataWriteAppsDetail];
    
    
    classArr=[[NSArray alloc]init];
    classArr =[dict objectForKey:@"settings"];
    NSString *isMulti=[[classArr objectAtIndex:0]objectForKey:@"multi_guide"];
    
    
    //code change by ram for saving settings in dictionary
    settingDict=[classArr objectAtIndex:0];
    
    [[NSUserDefaults standardUserDefaults]setObject:isMulti forKey:@"ismulti"];
    
    bannerImgURL=[[classArr objectAtIndex:0]objectForKey:@"guide_header_img"];
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    [userDefaults setObject:bannerImgURL forKey:@"bannerImgURL"];
    [userDefaults synchronize];
    
    
   // [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:[NSArray arrayWithObject:bannerImgURL]];
    
    if([mainArray count]==1)
    {
        NSString *str= [[mainArray objectAtIndex:0]objectForKey:@"message"]; ;
        
        if([str isEqualToString:@"Sorry No App Available"])
        {
            noAppAvailableLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 150, self.view.frame.size.width, 200)];
            noAppAvailableLbl.backgroundColor=[UIColor clearColor];
            noAppAvailableLbl.textColor=[UIColor blackColor];
            noAppAvailableLbl.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:noAppAvailableLbl];
            [self.view bringSubviewToFront:noAppAvailableLbl];
            noAppAvailableLbl.text=str;
            
            
          
            [mainArray removeAllObjects];
             [self setUpListView];
            return;
        }
        
      
       
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
   if ([DBOperations isSingleApp]||[isMulti isEqualToString:@"no"])
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"multi_guide"];
        
        isOneGuide=YES;
      
         [[NSUserDefaults standardUserDefaults]setObject:[DBOperations getMainAppid] forKey:@"currentAppId"];
        
        
        for (NSDictionary *dct in mainArray) {
            if ([[dct objectForKey:@"id"] isEqualToString:[DBOperations getMainAppid]]) {
                [[NSUserDefaults standardUserDefaults]setObject:[dct objectForKey:@"app_name"] forKey:@"currentAppName"];
                break;
            }
        }
        
      
        [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
        
        selectedCell=0;
        [self loadSplashImage];
        return;
    }
    
    [self setUpListView];
 
    
}

-(void)webServiceSplashResponseTest:(NSData *) responseData
{
    //tblView.userInteractionEnabled=YES;
   
    //[self.view addSubview:vwLoading];
    
    
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSString *splashImgUrl=[[[dict objectForKey:@"splash"]objectAtIndex:0]objectForKey:@"splash_img"];
    NSDictionary *dicWeb= [NSDictionary dictionaryWithObjects:mainArray
                                                      forKeys:[mainArray valueForKey:@"id"]];
    int i;
    NSLog(@"dicWeb : %@",[[mainArray valueForKey:@"id"]objectAtIndex:i]);
        NSString *splashImgKey=[NSString stringWithFormat:@"splash%@",[[mainArray valueForKey:@"id"]objectAtIndex:i]];
     NSLog(@"i :%d",i);
    
   [mutStrng insertObject:splashImgUrl atIndex:i];
    if(j==21)
    {
        NSLog(@"mutStrng :%@",mutStrng);
    }
    
    NSString *key = [splashImgUrl MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
       // cell.imageView.image = image;
        resultImg = image;
    }
    else
    {
        
    }

    [[NSUserDefaults standardUserDefaults]setObject:splashImgUrl forKey:splashImgKey];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
     i++;
    
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:@"Splash"];
   
    if (!splashImgv.image) {
        
     
        [imageCache storeImage:resultImg forKey:splashImgUrl completion:^{  }];

        
        
    }
    else
    {
       
    }
    
}

-(void)loadSplashImageTest
{
    [self.view addSubview:vwLoading];
    [activityIndicator1 startAnimating];
    splashImgv.contentMode=UIViewContentModeScaleToFill;
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    
   NSArray *arr= [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    NSString*str=[[mainArray objectAtIndex:indexe]objectForKey:@"splash_image"];//[arr objectAtIndex:indexe]
    
    splashImgv.image=[imageCache imageFromDiskCacheForKey:str];
    
    [splashImgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
    
    if (!splashImgv.image)
    {
        if (![str isEqualToString:@"NA"])
        {
            
            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
            
    NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
            
           
            
            
      NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];

NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];

            NSLog(@"firstBit folder :%@",firstBit);
            NSLog(@"secondBit folder :%@",secondBit2);
            
            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            NSLog(@"str url :%@",str);
            
        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/splash",secondBit2]];

            NSArray* str3 = [str componentsSeparatedByString:@"splash/"];
            
            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit3);
            NSLog(@"secondBit folder :%@",secondBit3);
            
            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];

            UIImage* image = [UIImage imageWithContentsOfFile:Path];
            
           //
            
         //   [self loadImageFromURL1:str image:splashImgv];
            
            
            splashImgv.image = image;
        }
        
        
       
    }

    
    int64_t delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [vwLoading removeFromSuperview];
        
        
        
    });

    
    
   
}

-(void)loadSplashImageTest2
{
    [self.view addSubview:vwLoading];
    [activityIndicator1 startAnimating];
    splashImgv.contentMode=UIViewContentModeScaleToFill;
    splashImgv.image=nil;
    
  NSMutableArray *tArr =  [[NSUserDefaults standardUserDefaults]objectForKey:@"loadingsplashSearch"];
    NSLog(@"tArr :%@",tArr);
    
       NSString *appId= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    NSString *dicId= [tArr valueForKey:@"id"];
    
    int i;
    if([appId isEqualToString:dicId])
    {
        
        
        for (i = 0; i < [mainArray count]; i++) {
            
            if([dicId isEqualToString:[[mainArray objectAtIndex:i]valueForKey:@"id"]])
            {
            id myArrayElement = [mainArray objectAtIndex:i];
            NSLog(@"myArrayElement : %@",myArrayElement);

                break;
                }
        }
        
        
        NSLog(@"same id");
        
        NSDictionary *dic=[mainArray objectAtIndex:i];
       
        NSString *splashImgKey=[NSString stringWithFormat:@"splash%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        NSString *splashImgName=[[NSUserDefaults standardUserDefaults]objectForKey:splashImgKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:@"Splash"];
        
        NSString *splashKeyName= [NSString stringWithFormat:@"splash%@",dicId];
        
        if([splashImgKey isEqualToString:@"splash1"])
        {
            splashImgName=[[mainArray objectAtIndex:i]objectForKey:@"splash_image"];
            
        }
        
        if([splashImgKey isEqualToString:splashKeyName])
        {
            splashImgName=[[mainArray objectAtIndex:i]objectForKey:@"splash_image"];
            
        }
        NSLog(@"***********************************");
        NSLog(@"splashImgKey :%@",splashImgKey);  NSLog(@"splashKeyName :%@",splashKeyName);
        
        
        splashImgv.image=[imageCache imageFromDiskCacheForKey:splashImgName];
        
        
        
        
        if (!splashImgv.image) {
            
            if([self reachable])
            {
                
                @try {
                    if([mutAry count]<i)
                    {
                        [self startSplashWebService];
                        
                        return;
                        
                    }
                    
                    
                    if([[mutAry objectAtIndex:i] isEqual:[NSNull null]])
                    {
                        
                    }
                    else
                    {
                        if(![splashImgName isEqualToString:@"NA"])
                            splashImgv.image= [mutAry objectAtIndex:i];//resultImg;
                    }
                    [self startLoadingApp];
                    
                    
                }
                @catch (NSException * e)
                {
                    NSLog(@"Exception: %@", e);
                    
                    
                }
                
            }
            else
            {
                
                [self startLoadingApp];
            }
            
        }
        else
        {
            
            NSString *spurlStr = [[mainArray objectAtIndex:i]objectForKey:@"splash_image"];
            
            if([spurlStr isEqualToString:@"NA"])
            {
                int64_t delayInSeconds = 1.5;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                    [vwLoading removeFromSuperview];
                    
                });
                
                [self startLoadingApp];
                
                return;
                
            }
            
            
            
            splashImgv.contentMode=UIViewContentModeScaleToFill;
            
            if([splashImgName isEqualToString:[[mainArray objectAtIndex:i]objectForKey:@"splash_image"]])
            {
                NSLog(@"splash image has the same name as previous image");
                
                NSLog(@"splashImage :%@",[[mainArray objectAtIndex:i]objectForKey:@"splash_image"]);
                
                //            SDImageCache *imageCache =[SDImageCache sharedImageCache];
                //            imageCache = [imageCache initWithNamespace:@"Splash"];
                UIImage *spImage = [[UIImage alloc]init];
                spImage = splashImgv.image ;
                NSLog(@"spImage :%@",spImage);
                //            [imageCache storeImage:spImage forKey:splashImgKey];
                //splashImgv.image= [mutAry objectAtIndex:i];
                NSString *urlStr = [[mainArray objectAtIndex:i]objectForKey:@"splash_image"];
                
                NSString *splashImgKeyy=[NSString stringWithFormat:@"splash%@",[[mainArray objectAtIndex:i]objectForKey:@"id"]];
                //NSString *splashImgName=[[NSUserDefaults standardUserDefaults]objectForKey:splashImgKey];
                
                [[NSUserDefaults standardUserDefaults]setObject:urlStr forKey:splashImgKeyy];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:@"Splash"];
                
                [imageCache storeImage:spImage forKey:splashImgKeyy completion:^{  }];
                
                
                
            }
            else
            {
                NSLog(@"splash image has changed : %@",splashImgName);
                
                if([self reachable])
                {
                    if([mutAry count]<i)
                    {
                        [vwLoading removeFromSuperview];
                        
                        [self startLoadingApp];
                        
                        return;
                        
                    }
                    
                }
                
                NSArray* foo = [splashImgName componentsSeparatedByString: @"splash_"];
                NSString* firstBit = [foo objectAtIndex: 0];
                
                NSLog(@"firstBit :%@",firstBit);
                
                
                
                NSArray* oof = [firstBit componentsSeparatedByString: @"http://insightto.com/splash/"];
                NSString* secondBit = [oof objectAtIndex: 1];
                
                NSLog(@"secondBit :%@",secondBit);
                
                // NSString *trimmedStr= secondBit;
                
                NSString *idStr = [[mainArray objectAtIndex:selectedCell]objectForKey:@"id"];
                if([secondBit isEqualToString:idStr])
                {
                    splashImgv.image= [mutAry objectAtIndex:i];
                    
                }
                else
                {
                    NSString *urlStr = [[mainArray objectAtIndex:i]objectForKey:@"splash_image"];
                    
                   
                    
                    if([urlStr isEqualToString:@"NA"])
                    {
                        int64_t delayInSeconds = 1.5;
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                            
                            [vwLoading removeFromSuperview];
                            
                        });
                        
                        [self startLoadingApp];
                        
                        return;
                        
                    }
                    
                    if([splashImgKey isEqualToString:[NSString stringWithFormat:@"splash%@",[[mainArray objectAtIndex:i]objectForKey:@"id"]]])
                    {
                        splashImgName=[[mainArray objectAtIndex:i]objectForKey:@"splash_image"];
                        
                    }
                    
                    splashImgv.image=[imageCache imageFromDiskCacheForKey:splashImgName];
                    
                    NSString *splashImgKeyy=[NSString stringWithFormat:@"splash%@",[[mainArray objectAtIndex:i]objectForKey:@"id"]];
                   
                    
                    NSLog(@"splashImgv.image :%@",splashImgv.image);
                    
                    
                    
                }
                
                
            }
            
            
            [self startLoadingApp];
            
        }
    }
    else
    {
        [vwLoading removeFromSuperview];
        
        [self startLoadingApp];
        
    }
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    
}



#pragma mark - CoreData Functions for writing & Reading inner Apps

/*This method writes details of all apps to coredata for offline purpose. This doesnot just clear the entire data and insert. Instead it updates existing apps, removes deleted apps and will add new apps*/
-(void)coreDataWriteAppsDetail{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
 
    
    NSError *error;
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *appArray=[[NSMutableArray alloc]init];
   
    [appArray addObjectsFromArray:mainArray];
    
    for(Apps *object in results)
    {
        NSString *appidFromDb=object.appid;
        BOOL rowFoundInDb=false;
        
        for (i=0; i<[appArray count]; i++)
        {
            
             NSString *appidFromServer=[[appArray objectAtIndex:i] objectForKey:@"id"];
       
            if([appidFromDb isEqualToString:appidFromServer])
            {
                //Update the DB
                NSManagedObject* favoritsGrabbed = object;
                
                NSString*imageStr=[[appArray objectAtIndex:i] objectForKey:@"image"];
                imageStr= [imageStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [favoritsGrabbed setValue:imageStr forKey:@"image"];
                [favoritsGrabbed setValue:[[appArray objectAtIndex:i]objectForKey:@"app_name"]forKey:@"name"];
                
                
                
              @try
                {
                    
                
                if (![readContext save:&error]) {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
                else{
                 
                    
                    
                    rowFoundInDb=true;
                    [appArray removeObjectAtIndex:i];
                    
                    break; //
                    
                }
                
                
            }
            @catch (NSException * e) {
                
                
            }
                //Update the DB
            }
  
        }
        
        
        if(rowFoundInDb==false)
        {
            //Delete row in Db which is not in server
            
            [readContext deleteObject:object];
        }
        
    }
 
   
    for(int i =0;i<[appArray count];i++)
    {
    //Insert into DB
    NSManagedObjectContext *insertContext = [appDelegate managedObjectContext];
    NSManagedObject *Que = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Apps"
                            inManagedObjectContext:insertContext];
    
    
        NSString *STRR=[NSString stringWithFormat:@"%@",[[appArray objectAtIndex:i] objectForKey:@"id"] ];
    [Que setValue:STRR forKey:@"appid"];
    
    NSString*imageStr=[[appArray objectAtIndex:i] objectForKey:@"image"];
    imageStr= [imageStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [Que setValue:imageStr forKey:@"image"];
    
    
    [Que setValue:[[appArray objectAtIndex:i]objectForKey:@"app_name"]forKey:@"name"];
    
    
    if (![insertContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    
    }
}
/*This method read details of all apps for offline purpose*/
-(void)coreDataCodeReadAppDetails{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];

    
  
    
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (Apps *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.appid forKey:@"id"];
        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
        [tempManufacturerDictionary setObject:manuf.name forKey:@"app_name"];
 
        [array addObject:tempManufacturerDictionary];
    }
 
    mainArray=[[NSMutableArray alloc]init];
    mainArray=array;
    
    [[NSUserDefaults standardUserDefaults]setObject:mainArray forKey:@"guidesName"];
    
    if([mainArray count]==1)
    {
        NSString *isMulti=[[NSUserDefaults standardUserDefaults]objectForKey:@"multi_guide"];
          if (![isMulti isEqualToString:@"yes"]) {
        
        isOneGuide=YES;
        [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:0]objectForKey:@"id"] forKey:@"currentAppId"];
        [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:0]objectForKey:@"app_name"] forKey:@"currentAppName"];
        [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
        
        selectedCell=0;
        [self loadSplashImage];
        }
        
        
        return;
    }
    
    [self setUpListView];

    [refreshControl endRefreshing];
    
    
    
}





-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
 
      CLLocation *location_updated = [locations lastObject];
    
      float latt  =  location_updated.coordinate.latitude;
      float longg =  location_updated.coordinate.longitude;
    //http://insightto.com/webservice/guides.php?latitude=8.567749&longitude=76.873516&app_id=1&distance_in=mi&clid=3,4

    //radi
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:7];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString1 = [formatter stringFromNumber:[NSNumber numberWithFloat:latt]];
    
    NSString *numberString2 = [formatter stringFromNumber:[NSNumber numberWithFloat:longg]];
    
    NSLog(@"numberString1 :%@",numberString1); NSLog(@"numberString2 :%@",numberString2);
    
    
    [[NSUserDefaults standardUserDefaults]setObject:numberString1 forKey:@"doubleLatitude"];
    [[NSUserDefaults standardUserDefaults]setObject:numberString2 forKey:@"doubleLongitude"];
    
    mainWebServiceParamloc=[NSString stringWithFormat:@"latitude=%f&longitude=%f",latt,longg];
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    
    if(callMainService==NO)
     [obj readSelectedGuideClassification];
    
    callMainService=YES;
    
//    if([[NSUserDefaults standardUserDefaults] objectForKey:@"showSplash"]&&(k==[mainArray count]-1))
//    {
//        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"showSplash"];
//    }
    
    
    
    
    [[NSUserDefaults standardUserDefaults]synchronize];


    
    
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
       // if(!(IS_IPHONE_4_OR_LESS))
    {
        
       // mainWebServiceParamloc=@"";
        
        DBOperations *obj=[[DBOperations alloc]init];
        obj.delegate=self;
        [obj readSelectedGuideClassification];
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    NSString *st = [[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"];
    
    if([st isEqualToString:@"on"])
    {
      [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"nearbyisOFF"];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"nearbyisOFF"];
    }
    [vwLoading removeFromSuperview];
    //[[NSUserDefaults standardUserDefaults]setObject:@"K" forKey:@"showDistanceIn"];
    
     if([self reachable]){
     }
    else
    {
        [vwLoading removeFromSuperview];
    }
    tblView.userInteractionEnabled = YES;
    
      callMainService=NO;
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    
    if(!onlyOnce)
    {
        onlyOnce=true;
    [_locationManager startUpdatingLocation];
    }
    
   // [tblView setContentOffset:CGPointZero animated:NO];
    
    [self->searchController setActive:FALSE];
  //  tblView.tableHeaderView =  nil;
    tblView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,tblView.bounds.size.width, 0.01f)];
    searchBar.text=@"";
    
    
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"firstInstall"])
    {
        UIView *loaderFI= [[UIView alloc]initWithFrame:self.view.frame];
        loaderFI.backgroundColor=[UIColor whiteColor];
        
        CGRect frame=CGRectZero;
        frame=loaderFI.frame;
        frame.origin.y=settingsBtn.frame.origin.y+settingsBtn.frame.size.height+2;
        loaderFI.frame=frame;
        
        UIActivityIndicatorView *spinnerFI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinnerFI.center = CGPointMake([[UIScreen mainScreen]bounds].size.width / 2.0, ([[UIScreen mainScreen]bounds].size.height / 2.0)-frame.origin.y);
        [spinnerFI startAnimating];
        
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, spinnerFI.frame.origin.y+spinnerFI.frame.size.height+5, loaderFI.frame.size.width, spinnerFI.frame.size.height+5)];
        lbl.text=@"LOADING GUIDES";
        lbl.textColor=[UIColor blackColor];
        lbl.font=[UIFont systemFontOfSize:15];
        lbl.textAlignment=NSTextAlignmentCenter;
        lbl.backgroundColor=[UIColor clearColor];
        
        [loaderFI addSubview:lbl];
        
        [loaderFI addSubview:spinnerFI];
        
        [self.view addSubview:loaderFI];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"firstInstall"];
        
        fi=true;
    }
    else if(![[NSUserDefaults standardUserDefaults] objectForKey:@"firstLoading"]&!fi)
    {
        
        UIView *loaderFI= [[UIView alloc]initWithFrame:self.view.frame];
        loaderFI.backgroundColor=[UIColor whiteColor];
        
        CGRect frame=CGRectZero;
        frame=loaderFI.frame;
        frame.origin.y=settingsBtn.frame.origin.y+settingsBtn.frame.size.height+2;
        loaderFI.frame=frame;
        
        UIActivityIndicatorView *spinnerFI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinnerFI.center = CGPointMake([[UIScreen mainScreen]bounds].size.width / 2.0, ([[UIScreen mainScreen]bounds].size.height / 2.0)-frame.origin.y);
        [spinnerFI startAnimating];
        
        [loaderFI addSubview:spinnerFI];
        
        [self.view addSubview:loaderFI];
        
      [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"firstLoading"];
    }
    
     if([[NSUserDefaults standardUserDefaults]objectForKey:@"thisForBackFromSettings"])
     {
         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"thisForBackFromSettings"];
         
         NSString *appida= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
         if([self isTheAppHasOflineData:appida])
             [settingsBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
         
     }
    
    
     if(_toSettings)
     {
         _toSettings=NO;
       [settingsBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
     }
    
    
}
/*-(void)viewDidAppear:(BOOL)animated
{
    
    
  NSIndexPath *myIP = [NSIndexPath indexPathForRow:79 inSection:1] ;
    
    NSLog(@" viewdidappear :%lu",(unsigned long)myIP);
    
    [self tableView: self->tblView cellForRowAtIndexPath:myIP];
}*/

-(NSString *)sizeOfFolder:(NSString *)folderPath
{
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *contentsEnumurator = [contents objectEnumerator];
    
    NSString *file;
    unsigned long long int folderSize = 0;
    
    while (file = [contentsEnumurator nextObject]) {
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:file] error:nil];
        folderSize += [[fileAttributes objectForKey:NSFileSize] intValue];
    }
    
    //This line will give you formatted size from bytes ....
    NSString *folderSizeStr = [NSByteCountFormatter stringFromByteCount:folderSize countStyle:NSByteCountFormatterCountStyleFile];
    
    NSLog(@"folderSizeStr folder :%@",folderSizeStr);
    
    return folderSizeStr;
}

- (UIImage*)loadImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @".zip" ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
   // [self sendAction:path];
    return image;
}
-(void)downloadZip
{
//    dispatch_queue_t queue = dispatch_get_global_queue(0,0);
//    dispatch_async(queue, ^{
    
       NSString *url1 = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Downloading Started");
        
       
    
    
  //  NSURL *url = [NSURL URLWithString:url1];
   NSURL *url = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
       
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            NSInteger httpStatus = [((NSHTTPURLResponse *)response) statusCode];
            NSLog(@"responsecode:%ld", (long)httpStatus);
            
            
            if (error||httpStatus == 404) {
                
                NSLog(@"Download Error:%@",error.description);
                
                static NSString *simpleTableIdentifier = @"SimpleTableCell";
                
                PDInappCell *cell = (PDInappCell *)[tblView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
                
                
                [m_timer invalidate];
                m_timer = nil;
                tblView.scrollEnabled=YES;
                settingsBtn.enabled=YES;
                tblView.userInteractionEnabled=YES;
                [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusNone];
                [tblView reloadData];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                                message:@"System error occured"
                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                return ;
            }
            if (data) {
                
                
//                NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
//                [[NSUserDefaults standardUserDefaults]setObject:@"yess" forKey:ida];
                
                
                // find Documents directory and append your local filename
                NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                documentsURL = [documentsURL URLByAppendingPathComponent:@"localFile.zip"];
                
                // and finally save the file
                [data writeToURL:documentsURL atomically:YES];
                
                
                NSString *filename = @"localFile.zip";
                NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString * zipPath = [documentsDirectory stringByAppendingPathComponent:filename];
                
                NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
                

                NSLog(@"File is saved  ");
                
                [self toChangetoPlayBtn];
                
            }
       
        
            
                
                
                
                // list contents of Documents Directory just to check
                    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                
                NSArray *contents = [[NSFileManager defaultManager]contentsOfDirectoryAtURL:documentsURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
                
                NSLog(@"description for zip:%@", [contents description]);
            
          
            
            //753_514
                
             /*   NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:imageCacheFolder];
                
            NSString *imageUrl ;//=destinationPath;
                
                UIImageView*imagevw=[[UIImageView alloc]init];
            
            
                [imageCache storeImage:imagevw.image forKey:imageUrl completion:^{  }];*/

           }];
        
        
        ///
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURL *urlforData = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithURL: urlforData];
        
        [dataTask resume];
        
        ///
        
        

    
   // });
    

}

-(void)test
{
    
        NSString *wUrl=[NSString stringWithFormat:@"%@%@&multi=yes",OFFLINE_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
   // NSString *wUrl=[NSString stringWithFormat:@"%@1&multi=yes",OFFLINE_WEBSERVICE];
        NSLog(@"url = %@",wUrl);
        
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self test];
            return ;
        }
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
        // NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
             NSArray *totalSize = [json objectForKey:@"total_file_size"];
            
            NSString *urlZip = [json objectForKey:@"app_image_folder"];
            
            [[NSUserDefaults standardUserDefaults]setObject:urlZip forKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            [self downloadZip];
            
            NSLog(@"totalSize :%@",totalSize);
            
            [[NSUserDefaults standardUserDefaults]setObject:totalSize forKey:@"TS"];
            
            
        });
        
    }];
    
    [dataTask resume];



}
- (void)viewDidLoad {

   // [self test];
     searchData = [[NSMutableArray alloc] init];
    tblVieeOnce=NO;
    
    tblView.userInteractionEnabled = YES;
   
    callMainService=NO;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestAlwaysAuthorization];}
    
    
    [_locationManager startUpdatingLocation];
    
    
    isOneGuide=NO;
    
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.delegate = self;
    isLocationUpdated = NO;
    if (IS_IPHONE5) {
      [[NSBundle mainBundle] loadNibNamed:@"PDInappHomeViewController" owner:self options:nil];
        scrollHeight = 450;
        banery = 470;
    }
    else {
        [[NSBundle mainBundle] loadNibNamed:@"PDInappHomeViewController4" owner:self options:nil];
        scrollHeight = 365;
        banery = 382;
    }
    
    [super viewDidLoad];
    
      [self.view addSubview:vwLoading];
     vwLoading.frame=self.view.frame;
    splashImgv.image=nil;
    [activityIndicator1 startAnimating];
    
    if (IS_IPHONE5) {
    scrollHeight=self.view.frame.size.height-65;
    }
    else
    {
        scrollHeight=self.view.frame.size.height-160;
    }
    

    btnTapToRetry.hidden = YES;
    
    
    
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
      searchBar.placeholder=@"Search Guides";
    searchBar.delegate = self;
    searchBar.showsCancelButton=YES;
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTintColor:[UIColor whiteColor]];
    
    
    loaderView= [[UIView alloc]initWithFrame:self.view.frame];
    loaderView.backgroundColor=[UIColor whiteColor];
    
    
    spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake([[UIScreen mainScreen]bounds].size.width / 2.0, [[UIScreen mainScreen]bounds].size.height / 2.0);
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(fValues:)
//                                                 name:@"toloadFooterValuesFromSearch"
//                                               object:nil];//footerSearch
    
    yThis=false;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"yThis"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fValues:)
                                                 name:@"toloadFooterValuesFromSearch"
                                               object:nil];//footerSearch
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(forTableviewCellSelect:)
                                                 name:@"forTableviewCellSelect"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTableView:)
                                                 name:@"reloadTableView"
                                               object:nil];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"forTableviewCellSelect" object:self];
// [[NSNotificationCenter defaultCenter] postNotificationName:@"headerTest" object:self]
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"thisForBackFromSettings"];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshFooterFromVC:)
                                                 name:@"refreshFooterFromVC"
                                               object:nil];
    
  
    ACPIndeterminateGoogleLayer * layer = [ACPIndeterminateGoogleLayer new];
    [layer updateColor:[UIColor grayColor]];
    [self.downloadView setIndeterminateLayer:layer];
   
    
    NSString *statusBool ;
    statusBool = @"";
   NSLog(@"%@",[NSString stringWithFormat: @"Status: %@", (statusBool ? @"Approved" : @"Rejected")]);
    
    addAppId = [[NSMutableArray alloc]init];
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
     myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
    }
    
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"clasiificationLimiter"];
    
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    // Kick off a timer to count it down
   
}

- (void)decrementSpin
{
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    PDInappCell *cell = (PDInappCell *)[tblView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
   //  PDInappCell * cellSelected = (PDInappCell*)[tblView cellForRowAtIndexPath:indexe];
    
//    NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"TS"];
//    
//    NSArray* foo = [str componentsSeparatedByString: @" "];
//    NSString* firstBit = [foo objectAtIndex:0];
//    
//    NSString* secondBit = [foo objectAtIndex:1];
//    
//    NSLog(@"firstBit :%@",firstBit);
//    NSLog(@"secondBit :%@",secondBit);
//
    
//    
//    NSString *appCheck = [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
//    
//    if([appCheck isEqualToString:@"1"])
//    {
//       // percenta = (float)percenta+15.0f;
//    
//    }
//    else if([appCheck isEqualToString:@"25"])
//    {
//       // percenta = (float)percenta+15.0f;
//        
//    }
//    //int sizee = (6/percenta)*100;
//    NSLog(@"percenta :%f",percenta);
    
    
    //[self sizeOfCurrentAppFolder];
  
   // float sizeFolder =[sizeOFFoler floatValue];
    
    float percenta = (float)_downloadSize;
    
    float sizeOFFoler=(float)[_dataToDownload length];
    
    NSLog(@"sizeFolder :%f",sizeOFFoler);
    
    float sizee = ((int)sizeOFFoler/(int)percenta)*100;//6.0f / 100.0f);
    
    NSLog(@"sizee :%f",sizee);
    
     NSLog(@"m_testView.percent :%f",m_testView.percent);
    
    
        if (sizee < 100) {
           
            if (m_testView.percent < 1)
            {
                m_testView.percent = (float)sizee;
                [m_testView setNeedsDisplay];
            }
            else
            {
                m_testView.percent = (int)sizee;
                [m_testView setNeedsDisplay];
            }
            
                   }
       
        
        
        
           
      
    
        
    
    else {
        [m_timer invalidate];
        m_timer = nil;
        tblView.scrollEnabled=YES;
        settingsBtn.enabled=YES;
        tblView.userInteractionEnabled=YES;
        [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusCompleted];
        [tblView reloadData];
//        NSIndexPath *myIP = [NSIndexPath indexPathForRow:indexe inSection:1] ;
//        
        ///////
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSMutableArray *arrFavorites;
        
        {
            
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
                arrFavorites = [plistDict objectForKey:@"downloadsArray"];
                
                
                
                //        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                //        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                //        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                //        NSFileManager * fileManager = [NSFileManager defaultManager];
                //        myMutableArrayAgain = [NSMutableArray new];
                if([fileManager fileExistsAtPath:finalPath])
                {
                    NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                    myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
                }
                
                
                if (!myMutableArrayAgain) myMutableArrayAgain = [[NSMutableArray alloc] init];
                
                // NSArray *array;
                
                if ([myMutableArrayAgain containsObject: [mainArray objectAtIndex:indexe]]) // YES
                {
                    NSLog(@"alreadycontains");
                }
                else
                {
                    if([myMutableArrayAgain count]==0)
                    {
                        myMutableArrayAgain = [[NSMutableArray alloc]init];
                        // [myMutableArrayAgain insertObject:[mainArray objectAtIndex:indexPath.row] atIndex:0];
                    }
                    //                else
                    [myMutableArrayAgain addObject:[mainArray objectAtIndex:indexe]];
                }
                
                [plistDict setObject:myMutableArrayAgain forKey:@"downloadsArray"];
                [plistDict writeToFile:finalPath atomically:YES];
                
            }
        }
        ///////
    }

}
-(void)toChangetoPlayBtn
{
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    PDInappCell *cell = (PDInappCell *)[tblView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    
    [m_timer invalidate];
    m_timer = nil;
    tblView.scrollEnabled=YES;
    settingsBtn.enabled=YES;
    tblView.userInteractionEnabled=YES;
    [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusCompleted];
    [tblView reloadData];
    //        NSIndexPath *myIP = [NSIndexPath indexPathForRow:indexe inSection:1] ;
    //
    ///////
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
            //        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            //        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            //        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            //        NSFileManager * fileManager = [NSFileManager defaultManager];
            //        myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
            }
            
            
            if (!myMutableArrayAgain) myMutableArrayAgain = [[NSMutableArray alloc] init];
            
            // NSArray *array;
            
            if ([myMutableArrayAgain containsObject: [mainArray objectAtIndex:indexe]]) // YES
            {
                NSLog(@"alreadycontains");
            }
            else
            {
                if([myMutableArrayAgain count]==0)
                {
                    myMutableArrayAgain = [[NSMutableArray alloc]init];
                    // [myMutableArrayAgain insertObject:[mainArray objectAtIndex:indexPath.row] atIndex:0];
                }
                //                else
                
                NSString *fl = [NSByteCountFormatter stringFromByteCount:_downloadSize countStyle:NSByteCountFormatterCountStyleFile];
                NSLog(@"fl :%@",fl);
                
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                
                [dict setObject:fl forKey:@"FolderSize"];
                
               id something = [dict objectForKey:@"FolderSize"];
                
                NSLog(@"something :%@",something);
                
                [[mainArray objectAtIndex:indexe]setObject:fl forKey:@"FolderSize"];
                
                NSLog(@"%@",[mainArray objectAtIndex:indexe]);
                
                [myMutableArrayAgain addObject:[mainArray objectAtIndex:indexe]];
            }
            
            [plistDict setObject:myMutableArrayAgain forKey:@"downloadsArray"];
            [plistDict writeToFile:finalPath atomically:YES];
            
        }
    }
    ///////
}
-(NSString*)sizeOfDownloadedFolder
{
   // NSLog(@"HOME > %@", NSHomeDirectory());
    
    NSMutableArray *arrayCacheP;
    NSString *cachePath;
    
   
    NSString* strCachePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    {
        
        
        NSError *error;
        NSMutableArray *arrayCachePaths = [[NSMutableArray alloc] init];
        arrayCacheP = [[NSMutableArray alloc]init];
        
        int s =0;
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:strCachePath error:&error];
        for (NSString *strName in dirContents) {
            
            //  if([strName isEqualToString:@"com.pidigi.lgdev"])
            
                NSString *string =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]]];
                
                {
                NSArray* foo = [string componentsSeparatedByString:@".zip"];
                
                NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                
                
                
                
                NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                
                NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                
                NSLog(@"firstBit folder :%@",firstBit);
                NSLog(@"secondBit folder :%@",secondBit2);
                
                
                
            
            
                if ([strName containsString:@"localFile"]) {
                    
                    
                    cachePath = [NSString stringWithFormat:@"%@",cachePath];
                    NSLog(@"cachePath :%@",cachePath);
                    
                    
                    
                    
                    [arrayCachePaths insertObject:cachePath atIndex:s];
                    // NSString *test = [arrayCachePaths objectAtIndex:s];
                    NSLog(@"%@", arrayCachePaths);
                    
                    NSString *str = [self sizeOfFolder:cachePath];
                    
                    NSString *code = [str substringFromIndex: [str length] - 2];
                    NSLog(@"code :%@",code);
                    
                    if([code isEqualToString:@"MB"]||[code isEqualToString:@"GB"])
                    {
                        [arrayCacheP insertObject:str atIndex:s];
                    }
                    else
                    {
                        [arrayCacheP insertObject:@"0.1" atIndex:s];
                    }
                    s++;
                    
                }
                
            }
        }
    }
    
    
    NSLog(@"arrayCacheP :%@",arrayCacheP);
    
    
    
    double sum = 0;
    for (NSNumber * n in arrayCacheP) {
        sum += [n doubleValue];
    }
    
    NSLog(@"sum :%f",sum);
    
    //cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@",[self sizeOfFolder:cachePath]];
    NSString * sizeStr =[NSString stringWithFormat:@"%.2f",sum];
    
    NSString *size = [NSByteCountFormatter stringFromByteCount:sizeStr countStyle:NSByteCountFormatterCountStyleFile];
    
    
    
    return sizeStr;
    
    
}
-(NSString*)sizeOfCurrentAppFolder
{
    NSLog(@"HOME > %@", NSHomeDirectory());
    
    NSMutableArray *arrayCacheP;
    NSString *cachePath;
    
    // NSFileManager *fileManagerr = [NSFileManager defaultManager];
    NSString* strCachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    // if([fileManagerr fileExistsAtPath:[cachePath stringByAppendingPathComponent:@"com.pidigi.lgdev"]])
    {
        
        
        NSError *error;
        NSMutableArray *arrayCachePaths = [[NSMutableArray alloc] init];
        arrayCacheP = [[NSMutableArray alloc]init];
        
        int s =0;
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:strCachePath error:&error];
        for (NSString *strName in dirContents) {
            
            //  if([strName isEqualToString:@"com.pidigi.lgdev"])
            {
                if ([strName containsString:[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]]) {
                    
                    
                    cachePath = [NSString stringWithFormat:@"%@/%@/com.hackemist.SDWebImageCache.MyFolder%@",strCachePath,strName,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                    NSLog(@"cachePath :%@",cachePath);
                    
                    
                    
                    
                    [arrayCachePaths insertObject:cachePath atIndex:s];
                    // NSString *test = [arrayCachePaths objectAtIndex:s];
                    NSLog(@"%@", arrayCachePaths);
                    
                    NSString *str = [self sizeOfFolder:cachePath];
                    
                    NSString *code = [str substringFromIndex: [str length] - 2];
                    NSLog(@"code :%@",code);
                    
                    if([code isEqualToString:@"MB"]||[code isEqualToString:@"GB"])
                    {
                        [arrayCacheP insertObject:str atIndex:s];
                    }
                    else
                    {
                        [arrayCacheP insertObject:@"0.1" atIndex:s];
                    }
                    s++;
                    
                }
                
            }
        }
    }
    
    
    NSLog(@"arrayCacheP :%@",arrayCacheP);
    
    
    
    double sum = 0;
    for (NSNumber * n in arrayCacheP) {
        sum += [n doubleValue];
    }
    
    NSLog(@"sum :%f",sum);
    
    //cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@",[self sizeOfFolder:cachePath]];
    NSString * sizeStr =[NSString stringWithFormat:@"%.2f",sum];
    
    NSString *size = [NSByteCountFormatter stringFromByteCount:sizeStr countStyle:NSByteCountFormatterCountStyleFile];
    
   
    
    return sizeStr;


}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    completionHandler(NSURLSessionResponseAllow);
    
    
    _downloadSize=[response expectedContentLength];
    _dataToDownload=[[NSMutableData alloc]init];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    [_dataToDownload appendData:data];
   // progressBar.progress=[ _dataToDownload length ]/_downloadSize;
    
    NSLog(@"_dataToDownload :%lu  ,_downloadedSize :%f",(unsigned long)[_dataToDownload length ],_downloadSize); //[_dataToDownload length ]/_downloadSize
    
    
    float percenta = (float)_downloadSize;
    
    float sizeOFFoler=(float)[_dataToDownload length];
    
    NSLog(@"sizeFolder :%f",sizeOFFoler);
    
    float sizee = ((float)sizeOFFoler/(float)percenta)*100;//6.0f / 100.0f);
    
    NSLog(@"sizee :%f",sizee);
    
    NSLog(@"m_testView.percent :%f",m_testView.percent);
    
    
    if (sizee < 100) {
        
        if (m_testView.percent < 1)
        {
            m_testView.percent = (float)sizee;
            [m_testView setNeedsDisplay];
        }
        else
        {
            m_testView.percent = (int)sizee;
            [m_testView setNeedsDisplay];
        }
        
    }
    

}


- (void)increment:(NSTimer *)timer {
    
    //self.progress =0.0f;
    self.progress = (self.progress +0.2f);
   //self.progress = (self.progress <=10.0f ? self.progress + 0.20f : 0.20f);
   // self.progress=[ _dataToDownload length ]/_downloadSize;
   // self.progress = (self.progress <= 0.81f ? self.progress + 0.21f : 0.0f);
    
    
   progresss  = ![_timer isValid] ? 300.0f / 10.0f : progresss + 0.01f;
   // [self.progressView setProgress:progress animated:YES];
    
    NSLog(@"Progress %f", self.progress);
    
     dispatch_async(dispatch_get_main_queue(), ^{
         
        // [self.downloadView setProgress:self.progress animated:YES];
       // [self.downloadView setProgress:self.progress animated:YES];
    });
    
}

-(void) forTableviewCellSelect:(NSNotification *) notification
{
     [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"thisForBackFromSettings"];
    
     [[NSUserDefaults standardUserDefaults]setObject:@"test" forKey:@"thisForBackFromSettings"];
    [self tableView:tblView didSelectRowAtIndexPath:0];
    
    //[tblView selectRowAtIndexPath:0 animated:false scrollPosition:UITableViewScrollPositionMiddle];
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    DBOperations *obj=[[DBOperations alloc]init];
     obj.delegate=self;
     [obj readSelectedGuideClassification];
    
    if ([[notification name] isEqualToString:@"TestNotification"])
    {
        [self testRefresh:nil];
        
        NSLog (@"Successfully received the test notification!");
    }
}
-(void)footerSearch:(NSNotification *) notification
{
    yThis = true;
    [self footerValuesWebService];

}
- (void) fValues:(NSNotification *) notification
{
    //[self.view addSubview:vwLoading];
    
     yThis = true;
    
    [self.view addSubview:loaderView];
    
    [self loadSplashImageTest2];
    
   NSString *stri= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    NSLog(@"stri :%@",stri);
    
    if([self reachable]){
        [self  startSettingsWebServiceOfCurrentApp];
    }
    else if([self isTheAppHasOflineData:stri])
    {
        
        
        
        
        [self coreDataReadAppsSettings];
    }
    else
    {
        
        [vwLoading removeFromSuperview];
        
        [loaderView removeFromSuperview];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"This app is not not available offline"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self performSelector:@selector(tableViewEnableProperty) withObject:nil afterDelay:0.6];
        
    }

    [loaderView removeFromSuperview];
    
    
}

-(void)makeViewShine:(UIView*) view
{
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    view.layer.shadowRadius = 10.0f;
    view.layer.shadowOpacity = 1.0f;
    view.layer.shadowOffset = CGSizeZero;
    
    
    [UIView animateWithDuration:0.7f delay:0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAllowUserInteraction  animations:^{
        
        [UIView setAnimationRepeatCount:15];
        
        view.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
        
        
    } completion:^(BOOL finished) {
        
        view.layer.shadowRadius = 0.0f;
        view.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                    message:@"System Error occured"
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
   // [alert show];
    
    return ;

    NSLog(@"MEMORY WARNING");
}

/*This method Checks internet connection */
-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
/*- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}*/

#pragma mark - UITableView  datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
// code change by Ram for adding banner header
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section==0){
        
        return kTableViewHeaderHeight;
    }
    else{
        return 0;
        
    }
}


//remove activity indicator while connection did finish loadin
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   /* [activityIndicatorNew stopAnimating];
    [activityIndicatorNew removeFromSuperview];
    self.view.userInteractionEnabled = YES;
    */
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
   
    
}

-(void)removeActivity
{
    [activityIndicatorNew stopAnimating];
    [activityIndicatorNew removeFromSuperview];

}

- (int)lineCountForText:(NSString *) text
{
    UIFont *font = [UIFont fontWithName:@"Verdana" size:18];
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(200, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName : font}
                                     context:nil];
    
    return ceil(rect.size.height / font.lineHeight);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section==0){
        
//        activityIndicatorNew = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        activityIndicatorNew.frame = CGRectMake(tableView.bounds.size.width/2, kTableViewHeaderHeight, 37, 37);
//        [activityIndicatorNew startAnimating];
//        [self.view addSubview:activityIndicatorNew];
        
        
        
        tblHeaderImage = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, kTableViewHeaderHeight)];
        [tblHeaderImage setBackgroundColor:[UIColor clearColor]];
        
        
        NSString *headerImgKey=[NSString stringWithFormat:@"header%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        NSString *headerImgName=[[NSUserDefaults standardUserDefaults]objectForKey:headerImgKey];
        
        
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:@"Header"];
        
        
        
        tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:headerImgName];
        
        NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
        bannerImgURL=[userDefaults objectForKey:@"bannerImgURL"];
        
        tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:headerImgName];
        
        
        [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:bannerImgURL] placeholderImage:nil options:SDWebImageRefreshCached];
        
       
        
        
        tblHeaderImage.imageURL = [NSURL URLWithString:bannerImgURL];
                                    
        if(bannerImgURL!=nil){
        
            [self loadImageFromURL1:bannerImgURL image:tblHeaderImage];
            
           // [self loadImageFromURL1:bannerImgURL image:tblHeaderImage];
            
           // [self performSelector:@selector(removeActivity) withObject:nil afterDelay:0.6];
            
          // [activityIndicatorNew setHidesWhenStopped:YES];
            
        }
        
        
       
        
       /* if (!tblHeaderImage.image) {
            
            
            
            
            if([self reachable])
            {
                
                tblHeaderImage.imageURL=[NSURL URLWithString:bannerImgURL];
            }
            else
            {
                
                tblHeaderImage.imageURL=[NSURL URLWithString:bannerImgURL];
            }
            
        }*/
            
       // [self loadImageFromURL1:[dict objectForKey:@"image"] image:cell.appImage];
        return tblHeaderImage;
    }
    else{
        return nil;
        
    }
   
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *stringNearby;
    
   stringNearby = [[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"];
    
   // if([stringNearby isEqualToString:@"on"])
    {
    
    NSInteger x=0;
     x =  [[NSUserDefaults standardUserDefaults]integerForKey:@"numberofAppsToShow"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSObject * object = [prefs objectForKey:@"numberofAppsToShow"];
    
    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"nearbyisOFF"];
    
//    if(section==0){
//        
//
//        return 0;
//    }
//    else
    {
        
        if(object != nil){
            //object is there
            if(x+1>[mainArray count])
            {return [mainArray count];}
            else
             return x+1;// x+1 "+1 is because first guide is hidden"
        }
        else
        {
           
            if([str isEqualToString:@"yes"])
            {
                [mainArray count];
            }
            else
            return 11;//[mainArray count]; //make 10 if nsuserdefaults is nil.
        }
        
    }
    if(object==nil)
    {
        return 11;
    }
    else
    return [mainArray count];
        
    }
//    else
//    {
//        return [mainArray count];
//    }
}
-(void)reloadTableView:(NSNotification *) notification
{
    if([mainArray count]==0)
    {
        return;
    }
 else
 {
    [tblView reloadData];
     tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
 }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicttest = [mainArray objectAtIndex:indexPath.row];
    NSString *sttname = [dicttest objectForKey:@"app_name"];
    
    /// please note this is to be fixed....when nearby guide is NO
    
    if(indexPath.row==0&&[sttname isEqualToString:@"LocalsGuide"])
    

    // if(indexPath.row==0)
     {
         return 0;
     }
    else
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
   
    
    PDInappCell *cell = (PDInappCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDInappCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
        
       
    }
    
    tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    NSDictionary *dicttest = [mainArray objectAtIndex:indexPath.row];
    NSString *sttname = [dicttest objectForKey:@"app_name"];
    
   
    
    if(indexPath.row==0&&[sttname isEqualToString:@"LocalsGuide"])
    {
         //if([classIds isEqualToString:@""])
        cell.hidden=YES;
        
    }
    else if(indexPath.row==0)
    {
    
    }
    
//    if ([cell.contentView subviews]){
//        for (UIView *subview in [cell.contentView subviews]) {
//            [subview removeFromSuperview];
//        }
//    }
    
    cell.clipsToBounds = YES;
    
    if([mainArray count]==0)
    {
       
        tableView.hidden=YES;
         return cell;
    }

    NSLog(@"indexpath.row :%ld",(long)indexPath.row);
    
    NSDictionary *dict = [mainArray objectAtIndex:indexPath.row];
    
   
    
   // [cell.appImage setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholder:[UIImage imageNamed:@"placeholder_new.png"]];
    
     NSString *inte = [self extractNumberFromText:[dict objectForKey:@"image"]];
    
    [cell.appImage setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] key:inte placeholder:[UIImage imageNamed:@"placeholder_new.png"]];
    
   

  
    
    
    
    cell.appNameLbl.text = [dict objectForKey:@"app_name"];
    
   // cell.appNameLbl.backgroundColor=[UIColor redColor];
    
    
    cell.appNameLbl.font=[UIFont fontWithName:@"Verdana" size:18];//boldSystemFontOfSize:13];
    //[titlelabelh setFont:[UIFont fontWithName:@"Verdana" size:18]]
    //cell.appNameLbl.backgroundColor=[UIColor redColor];
    
    NSInteger i=0;
    
   i = [self lineCountForText:cell.appNameLbl.text];
    
    NSLog(@"NOL :%ld",(long)i);
    
    if(i >1)
    {
        CGRect frame = CGRectZero;
        frame=cell.distLbl.frame;
        frame.origin.y=frame.origin.y +8;
        cell.distLbl.frame=frame;
    }

    
    cell.distLbl.text=@"";
    NSString *ss;
    
    double dis=[[dict objectForKey:@"distance"] doubleValue];
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    NSString* formattedNumber= [fmt stringFromNumber:[NSNumber numberWithFloat:dis]];
    
   
    if ([distUnit isEqualToString:@"M"])
       ss=[NSString stringWithFormat:@"%@ mi",formattedNumber];
    else
        ss=[NSString stringWithFormat:@"%@ km",formattedNumber];
        
    if (![ss isEqualToString:@"(null)"] || ![ss isEqualToString:@"0 mi"] || [ss isEqualToString:@"0 km"])
    {
       // double distt=[ss doubleValue];
        
        
        //Ram
        //cell.distLbl.backgroundColor=[UIColor redColor];
        cell.distLbl.text=ss;
       
        
    }
    
    
    
    
    
    
    NSString *hide_dist = [settingDict valueForKey:@"hide_distance"];
    
    
    //Code change by ram for hiding the distance if  "location_aware_guide" = no;
    if ([[settingDict valueForKey:@"location_aware_guide"] isEqualToString:@"no"]) {
        if ([[DBOperations getMainAppid] isEqualToString:[dict objectForKey:@"id"]]) {
            [cell.distLbl setHidden:YES];
        }
    }
    
    
    UILabel *lineLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 59, self.view.frame.size.width, 0.5)];
    lineLbl.backgroundColor=[UIColor colorWithRed:200/255.0f green:200/255.0f blue:200/255.0f alpha:1];
    [cell addSubview:lineLbl];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    
    if([hide_dist isEqualToString:@"no"])
    {
        [cell.distLbl setHidden:NO];
    }
    else if([hide_dist isEqualToString:@"yes"])
    {
    if ([[DBOperations getMainAppid] isEqualToString:[dict objectForKey:@"id"]])
        {
          [cell.distLbl setHidden:YES];
        }
        
    }

    
    if (![self reachable]) {
        cell.distLbl.text=@"";
    }

    
    // If you want to change the images presented
    ACPStaticImagesAlternative * myOwnImages = [ACPStaticImagesAlternative new];
    [myOwnImages updateColor:[UIColor grayColor]];
    [cell.exampleIndicator setImages:myOwnImages];
    
    //Status by default.
    //[cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusNone];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
   // [cell.exampleIndicator setActionForTap:^(ACPDownloadView *downloadView, ACPDownloadStatus status)
    {
        switch (cell.exampleIndicator.currentStatus) {
            case ACPDownloadStatusNone:
                [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusNone];
                break;
            case ACPDownloadStatusRunning:
                [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusRunning];
                break;
            case ACPDownloadStatusIndeterminate:
                [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusIndeterminate];
                break;
            case ACPDownloadStatusCompleted:
                [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusCompleted];
                break;
                
            default:
                break;
        }
    }
    
    //];

    //////
    
    PDInappCell * cellSelected = (PDInappCell*)[tableView cellForRowAtIndexPath:indexPath];

    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
            //        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            //        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            //        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            //        NSFileManager * fileManager = [NSFileManager defaultManager];
            //        myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
            }
        }}
    
    if ([myMutableArrayAgain count] == 0) {
        isDownloaded = NO;
    }
    for (NSDictionary *dic in myMutableArrayAgain) {
        
        
            if ([[dict objectForKey:@"app_name"] isEqualToString:[dic objectForKey:@"app_name"]])
            {
               
                isDownloaded = YES;
                
                
                
                break;
            }
            else {
                isDownloaded = NO;
                
            }
        
    }
    
    
    
    if (isDownloaded) {
        
       
//        m_testView.percent = 100;
//        m_testView.backgroundColor=[UIColor clearColor];
//        //[m_testView sizeToFit];
//        [cell addSubview:m_testView];
        
        [cell.exampleIndicator setIndicatorStatus:ACPDownloadStatusCompleted];
        
        isDownloaded = YES;
       
    }
    else {
        
        
        
        isDownloaded = NO;
        
    }
    
    //////
    
    NSLog(@"cell exampleIndicator frame :%@",NSStringFromCGRect(cell.exampleIndicator.frame));
    
    NSLog(@"myMutableArrayAgain :%@",myMutableArrayAgain);
    
    
   
    
    //exampleIndicator
    
    
//    cell.exampleIndicator.layer.zPosition = 0;
//    
//    [cell.window sendSubviewToBack:cell.exampleIndicator];
//    
//    [self.view bringSubviewToFront:cell.contentView];
    
    
    cell.exampleIndicator.userInteractionEnabled=NO;
    
    cell.btnOverAppLabel.tag = indexPath.row;
    
    [cell.btnOverAppLabel addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSInteger lablWidth = cell.appNameLbl.intrinsicContentSize.width;
    NSLog(@"titlelabelh width :%ld",(long)lablWidth);
     if(lablWidth>200)
     {
     CGRect widthTrunc;
     widthTrunc=CGRectZero;
     widthTrunc=cell.appNameLbl.frame;
     widthTrunc.size.width=200;
     cell.appNameLbl.frame=widthTrunc;
     }

    
    return cell;
}

-(void)yourButtonClicked:(UIButton*)sender
{
    if (sender.tag == 0)
    {
        // Your code here
    }
    
    {
        
        
        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"DataDownloaded"];
        
        [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:sender.tag]objectForKey:@"id"] forKey:@"currentAppId"];
        [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:sender.tag]objectForKey:@"app_name"] forKey:@"currentAppName"];
        [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
        selectedCell=sender.tag;
        
        indexe = sender.tag;
        
        NSLog(@"indexe : %ld",(long)indexe);
        
        //  [self loadSplashImage];
        
        [self makeViewShine:activityIndicator1];
        
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = activityIndicator1.bounds;
        
        
        
        
        if([self isTheAppHasOflineData:[[mainArray objectAtIndex:selectedCell]objectForKey:@"id"]])
        {
            if([[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"%@",[[mainArray objectAtIndex:selectedCell]objectForKey:@"app_name"]]])
            {
                //[self loadSplashImageTest];
                
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"isthere" forKey:[NSString stringWithFormat:@"%@",[[mainArray objectAtIndex:selectedCell]objectForKey:@"app_name"]]];
                
                //  [[NSNotificationCenter defaultCenter] postNotificationName:@"headerTest" object:self];
                
                //[self loadSplashImageTest];
            }
            
            
            
            
        }
       // else
            //[self loadSplashImageTest];
        
        
        
        
        [self startLoadingApp];
        
        
        activityIndicator1.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
        
        
        
        tblView.userInteractionEnabled = NO;
        
        
    }
}
- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage {
    UIImage *image = nil;
    
    CGSize newImageSize = CGSizeMake(MAX(firstImage.size.width, secondImage.size.width), MAX(firstImage.size.height, secondImage.size.height));
    if (&UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(newImageSize);
    }
    [firstImage drawAtPoint:CGPointMake(roundf((newImageSize.width-firstImage.size.width)/2),
                                        roundf((newImageSize.height-firstImage.size.height)/2))];
    [secondImage drawAtPoint:CGPointMake(roundf((newImageSize.width-secondImage.size.width)/2),
                                         roundf((newImageSize.height-secondImage.size.height)/2))];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

#pragma mark - UITableView delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dicttest = [mainArray objectAtIndex:indexPath.row];
    NSString *sttname = [dicttest objectForKey:@"app_name"];
    
    /// please note this is to be fixed....when nearby guide is NO
    
    if(indexPath.row==0&&[sttname isEqualToString:@"LocalsGuide"])
        
 // if(indexPath.row==0)
  {
      [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
      [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"app_name"] forKey:@"currentAppName"];
      [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
      selectedCell=indexPath.row;
      
      indexe = indexPath.row;

      
      [self startLoadingApp];
      return;
  }
    
    ////////////////////////// For loading process /////////////////////////////////
    
    PDInappCell * cellSelected = (PDInappCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    ACPIndeterminateGoogleLayer * layer = [ACPIndeterminateGoogleLayer new];
    [layer updateColor:[UIColor grayColor]];
    [self.downloadView setIndeterminateLayer:layer];
    
    {
        
    [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
    [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"app_name"] forKey:@"currentAppName"];
    [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
    selectedCell=indexPath.row;
    
    indexe = indexPath.row;

        [[NSUserDefaults standardUserDefaults]setInteger:indexe forKey:@"forGettingIndexPath"];
        
        [addAppId addObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"id"]];
       
        
        
        NSLog(@"addAppId :%@",addAppId);
        
    }
    
    
    NSString *offlineGuideStr = [[mainArray objectAtIndex:indexPath.row]objectForKey:@"offline_guide"];
    
    if([offlineGuideStr isEqualToString:@""]||[offlineGuideStr isEqualToString:@"no"])
        
        {
            [self startLoadingApp];
            return;
        }
    
  //   m_timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(decrementSpin) userInfo:nil repeats:YES];
    
     NSString*string = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
    
    if([string isEqualToString:@"skipIntermediate"])
    {
       [cellSelected.exampleIndicator setIndicatorStatus:ACPDownloadStatusCompleted];
    }
    
    switch (cellSelected.exampleIndicator.currentStatus)
    {
        case ACPDownloadStatusNone:
        
        {
            
            PDIntermediatePageViewController *interPage = [[PDIntermediatePageViewController alloc]init];
            [[self navigationController] pushViewController:interPage animated:YES];
            
            return;
            
           /* if([self reachable])
                [self test];
            
            if([self reachable])
            {
                NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                
                NSLog(@"ida :%@",ida);
                [[NSUserDefaults standardUserDefaults]setObject:@"noo" forKey:ida];
                
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"DataDownloaded"];
            
                 [self downloadSplashImage];
                
               
                
            [self  startSettingsWebServiceOfCurrentApp];
                
                //[self downloadZip];
//                // list contents of Documents Directory just to check
//                NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
//                
//                NSArray *contents = [[NSFileManager defaultManager]contentsOfDirectoryAtURL:documentsURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
//                
//                NSLog(@"description for zip:%@", [contents description]);
                
                
                [[NSUserDefaults standardUserDefaults]setInteger:indexe forKey:@"forIndexinHeaderArray"];
        
                [self insertHeaderImg];
                
                [cellSelected.exampleIndicator setIndicatorStatus:ACPDownloadStatusIndeterminate];
                
                m_testView = [[UICircle alloc] initWithFrame:CGRectMake(285, 23, 28, 28)];
                m_testView.percent = 0;
                m_testView.backgroundColor=[UIColor clearColor];
                //[m_testView sizeToFit];
                [cellSelected addSubview:m_testView];
                
                tableView.userInteractionEnabled=NO;
                tableView.scrollEnabled=NO;
                settingsBtn.enabled=NO;
                
 
                        
                
            
            
           // [self  startSettingsWebServiceOfCurrentApp];
            
            }
            return;*/
            
            
        }
            break;
        case ACPDownloadStatusRunning:
            //[cellSelected.exampleIndicator setIndicatorStatus:ACPDownloadStatusCompleted];
        
            return;
            break;
        case ACPDownloadStatusIndeterminate:
            //[cellSelected.exampleIndicator setIndicatorStatus:ACPDownloadStatusRunning];
           // [cellSelected.exampleIndicator setProgress:self.progress animated:YES];
            return;
            break;
        case ACPDownloadStatusCompleted:
            //[cellSelected.exampleIndicator setIndicatorStatus:ACPDownloadStatusNone];
        {
            
            
            [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"DataDownloaded"];
            
            [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
            [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"app_name"] forKey:@"currentAppName"];
            [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
            selectedCell=indexPath.row;
            
            indexe = indexPath.row;
            
            NSLog(@"indexe : %ld",(long)indexe);
            
            //  [self loadSplashImage];
            
            [self makeViewShine:activityIndicator1];
            
            UIVisualEffect *blurEffect;
            blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
            
            UIVisualEffectView *visualEffectView;
            visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
            
            visualEffectView.frame = activityIndicator1.bounds;
            
            
            
            
            if([self isTheAppHasOflineData:[[mainArray objectAtIndex:selectedCell]objectForKey:@"id"]])
            {
                if([[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"%@",[[mainArray objectAtIndex:selectedCell]objectForKey:@"app_name"]]])
                {
                   [self loadSplashImageTest];
                    
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults]setObject:@"isthere" forKey:[NSString stringWithFormat:@"%@",[[mainArray objectAtIndex:selectedCell]objectForKey:@"app_name"]]];
                    
                  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"headerTest" object:self];
                    
                    [self loadSplashImageTest];
                }
                
                
                
                
            }
            else
                [self loadSplashImageTest];
            
           
           
                
                [self startLoadingApp];
                
                
            activityIndicator1.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
            
            
            
            tableView.userInteractionEnabled = NO;

        
        }
            return;
            break;
            
        default:
            break;
            
            
    }
    

    
    ///////////////////////////////////////////////////////////
   [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
    [[NSUserDefaults standardUserDefaults]setObject:[[mainArray objectAtIndex:indexPath.row]objectForKey:@"app_name"] forKey:@"currentAppName"];
    [ [NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
    selectedCell=indexPath.row;
    
    indexe = indexPath.row;
    
    NSLog(@"indexe : %ld",(long)indexe);
    
  //  [self loadSplashImage];
    
     [self makeViewShine:activityIndicator1];
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = activityIndicator1.bounds;
   
    
    if([self isTheAppHasOflineData:[[mainArray objectAtIndex:selectedCell]objectForKey:@"id"]])
    {
        [self startLoadingApp];
    }
    else
        [self loadSplashImageTest];
    
    
    activityIndicator1.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
    
    
   
      tableView.userInteractionEnabled = NO;
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
-(void)readListOfSplashImagesToDownload
{
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:readContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        @try {
            
            
            for(IMAGES_TO_CACHE *object in results)
            {
                
                NSString *imageUrl=object.imageurl;
                
                NSURL *urlimg=[NSURL URLWithString:imageUrl];
                
                UIImageView*imagevw=[[UIImageView alloc]init];
                imagevw.image=[imageCache imageFromDiskCacheForKey:imageUrl];
                
                
                if (!imagevw.image)
                {
                    
                    
                    
                    NSData *data = [NSData dataWithContentsOfURL:urlimg];
                    imagevw.image = [UIImage imageWithData:data];
                    [imageCache storeImage:imagevw.image forKey:imageUrl completion:^{  }];
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
                    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                    NSEntityDescription *entity = [NSEntityDescription
                                                   entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:context1];
                    [fetchRequest setEntity:entity];
                    
                    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"imageurl=%@",imageUrl];
                    NSError *error;
                    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
                    IMAGES_TO_CACHE *currentQUE;
                    for(currentQUE in listOfQUEToBeDeleted)
                    {
                        
                        [context1 deleteObject:currentQUE];
                        
                        
                        
                    }
                    
                    if (![context1 save:&error]) {
                        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                    }
                }
                
            }
            
            
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
            
        }
        @catch (NSException * e)
        
        {
            
            NSLog(@"Exception: %@", e);
            
            
        }
        
    });
    
    
    
    
    
    
}



-(void)downloadSplashImage
{
  NSArray *dict =[[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];

    if ([[dict objectAtIndex:indexe]objectForKey:@"splash_image"]) {
        if (![[[dict objectAtIndex:indexe]objectForKey:@"splash_image"] isEqualToString:@""]) {
           
            NSString*str=[[dict objectAtIndex:indexe]objectForKey:@"splash_image"];
            
            NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            SDImageCache *imageCache =[SDImageCache sharedImageCache];
            imageCache = [imageCache initWithNamespace:imageCacheFolder];
            
            
            splashImgv.image=[imageCache imageFromDiskCacheForKey:str];
            
           
            if(str.length>0)
            {
                
                //[self insertSplashImagesToDownload:str:^{
                 //   NSLog(@"It finished");
               // }];
                // [self sdImageCaching:str];
                [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",str], nil]];
                
                
               // [self readListOfSplashImagesToDownload];
                
              //  [self insertSplashImagesToDownload:str];
                
                
                
                NSLog(@"splashImg :%@",splashImgv.image);
            }
            

        }
    }
}

-(void)insertSplashImagesToDownload:(NSString*)imgstr //:(void (^)(void))completion;
{
    
   /* UIImageView*img=[[UIImageView alloc]init];
    
    [self loadImageFromURL1:imgstr image:img];
    
    UIImage *imageFromImageView = img.image;
    
    UIImage *screw =  [self imageByScalingImage:imageFromImageView];
    
    
    
    NSData *imageData = UIImagePNGRepresentation(screw);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",@"cached"]];
    
    NSLog(@"pre writing to file");
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        NSLog(@"Failed to cache image data to disk");
    }
    else
    {
        NSLog(@"the cachedImagedPath is %@",imagePath);
    }
    
    splashImgv.image=screw;
    
    
   */
    
    
    
    
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            
            
            {
                
                
               
                
               
               
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    
    @try{
        
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *Que = [NSEntityDescription
                                insertNewObjectForEntityForName:@"IMAGES_TO_CACHE"
                                inManagedObjectContext:context];
        
        
        
        
        [Que setValue:imgstr forKey:@"imageurl"];
        [Que setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"] forKey:@"appid"];
        
        
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Whoops, couldn't download image"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
             //[alert show];
            
            
            
        }
        else{
            
            NSLog(@"SPLASH IMAGE DOWNLOADED ");
            //            [self performSelectorInBackground:@selector(updateProgress:) withObject:[NSNumber numberWithFloat:i]];
            // [self stopCircleLoader4];
        }
        
        
    }
    @catch (NSException * e)
    {
        NSLog(@"Exception: %@", e);
        
        
    }
   
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [self loadImageFromURL1:imgstr image:splashImgv];
                
                
//                if (completion) {
//                    completion();
//                }
                
                
                
                
            });
            
        });
    

    
}

-(void)insertHeaderImg
{
    NSString*str=[[NSUserDefaults standardUserDefaults]objectForKey:@"app_home_header_img"];
    
  /*  NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    
    splashImgv.image=[imageCache imageFromDiskCacheForKey:str];*/
    
    NSString *headerImgKey=[NSString stringWithFormat:@"header%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *headerImgName=[[NSUserDefaults standardUserDefaults]objectForKey:headerImgKey];
    
    
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:@"Header"];
    
    
    
//    tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:headerImgName];
    
[[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",str], nil]];
    
    NSLog(@"header url :%@",str);
    [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"forTableHeaderImageUrl"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"forTableHeaderImage" object:self];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"headerTest" object:self];
    
    if(str.length>0)
    {
        
//        [self insertSplashImagesToDownload:str:^{
//            NSLog(@"It finished");
//        }];
        
       // [self insertSplashImagesToDownload:str];
        // [self sdImageCaching:str];
        
       
        
//        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//        dispatch_async(queue, ^{
//            
//            
//            {
        
//                [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//                NSURL *imgURL = [NSURL URLWithString:str];
//                [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:imgURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//                    if (!connectionError) {
//                        UIImage *img = [[UIImage alloc] initWithData:data];
//                        // pass the img to your imageview
//                        [mutAry addObject:img];
//                        
//                        NSLog(@"mutAry :%@",mutAry);
//                        
//                    }else{
//                        NSLog(@"%@",connectionError);
//                    }
//                }];
                
//            }
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                
//                   // [self loadImageFromURL1:str image:tblHeaderImage];
//                    
//            });
//            
//        });
        

    }
    
    

    
}
-(UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}


- (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}

#pragma mark - Webservice for settings

/*This functions request for settings from server of the selected app*/
-(void)startSettingsWebServiceOfCurrentApp
{

    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(settingsServiceResponseOfCurrentApp:);
    isSingleApp=[DBOperations isSingleApp];
    NSString *str;
    if (isSingleApp) {
        str=[NSString stringWithFormat:@"%@%@&multi=no",SETTINGS_WEBSERVICE_SUB,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    }
    else{
        NSString *ismulti=@"yes";
        /*if ([[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"])
             ismulti=  [[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"];*/
        
        str=[NSString stringWithFormat:@"%@%@&multi=%@",SETTINGS_WEBSERVICE_SUB,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"],ismulti];
    }
    [webService startParsing:str];
}
/*Webservice response method of selected app settings */
-(void)settingsServiceResponseOfCurrentApp:(NSData *) responseData
{
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSArray *settingsArray=[dict objectForKey:@"settings"];
    NSDictionary *inappDict = [dict objectForKey:@"inap_settings"];
   
    if([settingsArray count]>0)
        
    {
        NSDictionary *settingsDict=[settingsArray objectAtIndex:0];
        
        [self saveCurrentAppSettings:settingsDict];
        [self saveCurrentInappSetting:inappDict];
        
        
        //Ram core data write restric
        [self coreDataWriteAppsSettings:settingsDict forInappDic:inappDict];
        
        if([self reachable])
            [self footerValuesWebService];
        else
            [self coreDataReadFooterList];

    }
    
    
}
/*This function save selected app details to nsuserdefault for accessing the Inapp settings while control is inside the selected app. This storage is for only accessing it from anywhere of the app. When a new app is selected this replaces the data with Inapp settings data of new app*/
-(void)saveCurrentInappSetting:(NSDictionary*)inappDict
{
  [[NSUserDefaults standardUserDefaults]setObject:inappDict forKey:@"inappSettings"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSDictionary *dictionary=[inappStr objectAtIndex:0];
    [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
    [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
    
    
    
}
/*This function save selected app details to nsuserdefault for accessing the settings while control is inside the selected app. This storage is for only accessing it from anywhere of the app. When a new app is selected this replaces the data with settings data of new app*/
-(void)saveCurrentAppSettings:(NSDictionary*)dict
{

    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"app_home_header_img"] forKey:@"app_home_header_img"];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"app_title"] forKey:@"AppTitle"];
    
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
    
    
    [tempDict setValue:[dict objectForKey:@"ad_1"] forKey:@"ad_1"];
    [tempDict setValue:[dict objectForKey:@"ad_2"] forKey:@"ad_2"];
    [tempDict setValue:[dict objectForKey:@"ad_3"] forKey:@"ad_3"];
    
    [tempDict setValue:[dict objectForKey:@"link_1"] forKey:@"link_1"];
    [tempDict setValue:[dict objectForKey:@"link_2"] forKey:@"link_2"];
    [tempDict setValue:[dict objectForKey:@"link_3"] forKey:@"link_3"];
    
   [[NSUserDefaults standardUserDefaults] setObject:tempDict forKey:@"CustomAdDict"];
  
    
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"admob_status"] forKey:@"admob_status"];
    if ([[dict objectForKey:@"admob_status"] isEqualToString:@"Yes"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"CustomAd"];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"admob_id"] forKey:@"admob_id"];
    }
    else
    {
         [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"CustomAd"];
    }
    
    if ([[dict objectForKey:@"distance_in"] isEqualToString:@"km"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"KM"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"KM"];
    }
    if ([[dict objectForKey:@"app_view"] isEqualToString:@"grid"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"VIEW_GRID"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"VIEW_GRID"];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"]) {
           }
    if([[dict objectForKey:@"offline_guide"] isEqualToString:@"yes"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"offline_guide"];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"refresh_interval"] forKey:@"refresh_interval"];
    }
    else
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"offline_guide"];
    
    
    if([[dict objectForKey:@"fav_icon"] isEqualToString:@"heart"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"heart" forKey:@"fav_icon"];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"star" forKey:@"fav_icon"];
    }

    
}

/*This writes selected app settings to coredata for offline use*/
-(void)coreDataWriteAppsSettings:(NSDictionary*)dict forInappDic:(NSDictionary*)inappDict{
 
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
   
    
    [fetchRequest setPredicate:predicate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];

      NSError *error;
    NSArray *results = [context1 executeFetchRequest:fetchRequest error:&error];
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:dict forKey:@"settings"];
    [favoritsGrabbed setValue:inappDict forKey:@"inappsettings"];
    
    
   
  //  NSError *error;
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else{
      //  [self coreDataReadAppsSettings];
        
    }

    
}

/*This read App settings of selected app if online data is not available*/
-(void)coreDataReadAppsSettings{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
   
    
    
    [fetchRequest setPredicate:predicate];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];

    
    
    @try {
        Apps *appsObj=[fetchedObjects objectAtIndex:0];
        NSMutableDictionary *dicObject=[[NSMutableDictionary alloc]init];
        [dicObject setObject:appsObj.settings forKey:@"settings"];
        [dicObject setObject:appsObj.inappsettings forKey:@"inappsettings"];
        
        NSDictionary *setObjDict=[dicObject objectForKey:@"settings"];
        NSDictionary *inappSetObjDict=[dicObject objectForKey:@"inappsettings"];
        
        arrFooters  = [[NSArray alloc]init];
        [refreshControl endRefreshing];
        [self saveCurrentAppSettings:setObjDict];
        
        [self saveCurrentInappSetting:inappSetObjDict];
 
       
        if([self isTheAppHasOflineData:[[mainArray objectAtIndex:selectedCell]objectForKey:@"id"]])
        {
            if([self reachable])
                [self footerValuesWebService];
            else
           [self coreDataReadFooterList];
        }
       else if([self reachable])
            [self footerValuesWebService];
        else
            [self coreDataReadFooterList];
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Could not connect to server"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alert show];
        [self  startSettingsWebServiceOfCurrentApp];
        
    }
    
    
    
    
    
}

-(void)refreshFooterFromVC:(NSNotification *) notification
{
   // [self footerValuesWebService];
    
}
#pragma mark - Webservice and methods for Footer
/*This function request for tabbar value details from server for selected the app*/
-(void)footerValuesWebService
{
  

    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(footerValuesWebServiceResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@",FOOTER_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}

/*Webservice response function for tabbar details*/
-(void)footerValuesWebServiceResponse:(NSData *) responseData
{
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    arrFooters = [dict objectForKey:@"footer"];
    
    if ([arrFooters isKindOfClass:[NSNull class]])
    {
        UILabel *errorLbl = [[UILabel alloc]initWithFrame:self.view.frame];
        errorLbl.textAlignment = NSTextAlignmentCenter;
        errorLbl.textColor=[UIColor blackColor];
        errorLbl.font=[UIFont systemFontOfSize:14];
        errorLbl.text=@"Null value is being returned. Please check webservice!";
        errorLbl.numberOfLines=2;
        //[self.view addSubview:errorLbl];
            return;
    }
    
    
    [[NSUserDefaults standardUserDefaults] setObject:arrFooters forKey:@"footerListArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //Ram Core data write restrict
    if(![self isTheAppHasOflineData:[[mainArray objectAtIndex:selectedCell]objectForKey:@"id"]])
       
    {
        [self coreDataWritefooterValues:dict];
    }
    [self setFooterWithArray:arrFooters];
    

}

/*This function write tabbar values to coredata for offline use*/
-(void)coreDataWritefooterValues:(NSDictionary*)dict{
   
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
   
    
    
    [fetchRequest setPredicate:predicate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
 
    NSError *error;
    NSArray *results = [context1 executeFetchRequest:fetchRequest error:&error];
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:dict forKey:@"footerList"];
    
    
    //  NSError *error;
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else{
       // [self coreDataReadFooterList];
        
    }

    
}


/*This read details of tabview details from coredata if online data is not available*/
-(void)coreDataReadFooterList{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    [fetchRequest setPredicate:predicate];
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
  
    
    
    
      arrFooters  = [[NSArray alloc]init];
    @try {
        Apps *appsObj=[fetchedObjects objectAtIndex:0];
        NSMutableDictionary *dicObject=[[NSMutableDictionary alloc]init];
        
        NSString *identifier = appsObj.footerList;
        
        if(identifier !=nil)
            [dicObject setObject:appsObj.footerList forKey:@"footerList"];
        else
            [dicObject setObject:@"" forKey:@"footerList"];
        
        
        arrFooters=[[dicObject objectForKey:@"footerList"]objectForKey:@"footer"];
        [self setFooterWithArray:arrFooters];
        
    }
    @catch (NSException * e) {
        
        NSLog(@"===%@",e);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Could not connect to server"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alert show];
        
        //[self startSettingsWebServiceOfCurrentApp];
        
        [self footerValuesWebService];
    }
    
    
    
    
    
    
    [refreshControl endRefreshing];
   
    
    
}

/*This function load footer values*/
-(void)loadFooterValues
{
    
   
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FOOTER_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]]]
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                  
                                   
                                   NSError *er;
                                   NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&er];
                                   arrFooters = [dict objectForKey:@"footer"];
                                   [[NSUserDefaults standardUserDefaults] setObject:arrFooters forKey:@"footerListArray"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                   
                                   
                                   [self performSelector:@selector(setFooterWithArray:) withObject:arrFooters afterDelay:0.0];
                                                                    NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
                                   if (![myMutableArrayAgain isEqualToArray:arrFooters]) {
                                       
                                       
                                      [self setFooterWithArray:arrFooters];
                                   }
                                   
                               }
                               else {
                                   
                                   
                                   NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
                                   [self setFooterWithArray:myMutableArrayAgain];
                                   
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Couldnot fetch values from server. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                   [alert show];
                               }
                               
                                                          }];
}


/*This function set appropriate tab to tabviewcontroller*/
-(void)setFooterWithArray:(NSArray *) arrayFooter {
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"order"  ascending:YES];
    NSArray *arrtemp3 = [arrayFooter sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    NSArray *arrVals = [arrtemp3 copy];
    int selectedIndex;
    NSMutableArray *arrayFooters = [[NSMutableArray alloc] init];
    for (int i = 0; i < [arrVals count]; i++) {
        NSDictionary *dict1 = [arrVals objectAtIndex:i];
        if ([[dict1 objectForKey:@"name"] isEqualToString:@"Home"]) {
            selectedIndex = i;
        }
        UIViewController *viewController = [self checkAndReturnClass:dict1];
               UINavigationController *navig1 = [[UINavigationController alloc] initWithRootViewController:viewController];
        [arrayFooters addObject:navig1];
    }

    
    
    NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
    
    tabbar = [[UITabBarController alloc] init];
    if (IS_OS_7_OR_LATER) {
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Verdana" size:10.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
        //[[UITabBar appearance] setBarTintColor:[UIColor blackColor]];
       // [[UITabBar appearance] setTintColor:[UIColor colorWithRed:21/255.0f green:107/255.0f blue:0/255.0f alpha:1.0]];
        
       // [[UITabBar appearance] setTintColor:[UIColor yellowColor]];//223, 223, 32
        
        //251, 173, 24
        
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:251/255.0f green:173/255.0f blue:24/255.0f alpha:1.0]];
        [[UITabBar appearance] setTranslucent:YES];
    }
    else {
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Verdana" size:10.0f], UITextAttributeFont, nil] forState:UIControlStateNormal];
    }
    
    
    
    
    [tabbar setViewControllers:arrayFooters];
    tabbar.selectedIndex = selectedIndex;
    tabbar.delegate = self;
    

    
    

    //loading values
    
    
    DBOperations *dbObject=[[DBOperations alloc]init];
    [dbObject setDelegate:self];
    [dbObject startOfflineDataLoading];

}
- (void) processCompleted:resultArray
{
    
    NSString*string=[[NSUserDefaults standardUserDefaults]objectForKey:@"app_home_header_img"];
    
    UIImageView *image = [[UIImageView alloc]init];
    
   // [self loadImageFromURL1:string image:image];
    
    NSLog(@"headerImage.image :%@",image.image);
    
    
    
    NSLog(@"result array=%@",resultArray);
    if([resultArray count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Could not connect to server"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       // [alert show];
    }
    
//    [[NSUserDefaults standardUserDefaults] setObject:resultArray forKey:@"offlineresultArray"];
//    [[NSUserDefaults standardUserDefaults] synchronize];

    NSString *str =   [[NSUserDefaults standardUserDefaults]objectForKey:@"DataDownloaded"];
    
    if([str isEqualToString:@"yes"])
        [[self navigationController] pushViewController:tabbar animated:YES];
    
    
    [homeVC fillDataArray:resultArray];
    // [vwLoading removeFromSuperview];
    if(![self.navigationController.topViewController isKindOfClass:[tabbar class]]) {
        if(yThis)
        {
            
           [[NSUserDefaults standardUserDefaults]setBool:yThis forKey:@"yThis"];
           [[self navigationController] pushViewController:tabbar animated:NO];
            
            [vwLoading removeFromSuperview];
            yThis=false;
            return;
            
        }
        
    
       
        //[self.navigationController pushViewController:homeVC animated:NO];
        
        
    }
    
    
    //[self.view addSubview:vwLoading];
    
    
    
 /*if([self reachable])
  {
     if (!splashImgv.image)
     {
           {
             [[UIApplication sharedApplication].keyWindow addSubview:vwLoading];
             
             
             int64_t delayInSeconds = 5.0;
             dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
             dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 
                 // if (homeVC.isViewLoaded && homeVC.view.window)
                 {
                     
                     [vwLoading removeFromSuperview];
                     
                     
                 }
                 // [vwLoading removeFromSuperview];
                 
             });
             
             
         }
         
         
     }
        else
            {
                [[UIApplication sharedApplication].keyWindow addSubview:vwLoading];
                
                NSLog(@"two seconds in delay");
                
                
                
                int64_t delayInSeconds = 3.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    
                    // if (homeVC.isViewLoaded && homeVC.view.window)
                    {
                        
                        [vwLoading removeFromSuperview];
                        
                        
                    }
                    // [vwLoading removeFromSuperview];
                    
                });
                
                
            }
      
    }*/
    
    
}

-(void)removeLoaderView
{
   [vwLoading removeFromSuperview];
}
/*This is to return appropriate class as per webservice request, The returned viewcontroller from this class is used to set in each tab of tabviewcontroller*/
-(UIViewController *)checkAndReturnClass:(NSDictionary *) dictionary
{
    NSString *value = [dictionary objectForKey:@"content_type"];
    if ([value isEqualToString:@"Favorites"]) {
        PDFavoriteViewController *viewController = [[PDFavoriteViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
  
        UIImageView*img=[[UIImageView alloc]init];
        [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
        
        UIImage *imageFromImageView = img.image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
       // subCategoryViewController.tabBarItem.image = screw;
        
        viewController.tabBarItem.image = screw;//img.image;
        
        
        return viewController;
    }
    if ([value isEqualToString:@"Map"]){
        PDMapViewController *viewController = [[PDMapViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];

        UIImageView*img=[[UIImageView alloc]init];
        [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
        
        UIImage *imageFromImageView = img.image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
       // subCategoryViewController.tabBarItem.image = screw;
        
        viewController.tabBarItem.image = screw;//img.image;
        return viewController;
    }
    if ([value isEqualToString:@"form"]) {
        PDRelocationViewController *viewController = [[PDRelocationViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
 
        UIImageView*img=[[UIImageView alloc]init];
        [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
        
        UIImage *imageFromImageView = img.image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
       // subCategoryViewController.tabBarItem.image = screw;
        
        viewController.tabBarItem.image = screw;//img.image;
        return viewController;
    }
    if ([value isEqualToString:@"Deals"]) {
        PDDealsViewController *viewController = [[PDDealsViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        if (![[dictionary objectForKey:@"maxdealid"] isEqualToString:@"FALSE"]){
            if (![[dictionary objectForKey:@"maxdealid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxDealId"]]) {
                viewController.tabBarItem.badgeValue = @"new";
            }
        }
 
        UIImageView*img=[[UIImageView alloc]init];
        [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
        
        UIImage *imageFromImageView = img.image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
        //subCategoryViewController.tabBarItem.image = screw;
        
        viewController.tabBarItem.image = screw;//img.image;
        return viewController;
    }
    
    if ([value isEqualToString:@"Buy"]||[value isEqualToString:@"Purchase"]) {
        PDBuyViewController *viewController = [[PDBuyViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
   
        UIImageView*img=[[UIImageView alloc]init];
        [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
        
        UIImage *imageFromImageView = img.image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
       // subCategoryViewController.tabBarItem.image = screw;
        
        viewController.tabBarItem.image = screw;//img.image;
        return viewController;
    }
    if ([value isEqualToString:@"Events"]) {
        PDEventsViewController *viewController = [[PDEventsViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        if (![[dictionary objectForKey:@"maxeventid"] isEqualToString:@"FALSE"]) {
            if (![[dictionary objectForKey:@"maxeventid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxEventId"]]) {
                viewController.tabBarItem.badgeValue = @"new";
            }
        }

        UIImageView*img=[[UIImageView alloc]init];
        [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
        
        UIImage *imageFromImageView = img.image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
        // subCategoryViewController.tabBarItem.image = screw;
        
        viewController.tabBarItem.image = screw;
        viewController.hidesBottomBarWhenPushed=YES;//img.image;
        return viewController;
    }
    if ([value isEqualToString:@"Home"]) {
        homeVC = [[PDViewController alloc] init];
        homeVC.showBkBtn=YES;
        if (isOneGuide)
            homeVC.showBkBtn=NO;
            [homeVC initwithNavigationController:self.navigationController];
         homeVC.title = [dictionary objectForKey:@"name"];
        homeVC.appName=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppName"];
        UIImageView*img=[[UIImageView alloc]init];
        [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
        
        UIImage *imageFromImageView = img.image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
        // subCategoryViewController.tabBarItem.image = screw;
        
        homeVC.tabBarItem.image = screw;//img.image;
        
        NSInteger lengt =  homeVC.title.length;
        NSLog(@"lengt :%ld",(long)lengt);
        
        if (lengt > 8) {
            // User cannot type more than 15 characters
            homeVC.title = [homeVC.title substringToIndex:8];
        }

        
        return homeVC;
    }
    if ([value isEqualToString:@"category"]) {
        if ([[dictionary objectForKey:@"type"] isEqualToString:@"url"])
        {
            PDWebViewController *webView = [[PDWebViewController alloc] initWithNibName:@"PDWebViewController" bundle:nil];
            webView.title = [dictionary objectForKey:@"name"];
            [webView getDictionary:dictionary];
            
            UIImageView*img=[[UIImageView alloc]init];
            [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
            
            UIImage *imageFromImageView = img.image;
            
            UIImage *screw =  [self imageByScalingImage:imageFromImageView];
            
            
            // subCategoryViewController.tabBarItem.image = screw;
            
            webView.tabBarItem.image = screw;//img.image;
            return webView;
        }
        else if ([[dictionary objectForKey:@"type"] isEqualToString:@"static"])
        {
            PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
            [detailViewController getDictionary:dictionary];
            
            UIImageView*img=[[UIImageView alloc]init];
            [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
            
            UIImage *imageFromImageView = img.image;
            
            UIImage *screw =  [self imageByScalingImage:imageFromImageView];
            
            
            // subCategoryViewController.tabBarItem.image = screw;
            
            detailViewController.tabBarItem.image = screw; //img.image;
            
            detailViewController.title = [dictionary objectForKey:@"name"];
            detailViewController.hidesBottomBarWhenPushed=YES;
            
            return detailViewController;
        }
        else if ([[dictionary objectForKey:@"type"] isEqualToString:@"rss"]) {
            PDRssListViewController *rssListViewController = [[PDRssListViewController alloc] initWithNibName:@"PDRssListViewController" bundle:nil];
            [rssListViewController getDictionary:dictionary];
          
            UIImageView*img=[[UIImageView alloc]init];
            [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
            
            UIImage *imageFromImageView = img.image;
            
            UIImage *screw =  [self imageByScalingImage:imageFromImageView];
            
            
            // subCategoryViewController.tabBarItem.image = screw;
            
            rssListViewController.tabBarItem.image = screw;//img.image;
            
            rssListViewController.title = [dictionary objectForKey:@"name"];
            return rssListViewController;
        }
        else {
            if ([[dictionary objectForKey:@"type"] isEqualToString:@"fixed"] || [[dictionary objectForKey:@"type"] isEqualToString:@"location"] ) {
                if ([[dictionary objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
                    PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
                    [subCategoryViewController getSubId:[dictionary objectForKey:@"Id"] ofType:@"fixed" withName:[dictionary objectForKey:@"name"]];
                    
                    UIImageView*img=[[UIImageView alloc]init];
                    [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
                    
        
                    UIImage *imageFromImageView = img.image;
                    
                  UIImage *screw =  [self imageByScalingImage:imageFromImageView];
                   
                    
                    subCategoryViewController.tabBarItem.image = screw;//img.image;
                    
                    subCategoryViewController.title = [dictionary objectForKey:@"name"];
                    subCategoryViewController.hidesBottomBarWhenPushed=YES;
                    
                    
                    NSInteger lengt =  subCategoryViewController.title.length;
                    NSLog(@"subCategoryViewController lengt :%ld",(long)lengt);
                    
                    if (lengt > 8) {
                        // User cannot type more than 15 characters
                        subCategoryViewController.title = [subCategoryViewController.title substringToIndex:8];
                    }
                    
                    return subCategoryViewController;
                }
                else {
                    PDListViewController *listViewController = [[PDListViewController alloc] init];
                    [listViewController getDictionary:dictionary];
                    
                    UIImageView*img=[[UIImageView alloc]init];
                    [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
                    
                    UIImage *imageFromImageView = img.image;
                    
                    UIImage *screw =  [self imageByScalingImage:imageFromImageView];
                    
                    
                    // subCategoryViewController.tabBarItem.image = screw;
                    
                    listViewController.tabBarItem.image = screw;//img.image;
                    
                    listViewController.title = [dictionary objectForKey:@"name"];
                    listViewController.hidesBottomBarWhenPushed=YES;
                    
                    NSInteger lengt =  listViewController.title.length;
                    NSLog(@"listViewController lengt :%ld",(long)lengt);
                    
                    
                    if (lengt > 8) {
                        // User cannot type more than 15 characters
                        listViewController.title = [listViewController.title substringToIndex:8];
                    }

                    //listViewController.title.l;
                    
                    return listViewController;
                    
                }
                
            }
            
             if ([[dictionary objectForKey:@"type"] isEqualToString:@"normal"]) {
                 
                 PDListViewController *listViewController = [[PDListViewController alloc] init];
                 [listViewController getDictionary:dictionary];
                 
                 UIImageView*img=[[UIImageView alloc]init];
                 [self loadImageFromURL1:[dictionary objectForKey:@"footer_icon"] image:img];
                 
                 UIImage *imageFromImageView = img.image;
                 
                 UIImage *screw =  [self imageByScalingImage:imageFromImageView];
                 
                 
                 // subCategoryViewController.tabBarItem.image = screw;
                 
                 listViewController.tabBarItem.image = screw;//img.image;
                 
                 listViewController.title = [dictionary objectForKey:@"name"];
                 listViewController.hidesBottomBarWhenPushed=YES;
                 
                 return listViewController;

             }
        }
    }
    return 0;
}

- (UIImage*)imageByScalingImage:(UIImage*)imgVw
{
    UIImage *screw =imgVw;
    if(imgVw.size.height>32 ||imgVw.size.width>32)
    {
     screw = [UIImage imageWithData:UIImagePNGRepresentation(imgVw) scale:2];
    }
    return screw;
}

#pragma mark - Webservice for subapp splash screen
/* This function is to show/download splash screen of selected app while loading data*/
-(void)loadSplashImage
{
    [self.view addSubview:vwLoading];
    [activityIndicator1 startAnimating];
    splashImgv.contentMode=UIViewContentModeScaleToFill;
    splashImgv.image=nil;
    NSString *splashImgKey=[NSString stringWithFormat:@"splash%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *splashImgName=[[NSUserDefaults standardUserDefaults]objectForKey:splashImgKey];
    
    
 //   NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:@"Splash"];
    
 

     splashImgv.image=[imageCache imageFromDiskCacheForKey:splashImgName];
    
    
   
    
   
    
    if (!splashImgv.image) {
        
        if([self reachable])
        {
            
            [self startSplashWebService];
        }
        else
        {
           
         [self startLoadingApp];
        }
        
    }
    else
    {
        splashImgv.contentMode=UIViewContentModeScaleToFill;
        //delay for image
//        if([self reachable])
//            [self startSplashWebService];
//        else
         [self startLoadingApp];
        
    }
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    

}
/*
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor redColor];
    }
}
 */
-(UIStatusBarStyle)preferredStatusBarStyle{
    
   // [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
    
     
    return UIStatusBarStyleDefault;;
}
-(void)tableViewEnableProperty
{
    tblView.userInteractionEnabled = YES;

}
- (void)startLoadingApp {
    
    
    if([self isTheAppHasOflineData:[[mainArray objectAtIndex:selectedCell]objectForKey:@"id"]])
    {
        [self coreDataReadAppsSettings];
    }
    else if([self reachable])
    {
        [self  startSettingsWebServiceOfCurrentApp];
    }
    else
    {
        
        [vwLoading removeFromSuperview];
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"This app is not not available offline"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [self performSelector:@selector(tableViewEnableProperty) withObject:nil afterDelay:0.6];
       
    }
    
}

/*Splash screen webservice request function. This requests splash image for the selected app*/
-(void)startSplashWebService
{
  
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceSplashResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@",SPLASH_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}



/*This splash screens web service response function for individual app. This caches the splash image to folder for the particular app*/
-(void)webServiceSplashResponse:(NSData *) responseData
{
    
    
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSString *splashImgUrl=[[[dict objectForKey:@"splash"]objectAtIndex:0]objectForKey:@"splash_img"];
    NSString *splashImgKey=[NSString stringWithFormat:@"splash%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    [[NSUserDefaults standardUserDefaults]setObject:splashImgUrl forKey:splashImgKey];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:@"Splash"];
   //splashImgv.contentMode=UIViewContentModeScaleToFill;
    splashImgv.image=[imageCache imageFromDiskCacheForKey:splashImgUrl];
    if (!splashImgv.image) {
    
         NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:splashImgUrl]];
         UIImage *splashImg = [UIImage imageWithData:data];
         splashImgv.image=splashImg;
          [imageCache storeImage:splashImg forKey:splashImgUrl completion:^{  }];
        
        if([splashImgUrl isEqualToString:@"NA"])
            splashImgv.image=[UIImage imageNamed:@"placeholderForNoSplash.png"];
        
        [self startLoadingApp];
       // [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startLoadingApp) userInfo:nil repeats:NO];

         
     }
    else
    {
        [self startLoadingApp];
       //[NSTimer scheduledTimerWithTimeInterval:1.0/2 target:self selector:@selector(startLoadingApp) userInfo:nil repeats:NO];
    }
    
    
}
#pragma mark - Checking for Offline Data


/*  This function check for offline data availability in the selected app. If it has, true value will return*/
-(BOOL)isTheAppHasOflineData:(NSString*)appid
{

NSError *error;
PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];

NSManagedObjectContext *context = [appDelegate managedObjectContext];
NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
NSEntityDescription *entity = [NSEntityDescription
                               entityForName:@"Categories" inManagedObjectContext:context];
fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",appid];
[fetchRequest setEntity:entity];



NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
     BOOL hasData=false;
if([fetchedObjects count]>0)
    hasData=true;// Offline Data available
    return hasData;



}
#pragma mark - Image caching

- (void) loadImageFromURL1:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        
        im.image = [UIImage imageNamed:@"img_def"];
     /*   //        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        //        dispatch_async(queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:imageURL];
        [FTWCache setObject:data forKey:key];
        UIImage *image = [UIImage imageWithData:data];
        // dispatch_sync(dispatch_get_main_queue(), ^{
        im.image = image;
        //   });
        // });*/
        
        
         // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
              
              NSData *data = [NSData dataWithContentsOfURL:imageURL];
              [FTWCache setObject:data forKey:key];
              UIImage *image = [UIImage imageWithData:data];
              
      //   dispatch_async(dispatch_get_main_queue(), ^{
         
             im.image = image;
         
      //   });
      //   });

    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

    @end
