//
//  PDGalleryDetailViewController.h
//  Visiting Ashland
//
//  Created by swaroop on 06/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDGalleryDetailViewController : UIViewController<GADBannerViewDelegate,UIPageViewControllerDelegate, UIPageViewControllerDataSource>
{
      GADBannerView *bannerView_;
    
    IBOutlet UIButton *stopBtn;
}
-(void)getDictionary:(NSDictionary *) dictHere;
-(void)getArray:(NSArray *) ArrayHere : (long int) index;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollDetailGallery;
@property (weak, nonatomic) IBOutlet UILabel *headingLbl;
@property (weak, nonatomic) IBOutlet UILabel *currentImgLbl;

@property (strong, nonatomic) NSMutableArray *imageArray;

@property (strong, nonatomic) IBOutlet UIView *start;

@property (strong, nonatomic) UIPageViewController *pageViewController;


-(IBAction)closeImage:(id)sender;
@end
