//
//  PDMapCatCell.h
//  LocalsGuide
//
//  Created by swaroop on 06/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDMapCatCell : UITableViewCell
{
}
@property(nonatomic, retain) IBOutlet UILabel *lblCatName;
@property(nonatomic, retain) IBOutlet UISwitch *switchCat;
@property(nonatomic, retain) id responseTarget;
@property SEL respondToMethod;
-(IBAction)onValueChanged:(id)sender;
@end
