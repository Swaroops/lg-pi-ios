//
//  PDEventCatCell.h
//  Visiting Ashland
//
//  Created by Anu on 13/05/16.
//  Copyright © 2016 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDEventCatCell : UITableViewCell
{
    
}
@property SEL respondToMethod;
@property(nonatomic, retain) id responseTarget;
@property(nonatomic, retain) IBOutlet UILabel *lblCatName;
@property(nonatomic, retain) IBOutlet UISwitch *switchCat;


@end
