

#import "PDFavoriteViewController.h"
#import "PDEventDetailViewController.h"
#import "PDDealDetailViewController.h"
#import "PDDealsCell.h"
#import "PDEventsCell.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
@interface PDFavoriteViewController () <CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *arrFavorites;
    int banery;
    BOOL isHotSpotAd;
}
@end

@implementation PDFavoriteViewController
CLLocation *location_updated;
NSString *adLink;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
     [self.tabBarController.tabBar setHidden:YES];
    if (IS_IPHONE5) {
        banery = 470;
    }
    else {
        banery = 382;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_fav])
    if(![removAdStatus isEqualToString:@"purchased"])
        [self showAd];
}
-(void)viewWillAppear:(BOOL)animated{
    [tblFavorites removeFromSuperview];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:finalPath])
    {
       // [arrFavorites removeAllObjects];
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrFavorites = [plistDict objectForKey:@"favoritesArray"];
    }
    int scrollHeight;
    if (IS_IPHONE5)
        scrollHeight = 501;
    else
        scrollHeight = 311;
    
    // scrollHeight=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height;
    tblFavorites = [[UITableView alloc] initWithFrame:CGRectMake(0, 68, 320, scrollHeight)];
    tblFavorites.backgroundColor = [UIColor clearColor];
    tblFavorites.dataSource = self;
    tblFavorites.delegate = self;
    [self.view addSubview:tblFavorites];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable_fav])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable_fav])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
#pragma mark - Setting ad
-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    int scrollHeight;
    if (IS_IPHONE5)
        scrollHeight = 501;
    else
        scrollHeight = 311;
    
     //scrollHeight=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height;
 
    tblFavorites.frame = CGRectMake(0, 68, 320, scrollHeight);
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}

-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_listing"] isEqualToString:@"FALSE"])) {
            AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
            [self.view addSubview:ad1Image];
            UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
            ad1Btn.frame = CGRectMake(0, banery, 320, 50);
            [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:ad1Btn];
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            if (![[dict objectForKey:@"image_listing"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
                adLink = [dict objectForKey:@"link_listing"];
                int scrollHeight;
                if (IS_IPHONE5)
                    scrollHeight = 501;
                else
                    scrollHeight = 311;
                
                // scrollHeight=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-50;
                tblFavorites.frame = CGRectMake(0, 68, 320, scrollHeight);
            }
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
            [self.view addSubview:ad1Image];
            UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
            ad1Btn.frame = CGRectMake(0, banery, 320, 50);
            [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:ad1Btn];
            if (![[dict objectForKey:@"ad_1"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_1"]];
                adLink = [dict objectForKey:@"link_1"];
                int scrollHeight;
                if (IS_IPHONE5)
                    scrollHeight = 501;
                else
                    scrollHeight = 311;
                
                // scrollHeight=self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height;
                tblFavorites.frame = CGRectMake(0, 68, 320, scrollHeight);
            }
        }
    }
}
#pragma mark - UITableView delegate and datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrFavorites count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tabBarController.tabBar setHidden:YES];
    
    NSDictionary *dict = [arrFavorites objectAtIndex:indexPath.row];
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    if ([dict objectForKey:@"geometry"]||[[dict objectForKey:@"type"] isEqualToString:@"location"]||[[dict objectForKey:@"type"] isEqualToString:@"fixed"]||[[dict objectForKey:@"type"] isEqualToString:@"normal"]||[[dict objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dict objectForKey:@"type"] isEqualToString:@"ordered list"]) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
        }
        UIImageView *imgCellBg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 310, 96)];
        imgCellBg.image = [UIImage imageNamed:@"listbg.png"];
        [cell.contentView addSubview:imgCellBg];
        
        UIImageView *imgPlaceholder = [[UIImageView alloc] initWithFrame:CGRectMake(14, 19, 82, 62)];
        imgPlaceholder.image = [UIImage imageNamed:@"listPlaceholder.png"];
        //[cell.contentView addSubview:imgPlaceholder];
        if ([dict objectForKey:@"star_rating"]) {
            if ([[dict objectForKey:@"star_rating"] isEqualToString:@"yes"]) {
                UIImageView *imgRating = [[UIImageView alloc] initWithFrame:CGRectMake(100, 73, 50, 8)];
                imgRating.image = [UIImage imageNamed:@"5star.png"];
                [cell.contentView addSubview:imgRating];
            }
        }
       
        if ([dict objectForKey:@"thumb"]) {
            if (![[dict objectForKey:@"thumb"] isEqualToString:@""]) {
                UIImageView *img = [[AsyncImageView alloc] init];
               // img.frame = CGRectMake(8.5, 6, 88, 88);
               // img.frame = CGRectMake(12, 20, 80, 60);
                
                img.frame = CGRectMake(8.5, 6, 88, 88);
                img.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
                
                img.layer.cornerRadius=6.0;
                img.layer.masksToBounds = YES;

                
                [cell.contentView addSubview:img];
                
               
                NSString *str=[dict objectForKey:@"thumb"];
                
                img.image=[imageCache imageFromDiskCacheForKey:str];
                
                if (!img.image)
                {
                 
                        
                        
                        [img sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
 
                        img.imageURL = [NSURL URLWithString:str];
                    img.layer.cornerRadius=6.0;
                    img.layer.masksToBounds = YES;
                    
                }

            }
            /*else {
                UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                img.frame = CGRectMake(15, 15, 80, 60);
                [cell.contentView addSubview:img];
            }*/
        }
        else {
#define IMAGE_VIEW_TAG 99
            UIImageView *img = [[AsyncImageView alloc] init];
            img.frame = CGRectMake(8.5, 6, 88, 88);
            //img.frame = CGRectMake(15, 20, 80, 60);
            img.layer.cornerRadius=6.0;
            NSString *str;
            if ([dict objectForKey:@"icon"]) {
                if (([[dict objectForKey:@"listing_icon"] isEqualToString:@"FALSE"]||[[dict objectForKey:@"listing_icon"] isEqualToString:@""]))
                    str = [dict objectForKey:@"icon"];
                else
                    str= [dict objectForKey:@"listing_icon"];
//                if([[dict objectForKey:@"types"] isEqualToString:@"parking"])
//                {
//                }
            }
            else
                if ([[dict objectForKey:@"image"] isKindOfClass:[NSArray class]]) {
                    str = [[dict objectForKey:@"image"] objectAtIndex:0];
                }
                else{
                    
                    str = [dict objectForKey:@"image"];
                }
            
            
           
            
            img.image=[imageCache imageFromDiskCacheForKey:str];
            
            if (!img.image)
            {
                
                
                
                [img sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                
                img.imageURL = [NSURL URLWithString:str];
                img.tag = IMAGE_VIEW_TAG;
                img.layer.cornerRadius=6.0;
                
                if([[dict objectForKey:@"scope"] isEqualToString:@"GOOGLE"])
                                {
                                    
                                    str = @"http://insightto.com/listing_icon/458.png";
                                    
                                    [img sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                                    
                                    img.imageURL = [NSURL URLWithString:str];
                                    img.tag = IMAGE_VIEW_TAG;
                                    img.layer.cornerRadius=6.0;

                                    
                                }

            }
            img.layer.cornerRadius=6.0;
            img.layer.masksToBounds = YES;
            [cell.contentView addSubview:img];
        }
        UILabel *titlelabel = [[UILabel alloc] init];
        titlelabel.frame = CGRectMake(100, 7, 180, 35);
        titlelabel.numberOfLines = 2;
        //[titlelabel setFont:[UIFont fontWithName:@"Verdana" size:13]];//Helvetica
        
        [titlelabel setFont:[UIFont fontWithName:@"Helvetica" size:18]];//Helvetica
        titlelabel.textColor=[UIColor blackColor];
        //[titlelabel setFont:[UIFont systemFontOfSize:17]];
        titlelabel.text = [dict objectForKey:@"name"];
        [cell.contentView addSubview:titlelabel];
        
        UILabel *lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(100, 40, 200, 30)];
        lblDesc.numberOfLines = 2;
        lblDesc.backgroundColor = [UIColor clearColor];
        [lblDesc setFont:[UIFont systemFontOfSize:13]];
        if (![dict objectForKey:@"vicinity"])
            
            if (![dict objectForKey:@"address"])
            lblDesc.text = [dict objectForKey:@"address"];
            else
                lblDesc.text = [dict objectForKey:@"preview"];
        else
        {
            
            lblDesc.text = [dict objectForKey:@"vicinity"];
        }
        [cell.contentView addSubview:lblDesc];
        
        UILabel *lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(100, 80, 150, 16)];
        lblDistance.backgroundColor = [UIColor clearColor];
        [lblDistance setFont:[UIFont fontWithName:@"Verdana" size:10]];
        //[cell.contentView addSubview:lblDistance];
        CLLocation *plLocation;
        if ([dict objectForKey:@"geometry"]) {
            
            NSDictionary *dictLoc = [dict objectForKey:@"geometry"];
            NSDictionary *dictLatLng = [dictLoc objectForKey:@"location"];
            plLocation = [[CLLocation alloc] initWithLatitude:[[dictLatLng objectForKey:@"lat"] doubleValue] longitude:[[dictLatLng objectForKey:@"lng"] doubleValue]];
        }
        else {
            plLocation = [[CLLocation alloc] initWithLatitude:[[dict objectForKey:@"latitude"] doubleValue] longitude:[[dict objectForKey:@"longitude"] doubleValue]];
        }
        
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        //if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KM"] isEqualToString:@"NO"])
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
        {
            actdist = meters * 0.000621371;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
            int k = actdist;
            if (k <= 0)
                lblDistance.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
            else
                lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
        }
        else {
            actdist = meters * 0.001;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
            int k = actdist;
            if (k <= 0)
                lblDistance.text = [NSString stringWithFormat:@"0%@ km",trimmed];
            else
                lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
        }
        UIImageView *imDetail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listing_arrow.png"]];//arrow_list.png
        imDetail.frame = CGRectMake(295, 40, 25, 25);
        [cell.contentView addSubview:imDetail];
        
       
        return cell;
    }
    else if ([dict objectForKey:@"deal_title"]) {
        static NSString *simpleTableIdentifier = @"SimpleTableCell";
        PDDealsCell *cell = (PDDealsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDDealsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.lblName.text = [dict objectForKey:@"deal_title"];
        cell.lblDesc.text = [dict objectForKey:@"deal_subtitle"];
        cell.lblPhone.text = [dict objectForKey:@"deal_phone"];
        
        
        
        NSString *str=[dict objectForKey:@"thumb"];
        
        cell.imgDeal.image=[imageCache imageFromDiskCacheForKey:str];
        
        cell.imgDeal.layer.cornerRadius=6.0;
        cell.imgDeal.layer.masksToBounds = YES;
        
        if (!cell.imgDeal.image)
        {
            
            
            
            [cell.imgDeal sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
            
            cell.imgDeal.imageURL = [NSURL URLWithString:str];
            
        }
        
        cell.imgDeal.layer.cornerRadius=6.0;
        cell.imgDeal.layer.masksToBounds = YES;
        
        return cell;
    }
    else if ([dict objectForKey:@"event_title"]) {
        static NSString *simpleTableIdentifier = @"SimpleTableCell";
        PDEventsCell *cell = (PDEventsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDEventsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.lblName.text = [dict objectForKey:@"event_title"];
        cell.lblTime.text = [dict objectForKey:@"event_time"];
        cell.lblPlace.text = [dict objectForKey:@"event_address"];
        cell.lblDesc.text = [dict objectForKey:@"event_description"];
        cell.imgEvent.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
        NSString *str=[dict objectForKey:@"thumb"];
        
        cell.imgEvent.image=[imageCache imageFromDiskCacheForKey:str];
        
        if (!cell.imgEvent.image)
        {
            
            
            
            [cell.imgEvent sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
            
            cell.imgEvent.imageURL = [NSURL URLWithString:str];
            
        }
        
        cell.imgEvent.layer.cornerRadius=6.0;
        cell.imgEvent.layer.masksToBounds = YES;
        
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    UIImageView *imgCellBg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 310, 96)];
    imgCellBg.image = [UIImage imageNamed:@"listbg.png"];
    [cell.contentView addSubview:imgCellBg];
    
    UIImageView *imgPlaceholder = [[UIImageView alloc] initWithFrame:CGRectMake(14, 19, 82, 62)];
    imgPlaceholder.image = [UIImage imageNamed:@"listPlaceholder.png"];
    
    UILabel *titlelabel = [[UILabel alloc] init];
    titlelabel.frame = CGRectMake(100, 7, 180, 35);
    titlelabel.numberOfLines = 2;
    //[titlelabel setFont:[UIFont fontWithName:@"Verdana" size:13]];
    //[titlelabel setFont:[UIFont systemFontOfSize:17]];
    [titlelabel setFont:[UIFont fontWithName:@"Helvetica" size:18]];//Helvetica
    titlelabel.textColor=[UIColor blackColor];
   // titlelabel.text = [dict objectForKey:@"title"];
    titlelabel.text = [dict objectForKey:@"title"];
    [cell.contentView addSubview:titlelabel];
    
    UIImageView *img = [[AsyncImageView alloc] init];
   // img.frame = CGRectMake(15, 20, 80, 60);//8.5, 6, 88, 88
    
     img.frame = CGRectMake(8.5, 6, 88, 88);
    
   // img.imageURL = [NSURL URLWithString:[dict objectForKey:@"image"]];
    
    NSArray*arrays=[dict objectForKey:@"image"];
    NSString *str= [arrays objectAtIndex:0];
   
    img.image=[imageCache imageFromDiskCacheForKey:str];
    
    if (!img.image)
    {
        
        
        
        [img sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
        
        img.imageURL = [NSURL URLWithString:str];
        img.layer.cornerRadius=6.0;
        
    }
    img.layer.cornerRadius=6.0;
    img.layer.masksToBounds = YES;
    [cell.contentView addSubview:img];
    
    UILabel *lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(100, 40, 200, 30)];
    lblDesc.numberOfLines = 2;
    lblDesc.backgroundColor = [UIColor clearColor];
    [lblDesc setFont:[UIFont fontWithName:@"Verdana" size:11]];
    lblDesc.text = [dict objectForKey:@"address"];
    [cell.contentView addSubview:lblDesc];
   
    UILabel *lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(100, 80, 150, 16)];
    lblDistance.backgroundColor = [UIColor clearColor];
    [lblDistance setFont:[UIFont fontWithName:@"Verdana" size:10]];
    //[cell.contentView addSubview:lblDistance];
    CLLocation *plLocation = [[CLLocation alloc] initWithLatitude:[[dict objectForKey:@"latitude"] doubleValue] longitude:[[dict objectForKey:@"longitude"] doubleValue]];
    
    CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
    float actdist;
   // if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KM"] isEqualToString:@"NO"])
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
    {
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        int k = actdist;
        if (k <= 0)
            lblDistance.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
        else
            lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
    }
    else {
        actdist = meters * 0.001;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        int k = actdist;
        if (k <= 0)
            lblDistance.text = [NSString stringWithFormat:@"0%@ km",trimmed];
        else
            lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
    }
    
    UIImageView *imDetail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listing_arrow.png"]];// arrow_list.png
    imDetail.frame = CGRectMake(295, 40, 25, 25);
    [cell.contentView addSubview:imDetail];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [arrFavorites objectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"type"] isEqualToString:@"event"]) {
        PDEventDetailViewController *eventDetailViewController = [[PDEventDetailViewController alloc] init];
        [eventDetailViewController getEventDetails:dict];
         eventDetailViewController.isFromFav=YES;
        [self.navigationController pushViewController:eventDetailViewController animated:YES];
    }
    else if([[dict objectForKey:@"type"] isEqualToString:@"deal"]) {
        PDDealDetailViewController *dealDetailViewController = [[PDDealDetailViewController alloc] init];
        [dealDetailViewController getDealDetails:dict];
        dealDetailViewController.isFromFav=YES;
        [self.navigationController pushViewController:dealDetailViewController animated:YES];
    }
    else if([[dict objectForKey:@"type"] isEqualToString:@"static"]) {
        PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
        [detailViewController setStaticDictFromFavorite:dict];
         detailViewController.isFromFav=YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else {
        PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
        
        [detailViewController getDictionary:dict];
        detailViewController.isFromFav=YES;
       
//        int64_t delayInSeconds = 1.0;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            
//        });
        if((![self reachable_fav])&([[dict objectForKey:@"scope"] isEqualToString:@"GOOGLE"]))
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"This listing is not available in offline." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
        [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [arrFavorites removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
        [plistDict setObject:arrFavorites forKey:@"favoritesArray"];
        [plistDict writeToFile:finalPath atomically:YES];
    }
}
-(BOOL)reachable_fav {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
-(IBAction)onBack:(id)sender
{
   
    
   
    NSLog(@"title2 :%@",self.navigationItem.title);
    
    int i;
    
    for (i=0; i<[self.tabBarController.tabBar.items count]; i++)
    {
        
         NSLog(@"title2 :%@",[self.tabBarController.tabBar.items objectAtIndex:i].title );
        NSString *title =[self.tabBarController.tabBar.items objectAtIndex:i].title;
        if([title isEqualToString:@"Home"])
        {
            [self.tabBarController setSelectedIndex:i];
            break;
        }
        
    }
    
   // [self.tabBarController setSelectedIndex:0];
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    PDViewController *viewController = [[PDViewController alloc] init];
//    [self.navigationController pushViewController:viewController animated:NO];
}
@end
