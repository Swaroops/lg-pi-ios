

#import <UIKit/UIKit.h>

@interface PDRssCell : UITableViewCell
@property (nonatomic, retain) IBOutlet UILabel *lblName;
@property (nonatomic, retain) IBOutlet UILabel *lblTime;
@property (nonatomic, retain) IBOutlet UILabel *lblPlace;
@property (nonatomic, retain) IBOutlet UIImageView *imgRss;

@end
