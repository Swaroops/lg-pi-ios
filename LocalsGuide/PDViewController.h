

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PDInappHomeViewController.h"
@interface PDViewController : UIViewController <UIAlertViewDelegate,CLLocationManagerDelegate, UIWebViewDelegate, GADBannerViewDelegate, UITabBarControllerDelegate,PDInappHomeViewController>
{
    UIScrollView *scrollHome;
    NSMutableArray *arrayIndexNames;
    GADBannerView *bannerView_;
    UIButton *fShare, *fPage, *tPage, *about;
    IBOutlet UIButton *more;
    UIWebView *facebookPage, *twitterPage;
    UIActivityIndicatorView *activityIndicator;
    UIActivityIndicatorView *activityIndicator1;
    IBOutlet UIView *viewAbout;
    IBOutlet UIView *vwLoading;
    IBOutlet UILabel *titlLabel;
    IBOutlet UILabel *titlLabel2;
    IBOutlet UIButton *btnTapToRetry;
    IBOutlet UIButton *backBtn;
    
    IBOutlet UIView *vwSpLoading;
    
    IBOutlet UIImageView *SpImage;
    
    UINavigationController *mainNavigationController;
    
   UISearchBar *searchBar;
    NSMutableArray *searchArray;
    
    
}
@property (weak, nonatomic) IBOutlet UIImageView *backBtnImg;
@property(nonatomic) BOOL isCellClicked;
@property(nonatomic)BOOL showBkBtn;
@property(nonatomic,strong)NSMutableArray* queArray;
@property (nonatomic, retain)UINavigationController *mainNavigationController;
@property(nonatomic,retain)NSString *appName;
@property (nonatomic, retain) CLLocationManager *locationManager;

@property(nonatomic)BOOL *isFromSearch;
@property(nonatomic)BOOL *fromOfficialGuideSearch;



-(IBAction)onBack:(id)sender;
-(IBAction)onTapToRetry:(id) sender;

-(void)fillDataArray:(NSArray*)dataArr;

-(void)initwithNavigationController:(UINavigationController*)obj;
@end
