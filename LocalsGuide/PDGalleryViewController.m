//
//  PDGalleryViewController.m
//  Visiting Ashland
//
//  Created by swaroop on 03/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "PDGalleryViewController.h"
#import "Reachability.h"
#import "UIImageView+AFNetworking.h"
#import "PDGalleryDetailViewController.h"
#import "PDAppDelegate.h"
#import "Gallery.h"
#import "UIImageView+WebCache.h"

@interface PDGalleryViewController ()
{
    NSArray *arGalleryDetails;
    UIScrollView*scrollList;
    int index;
    int scrollHeight;
    int banery;
    NSDictionary*dics;
    
     UIRefreshControl *refreshControl;
}
@end

@implementation PDGalleryViewController
@synthesize healdingLbl,backgroundImgv;

-(BOOL)shouldAutorotate {
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
    // Use this to allow upside down as well
    //return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}
- (NSUInteger) supportedInterfaceOrientations {
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}
-(void)getDictionary:(NSDictionary *) dict
{
    dics=dict;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)viewDidLoad {
    [super viewDidLoad];

 //  [self.tabBarController.tabBar setHidden:YES];
    //code change by Ram for changing GAD y position
    if (IS_IPHONE5) {
        [[NSBundle mainBundle] loadNibNamed:@"PDGalleryViewController" owner:self options:nil];
        scrollHeight = 450;
       // banery = 470;
         banery=515;
    }
    else {
        [[NSBundle mainBundle] loadNibNamed:@"PDGalleryViewController4" owner:self options:nil];
        scrollHeight = 365;
        //banery = 382;
        banery=427;
    }
    
    backgroundImgv.backgroundColor=[UIColor blackColor];
    scrollHeight=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-20;
    healdingLbl.text=[dics objectForKey:@"name"];
    if (![self offlineAvailable]) {
        [self startGalleryWebService];
    }
    else{
        [self coreDataCodeRead_gallery];
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];

}

-(void)viewDidAppear:(BOOL)animated
{
    
   // [self.tabBarController.tabBar setHidden:YES];
}

-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

-(BOOL)offlineAvailable
{
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)testRefresh:(UIRefreshControl *)refreshControlH
{
    if ([self reachable])
        [self startGalleryWebService];
    else
         [refreshControl endRefreshing];
    
    
}


-(void)setUpScroll {
    index = 0;
  //  if(scrollList.frame.size.width/156>=156)
    [scrollList removeFromSuperview];
    scrollList = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20+3, 320, scrollHeight - 3)];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    [scrollList addSubview:refreshControl];
    [self.view addSubview:scrollList];
    scrollList.bounces = YES;
    
    
    
    
    
    
    float excessWidth=(int)scrollList.frame.size.width%106;
    int numberOfColumns=(int)scrollList.frame.size.width/106;
    
    
    float imageWidth=106+(excessWidth/numberOfColumns);
    
    float widthHieghtRatio=155.0f/106.0f;
    float imageHeight=imageWidth*widthHieghtRatio;
//    refreshControl = [[UIRefreshControl alloc] init];
    
//    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
//    [scrollList addSubview:refreshControl];
    NSInteger limit = [arGalleryDetails count];
    int looplimit;
    if (!((limit % numberOfColumns) == 0))
    {
        looplimit = (int)[arGalleryDetails count]/numberOfColumns + 1;
    }
    else
    {
        looplimit = (int)[arGalleryDetails count]/numberOfColumns;
    }
    [scrollList setContentSize:CGSizeMake(320, looplimit * imageHeight)];
    if (looplimit * imageHeight < scrollHeight) {
        [scrollList setContentSize:CGSizeMake(320, scrollHeight + 50)];
    }
      BOOL findAnyImage=false;
    
    for (int i = 0; i < looplimit; i++) {
        for (int k = 0; k < 3; k++) {
            if (index >= [arGalleryDetails count])
                continue;
            
            //            UIImageView *btnBg = [[UIImageView alloc] initWithFrame:CGRectMake((k * 100) + 16, (i * 120) + 11, 88, 88)];
            //            btnBg.image = [UIImage imageNamed:@"button_bg.png"];
            //            [scrollList addSubview:btnBg];
            
          
            
            NSDictionary *dict = [arGalleryDetails objectAtIndex:index];
            
            AsyncImageView *img = [[AsyncImageView alloc] initWithFrame:CGRectMake((k * imageWidth), (i * imageHeight), imageWidth, imageHeight)];
            //img.layer.cornerRadius = 7;
            img.clipsToBounds = YES;
            img.activityIndicatorStyle=UIActivityIndicatorViewStyleWhite;
            [img.layer setBorderWidth:1];
            [img.layer setBorderColor:[[UIColor blackColor] CGColor]];
            
            
            NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            SDImageCache *imageCache =[SDImageCache sharedImageCache];
            imageCache = [imageCache initWithNamespace:imageCacheFolder];
            
            NSString *imgKey=[dict objectForKey:@"thumb"];
            
            img.image=[imageCache imageFromDiskCacheForKey:imgKey];
          
            
            if (!img.image)
            {
                if(![self offlineAvailable])
                {
                    [img sd_setImageWithURL:[NSURL URLWithString:imgKey] placeholderImage:nil options:SDWebImageRefreshCached];
                    [img setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                        NSLog(@"success");
                        
                        img.image=imageX;
                        [[SDImageCache sharedImageCache] storeImage:imageX forKey:[dict objectForKey:@"thumb"] completion:^{  }];
                        
                        
                        
                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                        img.image=[UIImage imageNamed:@"noimage.png"];
                    }
                     ];
                    
                   
                    img.imageURL = [NSURL URLWithString:imgKey];
                }
                else
                {
                   // img.image=[UIImage imageNamed:@"noimage.png"];
                    
                    {
                        
                        NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                        
                        NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                        
                        
                        
                        
                        NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                        
                        NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit);
                        NSLog(@"secondBit folder :%@",secondBit2);
                        
                        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",imgKey);
                        
                        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/gallery/thumb",secondBit2]];
                        
                        NSArray* str3 = [imgKey componentsSeparatedByString:@"gallery_images/thumb/"];
                        
                        NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                        
                        NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        NSLog(@"firstBit folder :%@",firstBit3);
                        NSLog(@"secondBit folder :%@",secondBit3);
                        NSLog(@"decoded folder :%@",decoded);
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        img.image=image;
                        
                        // [self loadImageFromURL1:str image:tblHeaderImage];
                        
                        // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                        
                    }
                }
            }
            else
            {
                findAnyImage=true;
            }
            
            img.contentMode=UIViewContentModeScaleAspectFill;
            
           //            [img setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
//                
//                [img setImage:imageX];
//            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                NSLog(@"fail with %@ and %@",[error localizedDescription],[request URL]);
//            }
//             ];
            [scrollList addSubview:img];
            
            
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake((k * imageWidth), (i * imageHeight), imageWidth, imageHeight);
            btn.tag = index;
            //[btn setBackgroundColor:[UIColor redColor]];
            [btn addTarget:self action:@selector(onListButton_gallery:) forControlEvents:UIControlEventTouchUpInside];
            [scrollList addSubview:btn];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake((k * imageWidth) + 15, (i * 130) + 110, imageWidth-10, 20)];
            [lbl setFont:[UIFont fontWithName:@"Verdana" size:13]];
            lbl.text = [dict objectForKey:@"name"];
            lbl.backgroundColor = [UIColor clearColor];
            lbl.textAlignment = 1;
            [scrollList addSubview:lbl];
            index++;
        }
    }
    
//    if((![self reachable])&&(!findAnyImage))
//    {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Images not available in Offline" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//    }
    
    
}
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)startGalleryWebService
{
  //  btnTapToRetry.hidden = YES;
    NSLog(@"gallery service started");
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(GalleryServiceResponse:);
    [webService startParsing: [NSString stringWithFormat:@"%@%@",GALLERY_WEBSERVICE_NEW,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}

// Get web service response here
-(void)GalleryServiceResponse:(NSData *) responseData
{
     [refreshControl endRefreshing];
    NSLog(@"gallery response received");
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    //[self setAdBannerWithArray:[dict objectForKey:@"settings"]];
    arGalleryDetails = [dict objectForKey:@"gallery"];
    
    if([arGalleryDetails count]>0)
        [self coreDataCodeWrite_gallery:arGalleryDetails];
    
    
     [self setUpScroll];
//    NSDictionary *dic2 = [arAdDetails objectAtIndex:0];
//    titlLabel.text = [dic2 objectForKey:@"app_title"];
//    titlLabel2.text = [dic2 objectForKey:@"app_title"];
//    if ([[dic2 objectForKey:@"distance_in"] isEqualToString:@"km"]) {
//        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"KM"];
//    }
//    else {
//        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"KM"];
//    }
//    if ([[dic2 objectForKey:@"app_view"] isEqualToString:@"grid"]) {
//        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"VIEW_GRID"];
//    }
//    else {
//        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"VIEW_GRID"];
//    }
//    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"]) {
//        [self.view addSubview:vwLoading];
//        [self startWebService];
//    }
//    else {
//        NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HomeResponse"];
//        arrayContents = [dict objectForKey:@"maincategory"];
//        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"VIEW_GRID"] isEqualToString:@"YES"]) {
//            [scrollList removeFromSuperview];
//            [self setUpScroll];
//        }
//        else {
//            [tblView removeFromSuperview];
//            [self setUpListView];
//        }
//    }
//    [self startHotSpotWebService];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)onListButton_gallery:(id)sender {
    UIButton *btn = (UIButton *) sender;
    NSDictionary *dict = [arGalleryDetails objectAtIndex:btn.tag];
   
    
                PDGalleryDetailViewController *detailViewController = [[PDGalleryDetailViewController alloc] init];
                [detailViewController getDictionary:dict];
    [detailViewController getArray:arGalleryDetails :btn.tag];
    
    [self presentViewController:detailViewController animated:NO completion:nil];
              //  [self.navigationController pushViewController:detailViewController animated:YES];
    
}

- (IBAction)onBack_gallery:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)coreDataCodeWrite_gallery:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Gallery" inManagedObjectContext:context1];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Gallery *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try {
        
        
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Gallery"
                                    inManagedObjectContext:context];
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"id"]forKey:@"gall_id"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"title"]forKey:@"title"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"description"]forKey:@"gall_description"];
            //[Que setValue:[[queArray objectAtIndex:i] objectForKey:@"image"]forKey:@"image"];
            // [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"thumb"]forKey:@"thumb"];
            
            
            
            
            NSString*str1=[[queArray objectAtIndex:i] objectForKey:@"image"];
            
            
            
            
            
            
            //  str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            /*if(str1.length>0)
            {
                [self insertImagesToDownload:str1];
                [self sdImageCaching:str1];
            }*/
            
            [Que setValue:str1 forKey:@"image"];
            
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            //str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
           /* if(str.length>0)
            {
                [self insertImagesToDownload:str];
                [self sdImageCaching:str];
            }*/
            [Que setValue:str forKey:@"thumb"];
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    
    
}
-(void)coreDataCodeRead_gallery{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Gallery" inManagedObjectContext:context];
      fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
    int i;
    
    //    for (i=0; i<[dictCategory count]; i++) {
    //        //   NSString*categoryId=[[[[[queArray objectAtIndex:i]objectForKey:@"category"]objectForKey:@"text"]stringByReplacingOccurrencesOfString:@"\n" withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //        // NSDictionary* dictID=[arrResponse objectAtIndex:i];
    //        cateID=[dictCategory objectForKey:@"id"];
    //        NSPredicate * parentIdPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"category == '%@'",cateID]];
    //        if(![[dictSettings objectForKey:@"mode"] isEqualToString:@"no"]){
    //            NSPredicate * ModePredicate =[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"mode == '%@'", mode]];
    //            NSPredicate *compountPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[parentIdPredicate, ModePredicate]];
    //            [fetchRequest setPredicate:compountPredicate];
    //        }
    //        else{
    //            [fetchRequest setPredicate:parentIdPredicate];
    //        }
    
    //fetchRequest.sortDescriptors = @[descriptor];
    // [fetchRequest setPredicate:parentIdPredicate];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    
    NSLog( @"error %@",error);
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    //if([self reachable])
    {
    for (Gallery *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        if(manuf.gall_id)
        [tempManufacturerDictionary setObject:manuf.gall_id forKey:@"id"];
        if(manuf.title)
        [tempManufacturerDictionary setObject:manuf.title forKey:@"title"];
        if(manuf.gall_description)
        [tempManufacturerDictionary setObject:manuf.gall_description forKey:@"description"];
        if(manuf.image)
        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
        if(manuf.thumb)
        [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
   
        // Answer *dof=manuf.options;
        //            if([manuf.sub_exists isEqualToString:@"yes"]){
        //            Subcategory1*dof=manuf.subcategory;
        //            [tempManufacturerDictionary setObject:dof.catid forKey:@"Id"];
        //            [tempManufacturerDictionary setObject:dof.name forKey:@"name"];
        //            [tempManufacturerDictionary setObject:dof.googletype forKey:@"googletype"];
        //            [tempManufacturerDictionary setObject:dof.external_url forKey:@"external_url"];
        //            [tempManufacturerDictionary setObject:dof.rss_url forKey:@"rss_url"];
        //            [tempManufacturerDictionary setObject:dof.image forKey:@"image"];
        //            [tempManufacturerDictionary setObject:dof.listing_icon forKey:@"listing_icon"];
        //            [tempManufacturerDictionary setObject:dof.sub_exists forKey:@"sub_exists"];
        //            [tempManufacturerDictionary setObject:dof.ad forKey:@"ad"];
        //            [tempManufacturerDictionary setObject:dof.link forKey:@"link"];
        //            [tempManufacturerDictionary setObject:dof.subcategory forKey:@"subcategory"];
        //            }
        //
        ////            if(dof.ans1!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans1 forKey:@"ans1"];}
        ////            if(dof.ans2!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans2 forKey:@"ans2"];}
        ////            if(dof.ans3!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans3 forKey:@"ans3"];}
        ////            if(dof.ans4!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans4 forKey:@"ans4"];}
        //
        //
        //            [array addObject:tempManufacturerDictionary];
        //        }
        [array addObject:tempManufacturerDictionary];
    }
    NSLog(@"result %@",array);
    NSMutableDictionary*dict=[[NSMutableDictionary alloc]init];
    [dict setObject:array forKey:@"gallery"];
    arGalleryDetails = [dict objectForKey:@"gallery"];
    }
    [self setUpScroll];
    
    
    
}

@end
