//
//  PDInappHomeViewController.h
//  Visiting Ashland
//
//  Created by swaroop on 01/10/15.
//  Copyright © 2015 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBOperations.h"
@protocol PDInappHomeViewController <NSObject>
@required
- (void) exitedFromApp;
@end
// Protocol Definition ends here



 // Instance method

//<UITabBarControllerDelegate,DBOperationsDelegate>
@interface PDInappHomeViewController : UIViewController<UITabBarControllerDelegate,UITableViewDataSource, UITableViewDelegate,DBOperationsDelegate,CLLocationManagerDelegate>
{
    id <PDInappHomeViewController> _delegate;
    IBOutlet UIView *viewAbout;
    IBOutlet UIView *vwLoading;
    IBOutlet UILabel *titlLabel;
    IBOutlet UILabel *titlLabel2;
    IBOutlet UIButton *btnTapToRetry;
    IBOutlet UIActivityIndicatorView *activityIndicator1;
    
    UIActivityIndicatorView *activityIndicatorNew;
    UILabel *noAppAvailableLbl;
    NSInteger selectedCell;
    NSString *mainWebServiceParam;
    NSString *mainWebServiceParamloc;
    NSString *classIds;
    NSString *distUnit;
    BOOL callMainService;
    
    UIView *backgrounViewForToolTip;
    IBOutlet UIImageView *backgroundImgv;
    IBOutlet UIButton *settingsBtn;
    IBOutlet AsyncImageView * splashImgv;
    BOOL isOneGuide;
    
    BOOL isSingleApp;
    
    BOOL yThis;
    
    UIView *sampleView;
    
    NSArray *originalData;
    NSMutableArray *searchData;
    
    UISearchBar *searchBar;
    NSMutableArray *searchArray;
    UISearchDisplayController *searchController;
    
   // UIButton *downloadButton;
}
@property (nonatomic, retain) CLLocationManager *locationManager;

@property (nonatomic)BOOL toSettings;

-(IBAction)settingsAction:(id)sender;
-(void)initWithWhite;
-(void)removeLoaderView;
@property (nonatomic,strong) id delegate;
@end
