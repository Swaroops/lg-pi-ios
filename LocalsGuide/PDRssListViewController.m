

#import "PDRssListViewController.h"
#import "PDRssCell.h"
#import "Reachability.h"


@interface PDRssListViewController () <CLLocationManagerDelegate>
{
    NSMutableArray *arrRssFeeds;
    NSMutableDictionary *dictFeed;
    int banery;
    BOOL isHotSpotAd;
    int scrollHeight;
}
@end

@implementation PDRssListViewController

@synthesize itemsToDisplay;
@synthesize isFromHome;

CLLocation *location_updated;
NSString *rssFeedUrl, *adLink;
NSDictionary *dicHe;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(BOOL)reachable_rss {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if(![self reachable_rss]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertView show];
        return;
    }
    titlLabel.text = [dicHe objectForKey:@"name"];
    self.navigationController.navigationBarHidden = YES;
    [self.view addSubview:vwLoading];
    //code change by Ram for changing GAD y position
    if (IS_IPHONE5) {
        //banery = 470;
        banery=515;
    }
    else {
        //banery = 382;
        banery=427;
    }
    
    //Code change by ram for tblvw height if no ad
    CGRect frame=_tableView.frame;
    frame.origin.y=67;
    frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-21;
    
   // frame.origin.y=65;
       _tableView.frame = frame;
    
    formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterShortStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	parsedItems = [[NSMutableArray alloc] init];
	self.itemsToDisplay = [NSArray array];
	
    NSURL *feedURL = [NSURL URLWithString:rssFeedUrl];
	feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
	feedParser.delegate = self;
	feedParser.feedParseType = ParseTypeFull; // Parse feed info and all items
	feedParser.connectionType = ConnectionTypeAsynchronously;
	[feedParser parse];
    
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
      if([self reachable_rss])
    if(![removAdStatus isEqualToString:@"purchased"])
        [self showAd];
    // Do any additional setup after loading the view from its nib.
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    NSLog(@"updated coordinate are %@",location_updated);
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_rss])
    if(![removAdStatus isEqualToString:@"purchased"])
   [self showAd];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_rss])
   if(![removAdStatus isEqualToString:@"purchased"])
       [self showAd];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
        [self.view addSubview:ad1Image];
        UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        ad1Btn.frame = CGRectMake(0, banery, 320, 50);
        if (IS_IPHONE5)
            scrollHeight = 501;
        else
            scrollHeight = 346;
        self.tableView.frame = CGRectMake(0, 67, 320, scrollHeight);
        [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:ad1Btn];
        
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        //titlLabel.text = [dictHotspot objectForKey:@"app_title"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_listing"] isEqualToString:@"FALSE"])) {
            if (IS_IPHONE5)
                scrollHeight = 501;
            else
                scrollHeight = 346;
            self.tableView.frame = CGRectMake(0, 67, 320, scrollHeight);
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
            adLink = [dict objectForKey:@"link_listing"];
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            if(![[dict objectForKey:@"link_2"]isEqualToString:@"NA"]){
            if (IS_IPHONE5)
                scrollHeight = 501;
            else
                scrollHeight = 346;
            self.tableView.frame = CGRectMake(0, 67, 320, scrollHeight);
            //titlLabel.text = [dict objectForKey:@"app_title"];
            ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_2"]];
            adLink = [dict objectForKey:@"link_2"];
            }
            else{
                if (IS_IPHONE5)
                    scrollHeight = 501;
                
                else
                    scrollHeight = 410;
                self.tableView.frame = CGRectMake(0, 67, 320, scrollHeight);
            }
        }
    }
}
#pragma mark - Setting ad
// Set banner view with google or custom ad
-(void)setAdbannerWithKey:(NSString *) key
{
    NSLog(@"%@",key);
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (IS_IPHONE5)
        scrollHeight = 501;
    else
        scrollHeight = 346;
    self.tableView.frame = CGRectMake(0, 67, 320, scrollHeight);
}
//Loading custom ad in webview
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}
-(void)getDictionary:(NSDictionary *) dict
{
    rssFeedUrl = [dict objectForKey:@"rss_url"];
    dicHe = dict;
}
#pragma mark -
#pragma mark Parsing

// Reset and reparse
- (void)refresh {
	self.title = @"Refreshing...";
	[parsedItems removeAllObjects];
	[feedParser stopParsing];
	[feedParser parse];
}

- (void)updateTableWithParsedItems {
	self.itemsToDisplay = [parsedItems sortedArrayUsingDescriptors:
						   [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"date"
                                                                                ascending:NO]]];
	[self.tableView reloadData];
    [vwLoading removeFromSuperview];
}

#pragma mark -
#pragma mark MWFeedParserDelegate

- (void)feedParserDidStart:(MWFeedParser *)parser {
	NSLog(@"Started Parsing: %@", parser.url);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
	NSLog(@"Parsed Feed Info: “%@”", info.title);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
	NSLog(@"Parsed Feed Item: “%@”", item.title);
	if (item) [parsedItems addObject:item];
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
	NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
    [self updateTableWithParsedItems];
}

- (void)feedParser:(MWFeedParser *)parser didFailWithError:(NSError *)error {
	NSLog(@"Finished Parsing With Error: %@", error);
    if (parsedItems.count == 0) {
        self.title = @"Failed"; // Show failed message in title
    } else {
        // Failed but some items parsed, so show and inform of error
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Parsing Incomplete"
                                                        message:@"There was an error during the parsing of this feed. Not all of the feed items could parsed."
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
    }
    [self updateTableWithParsedItems];
}

#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return itemsToDisplay.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    PDRssCell *cell = (PDRssCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDRssCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
	// Configure the cell.
	MWFeedItem *item = [itemsToDisplay objectAtIndex:indexPath.row];
	if (item) {
		
		// Process

		NSString *itemTitle = item.title ? [item.title stringByConvertingHTMLToPlainText] : @"[No Title]";
		NSString *itemSummary = item.summary ? [item.summary stringByConvertingHTMLToPlainText] : @"[No Summary]";
		NSString *itemDate = item.date ? [formatter stringFromDate:item.date] : @"No date";

        cell.lblName.text = itemTitle;
        cell.lblPlace.text = itemSummary;
        cell.lblTime.text = itemDate;
        
		
	}
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	MWFeedItem *item = [itemsToDisplay objectAtIndex:indexPath.row];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.link]];
}

-(IBAction)onBack:(id)sender
{
    if (isFromHome) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        PDViewController *viewController = [[PDViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:NO];
    }
}

@end
