

#import <UIKit/UIKit.h>

@interface PDDealsCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblName;
@property (nonatomic, retain) IBOutlet UILabel *lblPhone;
@property (nonatomic, retain) IBOutlet UILabel *lblDesc;
@property (nonatomic, retain) IBOutlet UIImageView *imgDeal;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@end
