//
//  Categories.m
//  Visiting Ashland
//
//  Created by swaroop on 10/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "Categories.h"


@implementation Categories

@dynamic catId;
@dynamic name;
@dynamic paid;
@dynamic points;
@dynamic type;
@dynamic googletype;
@dynamic external_url;
@dynamic rss_url;
@dynamic image;
@dynamic listing_icon;
@dynamic sub_exists;
@dynamic ad;
@dynamic link;
@dynamic appid;
//@dynamic subcategory;
@dynamic subcategory;
@dynamic header_image;



-(NSString*)getAppid{
    
    if (self.appid== nil || self.appid==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.appid;
    }
    
}
-(NSString*)getCatID{
    
    if (self.catId== nil || self.catId==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.catId;
    }
    
}
-(NSString*)getName{
    
    if (self.name== nil || self.name==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.name;
    }
    
}

-(NSString*)getPaid{
    
    if (self.paid== nil || self.paid==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.paid;
    }
    
}

-(NSString*)getPoints{
    
    if (self.points== nil || self.points==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.points;
    }
    
}

-(NSString*)getType{
    
    if (self.type== nil || self.type==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.type;
    }
    
}

-(NSString*)getGoogleType{
    
    if (self.googletype== nil || self.googletype==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.googletype;
    }
    
}

-(NSString*)getExternalUrl{
    
    if (self.external_url== nil || self.external_url==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.external_url;
    }
    
}

-(NSString*)getRssUrl{
    
    if (self.rss_url== nil || self.rss_url==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.rss_url;
    }
    
}

-(NSString*)getImage{
    
    if (self.image== nil || self.image==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.image;
    }
    
}

-(NSString*)getListingIcon{
    
    if (self.listing_icon== nil || self.listing_icon==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.listing_icon;
    }
    
}

-(NSString*)getSubExists{
    
    if (self.sub_exists== nil || self.sub_exists==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.sub_exists;
    }
    
}

-(NSString*)getAd{
    
    if (self.ad== nil || self.ad==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.ad;
    }
    
}

-(NSString*)getLink{
    
    if (self.link== nil || self.link==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.link;
    }
    
}


-(NSString*)getheader_image{
    
    if (self.header_image== nil || self.header_image==(id)[NSNull null]) {
        return @"";
    }
    else{
        
        return self.header_image;
    }
    
}

//-(NSString*)getfav{
//    
//    if (self.fav== nil || self.fav==(id)[NSNull null]) {
//        return @"";
//    }
//    else{
//        
//        return self.fav;
//    }
//    
//}


-(id)getSubcategory{
    if (self.subcategory== nil || self.subcategory==(id)[NSNull null]) {
        return [NSArray array];
    }
    else{
        
        return self.subcategory;
    }

}
@end
