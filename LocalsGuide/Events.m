//
//  Events.m
//  Visiting Ashland
//
//  Created by swaroop on 31/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "Events.h"


@implementation Events

@dynamic appid;
@dynamic event_id;
@dynamic image;
@dynamic featured;
@dynamic event_title;
@dynamic event_time;
@dynamic event_address;
@dynamic event_description;
@dynamic thumb;
@dynamic event_continue;
@dynamic event_start_date;
@dynamic event_expire_date;
@dynamic event_expiration;

@end
