//
//  Businesses.m
//  Visiting Ashland
//
//  Created by swaroop on 13/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "Businesses.h"


@implementation Businesses

@dynamic busId;
@dynamic name;
@dynamic cat_id;
@dynamic cat_id2;
@dynamic cat_id3;
@dynamic cat_id4;
@dynamic cat_id5;
@dynamic cat_id6;
@dynamic cat_id7;
@dynamic cat_id8;
@dynamic cat_id9;
@dynamic cat_id10;

@dynamic address;
@dynamic appid;

@dynamic preview;
@dynamic details;
@dynamic phone;
@dynamic website;
@dynamic website_text;
@dynamic latitude;
@dynamic longitude;
@dynamic featured;
@dynamic star_rating;
@dynamic image;
@dynamic type;
@dynamic thumb;
@dynamic weight;
@dynamic add_to_favorites;
@dynamic share_listing;
@dynamic fixed_order;
@dynamic show_call;
@dynamic show_web;
@dynamic email_address;
@dynamic show_email;
@dynamic email_text;
@dynamic disp_address;

@dynamic guide_id;
@dynamic guide_name;

@end
