//
//  Constants.h
//  LocalsGuide
//
//  Created by swaroop on 31/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.


#ifndef LocalsGuide_Constants_h
#define LocalsGuide_Constants_h



#import "GADBannerView.h"
#import "WebService.h"
//com.localsguide.ashland
#define IS_IPHONE5 ([[UIScreen mainScreen] bounds].size.height == 568)?YES:NO

#define IS_IPAD UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad
#define IS_IPHONE_4_OR_LESS ([[UIScreen mainScreen] bounds].size.height  < 568.0)?YES:NO
#define IS_IPADD ([[UIScreen mainScreen] bounds].size.height == 512)

#define GOOGLE_API_KEY @"AIzaSyDrpiZsKPwltEANarG4isu6PhVTMl-Wo60"
#define IS_OS_8_OR_LATER ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)

#define FOOTER_WEBSERVICE @"http://insightto.com/webservice/footer_ios.php?id="
#define MAP_WEBSERVICE @"http://insightto.com/webservice/mapcategory.php?id="

#define HOM_WEBSERVICE @"http://insightto.com/webservice/maincategory_ios.php?id="

#define HOM_WEBSERVICE_REFRESH @"http://insightto.com/webservice/maincategory_ios.php?id="

//#define HOM_WEBSERVICE @"http://insightto.com/webservice/maincategory.php?id="

//#define HOM_WEBSERVICE_REFRESH @"http://insightto.com/webservice/maincategory.php?id="

#define SEARCH_URL @"http://insightto.com/wsipad/search_listing.php?"

#define APP_LIST_DETAILS @"http://insightto.com/webservice/guides.php?"
#define SETTINGS_WEBSERVICE_SUB @"http://insightto.com/webservice/settings.php?id="

#define HOT_SPOT_WEBSERVICE @"http://insightto.com/webservice/hotspots_new.php?id="
#define LIST_URL @"http://insightto.com/webservice/fixedcategory_ios.php?id="
#define STATIC_URL @"http://insightto.com/webservice/staticpage.php?id="
#define BUSINES_URL @"http://insightto.com/webservice/business.php?id="
#define DEALS_URL @"http://insightto.com/webservice/deallisting.php?id="
#define EVENTS_URL @"http://insightto.com/webservice/eventlistings.php?id="
#define EVENTS_NEW @"http://insightto.com/webservice/eventlistings_new.php?id="

#define SUB_CAT_URL @"http://insightto.com/webservice/subcategory.php?device=hdpi&id="
                      
#define RELOC_WEBSERVICE @"http://insightto.com/webservice/form.php?id="
#define FORM_SUBMIT_WEBSERVICE @"http://insightto.com/webservice/form_submit.php?id="
#define GALLERY_WEBSERVICE @"http://insightto.com/webservice/gallery.php?id="
#define GALLERY_WEBSERVICE_NEW @"http://insightto.com/webservice/gallery_new.php?id="
#define OFFLINE_WEBSERVICE @"http://insightto.com/webservice/offline_android.php?id="

#define SPLASH_WEBSERVICE @"http://insightto.com/webservice/splash.php?id="

#define EASY_SPLASH @"http://insightto.com/splash/"

#define kTableViewHeaderHeight 120.0

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
//[NSString stringWithFormat:@"%@%@&device=device_type",DEALS_URL,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]
#endif
