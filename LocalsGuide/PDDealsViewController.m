
#import "PDDealsViewController.h"
#import "PDViewController.h"
#import "PDEventsViewController.h"
#import "PDMapViewController.h"
#import "PDDealDetailViewController.h"
#import "PDDealsCell.h"
#import "PDAppDelegate.h"
#import "Deals.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "GMDCircleLoader.h"
#import "SDImageCache.h"
#import "SDWebImageDownloader.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface PDDealsViewController () <CLLocationManagerDelegate>
{
    int banery;
    NSArray *arrayResponse;
    BOOL isHotSpotAd;
    
     UIRefreshControl *refreshControl;
    
    UIButton *buttonImg;
    UIImage *btnImage;
    UIImage *btnImageSel;
    BOOL isFavorite;
}
@end

@implementation PDDealsViewController
CLLocation *location_updated;
NSString *adLink;
NSDictionary *dict;
NSDictionary *dictDetailsDeals;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)offlineAvailable
{
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}

-(BOOL)reachable_deals {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
- (void)testRefresh:(UIRefreshControl *)refreshControlH
{
    if([self reachable_deals])
            [self startWebService];
        else
             [refreshControl endRefreshing];
        
    
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tabBarController.tabBar setHidden:YES];
    
    NSUInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
//    if(currentIndex==0)
//        backBtn.hidden=YES;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    [tblDeals addSubview:refreshControl];
    
    
    self.navigationController.navigationBarHidden = YES;
    //code change by Ram for changing GAD y position
    if (IS_IPHONE5) {
        //banery = 470;
        banery=515;
    }
    else {
        //banery = 382;
        banery=427;
    }
    
    //Code change by ram for tblvw height if no ad
    CGRect frame=tblDeals.frame;
    frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-2;
    tblDeals.frame = frame;
    
    
     self.tabBarItem.badgeValue = 0;
    titleLabel.text = self.tabBarItem.title;
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    indiProgress.hidden = NO;
    if([self reachable_deals]){
    [self startWebService];
        
    }
    else{
        [self coreDataCodeRead_deals];
    }
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if ([self reachable_deals])
    if(![removAdStatus isEqualToString:@"purchased"])
        [self showAd];
    
    
    if(![self reachable_deals])
    {
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"offlineresultArray"];
       // NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        //NSDictionary* offData = [[NSDictionary alloc] initWithDictionary:retrievedDictionary];
        
        //NSDictionary* offData=[[NSUserDefaults standardUserDefaults]objectForKey:@"offlineresultArray"];
        NSMutableArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        
        NSDictionary* offData= [retrievedDictionary objectAtIndex:1];
        
        
        NSDictionary * bannerdict= [[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"];
        
        NSLog(@"bannerdict : %@",bannerdict);
        
        
        NSLog(@"offData : %@",offData);
        
        NSString*detailid = [dictDetailsDeals objectForKey:@"Id"];
        
        for(NSDictionary *dictDetailss in offData)
        {
            if([detailid isEqualToString:[NSString stringWithFormat:@"%@",[dictDetailss objectForKey:@"Id"]]])
            {
                
                if(![[dictDetailss objectForKey:@"fav_icon"] isEqualToString:@""])
                {
                    if([[dictDetailss objectForKey:@"fav_icon"] isEqualToString:@"heart"])
                    {
                        btnImage= [UIImage imageNamed:@"1.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"2.png"];
                        
                    }
                    
                    else
                    {
                        btnImage= [UIImage imageNamed:@"star.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                    }
                }
                else
                {
                    //test
                    // str =@"star";
                    //
                    NSString * str=[[NSUserDefaults standardUserDefaults]stringForKey:@"fav_icon"];
                    
                    if([str isEqualToString:@"star"])
                    {
                        btnImage= [UIImage imageNamed:@"star.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                        
                    }
                    else
                    {
                        btnImage= [UIImage imageNamed:@"1.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"2.png"];
                        
                        
                    }
                }
                
                
                
            }
            
        }
        
        
        
        
    }

    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self.tabBarController.tabBar setHidden:YES];
    
    {
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
        }
        
        if ([myMutableArrayAgain count] == 0) {
            isFavorite = NO;
        }
        for (NSDictionary *dict in myMutableArrayAgain) {
            if ([[dict objectForKey:@"Id"] isEqualToString:[dict objectForKey:@"Id"]]&&[[dict objectForKey:@"deal_title"] isEqualToString:[dict objectForKey:@"deal_title"]]) {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
        }
        if (isFavorite) {
            [buttonImg setImage:btnImageSel forState:UIControlStateNormal];
        }
        else {
            [buttonImg setImage:btnImage forState:UIControlStateNormal];
        }
    }

    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{ 
    location_updated = [locations lastObject];
    //NSLog(@"updated coordinate are %@",location_updated);
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if ([self reachable_deals])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if ([self reachable_deals])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
//        [self.view addSubview:ad1Image];
        UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        ad1Btn.frame = CGRectMake(0, banery, 320, 50);
        [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
       // [self.view addSubview:ad1Btn];
        
        if (isHotSpotAd) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            if (![[dict objectForKey:@"image_listing"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
                adLink = [dict objectForKey:@"link_listing"];
                [self.view addSubview:ad1Image];
                [self.view addSubview:ad1Btn];
                int scrollHeight;
                if (IS_IPHONE5)
                    scrollHeight = 400;
                else
                    scrollHeight = 311;
                tblDeals.frame = CGRectMake(0, 48, 320, scrollHeight);
            }
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            if (![[dict objectForKey:@"ad_2"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_2"]];
                adLink = [dict objectForKey:@"link_2"];
                [self.view addSubview:ad1Image];
                [self.view addSubview:ad1Btn];
                int scrollHeight;
                if (IS_IPHONE5)
                    scrollHeight = 400;
                else
                    scrollHeight = 311;
                tblDeals.frame = CGRectMake(0, 48, 320, scrollHeight);
            }
        }
    }
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}
-(void)setAdbannerWithKey:(NSString *) key
{
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    int scrollHeight;
    if (IS_IPHONE5)
        scrollHeight = 400;
    else
        scrollHeight = 311;
    tblDeals.frame = CGRectMake(0, 48, 320, scrollHeight);
}

#pragma mark -  Loader
UIView *overlaay;
-(void)loaderStartWithText :(NSString *)loaderText
{
    
    
    overlaay =[[UIView alloc]initWithFrame:self.view.window.rootViewController.view.bounds];
    overlaay.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    overlaay.tag = 1001;
    [overlaay setUserInteractionEnabled:YES];
    [GMDCircleLoader setOnView:overlaay withTitle:loaderText animated:YES];
    [self.view.window.rootViewController.view addSubview:overlaay];
    // [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self  selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
}

/*-(void) callAfterSixtySecond:(NSTimer*) t
 {
 NSLog(@"red");
 
 [GMDCircleLoader hideFromView:overlaay animated:YES];
 [overlaay removeFromSuperview];
 }
 */

-(void)loaderStop
{
    
    
    [GMDCircleLoader hideFromView:overlaay animated:YES];
    [overlaay removeFromSuperview];
}
#pragma mark - Web service
-(void)startWebService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@&device=device_type",DEALS_URL,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}
-(void)webServiceResponse:(NSData *) responseData
{
    [refreshControl endRefreshing];
    
    NSError *er;
    dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    arrayResponse=[[NSArray alloc]init];
    arrayResponse = [dict objectForKey:@"business"];
    if([arrayResponse count]>0)
        [self coreDataCodeWrite_deals:arrayResponse];
    NSDictionary *dicLatest = [arrayResponse objectAtIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:[dicLatest objectForKey:@"Id"] forKey:@"maxDealId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [tblDeals reloadData];
    indiProgress.hidden = YES;
}

-(void)getDictionarydeals:(NSDictionary *) dictHere
{
    dictDetailsDeals = dictHere;
}

#pragma mark - alert retry
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        //[self startWebService];
    }
}
#pragma mark - UITableView delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayResponse count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    PDDealsCell *cell = (PDDealsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDDealsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSDictionary *dict = [arrayResponse objectAtIndex:indexPath.row];
    cell.lblName.text = [dict objectForKey:@"deal_title"];
    cell.lblDesc.text = [dict objectForKey:@"deal_subtitle"];
    cell.lblPhone.text = [dict objectForKey:@"deal_phone"];
  //  if ([self reachable_deals]) {
   //      cell.imgDeal.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
   // }
   // else{
    
    tableView.backgroundColor=[UIColor whiteColor];
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];

    
    cell.imgDeal.image=[imageCache imageFromDiskCacheForKey:[dict objectForKey:@"thumb"]];

   if (!cell.imgDeal.image)
    {
//        if ([self reachable_deals]) {
//            
//          
//            [cell.imgDeal sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
//       
//          
//            [cell.imgDeal setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
//                NSLog(@"success");
//                
//                cell.imgDeal.image=imageX;
//                 [[SDImageCache sharedImageCache] storeImage:imageX forKey:[dict objectForKey:@"thumb"] completion:^{  }];
//                
//              
//                
//            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                               }
//             ];
//            
//             cell.imgDeal.image = [UIImage imageNamed:@"noimage.png"];
//        }
        
        NSString *str = [dict objectForKey:@"thumb"];
        
    if(str)
        
    {
            
        if (![self offlineAvailable])
        {
            
            
            
            [cell.imgDeal sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
            cell.imgDeal.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
            
        }
        
        else
            
        {
            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
            
            NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
            
            
            
            
            NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
            
            NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit);
            NSLog(@"secondBit folder :%@",secondBit2);
            
            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            NSLog(@"str url :%@",str);
            
            NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/deal_images/thumb",secondBit2]];
            
            NSArray* str3 = [str componentsSeparatedByString:@"deal_images/thumb/"];
            
            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit3);
            NSLog(@"secondBit folder :%@",secondBit3);
            
            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
            
            UIImage* image = [UIImage imageWithContentsOfFile:Path];
            
            cell.imgDeal.image=image;
            
            // [self loadImageFromURL1:str image:tblHeaderImage];
            
            // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
            
            
        }
        
    }
        else
            cell.imgDeal.image = [UIImage imageNamed:@"noimage.png"];
    }
  
    cell.callBtn.tag=indexPath.row;
    
    cell.imgDeal.layer.cornerRadius = 6.0;
    cell.imgDeal.layer.masksToBounds = YES;
    
            [cell.callBtn addTarget:self action:@selector(onCall:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
//    buttonImg = (UIButton *)[cell.contentView.subviews objectAtIndex:0];
//    
//    buttonImg =[[UIButton alloc]init];
//    UIImage *btnImage = [UIImage imageNamed:@"1.png"];
//    
//    buttonImg = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    
//    buttonImg.frame = CGRectMake(10, 70, 25, 25);
//    
//    
//    [buttonImg setBackgroundImage:btnImage forState:UIControlStateNormal];
//    [self->buttonImg addTarget:self action:@selector(onFavbtnList:) forControlEvents:UIControlEventTouchUpInside];
//    
//    buttonImg.userInteractionEnabled = YES;
//    
//    buttonImg.tag = indexPath.row;
//    
//    NSLog(@"buttonImg tag vc: %ld",(long)buttonImg.tag);
//    
//    [cell.contentView addSubview:buttonImg];
    
    buttonImg = (UIButton *)[cell.contentView.subviews objectAtIndex:0];
    
    buttonImg =[[UIButton alloc]init];
    
    btnImage = [UIImage imageNamed:@"1.png"];
    
    btnImageSel = [UIImage imageNamed:@"2.png"];
    
    NSString * str=[[NSUserDefaults standardUserDefaults]stringForKey:@"fav_icon"];
    
    NSLog(@"str fav_icon : %@",str);
    
    if([self reachable_deals])
    {
    
    if(![[dictDetailsDeals objectForKey:@"fav_icon"] isEqualToString:@""])
    {
        if([[dictDetailsDeals objectForKey:@"fav_icon"] isEqualToString:@"heart"])
        {
            btnImage= [UIImage imageNamed:@"1.png"];
            
            btnImageSel= [UIImage imageNamed:@"2.png"];
            
        }
        
        else
        {
            btnImage= [UIImage imageNamed:@"star.png"];
            
            btnImageSel= [UIImage imageNamed:@"star_sel.png"];
        }
    }
    else
    {
        //test
        // str =@"star";
        //
        if([str isEqualToString:@"star"])
        {
            btnImage= [UIImage imageNamed:@"star.png"];
            
            btnImageSel= [UIImage imageNamed:@"star_sel.png"];
            
        }
        else
        {
            btnImage= [UIImage imageNamed:@"1.png"];
            
            btnImageSel= [UIImage imageNamed:@"2.png"];
            
            
        }
    }

    }
    
    buttonImg = [UIButton buttonWithType:UIButtonTypeCustom];
    
    buttonImg.frame = CGRectMake(10, 45, 50, 50);
    
    // [buttonImg setTitleEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
    //top ,left , bottom ,right
    [buttonImg setImageEdgeInsets:UIEdgeInsetsMake(20, 5, 5, 20)];
    
    [[buttonImg imageView] setContentMode: UIViewContentModeScaleAspectFit];
    
    /////
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
    }
    
    if ([myMutableArrayAgain count] == 0) {
        isFavorite = NO;
    }
    for (dict in myMutableArrayAgain) {
        if ([[dict objectForKey:@"Id"] isEqualToString:[dict objectForKey:@"Id"]]&&[[dict objectForKey:@"deal_title"] isEqualToString:[dict objectForKey:@"deal_title"]]) {
            NSLog(@"these are same man!!!");
            isFavorite = YES;
            break;
        }
        else {
            isFavorite = NO;
        }
    }
    
    if (isFavorite) {
        [buttonImg setImage:btnImageSel forState:UIControlStateNormal];
    }
    else {
        [buttonImg setImage:btnImage forState:UIControlStateNormal];
    }
    
    /////
    
    //  [buttonImg setBackgroundImage:btnImage forState:UIControlStateNormal];
    [self->buttonImg addTarget:self action:@selector(onFavbtnList:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonImg.userInteractionEnabled = YES;
    
    buttonImg.tag = indexPath.row;
    
    NSLog(@"buttonImg tag vc: %ld",(long)buttonImg.tag);
    
    [cell.contentView addSubview:buttonImg];
    
    return cell;
}

-(void)onFavbtnList:(UIButton*)sender
{
    
    
    
//    NSLog(@"sender tag : %ld",(long)sender.tag);
//    
//    // if (sender.tag == 0)
//    {
//        NSLog(@"buttonImg tag : %ld",(long)buttonImg.tag);
//        
//        UIImage *btnImage2 = [UIImage imageNamed:@"2.png"];
//        NSMutableDictionary *dict3 = [arrayResponse objectAtIndex:sender.tag];
//        [sender setBackgroundImage:btnImage2 forState:UIControlStateNormal];
//        PDDealDetailViewController *dVC = [[PDDealDetailViewController alloc]init];
//        [dVC getDealDetails:dict3];
//        // [eVC getArray:arraySmpl :sender.tag];
//        [dVC onFavorite:sender];
//    }
    
    {
        
        
        
        NSLog(@"sender tag : %ld",(long)sender.tag);
        
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
        }
        
        if ([myMutableArrayAgain count] == 0) {
            isFavorite = NO;
        }
        
        NSDictionary *dict = [arrayResponse objectAtIndex:sender.tag];
        
        for (dict in myMutableArrayAgain) {
            if ([[dict objectForKey:@"Id"] isEqualToString:[dict objectForKey:@"Id"]]&&[[dict objectForKey:@"deal_title"] isEqualToString:[dict objectForKey:@"deal_title"]]) {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
        }
        
        
        if (!isFavorite)
        {
            
            isFavorite = YES;
            
            
                    UIImage *btnImage2 = [UIImage imageNamed:@"2.png"];
                    NSMutableDictionary *dict3 = [arrayResponse objectAtIndex:sender.tag];
                    [sender setImage:btnImageSel forState:UIControlStateNormal];
                    PDDealDetailViewController *dVC = [[PDDealDetailViewController alloc]init];
                    [dVC getDealDetails:dict3];
                    // [eVC getArray:arraySmpl :sender.tag];
                    [dVC onFavorite:sender];
        }
        
        else
        {
            isFavorite = NO;
            NSLog(@"buttonImg tag : %ld",(long)buttonImg.tag);
            
            UIImage *btnImage2 = [UIImage imageNamed:@"1.png"];
            NSMutableDictionary *dict3 = [arrayResponse objectAtIndex:sender.tag];
            [sender setImage:btnImage forState:UIControlStateNormal];
            PDDealDetailViewController *dVC = [[PDDealDetailViewController alloc]init];
            [dVC getDealDetails:dict3];
            // [eVC getArray:arraySmpl :sender.tag];
            //[eVC onFavorite:sender];
            [dVC onClick:sender];
        }
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PDDealDetailViewController *dealDetailViewCpntroller = [[PDDealDetailViewController alloc] init];
    [dealDetailViewCpntroller getDealDetails:[arrayResponse objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:dealDetailViewCpntroller animated:YES];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
-(IBAction)onCall:(id)sender{
    NSString*phoneNumber=[[arrayResponse objectAtIndex:[sender tag]]objectForKey:@"deal_phone"];
     phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *dialNumber = [@"telprompt://" stringByAppendingString:phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:dialNumber]];
}

#pragma mark - Button actions

-(IBAction)onFavorite:(id)sender
{
    
}
-(IBAction)onBack:(id)sender
{
  /*  PDViewController *viewController = [[PDViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:NO];*/
    
    NSUInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
    
   /* if(currentIndex==0)
    {
        [self.tabBarController setSelectedIndex:0];
//        PDViewController *viewController = [[PDViewController alloc] init];
//        [self.navigationController pushViewController:viewController animated:NO];
        
        [self.navigationController popViewControllerAnimated:YES];
              //  backBtn.hidden=YES;
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
    */
    
    NSLog(@"title2 :%@",self.navigationItem.title);
    
    int i;
    
    for (i=0; i<[self.tabBarController.tabBar.items count]; i++)
    {
        
        NSLog(@"title2 :%@",[self.tabBarController.tabBar.items objectAtIndex:i].title );
        NSString *title =[self.tabBarController.tabBar.items objectAtIndex:i].title;
        if([title isEqualToString:@"Home"])
        {
            [self.tabBarController setSelectedIndex:i];
            break;
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)sdImageCaching:(NSString*)imageUrlKey;
{
    
    NSString *imageCacheFolder=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    SDImageCache *imageCache = [[SDImageCache alloc] initWithNamespace:imageCacheFolder];
    NSURL *urlimg=[NSURL URLWithString:imageUrlKey];
    
    
    
    [imageCache queryCacheOperationForKey:imageUrlKey done:^(UIImage *image,NSData *data , SDImageCacheType cacheType)               {
        if (image) {
            //  completedBlock(image,nil); // return block
        }else{
            
            /////////
            // *    Request Image
            /////////
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:urlimg
                                                                      options:SDWebImageProgressiveDownload
                                                                     progress:^(NSInteger receivedSize, NSInteger expectedSize,NSURL *url) {}
                                                                    completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                                        
                                                                        if (finished && image){
                                                                            
                                                                        

        
                                                                            [imageCache storeImage:image
                                                                                         imageData:data
                                                                                            forKey:imageUrlKey
                                                                                            toDisk:YES completion:^{  }];
                                                                            
                                                                            //  completedBlock(image,error); // return block
                                                                        }
                                                                    }];
                
            });
        }
    }];
}
-(void)coreDataCodeWrite_deals:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Deals" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"catId == %@",[[queArray objectAtIndex:0] objectForKey:@"Id"]];
    
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Deals *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    
    //    for(currentQUE.options in listOfQUEToBeDeleted){
    //
    //         [context1 deleteObject:currentQUE.options];
    //    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try {
        
        
        
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Deals"
                                    inManagedObjectContext:context];
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"deal_id"];
            //  [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"image"]forKey:@"image"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"featured"]forKey:@"featured"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_title"]forKey:@"deal_title"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_subtitle"]forKey:@"deal_subtitle"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_description"]forKey:@"deal_description"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_phone"]forKey:@"deal_phone"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_web"]forKey:@"deal_web"];
            //[Que setValue:[[queArray objectAtIndex:i] objectForKey:@"thumb"]forKey:@"thumb"];
            
            
            
            
            
            NSMutableArray*muts=[[NSMutableArray alloc]init];
            @try {
                
                
                for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
                    
                    NSString*str1=str;
                    // str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    [muts addObject:str1];
                  /*  if(str1.length>0)
                    {
                        [self insertImagesToDownload:str1];
                        [self sdImageCaching:str1];
                    }*/
                    
                    
                }
            }
            @catch (NSException *exception) {
                
            }
            
            [Que setValue:muts forKey:@"image"];
            // str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
           /* if(str.length>0)
            {
                [self insertImagesToDownload:str];
                [self sdImageCaching:str];
            }*/
            
            [Que setValue:str forKey:@"thumb"];
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                
            }
            
        }
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
}
-(void)coreDataCodeRead_deals{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Deals" inManagedObjectContext:context];
      fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
    int i;
    
    //    for (i=0; i<[dictCategory count]; i++) {
    //        //   NSString*categoryId=[[[[[queArray objectAtIndex:i]objectForKey:@"category"]objectForKey:@"text"]stringByReplacingOccurrencesOfString:@"\n" withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //        // NSDictionary* dictID=[arrResponse objectAtIndex:i];
    //        cateID=[dictCategory objectForKey:@"id"];
    //        NSPredicate * parentIdPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"category == '%@'",cateID]];
    //        if(![[dictSettings objectForKey:@"mode"] isEqualToString:@"no"]){
    //            NSPredicate * ModePredicate =[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"mode == '%@'", mode]];
    //            NSPredicate *compountPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[parentIdPredicate, ModePredicate]];
    //            [fetchRequest setPredicate:compountPredicate];
    //        }
    //        else{
    //            [fetchRequest setPredicate:parentIdPredicate];
    //        }
    
    //fetchRequest.sortDescriptors = @[descriptor];
    // [fetchRequest setPredicate:parentIdPredicate];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    
    NSLog( @"error %@",error);
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    //if([self reachable_deals])
    {
    for (Deals *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.deal_id forKey:@"Id"];
        
        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
        [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
         [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
         [tempManufacturerDictionary setObject:manuf.deal_title forKey:@"deal_title"];
         [tempManufacturerDictionary setObject:manuf.deal_subtitle forKey:@"deal_subtitle"];
         [tempManufacturerDictionary setObject:manuf.deal_description forKey:@"deal_description"];
         [tempManufacturerDictionary setObject:manuf.deal_phone forKey:@"deal_phone"];
          [tempManufacturerDictionary setObject:manuf.deal_web forKey:@"deal_web"];
        
        
        // Answer *dof=manuf.options;
        //            if([manuf.sub_exists isEqualToString:@"yes"]){
        //            Subcategory1*dof=manuf.subcategory;
        //            [tempManufacturerDictionary setObject:dof.catid forKey:@"Id"];
        //            [tempManufacturerDictionary setObject:dof.name forKey:@"name"];
        //            [tempManufacturerDictionary setObject:dof.googletype forKey:@"googletype"];
        //            [tempManufacturerDictionary setObject:dof.external_url forKey:@"external_url"];
        //            [tempManufacturerDictionary setObject:dof.rss_url forKey:@"rss_url"];
        //            [tempManufacturerDictionary setObject:dof.image forKey:@"image"];
        //            [tempManufacturerDictionary setObject:dof.listing_icon forKey:@"listing_icon"];
        //            [tempManufacturerDictionary setObject:dof.sub_exists forKey:@"sub_exists"];
        //            [tempManufacturerDictionary setObject:dof.ad forKey:@"ad"];
        //            [tempManufacturerDictionary setObject:dof.link forKey:@"link"];
        //            [tempManufacturerDictionary setObject:dof.subcategory forKey:@"subcategory"];
        //            }
        //
        ////            if(dof.ans1!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans1 forKey:@"ans1"];}
        ////            if(dof.ans2!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans2 forKey:@"ans2"];}
        ////            if(dof.ans3!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans3 forKey:@"ans3"];}
        ////            if(dof.ans4!=nil){
        ////                [tempManufacturerDictionary setObject:dof.ans4 forKey:@"ans4"];}
        //
        //
        //            [array addObject:tempManufacturerDictionary];
        //        }
        [array addObject:tempManufacturerDictionary];
    }
    NSLog(@"result %@",array);
    NSMutableDictionary*dict=[[NSMutableDictionary alloc]init];
    [dict setObject:array forKey:@"business"];
    arrayResponse = [dict objectForKey:@"business"];
    NSDictionary *dicLatest = [arrayResponse objectAtIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:[dicLatest objectForKey:@"Id"] forKey:@"maxDealId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    [tblDeals reloadData];
    indiProgress.hidden = YES;
    
    
    
}
@end
