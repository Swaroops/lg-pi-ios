#import "PDDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "PDAppDelegate.h"
#import "Businesses.h"
#import "Static.h"
#import <MessageUI/MessageUI.h>




#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface PDDetailViewController () <GADBannerViewDelegate, CLLocationManagerDelegate,UIScrollViewDelegate,MFMailComposeViewControllerDelegate>
{
    int banery;
    AsyncImageView *ad1Image;
    BOOL isFavorite;
    UIButton *ad1Btn;
    BOOL isHotSpotAd;
    NSDictionary *dictDetails;
    int scrollHeight;
    UIScrollView *scrollView;
    UIPageControl*pageControl;
    NSMutableArray*scrollArray;
    NSArray *allArray;
    long int indeX;
     NSDictionary *dictresp;
    UISlider *seekbar;
    AVAudioPlayer *audioPlayer;
    NSTimer *updateTimer;
    UIButton * playBtn;
    UIButton * lblMail;
    UILabel *lblAddr1 ;
    NSString* mailAddres;
    
    NSString *webAddress;
    
}
@end
@implementation PDDetailViewController
@synthesize playerWeb;
@synthesize lblNameLabel,lblNameLabelStr,isFromFavorite,isFromFav;
CLLocation *location_updated;
NSDictionary *dictItemDetails;
NSDictionary *dictStaticFav;

NSString *adLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(void)setStaticDictFromFavorite:(NSDictionary *) dict
{
    isFromFavorite = YES;
    dictStaticFav = dict;
    dictDetails = dict;
    dictItemDetails = dict;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favfrmList:) name:@"listfav" object:nil];
    
    //[[NSNotificationCenter defaultCenter]postNotificationName:@"listfav" object:nil];

    

    //code change by Ram for changing GAD y position
    if (IS_IPHONE5) {
        [[NSBundle mainBundle] loadNibNamed:@"PDDetailViewController" owner:self options:nil];
        //banery = 470;
         banery=515;
    }
    else {
        [[NSBundle mainBundle] loadNibNamed:@"PDDetailViewController4" owner:self options:nil];
        //banery = 382;
        banery=427;
    }
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [activityIndicator startAnimating];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    
    lblDistanceStatic.hidden = NO;
    lblDistanceBusiness.hidden = NO;
    btDir.hidden = NO;
    btDirSt.hidden = NO;
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable2])
    if(![removAdStatus isEqualToString:@"purchased"])
        [self showAd];
    
    lblNameLabel.text=lblNameLabelStr;
}

-(void)favfrmList : (NSNotification *)aNotification
{
    
//    if ([aNotification.name isEqualToString:@"TestNotification"])
//    {
//        NSDictionary* userInfo = aNotification.userInfo;
//        NSNumber* total = (NSNumber*)userInfo[@"total"];
//        NSLog (@"Successfully received test notification! %i", total.intValue);
//    }
//    
//    [self onFavorite:nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.playerWeb stringByEvaluatingJavaScriptFromString: @"document.querySelector('audio').pause();"];
}
-(void)viewWillAppear:(BOOL)animated {
    
    if(_isFromFrom)
    {
        [self setStaticDisplayWithDic:dictItemDetails];
        [vwLoading removeFromSuperview];
        return;
    }
        
        if (isFromFavorite) {
        isFavorite = YES;
        
        [self setStaticDisplayWithDic:dictStaticFav];
        return;
    }
   else {
        if(![self offlineAvailable]){
        [self startWebService];
        }
        else{
            
                 [self webServiceResponse:nil];
            
        }
    }
    NSMutableArray* myFooterArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
    NSLog(@"%@",self.tabBarItem.title);
    for (NSDictionary *dictionary in myFooterArray) {
        if ([[dictionary objectForKey:@"name"] isEqualToString:self.tabBarItem.title]) {
            NSLog(@"%@",dictionary);
            if ([[dictionary objectForKey:@"type"] isEqualToString:@"static"]) {
                btnBackSt.hidden = YES;
            }
        }
    }
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray* myMutableArrayAgain;
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
    }
    
    if ([myMutableArrayAgain count] == 0) {
        isFavorite = NO;
    }
    for (NSDictionary *dict in myMutableArrayAgain) {
        if (![dict objectForKey:@"geometry"]) {
            if ([[dict objectForKey:@"Id"] isEqualToString:[dictItemDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"name"] isEqualToString:[dictItemDetails objectForKey:@"name"]]) {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
        }
        else {
            if ([[dict objectForKey:@"geometry"] isEqualToDictionary:[dictItemDetails objectForKey:@"geometry"]]&&[[dict objectForKey:@"icon"] isEqualToString:[dictItemDetails objectForKey:@"icon"]]) {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
        }
        
    }
    if (isFromFavorite) {
        isFavorite = YES;
    }
    if (isFavorite) {
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starNoFav.png"] forState:UIControlStateNormal];
        [btnFavoritesStatic setBackgroundImage:[UIImage imageNamed:@"starNoFav.png"] forState:UIControlStateNormal];
    }
    else {
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
        [btnFavoritesStatic setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
    }
}
-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_detail"] isEqualToString:@"FALSE"])) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
            ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_detail"]];
            adLink = [dict objectForKey:@"link_detail"];
            [self.view addSubview:ad1Image];
            if (IS_IPHONE5)
                scrollHeight = 501;
            else
                scrollHeight = 311;
            scrollMainView.frame = CGRectMake(0, 67, 320, scrollHeight);
            ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
            ad1Btn.frame = CGRectMake(0, banery, 320, 50);
            [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:ad1Btn];
        }
        else {
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"CatAdExist"] isEqualToString:@"YES"])
            {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"CatAd"];
                if (![[dict objectForKey:@"imageLink"] isEqualToString:@"NA"]) {
                    ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                    ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"imageLink"]];
                    adLink = [dict objectForKey:@"webLink"];
                    [self.view addSubview:ad1Image];
                    ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                    [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:ad1Btn];
                    if (IS_IPHONE5)
                        scrollHeight = 501;
                    else
                        scrollHeight = 311;
                    scrollMainView.frame = CGRectMake(0, 67, 320, scrollHeight);
                }
            }
            else {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
                if (![[dict objectForKey:@"ad_3"] isEqualToString:@"NA"]) {
                    ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                    ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_3"]];
                    adLink = [dict objectForKey:@"link_3"];
                    [self.view addSubview:ad1Image];
                    ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                    [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:ad1Btn];
                    if (IS_IPHONE5)
                        scrollHeight = 501;
                    else
                        scrollHeight = 311;
                    scrollMainView.frame = CGRectMake(0, 67, 320, scrollHeight);
                }
            }
        }///448 /// 513
    }
    
    NSDictionary *homDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HomeResponse"];
    
    
    NSArray *classArr=[[NSArray alloc]init];
    classArr =[homDict objectForKey:@"maincategory"];
   
    for(NSDictionary *dictsub in classArr)
{

    if ([[dictsub objectForKey:@"ad"] isEqualToString:@"FALSE"])
    {
    
        scrollMainView.frame = CGRectMake(0, 67, 320, 501);
    }
        
}
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictItems in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictItems objectForKey:@"lat"] doubleValue] longitude:[[dictItems objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictItems objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictItems forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable2])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable2])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.rootViewController = self;
    bannerView_.delegate = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"]) {
        [vwStatic addSubview:bannerView_];
    }
    else {
        [self.view addSubview:bannerView_];
    }
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (IS_IPHONE5)
        scrollHeight = 501;
    else
        scrollHeight = 311;
    scrollMainView.frame = CGRectMake(0, 67, 320, scrollHeight);
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}

-(BOOL)reachable2 {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

-(BOOL)offlineAvailable
{
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}


#pragma mark - Instance methods
-(void)getDictionary:(NSDictionary *) dict
{
    dictItemDetails = dict;
   isFromFavorite = NO;
}
-(void)getArray:(NSArray *) Array :(long int) Index
{
    allArray=Array;
    indeX=Index;
//    dictItemDetails = dict;
//    isFromFavorite = NO;
}

-(IBAction)onDone:(id)sender
{
    [vwDirection removeFromSuperview];
}
#pragma mark - alert retry
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self startWebService];
    }
}
#pragma mark - Web service
-(void)startWebService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    googleLocType=false;
    NSString *urlString;
    if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"])
        urlString = [NSString stringWithFormat:@"%@%@",STATIC_URL,[dictItemDetails objectForKey:@"Id"]];
    else if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"fixed"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"location"])
        urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[dictItemDetails objectForKey:@"Id"]];
    else if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"normal"])
        urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[dictItemDetails objectForKey:@"Id"]];
    else
    {
        urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&sensor=true&key=%@",[dictItemDetails objectForKey:@"reference"],GOOGLE_API_KEY];
        googleLocType=true;
    }
    
    [webService startParsing:urlString];
    [self.view addSubview:vwLoading];
}
-(void)setStaticDisplayWithDic:(NSDictionary *) dictResult
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray* myMutableArrayAgain;
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
    }
    
    if ([myMutableArrayAgain count] == 0) {
        isFavorite = NO;
    }
    for (NSDictionary *dictARR in myMutableArrayAgain) {
        
        if ([[dictARR objectForKey:@"Id"] isEqualToString:[dictResult objectForKey:@"Id"]]) {
            NSLog(@"these are same man!!!");
           
            
            
         
            isFavorite = YES;
            break;
        }
        else {
            isFavorite = NO;
        }
    }
    if (isFavorite) {
        [btnFavoritesStatic setBackgroundImage:[UIImage imageNamed:@"starNoFav.png"] forState:UIControlStateNormal];
    }
    else {
        [btnFavoritesStatic setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
    }
    if ([[dictResult objectForKey:@"latitude"] isEqualToString:@"0"]) {
        btDirSt.hidden = YES;
    }
  //  UIImageView *img = [[UIImageView alloc] init];
   // img.contentMode = UIViewContentModeScaleToFill;
   // CALayer * l = [img layer];
  //  l.borderColor=[UIColor lightGrayColor];
    //[l setMasksToBounds:YES];
   // [l setBorderWidth:0.5];
   // img.clipsToBounds = YES;
    
//    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
//    SDImageCache *imageCache =[SDImageCache sharedImageCache];
//    imageCache = [imageCache initWithNamespace:imageCacheFolder];
//    img.image= [imageCache imageFromDiskCacheForKey:[dictResult objectForKey:@"image"]];
    
    
    
    NSMutableArray *arrays=[[NSMutableArray alloc]init];
    arrays=[dictResult objectForKey:@"image"];
   // NSArray*arrays=[dictResult objectForKey:@"image"];
       @try{
           
         if ([arrays count]==0)  {
               AsyncImageView *imgMainView = [[AsyncImageView alloc] init];
               imgMainView.contentMode = UIViewContentModeScaleToFill;
               imgMainView.clipsToBounds = YES;
               NSString*str=[arrays objectAtIndex:0];
               // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
               NSURL *url = [NSURL URLWithString:str];
               
               
               NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
               SDImageCache *imageCache =[SDImageCache sharedImageCache];
               imageCache = [imageCache initWithNamespace:imageCacheFolder];
               imgMainView.image= [imageCache imageFromDiskCacheForKey:str];
               
               if (!imgMainView.image)
               {
                   
                   if(![self offlineAvailable]){
                       
                       [imgMainView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                       
                       [imgMainView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                           NSLog(@"success");
                           
                           
                           
                           float imgWidth=scrollStaticView.frame.size.width;
                           float heightImage = (imageX.size.height*imgWidth)/imageX.size.width;
                           
                           
                           imgMainView.frame = CGRectMake(0, 0, imgWidth, heightImage);
                           
                           
                           
                           imgMainView.image=imageX;
                           [scrollStaticView addSubview:imgMainView];
                           [self setStaticWithDict:dictResult Image:imgMainView];
                           
                       } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                           UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                           float imgWidth=scrollStaticView.frame.size.width;
                           imgH.frame = CGRectMake(0, 0, imgWidth, imgWidth/2);
                           [self setWithDict:dictResult imageView:imgH];
                           [vwLoading removeFromSuperview];                    }
                        ];
                   }
                   else
                   {
                       UIImageView *imgH = [[UIImageView alloc] init];//WithImage:[UIImage imageNamed:@"noimage.png"]];
                       {
                           NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                           
                           NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                           
                           NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                           
                           
                           
                           
                           NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                           
                           NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                           
                           NSLog(@"firstBit folder :%@",firstBit);
                           NSLog(@"secondBit folder :%@",secondBit2);
                           
                           NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                           
                           NSLog(@"str url :%@",str);
                           
                           NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/static_images",secondBit2]];
                           
                           NSArray* str3 = [str componentsSeparatedByString:@"static_images/"];
                           
                           NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                           
                           NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                           
                           NSLog(@"firstBit folder :%@",firstBit3);
                           NSLog(@"secondBit folder :%@",secondBit3);
                           NSLog(@"decoded folder :%@",decoded);
                           
                           NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                           
                           UIImage* image = [UIImage imageWithContentsOfFile:Path];
                           
                           imgH.image=image;
                           
                                    // [self loadImageFromURL1:str image:tblHeaderImage];
                           
                           // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                           
                           
                       }
                       float imgWidth=scrollStaticView.frame.size.width;
                       imgH.frame = CGRectMake(0, 0, imgWidth, imgWidth/2);
                       [scrollStaticView addSubview:imgH];
                       
                       
                       [self setStaticWithDict:dictResult Image:imgH];
                       [vwLoading removeFromSuperview];
                       
                       if(!str)
                       {
                           UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                           float imgWidth=scrollStaticView.frame.size.width;
                           imgH.frame = CGRectMake(0, 0, imgWidth, imgWidth/2);
                           [scrollStaticView addSubview:imgH];
                           [self setWithDict:dictResult imageView:imgH];
                           [vwLoading removeFromSuperview];
                       }
                   }
               }
               else
               {
                   float imgWidth=scrollStaticView.frame.size.width;
                   float heightImage = (imgMainView.image.size.height*imgWidth)/imgMainView.image.size.width;
                   
                   
                   imgMainView.frame = CGRectMake(0, 0, imgWidth, heightImage);
                   
                   
                   [scrollStaticView addSubview:imgMainView];
                   [self setStaticWithDict:dictResult Image:imgMainView];
               }
               
               
               
               
               
               
               
           }
           
    if ([arrays count]==1 ) {
        AsyncImageView *imgMainView = [[AsyncImageView alloc] init];
        imgMainView.contentMode = UIViewContentModeScaleToFill;
        imgMainView.clipsToBounds = YES;
        NSString*str=[arrays objectAtIndex:0];
        // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:str];
        
        
        NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        imgMainView.image= [imageCache imageFromDiskCacheForKey:str];
        
        if (!imgMainView.image)
        {
            
            if(![self offlineAvailable]){
                
                [imgMainView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                
                [imgMainView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                    NSLog(@"success");
                    
                    
                    
                    float imgWidth=scrollStaticView.frame.size.width;
                    float heightImage = (imageX.size.height*imgWidth)/imageX.size.width;
                    
                    
                    imgMainView.frame = CGRectMake(0, 0, imgWidth, heightImage);
                   
                    

                    imgMainView.image=imageX;
                    [scrollStaticView addSubview:imgMainView];
                  [self setStaticWithDict:dictResult Image:imgMainView];
                    
                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                    UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                    float imgWidth=scrollStaticView.frame.size.width;
                    imgH.frame = CGRectMake(0, 0, imgWidth, imgWidth/2);
                    [self setWithDict:dictResult imageView:imgH];
                    [vwLoading removeFromSuperview];                    }
                 ];
            }
            else
            {
                UIImageView *imgH = [[UIImageView alloc] init];
                
                {
                    NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                    
                    NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                    
                    NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                    
                    
                    
                    
                    NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                    
                    NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                    
                    NSLog(@"firstBit folder :%@",firstBit);
                    NSLog(@"secondBit folder :%@",secondBit2);
                    
                    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                    
                    NSLog(@"str url :%@",str);
                    
                    NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/static_images",secondBit2]];
                    
                    NSArray* str3 = [str componentsSeparatedByString:@"static_images/"];
                    
                    NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                    
                    NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    NSLog(@"firstBit folder :%@",firstBit3);
                    NSLog(@"secondBit folder :%@",secondBit3);
                    NSLog(@"decoded folder :%@",decoded);
                    
                    NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                    
                    UIImage* image = [UIImage imageWithContentsOfFile:Path];
                    
                    imgH.image=image;
                    
                    // [self loadImageFromURL1:str image:tblHeaderImage];
                    
                    // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                    
                    
                }
                
                float imgWidth=scrollStaticView.frame.size.width;
                imgH.frame = CGRectMake(0, 0, imgWidth, imgWidth/2);
                [scrollStaticView addSubview:imgH];

                
                [self setStaticWithDict:dictResult Image:imgH];
                [vwLoading removeFromSuperview];
            }
        }
        else
        {
            float imgWidth=scrollStaticView.frame.size.width;
            float heightImage = (imgMainView.image.size.height*imgWidth)/imgMainView.image.size.width;
            
            
            imgMainView.frame = CGRectMake(0, 0, imgWidth, heightImage);
            
            
            [scrollStaticView addSubview:imgMainView];
             [self setStaticWithDict:dictResult Image:imgMainView];
        }
        
        
        
        
        
        
        
    }
    else if ([arrays count]>1){
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
        scrollView.delegate=self;
        scrollView.showsHorizontalScrollIndicator=NO;
        scrollView.showsVerticalScrollIndicator=NO;
        scrollView.tag=10;
        scrollView.pagingEnabled = YES;
        int k =0;
        scrollArray=[[NSMutableArray alloc]init];
        AsyncImageView *imgMainView;
         AsyncImageView *copyOfFirstImgView;
        for (NSString *imageURLString in [dictResult objectForKey:@"image"]) {
            [scrollArray addObject:imageURLString];
            
            
            imgMainView = [[AsyncImageView alloc] initWithFrame:CGRectMake(k*self.view.frame.size.width, 0, self.view.frame.size.width, 250)];
            k++;
            imgMainView.tag=k;
            imgMainView.contentMode = UIViewContentModeScaleToFill;
            imgMainView.clipsToBounds = YES;
            NSString*str=imageURLString;
        
            
           
           
           

            NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            SDImageCache *imageCache =[SDImageCache sharedImageCache];
            imageCache = [imageCache initWithNamespace:imageCacheFolder];
            imgMainView.image= [imageCache imageFromDiskCacheForKey:str];
            if(k==1)
            {
                NSInteger imgCount=[[dictResult objectForKey:@"image"] count];
                
                copyOfFirstImgView= [[AsyncImageView alloc] initWithFrame:CGRectMake(imgCount*self.view.frame.size.width, 0, self.view.frame.size.width, 250)];
                //copyOfFirstImgView.backgroundColor=[UIColor greenColor];
                //copyOfFirstImgView=imgMainView;
                copyOfFirstImgView.image=imgMainView.image;
                [scrollView addSubview:copyOfFirstImgView];
                
            }
            
            if (!imgMainView.image)
            {
                if (![self offlineAvailable]) {
                    [imgMainView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                    imgMainView.imageURL = [NSURL URLWithString:str];
                    
                    if(k==1)
                        
                    {
                        [copyOfFirstImgView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                    }
                    
                }
                else
                {
                    {
                        NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                        
                        NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                        
                        
                        
                        
                        NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                        
                        NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit);
                        NSLog(@"secondBit folder :%@",secondBit2);
                        
                        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",str);
                        
                        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/static_images",secondBit2]];
                        
                        NSArray* str3 = [str componentsSeparatedByString:@"static_images/"];
                        
                        NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                        
                        NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        NSLog(@"firstBit folder :%@",firstBit3);
                        NSLog(@"secondBit folder :%@",secondBit3);
                        NSLog(@"decoded folder :%@",decoded);
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        imgMainView.image=image;
                        
                        // [self loadImageFromURL1:str image:tblHeaderImage];
                        
                        // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                        
                        
                    }
                    
                }
            }
            
            
            [scrollView addSubview:imgMainView];
            
        }
        [scrollView setContentSize:CGSizeMake((k+1) * self.view.frame.size.width, 250)];
        [scrollStaticView addSubview:scrollView];
        if(![self reachable2])
            [dictResult setValue:@"" forKey:@"mp3"];
        if([[dictResult objectForKey:@"type"]isEqualToString:@"guided tour"]&&(![[dictResult objectForKey:@"mp3"] isEqualToString:@""])){
            NSString*htmlString=[NSString stringWithFormat:@"<div style='width:304px;height:200px;text-align:center;'><audio controls><source src='%@' type='audio/mpeg'></audio></div>",[dictResult objectForKey:@"mp3"]];
            self.playerWeb=[[UIWebView alloc]init];
            self.playerWeb.frame=CGRectMake(0, scrollView.frame.size.height+scrollView.frame.origin.y, imgMainView.frame.size.width, 50);
            self.playerWeb.scrollView.scrollEnabled=NO;
            self.playerWeb.opaque=NO;
            self.playerWeb.backgroundColor=[UIColor colorWithRed:213.0f/255.0f green:213/255.0f blue:213/255.0f alpha:1];
            [self.playerWeb loadHTMLString:htmlString baseURL:nil];
            [scrollStaticView addSubview:self.playerWeb];
            
            pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, self.playerWeb.frame.size.height+self.playerWeb.frame.origin.y,16*[arrays count]+1, 20)];
        }
        else{
            pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height+scrollView.frame.origin.y,16*[arrays count]+1, 20)];
        }
        pageControl.currentPage=0;
        
        pageControl.pageIndicatorTintColor = [UIColor grayColor];
        pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0xffbb03);
        pageControl.backgroundColor=[UIColor clearColor];
        pageControl.numberOfPages=[arrays count];
        [scrollStaticView addSubview:pageControl];
        
        
        [self setStaticWithDict:dictResult Image:nil];
        
        
        
    }
       }    @catch (NSException * e)
           {
        UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
        imgH.frame = CGRectMake(20, 30, 280, 140);
        [vwLoading removeFromSuperview];
                [scrollStaticView addSubview:imgH];
        [self setStaticWithDict:dictResult Image:imgH];
       
    }
 
    
    
    

    
    
    
    
    //        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%@,%@&mode=driving&sensor=false&key=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,[dictResult objectForKey:@"latitude"],[dictResult objectForKey:@"longitude"],GOOGLE_DISTANCE_KEY];
    //        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //        [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //            if (responseObject) {
    //                //Pass and decode to string2''==
    //                NSError *e;
    //                NSDictionary *dictDic = [NSJSONSerialization JSONObjectWithData:responseObject options: NSJSONReadingMutableContainers error: &e];
    //                if ([[dictDic objectForKey:@"status"] isEqualToString:@"OK"]) {
    //                    NSArray *arrRows = [dictDic objectForKey:@"rows"];
    //                    NSDictionary *dictElements = [arrRows objectAtIndex:0];
    //                    NSArray *arrElements = [dictElements objectForKey:@"elements"];
    //                    NSDictionary *dicDist = [arrElements objectAtIndex:0];
    //                    NSDictionary *dictDistance = [dicDist objectForKey:@"distance"];
    //                    if ([[dicDist objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"]) {
    //                        [lblDistanceStatic removeFromSuperview];
    //                    }
    //                    // 0.621371192 = 1 km
    //                    NSDictionary *dictDistCond = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
    //                    if ([[dictDistCond objectForKey:@"distance_in"] isEqualToString:@"miles"]) {
    //                        NSString *strdist = [[dictDistance objectForKey:@"text"] stringByReplacingOccurrencesOfString:@" km" withString:@""];
    //                        float distanceInMiles = [strdist floatValue] * 0.621371192;
    //                        lblDistanceStatic.text = [NSString stringWithFormat:@"%.2f miles",distanceInMiles];
    //                    }
    //                    else {
    //                        lblDistanceStatic.text = [dictDistance objectForKey:@"text"];
    //                    }
    //                }
    //                else {
    //                    lblDistanceStatic.text = @"unknown";
    //                }
    //            }
    //        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //            //    NSLog(@"Error: %@", error);
    //        }];
    
    CLLocation *plLocation = [[CLLocation alloc] initWithLatitude:[[dictResult objectForKey:@"latitude"] doubleValue] longitude:[[dictResult objectForKey:@"longitude"] doubleValue]];
    CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
   if (meters<=0)
    {
       
            
            lblDistanceBusiness.hidden=YES;
            btDirSt.hidden=YES;
            btDir.hidden=YES;
            
       
    }
    
    float actdist;
    //if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KM"] isEqualToString:@"NO"])
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
    {
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
      //  int k = actdist;
        if (actdist <= 0)
            lblDistanceStatic.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
        else
            lblDistanceStatic.text = [NSString stringWithFormat:@"%@ mi",trimmed];
    }
    else {
        actdist = meters * 0.001;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        //int k = actdist;
        if (actdist <= 0)
            lblDistanceStatic.text = [NSString stringWithFormat:@"0%@ km",trimmed];
        else
            lblDistanceStatic.text = [NSString stringWithFormat:@"%@ km",trimmed];
    }
    
    
    if(_isfromSearch)
    {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
        {
            actdist = meters * 0.000621371;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
            //  int k = actdist;
            if (actdist <= 0)
                lblDistanceStatic.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
            else
                lblDistanceStatic.text = [NSString stringWithFormat:@"%@ mi",trimmed];
        }
        else{
            actdist = meters * 0.001;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
            //int k = actdist;
            if (actdist <= 0)
                lblDistanceStatic.text = [NSString stringWithFormat:@"0%@ km",trimmed];
            else
                lblDistanceStatic.text = [NSString stringWithFormat:@"%@ km",trimmed];
        }
        
    }
  /*  NSString *llll= lblDistanceStatic.text;
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setMaximumFractionDigits:2];
    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
    lblDistanceStatic.text = trimmed;*/
    
    if (isFromFav) {
        lblNameLabel.text=@"Favorites";
        lblStaticName2.text=@"Favorites";
    }
    
    
    if(_isFromFrom)
    {
        lblDistanceStatic.hidden=YES;
    }
   
}
-(void)webServiceResponse:(NSData *) responseData
{
    NSError *e;
   
     if(![self offlineAvailable]){
   dictresp = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
     }
     else{
         NSMutableArray*array=[[NSMutableArray alloc]init];
         [array addObject:dictItemDetails];
         NSMutableDictionary*dicto=[[NSMutableDictionary alloc]init];
          if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"]) {
              
              array=[self coreDataCodeRead_static];
               [dicto setObject:array forKey:@"static"];
              
              
          }
          else{
               [dicto setObject:array forKey:@"business"];
             //  [self coreDataCodeRead_guided];
          }
         dictresp=dicto;
     }
    NSDictionary *dictResult;
    if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"]) {
        NSArray *arr = [dictresp objectForKey:@"static"];
        if (!([arr count] == 0)) {
            dictResult = [arr objectAtIndex:0];
            dictDetails = [arr objectAtIndex:0];
            [self setStaticDisplayWithDic:dictResult];
            
            
        }

        
    }
    else if([[dictItemDetails objectForKey:@"type"] isEqualToString:@"location"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"fixed"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"normal"]) {
        
      
            
            
        
        NSArray *arr = [dictresp objectForKey:@"business"];
        dictResult = [arr objectAtIndex:0];
        NSLog(@"%@",dictResult);
        dictDetails = [arr objectAtIndex:0];
        NSLog(@"%@",dictDetails);
        
        if ([[dictResult objectForKey:@"latitude"] isEqualToString:@"0"]) {
            btDir.hidden = YES;
        }
        NSArray*arrays=[dictResult objectForKey:@"image"];
        NSString *str =[dictResult objectForKey:@"image"];
        @try {
            
       
        
        if ([arrays count]==1 ) {
            AsyncImageView *imgMainView = [[AsyncImageView alloc] init];
            imgMainView.contentMode = UIViewContentModeScaleToFill;
            imgMainView.clipsToBounds = YES;
            NSString*str=[arrays objectAtIndex:0];
           // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:str];
            
            
            NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            SDImageCache *imageCache =[SDImageCache sharedImageCache];
            imageCache = [imageCache initWithNamespace:imageCacheFolder];
            imgMainView.image= [imageCache imageFromDiskCacheForKey:str];
            
            if (!imgMainView.image)
            {
                
                if(![self offlineAvailable]){
                    
                    [imgMainView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                    
                    [imgMainView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                        NSLog(@"success");
                        
                        
                        
                        float imgWidth=scrollMainView.frame.size.width;
                        float heightImage = (imageX.size.height*imgWidth)/imageX.size.width;
                        NSData *data = [NSData dataWithContentsOfURL : url];
                       
                        imgMainView.image=imageX;
                        
                        imgMainView.frame = CGRectMake(0, 0, imgWidth, heightImage);
                        imgMainView.image=imageX;
                        
                        [scrollStaticView addSubview:imgMainView];
                      [self setWithDict:dictResult imageView:imgMainView];
                        
                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                        UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                        float imgWidth=scrollStaticView.frame.size.width;
                        imgH.frame = CGRectMake(0, 0, imgWidth, imgWidth/2);
                        [self setWithDict:dictResult imageView:imgH];
                        [vwLoading removeFromSuperview];                    }
                     ];
                }
                else
                {
                    UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                    
                    
                   
                    if(str.length>0)
                    {
                        NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                        
                        NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                        
                        
                        
                        
                        NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                        
                        NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit);
                        NSLog(@"secondBit folder :%@",secondBit2);
                        
                        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",str);
                        
                        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/storeimages",secondBit2]];
                        
                        NSArray* str3 = [str componentsSeparatedByString:@"storeimages/"];
                        
                        NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                        
                        NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        NSLog(@"firstBit folder :%@",firstBit3);
                        NSLog(@"secondBit folder :%@",secondBit3);
                        NSLog(@"decoded folder :%@",decoded);
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        imgH.image=image;
                        
                        // [self loadImageFromURL1:str image:tblHeaderImage];
                        
                        // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                        
                        
                    }
                    
                    float imgWidth=scrollStaticView.frame.size.width;
                    imgH.frame = CGRectMake(0, 0, imgWidth, imgWidth/2);
                    [self setWithDict:dictResult imageView:imgH];
                    [vwLoading removeFromSuperview];
                }
            }
            else
            {
                float imgWidth=scrollStaticView.frame.size.width;
                float heightImage = (imgMainView.image.size.height*imgWidth)/imgMainView.image.size.width;
                
                
                imgMainView.frame = CGRectMake(0, 0, imgWidth, heightImage);
                
                
                [scrollStaticView addSubview:imgMainView];
                [self setWithDict:dictResult imageView:imgMainView];
            }
            

            
            
            
            
           
        }
        else if ([arrays count]>1){
            scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
            scrollView.delegate=self;
            scrollView.tag=10;
            scrollView.pagingEnabled = YES;
            int k =0;
            scrollArray=[[NSMutableArray alloc]init];
            AsyncImageView *imgMainView;
            AsyncImageView *copyOfFirstImgView;
            for (NSString *imageURLString in [dictResult objectForKey:@"image"]) {
                [scrollArray addObject:imageURLString];
                
                
                
                
                imgMainView = [[AsyncImageView alloc] initWithFrame:CGRectMake(k*self.view.frame.size.width, 0, self.view.frame.size.width, 250)];
                k++;
                imgMainView.contentMode = UIViewContentModeScaleToFill;
                imgMainView.clipsToBounds = YES;
                NSString*str=imageURLString;
           
                //imageView.layer.masksToBounds=YES;
                NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:imageCacheFolder];
                imgMainView.image= [imageCache imageFromDiskCacheForKey:str];
                
                if(k==1)
                {
                    NSInteger imgCount=[[dictResult objectForKey:@"image"] count];
                    
                    copyOfFirstImgView= [[AsyncImageView alloc] initWithFrame:CGRectMake(imgCount*self.view.frame.size.width, 0, self.view.frame.size.width, 250)];
                    //copyOfFirstImgView.backgroundColor=[UIColor greenColor];
                    //copyOfFirstImgView=imgMainView;
                    copyOfFirstImgView.image=imgMainView.image;
                    [scrollView addSubview:copyOfFirstImgView];
                    
                }

                if (!imgMainView.image)
                {
                    if (![self offlineAvailable]) {
                                                [imgMainView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                        imgMainView.imageURL = [NSURL URLWithString:str];
                        if(k==1)
                        {
                            [copyOfFirstImgView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                        }

                    }
                    else
                    {
                        NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                        
                        NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                        
                        
                        
                        
                        NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                        
                        NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit);
                        NSLog(@"secondBit folder :%@",secondBit2);
                        
                        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",str);
                        
                        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/storeimages",secondBit2]];
                        
                        NSArray* str3 = [str componentsSeparatedByString:@"storeimages/"];
                        
                        NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                        
                        NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        NSLog(@"firstBit folder :%@",firstBit3);
                        NSLog(@"secondBit folder :%@",secondBit3);
                        NSLog(@"decoded folder :%@",decoded);
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        imgMainView.image=image;
                        
                        // [self loadImageFromURL1:str image:tblHeaderImage];
                        
                        // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                        
                        
                    }
                }
                
              
                [scrollView addSubview:imgMainView];
                
                
        }
             [scrollView setContentSize:CGSizeMake((k+1) * self.view.frame.size.width, 250)];
            [scrollMainView addSubview:scrollView];
       if(![self reachable2])
           [dictResult setValue:@"" forKey:@"mp3"];
             if([[dictResult objectForKey:@"type"]isEqualToString:@"guided tour"]&&(![[dictResult objectForKey:@"mp3"] isEqualToString:@""])){
                 NSString*htmlString=[NSString stringWithFormat:@"<div style='width:320px;cellpadding=\"0\" cellspacing=\"0\" height:200px;text-align:center;'><audio controls><source src='%@' type='audio/mpeg'></audio></div>",[dictResult objectForKey:@"mp3"]];
                 self.playerWeb=[[UIWebView alloc]init];
                 self.playerWeb.frame=CGRectMake(0, scrollView.frame.size.height+scrollView.frame.origin.y, imgMainView.frame.size.width, 50);
                 self.playerWeb.scrollView.scrollEnabled=NO;
                 self.playerWeb.opaque=NO;
                 self.playerWeb.backgroundColor=[UIColor colorWithRed:213.0f/255.0f green:213/255.0f blue:213/255.0f alpha:1];
                 [self.playerWeb loadHTMLString:htmlString baseURL:nil];
                 [scrollMainView addSubview:self.playerWeb];
                 
                  pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, self.playerWeb.frame.size.height+self.playerWeb.frame.origin.y,16*[arrays count]+1, 20)];
             }
             else{
            pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height+scrollView.frame.origin.y,16*[arrays count]+1, 20)];
             }
            pageControl.currentPage=0;
           
            pageControl.pageIndicatorTintColor = [UIColor grayColor];
            pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0xffbb03);
            pageControl.backgroundColor=[UIColor clearColor];
            pageControl.numberOfPages=[arrays count];
            [scrollMainView addSubview:pageControl];
            
            [self setWithDict:dictResult imageView:nil];
            
          
            
            
        }
        else {
            UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
            imgH.frame = CGRectMake(20, 30, 280, 140);
            [vwLoading removeFromSuperview];
            [self setWithDict:dictResult imageView:imgH];
            [scrollStaticView addSubview:imgH];
            [scrollView addSubview:imgH];
        }
        }
        @catch (NSException *exception) {
            UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
            
            if(str.length>0)
            {
                NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                
                NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                
                NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                
                
                
                
                NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                
                NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                
                NSLog(@"firstBit folder :%@",firstBit);
                NSLog(@"secondBit folder :%@",secondBit2);
                
                NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                
                NSLog(@"str url :%@",str);
                
                NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/storeimages",secondBit2]];
                
                NSArray* str3 = [str componentsSeparatedByString:@"storeimages/"];
                
                NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                
                NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                NSLog(@"firstBit folder :%@",firstBit3);
                NSLog(@"secondBit folder :%@",secondBit3);
                NSLog(@"decoded folder :%@",decoded);
                
                NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                
                UIImage* image = [UIImage imageWithContentsOfFile:Path];
                
                imgH.image=image;
                
                // [self loadImageFromURL1:str image:tblHeaderImage];
                
                // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                
                
            }
            imgH.frame = CGRectMake(0, 0, 320, 160);
            [vwLoading removeFromSuperview];
             [scrollView addSubview:imgH];
            [scrollStaticView addSubview:imgH];
            [self setWithDict:dictResult imageView:imgH];
           
        }
        //        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%@,%@&mode=driving&sensor=false&key=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,[dictResult objectForKey:@"latitude"],[dictResult objectForKey:@"longitude"],GOOGLE_DISTANCE_KEY];
        //        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        //        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //        [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //            if (responseObject) {
        //                //Pass and decode to string2''==
        //                NSError *e;
        //                NSDictionary *dictDic = [NSJSONSerialization JSONObjectWithData:responseObject options: NSJSONReadingMutableContainers error: &e];
        //                if ([[dictDic objectForKey:@"status"] isEqualToString:@"OK"]) {
        //                    NSArray *arrRows = [dictDic objectForKey:@"rows"];
        //                    NSDictionary *dictElements = [arrRows objectAtIndex:0];
        //                    NSArray *arrElements = [dictElements objectForKey:@"elements"];
        //                    NSDictionary *dicDist = [arrElements objectAtIndex:0];
        //                    NSDictionary *dictDistance = [dicDist objectForKey:@"distance"];
        //                    if ([[dicDist objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"]) {
        //                        [lblDistanceBusiness removeFromSuperview];
        //                    }
        //                    // 0.621371192 = 1 km
        //                    NSDictionary *dictDistCond = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
        //                    if ([[dictDistCond objectForKey:@"distance_in"] isEqualToString:@"miles"]) {
        //                        NSString *strdist = [[dictDistance objectForKey:@"text"] stringByReplacingOccurrencesOfString:@" km" withString:@""];
        //                        float distanceInMiles = [strdist floatValue] * 0.621371192;
        //                        lblDistanceBusiness.text = [NSString stringWithFormat:@"%.2f miles",distanceInMiles];
        //                    }
        //                    else {
        //                        lblDistanceBusiness.text = [dictDistance objectForKey:@"text"];
        //                    }
        //                }
        //                else {
        //                    lblDistanceBusiness.text = @"unknown";
        //                }
        //            }
        //        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //            //    NSLog(@"Error: %@", error);
        //        }];
        NSArray *smp = [dictresp objectForKey:@"business"];
        NSDictionary *dic2z = [smp objectAtIndex:0];
        CLLocation *plLocation = [[CLLocation alloc] initWithLatitude:[[dic2z objectForKey:@"latitude"] doubleValue] longitude:[[dic2z objectForKey:@"longitude"] doubleValue]];
        
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        
       if (meters<=0)
        {
           
            lblDistanceBusiness.hidden=YES;
            btDirSt.hidden=YES;
            btDir.hidden=YES;
            
        }
        float actdist;
        //if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KM"] isEqualToString:@"NO"])
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
        {
            actdist = meters * 0.000621371;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
           // int k = actdist;
            if (actdist <= 0)
                lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
            else
                lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ mi",trimmed];
        }
        else {
            actdist = meters * 0.001;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
           // int k = actdist;
            if (actdist <= 0)
                lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ km",trimmed];
            else
                lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ km",trimmed];
        }
        
        
        if(_isfromSearch)
        {
            
           /* if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            {
                actdist = meters * 0.000621371;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                // int k = actdist;
                if (actdist <= 0)
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
                else
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ mi",trimmed];
            }
            else{
                actdist = meters * 0.001;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                // int k = actdist;
                if (actdist <= 0)
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ km",trimmed];
                else
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ km",trimmed];
            }
            */
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            {
                lblDistanceBusiness.text=[NSString stringWithFormat:@"%@ mi",[[NSUserDefaults standardUserDefaults]objectForKey:@"distanceFromSearchForDetail"]];
            }
            
            else{
                
                lblDistanceBusiness.text=[NSString stringWithFormat:@"%@ km",[[NSUserDefaults standardUserDefaults]objectForKey:@"distanceFromSearchForDetail"]];
                
            }

            
            
        }

        
    }
    else {
            dictResult =[dictresp objectForKey:@"result"];
            dictDetails = [dictresp objectForKey:@"result"];
            NSLog(@"%@",dictDetails);
            NSArray *array = [dictResult objectForKey:@"photos"];
            if (!([array count] == 0)) {
                NSDictionary *dic2 = [array objectAtIndex:0];
                UIImageView *imgMainView = [[UIImageView alloc] init];
                imgMainView.contentMode = UIViewContentModeScaleToFill;
                imgMainView.clipsToBounds = YES;
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%@&sensor=true&key=%@",[dic2 objectForKey:@"photo_reference"],GOOGLE_API_KEY]];
                NSLog(@"%@",url);
                NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:imageCacheFolder];
                
                
                
                [imgMainView setImageWithURLRequest:[NSURLRequest requestWithURL:url] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                    NSLog(@"success");
                    NSLog(@"%f %f ",imageX.size.height,imageX.size.width);
                    [imgMainView setImage:imageX];
                    float heightImage = (imageX.size.height*300)/imageX.size.width;
                    imgMainView.frame = CGRectMake(20, 30, 280, heightImage);
                    [self setWithDict:dictResult imageView:imgMainView];
                    [vwLoading removeFromSuperview];
                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                    NSLog(@"fail with %@ and %@",[error localizedDescription],[request URL]);
                    UIImageView *imgVw;
                    [self setWithDict:dictResult imageView:imgVw];
                    [vwLoading removeFromSuperview];
                }
                 ];
            }
            else {
                UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                imgH.frame = CGRectMake(20, 30, 280, 140);
                [self setWithDict:dictResult imageView:imgH];
                [vwLoading removeFromSuperview];
            }
            
            //        NSDictionary *dictLoc = [dictResult objectForKey:@"geometry"];
            //        NSDictionary *dictLatLng = [dictLoc objectForKey:@"location"];
            //        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%@,%@&mode=driving&sensor=false&key=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,[dictLatLng objectForKey:@"lat"],[dictLatLng objectForKey:@"lng"],GOOGLE_DISTANCE_KEY];
            //        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            //        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            //        [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            if (responseObject) {
            //                //Pass and decode to string2''==
            //                NSError *e;
            //                NSDictionary *dictDic = [NSJSONSerialization JSONObjectWithData:responseObject options: NSJSONReadingMutableContainers error: &e];
            //                if ([[dictDic objectForKey:@"status"] isEqualToString:@"OK"]) {
            //                    NSArray *arrRows = [dictDic objectForKey:@"rows"];
            //                    NSDictionary *dictElements = [arrRows objectAtIndex:0];
            //                    NSArray *arrElements = [dictElements objectForKey:@"elements"];
            //                    NSDictionary *dicDist = [arrElements objectAtIndex:0];
            //                    if ([[dicDist objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"]) {
            //                        [lblDistanceBusiness removeFromSuperview];
            //                    }
            //                    NSDictionary *dictDistance = [dicDist objectForKey:@"distance"];
            //                    // 0.621371192 = 1 km
            //                    NSDictionary *dictDistCond = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            //                    if ([[dictDistCond objectForKey:@"distance_in"] isEqualToString:@"miles"]) {
            //                        NSString *strdist = [[dictDistance objectForKey:@"text"] stringByReplacingOccurrencesOfString:@" km" withString:@""];
            //                        float distanceInMiles = [strdist floatValue] * 0.621371192;
            //                        lblDistanceBusiness.text = [NSString stringWithFormat:@"%.2f miles",distanceInMiles];
            //                    }
            //                    else {
            //                        lblDistanceBusiness.text = [dictDistance objectForKey:@"text"];
            //                    }
            //                }
            //                else {
            //                    lblDistanceBusiness.text = @"unknown";
            //                }
            //            }
            //        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //            //    NSLog(@"Error: %@", error);
            //        }];
            
            
            NSDictionary *dictLoc = [dictResult objectForKey:@"geometry"];
            NSDictionary *dictLatLng = [dictLoc objectForKey:@"location"];
            CLLocation *plLocation = [[CLLocation alloc] initWithLatitude:[[dictLatLng objectForKey:@"lat"] doubleValue] longitude:[[dictLatLng objectForKey:@"lng"] doubleValue]];
            
            CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        if (meters<=0)
            {
                
                lblDistanceBusiness.hidden=YES;
                btDirSt.hidden=YES;
                btDir.hidden=YES;
                
            }
        
        
            float actdist;
           // if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KM"] isEqualToString:@"NO"])
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            {
                actdist = meters * 0.000621371;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
               //int k = actdist;
                if (actdist <= 0)
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
                else
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ mi",trimmed];
            }
            else {
                actdist = meters * 0.001;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
               // int k = actdist;
                if (actdist <= 0)
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ km",trimmed];
                else
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ km",trimmed];
            }
        
        if(_isfromSearch)
        {
            
            /*if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            {
                // dist=@"M";
                
                
                actdist = meters * 0.000621371;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                //int k = actdist;
                if (actdist <= 0)
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ mi",trimmed];
                else
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                
            }
            else{
                
                // dist=@"K";
                
                
                
                actdist = meters * 0.001;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                // int k = actdist;
                if (actdist <= 0)
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"0%@ km",trimmed];
                else
                    lblDistanceBusiness.text = [NSString stringWithFormat:@"%@ km",trimmed];
                
                
                
            }*/
           if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
           {
               lblDistanceBusiness.text=[NSString stringWithFormat:@"%@ mi",[[NSUserDefaults standardUserDefaults]objectForKey:@"distanceFromSearchForDetail"]];
           }
            
           else{
               
               lblDistanceBusiness.text=[NSString stringWithFormat:@"%@ km",[[NSUserDefaults standardUserDefaults]objectForKey:@"distanceFromSearchForDetail"]];
           
           }
            
            
            
            
        }

        
        }
    NSLog(@"lblDistanceBusiness :%@",lblDistanceBusiness.text);
    
    
//    if(![self reachable2]){
//        lblDistanceBusiness.hidden=YES;
//        lblDistanceStatic.hidden=YES;
//    }
    if ([[dictResult objectForKey:@"latitude"]isEqualToString:@"0"]&&[[dictResult objectForKey:@"longitude"]isEqualToString:@"0"]) {
        lblDistanceBusiness.hidden=YES;
        lblDistanceStatic.hidden=YES;
    }
}
-(void)setStaticWithDict:(NSDictionary *)dictResult
                   Image:(UIImageView *) img
{
    
    float q,w,e;
    
    NSLog(@"not error-static");
    if (IS_IPHONE5)
    {
       // scrollHeight = 450;
    
     scrollHeight = 501;
    }
    else
        scrollHeight = 361;
    
    float imgHeight=scrollView.frame.size.height+scrollView.frame.origin.y;
    if(img)
        imgHeight=img.frame.size.height+img.frame.origin.y;
        
    scrollStaticView.frame = CGRectMake(0, 66, 320, scrollHeight);
    [scrollStaticView addSubview:img];
    [vwLoading removeFromSuperview];
    [self.view addSubview:vwStatic];
    [vwStatic addSubview:ad1Image];
    [vwStatic addSubview:ad1Btn];
    NSString *strName = [dictResult objectForKey:@"title"];
    NSString *strAddress = [dictResult objectForKey:@"address"];
    NSString *strDesc = [dictResult objectForKey:@"content"];
    CGSize maximumLabelSize = CGSizeMake(280, FLT_MAX);

    lblDistanceStatic.frame = CGRectMake(lblDistanceStatic.frame.origin.x, imgHeight + 5, lblDistanceStatic.frame.size.width, lblDistanceStatic.frame.size.height);
    CGSize expectedLabelSize = [strName sizeWithFont:[UIFont fontWithName:@"Verdana-Bold" size:20] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    UILabel *lblStaticName = [[UILabel alloc] initWithFrame:CGRectMake(20, imgHeight + 30, 280, expectedLabelSize.height)];
    lblStaticName.numberOfLines = 25;
    [lblStaticName setFont:[UIFont fontWithName:@"Verdana-Bold" size:20]];
    lblStaticName.backgroundColor = [UIColor clearColor];
    lblStaticName.text = strName;
    [scrollStaticView addSubview:lblStaticName];
    lblStaticName2.text = strName;
    
    int ypos=lblStaticName.frame.size.height + lblStaticName.frame.origin.y + 10;
    if([[dictResult objectForKey:@"show_phone"]isEqualToString:@"yes"]){
         [btPhoneStat setTitle:[dictResult objectForKey:@"phone"] forState:UIControlStateNormal];
        btPhoneStat.hidden=NO;
        btPhoneStat.frame = CGRectMake(50, ypos, 280, 20);
        UIImageView*phoneIcon=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"call.png"]];
        phoneIcon.frame=CGRectMake(btPhoneStat.frame.origin.x-30, btPhoneStat.frame.origin.y, 20, 20);
        [scrollStaticView addSubview:phoneIcon];
        ypos=ypos+20+8;
        
        w=btPhoneStat.frame.size.height;
    }
    else{
        btPhoneStat.hidden=YES;
        w=0;
        
    }
    if([[dictResult objectForKey:@"show_url"]isEqualToString:@"yes"]){
        btWebStat.hidden=NO;
        webAddress=[dictResult objectForKey:@"url"];
        [btWebStat setTitle:[dictResult objectForKey:@"url_text"] forState:UIControlStateNormal];
        
        btWebStat.frame = CGRectMake(50, ypos, self.view.frame.size.width-60, 20);
        UIImageView*webIcon=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"web.png"]];
        webIcon.frame=CGRectMake(btWebStat.frame.origin.x-30, btWebStat.frame.origin.y, 20, 20);
        [scrollStaticView addSubview:webIcon];
        ypos=ypos+20+8;
        
         q = btWebStat.frame.size.height;
        // webIcon.contentMode = UIViewContentModeCenter;
        // webIcon.backgroundColor=[UIColor yellowColor];
    }
    else{

        btWebStat.hidden=YES;
        q=0;
    }
    if([[dictResult objectForKey:@"show_email"]isEqualToString:@"yes"]){
        lblMail=[[UIButton alloc]init];

        lblMail.frame=CGRectMake(50,ypos, 280,20);
        lblMail.hidden=NO;
        
        [lblMail setTitleColor:UIColorFromRGB(0x007AFF)  forState:UIControlStateNormal];
        lblMail.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        lblMail.titleLabel.font = [UIFont systemFontOfSize:16.0];
        // lblMail.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        
        [lblMail addTarget:self action:@selector(emailButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [scrollStaticView addSubview:lblMail];
        mailAddres=[dictResult objectForKey:@"email"];
         [lblMail setTitle:[dictResult objectForKey:@"email_text"] forState:UIControlStateNormal];
        UIImageView*mailIcon=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"email.png"]];
        mailIcon.frame=CGRectMake(lblMail.frame.origin.x-30, lblMail.frame.origin.y, 20, 20);
        [scrollStaticView addSubview:mailIcon];
        ypos=ypos+20+8;
        
        e=lblMail.frame.size.height;

    }
    else{
        
        lblMail.hidden=YES;
        e=0;
       
    }
    
    
    CGSize expectedSubTitleSize = [strAddress sizeWithFont:[UIFont fontWithName:@"Verdana" size:15.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    UILabel *lblStaticAddr = [[UILabel alloc] initWithFrame:CGRectMake(20, ypos, 280, expectedSubTitleSize.height)];
    lblStaticAddr.text = strAddress;
    lblStaticAddr.backgroundColor = [UIColor clearColor];
    lblStaticAddr.numberOfLines = 50;
    [lblStaticAddr setFont:[UIFont fontWithName:@"Verdana" size:15.0]];
    
    ypos=ypos+expectedSubTitleSize.height+20;
    
    if([[dictResult objectForKey:@"disp_add"]isEqualToString:@"yes"]){
        
    [scrollStaticView addSubview:lblStaticAddr];
    
    }
    else
    {
        lblStaticAddr.hidden=YES;
    }
    
    strDesc =[strDesc stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    strDesc =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",strDesc];
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[strDesc dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
   
    
    
    CGSize expectedContentSize = [strDesc sizeWithFont:[UIFont fontWithName:@"Verdana" size:13.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    UILabel *lblStaticContentView = [[UILabel alloc] initWithFrame:CGRectMake(20, ypos, 280, expectedContentSize.height)];
    lblStaticContentView.numberOfLines = 0;
    lblStaticContentView.backgroundColor = [UIColor clearColor];
    [lblStaticContentView setFont:[UIFont fontWithName:@"Verdana" size:13.0]];
    [scrollStaticView addSubview:lblStaticContentView];
    lblStaticContentView.text = strDesc;
    
   // lblStaticContentView.dataDetectorTypes = UIDataDetectorTypeAll;
  //  lblStaticContentView.dataDetectorTypes = UIDataDetectorTypeLink;
    
    lblStaticContentView.attributedText = attrStr;
    [lblStaticContentView sizeToFit];
//    lblStaticContentView.editable=NO;
    
      ypos=ypos+expectedContentSize.height+20;
    
    
     if([[dictResult objectForKey:@"favorites"]isEqualToString:@"yes"]){
    
    //btnFavoritesStatic.frame = CGRectMake(lblStaticAddr.frame.origin.x, ypos, 160, 40);
         btnFavoritesStatic.frame = CGRectMake(lblStaticAddr.frame.origin.x+50,lblStaticContentView.frame.origin.y+lblStaticContentView.frame.size.height+20 , 25, 25); //ypos

           //ypos=ypos+40;
         
         
//         btnShareDetail.frame = CGRectMake(20, lblDetals.frame.origin.y + lblDetals.frame.size.height + 5, 25, 25);
//         share4.frame=btnShareDetail.frame;
         
     }
    else
        btnFavoritesStatic.hidden=YES;
    
    if([[dictResult objectForKey:@"share"]isEqualToString:@"yes"]){
     btnShareDetailStatic.frame = CGRectMake(lblStaticAddr.frame.origin.x, lblStaticContentView.frame.origin.y+lblStaticContentView.frame.size.height+20, 25, 25);
    shareStatic4.frame=btnShareDetailStatic.frame;
        // ypos=ypos+40;
     }
    else
        btnShareDetailStatic.hidden=YES;
    
    [scrollStaticView setContentSize:CGSizeMake(scrollStaticView.frame.size.width, lblStaticContentView.frame.size.height + imgHeight +50+60 +50+ q+w+e +50)]; //ypos
    [scrollStaticView addSubview:img];
    
  float y=  lblStaticContentView.frame.size.height + imgHeight + lblStaticAddr.frame.size.height + lblDistanceStatic.frame.size.height +  lblStaticName.frame.size.height ;
    
    NSLog(@"y height scroll : %f",y);
    
    NSLog(@"scoll height : %f",lblStaticContentView.frame.size.height + imgHeight +50+60 +50 +q+w+e +50);
    
    if (isFromFav) {
        lblNameLabel.text=@"Favorites";
        lblStaticName2.text=@"Favorites";
    }
}
-(void)setWithDict:(NSDictionary *)dictResult
         imageView:(UIImageView *) img
{
 
    NSLog(@"Error-static");
    [vwLoading removeFromSuperview];
    NSLog(@"imageheight : %f",img.frame.size.height);
    [scrollMainView addSubview:img];
  
    NSString *strName = [dictResult objectForKey:@"name"];
    NSString *strAddress;
    NSString *strDetails;
    NSString *strEmail=[dictResult objectForKey:@"email_text"];
    mailAddres=[dictResult objectForKey:@"email_address"];
    float phoneHeight, webHeight;
     if(img){
         if(![self reachable2])
             [dictResult setValue:@"" forKey:@"mp3"];
  if([[dictResult objectForKey:@"type"]isEqualToString:@"guided tour"]&&(![[dictResult objectForKey:@"mp3"] isEqualToString:@""])){
      NSString*htmlString=[NSString stringWithFormat:@"<div style='width:304px;height:200px;text-align:center;'><audio controls><source src='%@' type='audio/mpeg'></audio></div>'",[dictResult objectForKey:@"mp3"]];
    self.playerWeb=[[UIWebView alloc]init];
    // NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictResult objectForKey:@"mp3"]]]];
    //[playerWeb loadRequest:requestObj];
    self.playerWeb.frame=CGRectMake(0, img.frame.origin.y+img.frame.size.height, scrollMainView.frame.size.width, 50);
    self.playerWeb.scrollView.scrollEnabled=NO;
    self.playerWeb.opaque=NO;
    self.playerWeb.backgroundColor=[UIColor colorWithRed:213.0f/255.0f green:213/255.0f blue:213/255.0f alpha:1];
    [self.playerWeb loadHTMLString:htmlString baseURL:nil];
    [scrollMainView addSubview:self.playerWeb];
      
//  playBtn=[[UIButton alloc]init];
//      playBtn.frame=CGRectMake(0, img.frame.origin.y+img.frame.size.height, 30, 30);
//      if([audioPlayer isPlaying]){
//        //  [playBtn setTitle:@"Pause" forState:UIControlStateNormal];
//          [playBtn setBackgroundImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal] ;
//      }
//      else{
//         //[playBtn setTitle:@"Play" forState:UIControlStateNormal];
//          [playBtn setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal] ;
//      }
//      
//      [playBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//      [[playBtn layer] setBorderWidth:1.0f];
//      [[playBtn layer] setBorderColor:[UIColor blackColor].CGColor];
//      // nextBtn.tag=ind
//      [playBtn addTarget:self action:@selector(onPlay:) forControlEvents:UIControlEventTouchUpInside];
//      [scrollMainView addSubview:playBtn];
      
//      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//      NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
//      NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"MyNewFolder"];
//      NSError*error;
//      if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
//          [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
//      NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictResult objectForKey:@"mp3"]]];
//      ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
//      [request setDownloadDestinationPath:[NSString stringWithFormat:@"%@",dataPath]]; //use the path from earlier
//      [request setDelegate:self];
//      [request startAsynchronous];
      
      
//      NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictResult objectForKey:@"mp3"]]]];
//        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://insightto.com/app_mp3/856.mp3"]];
//     
//      AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request] ;
//      
//      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//      NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"filename1"];
//      operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
//      
//      [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//          NSLog(@"Successfully downloaded file to %@", path);
//          
//          NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
//          
//          AVAudioPlayer *audio = [[AVAudioPlayer alloc]
//                                  initWithContentsOfURL:fileURL error:nil];
//          audioPlayer = audio;
//           seekbar.maximumValue = audioPlayer.duration;
//          updateTimer=[[NSTimer alloc]init];
//          updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
//      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//          NSLog(@"Error: %@", error);
//      }];
//      
//      [operation start];
      
    // NSURL *fileURL =  [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/audiofile.mp3",dataPath]];

//      
//     NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:@"bgm" ofType:@"mp3"]];
//      
//      AVAudioPlayer *audio = [[AVAudioPlayer alloc]
//                              initWithContentsOfURL:fileURL error:nil];
     
//      audioPlayer=[[AVAudioPlayer alloc]init];
// //   audioPlayer = audio;
//      seekbar=[[UISlider alloc]init];
//      seekbar.minimumValue = 0;
//    [seekbar addTarget:self action:@selector(seekTime:) forControlEvents:UIControlEventValueChanged];
//    //  seekbar.maximumValue = audioPlayer.duration;
//      seekbar.frame=CGRectMake(30, img.frame.origin.y+img.frame.size.height, scrollMainView.frame.size.width-30, 30);
//      seekbar.backgroundColor=[UIColor blackColor];
//      [scrollMainView addSubview:seekbar];
      
  //    [ audioPlayer play];
//      updateTimer=[[NSTimer alloc]init];
//      updateTimer =     [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
  }
     }
    
    if([dictResult objectForKey:@"formatted_address"])
    {
        strAddress = [dictResult objectForKey:@"formatted_address"];
        strDetails=strAddress;
        [btPhone setTitle:[dictResult objectForKey:@"formatted_phone_number"] forState:UIControlStateNormal];
        webAddress=[dictResult objectForKey:@"website"];
         [btWeb setTitle:[dictResult objectForKey:@"website_text"] forState:UIControlStateNormal];
        if(googleLocType)
            [btWeb setTitle:[dictResult objectForKey:@"website"] forState:UIControlStateNormal];
        
        if (![dictResult objectForKey:@"formatted_phone_number"])
            phoneHeight = 0;
        else
            phoneHeight = 20;
        if (![dictResult objectForKey:@"website"])
            webHeight = 0;
        else
            webHeight = 20;
    }
    else
    {
        strAddress = [dictResult objectForKey:@"address"];
        [btPhone setTitle:[dictResult objectForKey:@"phone"] forState:UIControlStateNormal];
         webAddress=[dictResult objectForKey:@"website"];
        
        [btWeb setTitle:[dictResult objectForKey:@"website_text"] forState:UIControlStateNormal];
       
        strDetails = [dictResult objectForKey:@"details"];
        if ([[dictResult objectForKey:@"show_call"] isEqualToString:@"Yes"])
            phoneHeight = 20;
        else
            phoneHeight = 0;
        if ([[dictResult objectForKey:@"website"] isEqualToString:@""])
            webHeight = 0;
        else
            webHeight = 20;
    }
   
  if([[dictResult objectForKey:@"type"]isEqualToString:@"guided tour"]&&(![[dictResult objectForKey:@"mp3"] isEqualToString:@""])){
        if(img){
            lblDistanceBusiness.frame = CGRectMake(lblDistanceBusiness.frame.origin.x, img.frame.origin.y + img.frame.size.height + 50, lblDistanceBusiness.frame.size.width, lblDistanceBusiness.frame.size.height);
        }
        else{
            lblDistanceBusiness.frame = CGRectMake(lblDistanceBusiness.frame.origin.x, scrollView.frame.origin.y + scrollView.frame.size.height +  50, lblDistanceBusiness.frame.size.width, lblDistanceBusiness.frame.size.height);
        }
    }
  else{
      if(img){
          lblDistanceBusiness.frame = CGRectMake(lblDistanceBusiness.frame.origin.x, img.frame.origin.y + img.frame.size.height + 5, lblDistanceBusiness.frame.size.width, lblDistanceBusiness.frame.size.height);
      }
      else{
          lblDistanceBusiness.frame = CGRectMake(lblDistanceBusiness.frame.origin.x, scrollView.frame.origin.y + scrollView.frame.size.height + 5, lblDistanceBusiness.frame.size.width, lblDistanceBusiness.frame.size.height);
      }

  }
    lblDistanceBusiness.backgroundColor=[UIColor clearColor];
    CGSize maximumLabelSize = CGSizeMake(280, FLT_MAX);
    
    CGSize expectedLabelSize = [strName sizeWithFont:[UIFont fontWithName:@"Verdana-Bold" size:20] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    UILabel *lblNameLabel2 ;
    if(![self reachable2])
        [dictResult setValue:@"" forKey:@"mp3"];
     if([[dictResult objectForKey:@"type"]isEqualToString:@"guided tour"]&&(![[dictResult objectForKey:@"mp3"] isEqualToString:@""])){
    
        
        if(img){
            lblNameLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(20, img.frame.size.height + img.frame.origin.y + 30+50, 280, expectedLabelSize.height)];
        }
        else{
            lblNameLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(20, scrollView.frame.size.height + scrollView.frame.origin.y + 30+50, 280, expectedLabelSize.height)];
        }
    }
    else
    {
        if(img){
            lblNameLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(20, img.frame.size.height + img.frame.origin.y + 30, 280, expectedLabelSize.height)];
        }
        else{
            lblNameLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(20, scrollView.frame.size.height + scrollView.frame.origin.y + 30, 280, expectedLabelSize.height)];
        }
    }
    lblNameLabel2.numberOfLines = 25;
    lblNameLabel2.backgroundColor = [UIColor clearColor];
    [lblNameLabel2 setFont:[UIFont fontWithName:@"Verdana-Bold" size:20]];
    lblNameLabel2.text = strName;
    
    [scrollMainView addSubview:lblNameLabel2];
    
//    CGSize expectedSubTitleSize = [strAddress sizeWithFont:[UIFont fontWithName:@"Verdana" size:13.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
//    UILabel *lblAddr = [[UILabel alloc] initWithFrame:CGRectMake(20, lblNameLabel2.frame.origin.y + lblNameLabel2.frame.size.height + 10, 280, expectedSubTitleSize.height)];
//    lblAddr.text = strAddress;
//    lblAddr.backgroundColor = [UIColor clearColor];
//    lblAddr.numberOfLines = 50;
//    [lblAddr setFont:[UIFont fontWithName:@"Verdana" size:13.0]];
//    [scrollMainView addSubview:lblAddr];
//    
//    if([[dictResult objectForKey:@"disp_address"]isEqualToString:@"yes"]){
//        lblAddr.hidden=NO;
//    }
//    else{
//        lblAddr.hidden=YES;
//    }
    
    if([[dictResult objectForKey:@"show_call"]isEqualToString:@"Yes"]||([dictResult objectForKey:@"formatted_phone_number"])){
        btPhone.hidden=NO;
         btPhone.frame = CGRectMake(50, lblNameLabel2.frame.size.height + lblNameLabel2.frame.origin.y + 10, 280, phoneHeight);
        UIImageView*phoneIcon=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"call.png"]];
        phoneIcon.frame=CGRectMake(btPhone.frame.origin.x-30, btPhone.frame.origin.y, 20, 20);
        [scrollMainView addSubview:phoneIcon];
    }
    else{
        btPhone.hidden=YES;
         btPhone.frame = CGRectMake(20, lblNameLabel2.frame.size.height + lblNameLabel2.frame.origin.y , 280, 0);
    }
    if([[dictResult objectForKey:@"show_web"]isEqualToString:@"Yes"]){
        btWeb.hidden=NO;
        
         btWeb.frame = CGRectMake(50, btPhone.frame.origin.y +  btPhone.frame.size.height + 8, self.view.frame.size.width-60, 20);
        UIImageView*webIcon=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"web.png"]];
        webIcon.frame=CGRectMake(btWeb.frame.origin.x-30, btWeb.frame.origin.y, 20, 20);
        [scrollMainView addSubview:webIcon];
       // webIcon.contentMode = UIViewContentModeCenter;
       // webIcon.backgroundColor=[UIColor yellowColor];
    }
    else{
        
        btWeb.hidden=YES;
         btWeb.frame = CGRectMake(20, btPhone.frame.origin.y +  btPhone.frame.size.height , 280, 0);
    }
   
   
    
    CGSize expectedEmailSize = [strEmail sizeWithFont:[UIFont fontWithName:@"Verdana" size:13.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    lblMail=[[UIButton alloc]init];
    lblMail.frame=CGRectMake(265, btnShareDetail.frame.origin.y+btnShareDetail.frame.size.height+5, 50, 30);
   
    [lblMail setTitleColor:UIColorFromRGB(0x007AFF)  forState:UIControlStateNormal];
    lblMail.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    lblMail.titleLabel.font = [UIFont systemFontOfSize:16.0];
   // lblMail.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);

    [lblMail addTarget:self action:@selector(emailButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [scrollMainView addSubview:lblMail];
   // UILabel *lblMail = [[UILabel alloc] init];
    if([[dictResult objectForKey:@"show_email"]isEqualToString:@"Yes"]){
        lblMail.frame=CGRectMake(50, btWeb.frame.origin.y +  btWeb.frame.size.height+10, 280, expectedEmailSize.height);
        lblMail.hidden=NO;
        UIImageView*mailIcon=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"email.png"]];
        mailIcon.frame=CGRectMake(lblMail.frame.origin.x-30, lblMail.frame.origin.y, 20, 20);
        [scrollMainView addSubview:mailIcon];
    }
    else{
      
        lblMail.hidden=YES;
        lblMail.frame=CGRectMake(20, btWeb.frame.origin.y +  btWeb.frame.size.height, 280, 0);
    }

   // lblMail.text = strEmail;
     [lblMail setTitle:strEmail forState:UIControlStateNormal];
    
   // lblMail.numberOfLines = 100;
    lblMail.backgroundColor = [UIColor clearColor];
   // [lblMail setFont:[UIFont fontWithName:@"Verdana" size:14.0]];
    [scrollMainView addSubview:lblMail];
    lblAddr1 = [[UILabel alloc] init];
    CGSize expectedSubTitleSize = [strAddress sizeWithFont:[UIFont fontWithName:@"Verdana" size:13.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    lblAddr1.frame = CGRectMake(20, lblMail.frame.origin.y + lblMail.frame.size.height + 10, 280, expectedSubTitleSize.height);
    lblAddr1.text = strAddress;
    lblAddr1.backgroundColor = [UIColor clearColor];
    lblAddr1.textColor=UIColorFromRGB(0x007AFF);
    lblAddr1.numberOfLines = 50;
    [lblAddr1 setFont:[UIFont systemFontOfSize:16.0]];
   
    [scrollMainView addSubview:lblAddr1];
    
    if([[dictResult objectForKey:@"disp_address"]isEqualToString:@"yes"]){
        lblAddr1.hidden=NO;
        lblAddr1.frame = CGRectMake(50, lblMail.frame.origin.y + lblMail.frame.size.height + 10, 280, expectedSubTitleSize.height+5);
         btnAddr.frame=lblAddr1.frame;
        UIImageView*pinIcon=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"red.png"]];
        pinIcon.frame=CGRectMake(lblAddr1.frame.origin.x-30, lblAddr1.frame.origin.y, 20, 20);
        [scrollMainView addSubview:pinIcon];
    }
    else{
        lblAddr1.hidden=YES;
        btnAddr.hidden=YES;
        lblAddr1.frame = CGRectMake(20, lblMail.frame.origin.y + lblMail.frame.size.height , 280,0);
    }
   
    
   
    
    CGSize expectedDetailSize = [strDetails sizeWithFont:[UIFont fontWithName:@"Verdana" size:15.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    
    
    UITextView *lblDetals = [[UITextView alloc] initWithFrame:CGRectMake(20, lblAddr1.frame.origin.y + lblAddr1.frame.size.height+10, 280, expectedDetailSize.height+30)];
    lblDetals.scrollEnabled=NO;
    //lblDetals.backgroundColor=[UIColor redColor];
    
//    lblDetals.numberOfLines = 100;
    lblDetals.backgroundColor = [UIColor clearColor];
    [lblDetals setFont:[UIFont fontWithName:@"Verdana" size:13.0]];
    
    strDetails =[strDetails stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    
    
     //strDetails =[strDetails stringByReplacingOccurrencesOfString:@"url" withString:@"http://www.craterlakeinstitute.com"];
    
    
    strDetails =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",strDetails];
    
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[strDetails dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
   lblDetals.dataDetectorTypes = UIDataDetectorTypeAll;
    lblDetals.dataDetectorTypes = UIDataDetectorTypeLink;
    
    lblDetals.attributedText = attrStr;
    lblDetals.editable=NO;
     [scrollMainView addSubview:lblDetals];
    
//    UIWebView *detailTxtWeb=[[UIWebView alloc]initWithFrame:CGRectMake(20, lblAddr1.frame.origin.y + lblAddr1.frame.size.height+10, 280, expectedDetailSize.height)
//                             ];
//    
//    [detailTxtWeb loadHTMLString:[strDetails stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"] baseURL:nil];
//    //[scrollMainView addSubview:detailTxtWeb];
//  
    
 
    
    btnFavorites.frame = CGRectMake(60, lblDetals.frame.origin.y + lblDetals.frame.size.height + 5, 25, 25);
     btnShareDetail.frame = CGRectMake(20, lblDetals.frame.origin.y + lblDetals.frame.size.height + 5, 25, 25);
    share4.frame=btnShareDetail.frame;
    
    [scrollMainView setContentSize:CGSizeMake(scrollMainView.frame.size.width, lblDetals.frame.origin.y + lblDetals.frame.size.height + 50+40+40)];
    [self.view bringSubviewToFront:btnFavorites];
    [self.view bringSubviewToFront:btnFavoritesStatic];
    
     if(([[dictResult objectForKey:@"type"]isEqualToString:@"guided tour"])){
         UIButton * nextBtn=[[UIButton alloc]init];
         nextBtn.frame=CGRectMake(lblDetals.frame.origin.x+lblDetals.frame.size.width-50, btnShareDetail.frame.origin.y+btnShareDetail.frame.size.height+5, 50, 30);
         [nextBtn setTitle:@"Next" forState:UIControlStateNormal];
         [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         nextBtn.titleLabel.font=[UIFont fontWithName:@"Verdana" size:14.0];
//         [[nextBtn layer] setBorderWidth:1.0f];
//         [[nextBtn layer] setBorderColor:[UIColor blackColor].CGColor];
        // nextBtn.tag=ind
         [nextBtn setBackgroundImage:[UIImage imageNamed:@"subCell.png"] forState:UIControlStateNormal];
         [nextBtn addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];
         [scrollMainView addSubview:nextBtn];
         if([allArray count]==indeX+1||[allArray count]==1){
             nextBtn.hidden=YES;
         }
         else{
             nextBtn.hidden=NO;
         }
         
         UIButton * previuosBtn=[[UIButton alloc]init];
         previuosBtn.frame=CGRectMake(20, btnShareDetail.frame.origin.y+btnShareDetail.frame.size.height+5, 75
                                      , 30);
         [previuosBtn setTitle:@"Previous" forState:UIControlStateNormal];
         [previuosBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [previuosBtn setBackgroundImage:[UIImage imageNamed:@"subCell.png"] forState:UIControlStateNormal];
         previuosBtn.titleLabel.font=[UIFont fontWithName:@"Verdana" size:14.0];
//         [[previuosBtn layer] setBorderWidth:1.0f];
//         [[previuosBtn layer] setBorderColor:[UIColor blackColor].CGColor];
         // nextBtn.tag=ind
         [previuosBtn addTarget:self action:@selector(onPrevious) forControlEvents:UIControlEventTouchUpInside];
         [scrollMainView addSubview:previuosBtn];
         if(indeX==0){
             previuosBtn.hidden=YES;
         }
         else{
             previuosBtn.hidden=NO;
         }
     }
    
    if(([[dictResult objectForKey:@"type"]isEqualToString:@"guided tour"]||[[dictResult objectForKey:@"type"]isEqualToString:@"ordered list"]||[[dictResult objectForKey:@"type"]isEqualToString:@"normal"])){
    if([[dictResult objectForKey:@"add_to_favorites"]isEqualToString:@"yes"]){
        
        btnFavorites.hidden=NO;
        btnFavoritesStatic.hidden=NO;
        
        
    }
    else{
        btnFavorites.hidden=YES;
        btnFavoritesStatic.hidden=YES;
    }
    if([[dictResult objectForKey:@"share_listing"]isEqualToString:@"yes"]){
      
        btnShareDetailStatic.hidden=NO;
        btnShareDetail.hidden=NO;
        shareStatic4.hidden=NO;
        share4.hidden=NO;
    }
    else{
        btnShareDetailStatic.hidden=YES;
        btnShareDetail.hidden=YES;
        share4.hidden=YES;
        shareStatic4.hidden=YES;
    }
//    if([[dictResult objectForKey:@"show_call"]isEqualToString:@"Yes"]){
//        btPhone.hidden=NO;
//    }
//    else{
//         btPhone.hidden=YES;
//       
//    }
//    if([[dictResult objectForKey:@"show_web"]isEqualToString:@"Yes"]){
//       btWeb.hidden=NO;
//    }
//    else{
//        
//         btWeb.hidden=YES;
//    }
//    if([[dictResult objectForKey:@"show_email"]isEqualToString:@"Yes"]){
//    }
//    else{
//        
//    }
    if([[dictResult objectForKey:@"star_rating"]isEqualToString:@"yes"]){
    }
    else{
        
    }
    }
else{
    btnShareDetailStatic.hidden=YES;
    btnShareDetail.hidden=YES;
    share4.hidden=YES;
    shareStatic4.hidden=YES;
    }
    
    
    if (isFromFav) {
        lblNameLabel.text=@"Favorites";
        lblStaticName2.text=@"Favorites";
    }
}
- (IBAction)emailButtonPressed:(id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
        [composeViewController setMailComposeDelegate:self];
        [composeViewController setToRecipients:@[mailAddres]];
        //[composeViewController setSubject:@"example subject"];
        [self presentViewController:composeViewController animated:YES completion:nil];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //Add an alert in case of failure
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)updateSeekBar{
    float progress =audioPlayer.currentTime;
    [seekbar setValue:progress];
}

- (IBAction)seekTime:(id)sender {
    
    audioPlayer.currentTime = seekbar.value;
    
}
- (IBAction)onPlay:(id)sender {
    if([audioPlayer isPlaying]){
        [audioPlayer pause];
       // [playBtn setTitle:@"Play" forState:UIControlStateNormal];
        [playBtn setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal] ;
        return;
    }
    else{
         [ audioPlayer play];
        //[playBtn setTitle:@"Pause" forState:UIControlStateNormal];
        [playBtn setBackgroundImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal] ;
    }
    
    
}
-(void)onNext{
    if([self reachable2]){
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    
    NSString *urlString;
//    if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"])
//        urlString = [NSString stringWithFormat:@"%@%@",STATIC_URL,[dictItemDetails objectForKey:@"Id"]];
//    else if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"fixed"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"location"])
//        urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[dictItemDetails objectForKey:@"Id"]];
//    else if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"guided tour"])
    if([allArray count]==indeX+1||[allArray count]==1){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Sorry no more guided tours" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else{
        
        indeX=indeX+1;
        dictItemDetails=[allArray objectAtIndex:indeX ];
        urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[[allArray objectAtIndex:indeX ]objectForKey:@"Id"]  ];
    }
    
//    else
//        urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&sensor=true&key=%@",[dictItemDetails objectForKey:@"reference"],GOOGLE_API_KEY];
        
        CGRect lblDisPrevFrame=lblDistanceBusiness.frame;
        CGRect btnSharePrevFrame = btnShareDetail.frame;
 [scrollMainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [scrollMainView addSubview:btPhone];
        [scrollMainView addSubview:btWeb];
        [scrollMainView addSubview:lblDistanceBusiness];
        lblDistanceBusiness.frame=lblDisPrevFrame;
        [scrollMainView addSubview:btnShareDetail];
        btnShareDetail.frame=btnSharePrevFrame;
  
//    scrollMainView=[[UIScrollView alloc]init];
//    if (IS_IPHONE5)
//        scrollHeight = 400;
//    else
//        scrollHeight = 311;
//    scrollMainView.frame = CGRectMake(0, 68, 320, scrollHeight);
        [self.playerWeb stringByEvaluatingJavaScriptFromString: @"document.querySelector('audio').pause();"];
    [webService startParsing:urlString];
        [self startWebService];
   
    [self.view addSubview:vwLoading];
    }
    else{
        if([allArray count]==indeX+1||[allArray count]==1){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Sorry no more guided tours" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        else{
            
            indeX=indeX+1;
            dictItemDetails=[allArray objectAtIndex:indeX ];
            [scrollMainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
           // urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[[allArray objectAtIndex:indeX ]objectForKey:@"Id"]  ];
            [self coreDataCodeRead_guided];
        }
        
        
        
    }
    
}
-(void)onPrevious{
    if([self reachable2]){
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    
    NSString *urlString;
    //    if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"])
    //        urlString = [NSString stringWithFormat:@"%@%@",STATIC_URL,[dictItemDetails objectForKey:@"Id"]];
    //    else if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"fixed"]||[[dictItemDetails objectForKey:@"type"] isEqualToString:@"location"])
    //        urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[dictItemDetails objectForKey:@"Id"]];
    //    else if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"guided tour"])
    if(indeX==0||[allArray count]==1){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Sorry no more guided tours" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else{
        
        indeX=indeX-1;
        dictItemDetails=[allArray objectAtIndex:indeX ];
        urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[[allArray objectAtIndex:indeX ]objectForKey:@"Id"]  ];
    }
    
    //    else
    //        urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&sensor=true&key=%@",[dictItemDetails objectForKey:@"reference"],GOOGLE_API_KEY];
        
        CGRect lblDisPrevFrame=lblDistanceBusiness.frame;
        CGRect btnSharePrevFrame = btnShareDetail.frame;
        [scrollMainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [scrollMainView addSubview:btPhone];
        [scrollMainView addSubview:btWeb];
        [scrollMainView addSubview:lblDistanceBusiness];
        lblDistanceBusiness.frame=lblDisPrevFrame;
        [scrollMainView addSubview:btnShareDetail];
        btnShareDetail.frame=btnSharePrevFrame;
    
    //    scrollMainView=[[UIScrollView alloc]init];
    //    if (IS_IPHONE5)
    //        scrollHeight = 400;
    //    else
    //        scrollHeight = 311;
    //    scrollMainView.frame = CGRectMake(0, 68, 320, scrollHeight);
        [self.playerWeb stringByEvaluatingJavaScriptFromString: @"document.querySelector('audio').pause();"];
    [webService startParsing:urlString];
    
    [self.view addSubview:vwLoading];
    }
    else{
        if(indeX==0||[allArray count]==1){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Sorry no more guided tours" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        else{
            
            indeX=indeX-1;
            dictItemDetails=[allArray objectAtIndex:indeX ];
           // urlString = [NSString stringWithFormat:@"%@%@",BUSINES_URL,[[allArray objectAtIndex:indeX ]objectForKey:@"Id"]  ];
            [scrollMainView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
           
            [self coreDataCodeRead_guided];
        }
    }

}
-(IBAction)onBack:(id)sender
{
    if(_isFromIn)
    {
     [self dismissViewControllerAnimated:NO completion:nil];
    }
    if(_isFromFrom)
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }

    
    if(_isOwnGuide)
    {
        PDInappHomeViewController *inApp = [[PDInappHomeViewController alloc]init];
        inApp.toSettings=YES;
        
        [self.navigationController pushViewController:inApp animated:NO];
        
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)onHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(IBAction)onWebSite:(id)sender
{
   // UIButton *btn = (UIButton *) sender;
    NSString *urlstr = webAddress;
    if ([[urlstr substringToIndex:3] isEqualToString:@"www"]) {
        urlstr = [urlstr stringByReplacingOccurrencesOfString:@"www" withString:@"http://www"];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlstr]];
}
-(IBAction)onCall:(id)sender
{
    UIButton *btn = (UIButton *) sender;
    NSString *phoneNumber = btn.titleLabel.text; // dynamically assigned
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt://%@", phoneNumber];
    phoneURLString = [phoneURLString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"phone url string = %@",phoneURLString);
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    [[UIApplication sharedApplication] openURL:phoneURL];

}

-(IBAction)onDirection:(id)sender
{
  /*  UIAlertController* mapAlert =  [UIAlertController alertControllerWithTitle:@"MAPS" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction* googleAction = [UIAlertAction actionWithTitle:@"Open in Google Maps" style:UIAlertActionStyleDestructive
                                                           handler:^(UIAlertAction * action)
    {
        NSString *str;
        if ([dictDetails objectForKey:@"geometry"]) {
            NSDictionary *dict5 = [dictDetails objectForKey:@"geometry"];
            NSDictionary *dictLocation = [dict5 objectForKey:@"location"];
            str = [NSString stringWithFormat:@"%@,%@",[dictLocation objectForKey:@"lat"],[dictLocation objectForKey:@"lng"]];
        }
        else {
            str = [NSString stringWithFormat:@"%@,%@",[dictDetails objectForKey:@"latitude"],[dictDetails objectForKey:@"longitude"]];
        }
        
        NSLog(@"%f, %f  %@",location_updated.coordinate.latitude, location_updated.coordinate.longitude,str);
        NSString *urlstr = [NSString stringWithFormat:@"https://maps.google.com/?saddr=%f,%f&daddr=%@&output=embed",location_updated.coordinate.latitude,location_updated.coordinate.longitude,str];
        [self.view addSubview:vwDirection];
        // NSURL *url = [NSURL URLWithString:urlstr];
        // NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        //[webView loadRequest:requestObj];
        
        ///449///518
        
        NSURL *url = [NSURL URLWithString:[urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSString *embedHTML = [NSString stringWithFormat:@"<html><head><title>.</title><style>body,html,iframe{margin:0;padding:0;}</style></head><body><iframe width=%f height=%f frameborder=\"0\" style=\"border:0\" src=%@> </iframe></body></html>",webView.frame.size.width,webView.frame.size.height, urlstr];
        
        [webView loadHTMLString:embedHTML baseURL:url];
                                                               
                                                               
        
    }];
    
    UIAlertAction* appleAction = [UIAlertAction actionWithTitle:@"Open in Apple Maps" style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction * action)
    {
        
        
       /* MGLMapView *mapView = [[MGLMapView alloc] initWithFrame:self.view.bounds];
        
        mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        // Set the map’s center coordinate and zoom level.
        [mapView setCenterCoordinate:CLLocationCoordinate2DMake(59.31, 18.06)
                           zoomLevel:9
                            animated:NO];
        
        [self.view addSubview:mapView];
        
        NSString *str;
        if ([dictDetails objectForKey:@"geometry"]) {
            NSDictionary *dict5 = [dictDetails objectForKey:@"geometry"];
            NSDictionary *dictLocation = [dict5 objectForKey:@"location"];
            str = [NSString stringWithFormat:@"%@,%@",[dictLocation objectForKey:@"lat"],[dictLocation objectForKey:@"lng"]];
        }
        else {
            str = [NSString stringWithFormat:@"%@,%@",[dictDetails objectForKey:@"latitude"],[dictDetails objectForKey:@"longitude"]];
        }
        
        NSLog(@"%f, %f  %@",location_updated.coordinate.latitude, location_updated.coordinate.longitude,str);
        NSString *urlstr = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,str];
        [self.view addSubview:vwDirection];
        //http://maps.apple.com/?saddr=San+Jose&daddr=San+Francisco&dirflg=r
        
        // NSURL *url = [NSURL URLWithString:urlstr];
        // NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        //[webView loadRequest:requestObj];
        
        ///449///518
        
        NSURL *url = [NSURL URLWithString:[urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSString *embedHTML = [NSString stringWithFormat:@"<html><head><title>.</title><style>body,html,iframe{margin:0;padding:0;}</style></head><body><iframe width=%f height=%f frameborder=\"0\" style=\"border:0\" src=%@> </iframe></body></html>",webView.frame.size.width,webView.frame.size.height, urlstr];
        
        
       
        
        if (([url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"]) && [url.host isEqualToString:@"maps.apple.com"]) { //it's an apple maps app request
            NSLog(@"Attempting Apple Maps app open");
            //[[UIApplication sharedApplication]openURL:url];
            return;
        }
        
        [webView loadHTMLString:embedHTML baseURL:url];
        
        * /
   
   [self.view addSubview:vwDirection];
   appleMap.frame=webView.frame;
   [webView addSubview:appleMap];
   
                                                             
        
    }];

    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                         }];
    
    [mapAlert addAction:googleAction];
    [mapAlert addAction:appleAction];
    [mapAlert addAction:cancelAction];
    [self presentViewController:mapAlert animated:YES completion:nil];
    
    return;
   */
    NSString *str;
    if ([dictDetails objectForKey:@"geometry"]) {
        NSDictionary *dict5 = [dictDetails objectForKey:@"geometry"];
        NSDictionary *dictLocation = [dict5 objectForKey:@"location"];
        str = [NSString stringWithFormat:@"%@,%@",[dictLocation objectForKey:@"lat"],[dictLocation objectForKey:@"lng"]];
    }
    else {
        str = [NSString stringWithFormat:@"%@,%@",[dictDetails objectForKey:@"latitude"],[dictDetails objectForKey:@"longitude"]];
    }
    
    NSLog(@"%f, %f  %@",location_updated.coordinate.latitude, location_updated.coordinate.longitude,str);
    NSString *urlstr = [NSString stringWithFormat:@"https://maps.google.com/?saddr=%f,%f&daddr=%@&output=embed",location_updated.coordinate.latitude,location_updated.coordinate.longitude,str];
    [self.view addSubview:vwDirection];
   // NSURL *url = [NSURL URLWithString:urlstr];
   // NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    //[webView loadRequest:requestObj];
    
    ///449///518
    
    NSURL *url = [NSURL URLWithString:[urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *embedHTML = [NSString stringWithFormat:@"<html><head><title>.</title><style>body,html,iframe{margin:0;padding:0;}</style></head><body><iframe width=%f height=%f frameborder=\"0\" style=\"border:0\" src=%@> </iframe></body></html>",webView.frame.size.width,webView.frame.size.height, urlstr];
    
    //[webView loadHTMLString:embedHTML baseURL:url];
    
   
    
   
  NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    NSString *lng=[NSString stringWithFormat:@"%f",location_updated.coordinate.longitude];
    NSString *lat=[NSString stringWithFormat:@"%f",location_updated.coordinate.latitude];
    
    NSString* modifiedHtml;
    modifiedHtml = [htmlString stringByReplacingOccurrencesOfString:@"lng" withString:lng];
    
    modifiedHtml = [modifiedHtml stringByReplacingOccurrencesOfString:@"lat" withString:lat];
    
    
   // [webView loadHTMLString:modifiedHtml baseURL:url];
  //  [webView loadHTMLString:embedHTML baseURL:url2];

    //[webView loadHTMLString:htmlString baseURL:url];
    
    NSLog(@"htmlString :%@",modifiedHtml);
    
    
    
    
    NSString *stra;
    NSString *str1a;
    
    if ([dictDetails objectForKey:@"geometry"]) {
        NSDictionary *dict5 = [dictDetails objectForKey:@"geometry"];
        NSDictionary *dictLocation = [dict5 objectForKey:@"location"];
        stra = [NSString stringWithFormat:@"%@",[dictLocation objectForKey:@"lat"]];
        
        str1a = [NSString stringWithFormat:@"%@",[dictLocation objectForKey:@"lng"]];
    }
    else {
        stra = [NSString stringWithFormat:@"%@",[dictDetails objectForKey:@"latitude"]];
        
        str1a = [NSString stringWithFormat:@"%@",[dictDetails objectForKey:@"longitude"]];
        
    }
    
    NSLog(@"%f, %f  %@ ,%@",location_updated.coordinate.latitude, location_updated.coordinate.longitude,stra,str1a);
    
    
   
    NSString *urlstra = [NSString stringWithFormat:@"http://insightto.com/wsipad/map.php?orin_lat=%f&orin_long=%f&dest_lat=%@&dest_long=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,stra,str1a];
    
    NSLog(@"dictDetails lat and long : %@ , %@",[dictDetails objectForKey:@"latitude"],[dictDetails objectForKey:@"longitude"]);
    
    NSLog(@"urlstra : %@",urlstra);
    
    
    
    
    
    NSURL *urla = [NSURL URLWithString:[urlstra stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *embedHTMLa = [NSString stringWithFormat:@"<html><head><title>.</title><style>body,html,iframe{margin:0;padding:0;}</style></head><body><iframe width=%f height=%f frameborder=\"0\" style=\"border:0\" src=%@> </iframe></body></html>",webView.frame.size.width,webView.frame.size.height, urlstra];
    
    [webView loadHTMLString:embedHTMLa baseURL:urla];
    
    NSLog(@"url : %@",urla); NSLog(@"embedHTML : %@",embedHTMLa); NSLog(@"urlstr : %@",urlstra);
    
    
    
}
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Right Swipe");
    }
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
  return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer;
{

    return YES;
}

-(void)handlePanGesture:(UIPanGestureRecognizer *)sender {
    
    
}

/*-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( [[[inRequest URL] absoluteString] hasPrefix:@"myscheme:"] ) {
        
        return NO;
    }
}*/

-(IBAction)onFavorite:(id)sender
{
    
    
    
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    if (!isFavorite) {
        isFavorite = YES;
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"favoritesArray"];
            if ([arrFavorites count] == 0) {
                arrFavorites = [NSMutableArray new];
                if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"]) {
                    NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                    [dictToSave setValue:@"static" forKey:@"type"];
                    [arrFavorites addObject:dictToSave];
                }
                else {
                    
                    if ([dictresp valueForKey:@"business"]) {
                        NSLog(@"%@",[[dictresp valueForKey:@"business"] objectAtIndex:0]);
                        [arrFavorites addObject:[[dictresp valueForKey:@"business"] objectAtIndex:0]];
                    }else{
                        [arrFavorites addObject:dictItemDetails];
                    }
                    
                    // [arrFavorites addObject:dictItemDetails];
                    
                }

            }
            else {
                if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"]) {
                    NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                    [dictToSave setValue:@"static" forKey:@"type"];
                    [arrFavorites insertObject:dictToSave atIndex:0];
                }
                else{
                    NSLog(@"%@",dictresp);
                    if ([dictresp valueForKey:@"business"]) {
                        NSLog(@"%@",[[dictresp valueForKey:@"business"] objectAtIndex:0]);
                        [arrFavorites insertObject:[[dictresp valueForKey:@"business"] objectAtIndex:0] atIndex:0];
                    }else{
                        [arrFavorites insertObject:dictItemDetails atIndex:0];
                    }
                }

                    //[arrFavorites insertObject:dictItemDetails atIndex:0];
            }
            [plistDict setObject:arrFavorites forKey:@"favoritesArray"];
            [plistDict writeToFile:finalPath atomically:YES];

        }
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starNoFav"] forState:UIControlStateNormal];
        [btnFavoritesStatic setBackgroundImage:[UIImage imageNamed:@"starNoFav"] forState:UIControlStateNormal];
    }
    else {
        isFavorite = NO;
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"favoritesArray"];
            int k =0;
            for (NSDictionary *dict in arrFavorites) {
                if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"]) {
                    if ([[dict objectForKey:@"Id" ] isEqualToString:[dictDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"title"] isEqualToString:[dictDetails objectForKey:@"title"]]) {
                        [arrFavorites removeObjectAtIndex:k];
                        break;
                    }
                }
                else {
                    if (![dict objectForKey:@"geometry"]) {
                        if ([[dict objectForKey:@"Id" ] isEqualToString:[dictItemDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"name"] isEqualToString:[dictItemDetails objectForKey:@"name"]]) {
                            [arrFavorites removeObjectAtIndex:k];
                            break;
                        }
                    }
                    else {
                        if ([[dict objectForKey:@"geometry"] isEqualToDictionary:[dictItemDetails objectForKey:@"geometry"]]&&[[dict objectForKey:@"icon"] isEqualToString:[dictItemDetails objectForKey:@"icon"]]) {
                            [arrFavorites removeObjectAtIndex:k];
                            break;
                        }
                    }
                }
                k++;
            }
            [plistDict setObject:arrFavorites forKey:@"favoritesArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
        
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
        [btnFavoritesStatic setBackgroundImage:[UIImage imageNamed:@"starFav"] forState:UIControlStateNormal];
    }
}
-(IBAction)onClick:(id)sender
{
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        isFavorite = NO;
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"favoritesArray"];
            int k =0;
            for (NSDictionary *dict in arrFavorites) {
                if ([[dictItemDetails objectForKey:@"type"] isEqualToString:@"static"]) {
                    if ([[dict objectForKey:@"Id" ] isEqualToString:[dictDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"title"] isEqualToString:[dictDetails objectForKey:@"title"]]) {
                        [arrFavorites removeObjectAtIndex:k];
                        break;
                    }
                }
                else {
                    if (![dict objectForKey:@"geometry"]) {
                        if ([[dict objectForKey:@"Id" ] isEqualToString:[dictItemDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"name"] isEqualToString:[dictItemDetails objectForKey:@"name"]]) {
                            [arrFavorites removeObjectAtIndex:k];
                            break;
                        }
                    }
                    else {
                        if ([[dict objectForKey:@"geometry"] isEqualToDictionary:[dictItemDetails objectForKey:@"geometry"]]&&[[dict objectForKey:@"icon"] isEqualToString:[dictItemDetails objectForKey:@"icon"]]) {
                            [arrFavorites removeObjectAtIndex:k];
                            break;
                        }
                    }
                }
                k++;
            }
            [plistDict setObject:arrFavorites forKey:@"favoritesArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
        
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
        [btnFavoritesStatic setBackgroundImage:[UIImage imageNamed:@"starFav"] forState:UIControlStateNormal];
    }

}
#pragma mark - Webview delegates
- (void)webViewDidStartLoad:(UIWebView *)webVw {
    
    [webView addSubview:activityIndicator];
}

-(void)webViewDidFinishLoad:(UIWebView *)webVw {
    [activityIndicator removeFromSuperview];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)sender
{
   /* if(page==((Currpage-1)-1))
    {//last image
        
        scrollView.contentSize=CGSizeMake(size.width+320, size.height);
        UIImageView *firstimgview=(UIImageView *)[scrollView viewWithTag:0];
        [scrollView addSubview:firstimgview];
        firstimgview.frame=CGRectMake(size.width, 0, 320, 250);
        
        
    }*/
    
   

}



- (void)scrollViewDidScroll:(UIScrollView *)sender {
    //if (!pageControlBeingUsed) {
    if(scrollView.tag==10){
     
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGPoint offset = scrollView.contentOffset;
        CGSize size = scrollView.contentSize;
        
 //       CGPoint newXY = CGPointMake(size.width-600, size.height-480);
        CGFloat pageWidth = scrollView.frame.size.width;
       long int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
 //       long int Currpage=pageControl.numberOfPages;
//
    
        if (offset.x > (size.width - (320+0.1)))
        {
             [scrollView setContentOffset:CGPointMake(0, 0)];
        
            pageControl.currentPage = 0;
        }
        else
             pageControl.currentPage = page;
        /* if (offset.x > size.width - 320){
       
        }
        else*/
       
        /*
        else  if (offset.x < 0) {
              [scrollView setContentOffset:CGPointMake(Currpage*pageWidth-320, 0)];
            // [scrollView setContentOffset:CGPointMake(Currpage*pageWidth-320, 0) animated:YES];
        }*/
        
        
        
        

    }
}
//-(void)scrollViewWillBeginDecelerating:(UIScrollView *)ScrollView{
//    CGPoint offset = scrollView.contentOffset;
//    CGSize size = scrollView.contentSize;
//    if (offset.x > size.width - 320){
//    [scrollView setContentOffset:scrollView.contentOffset animated:YES];
//    }
//}
- (IBAction)onShareDetail:(id)sender {
    UIImage*imagetoshare= [self renderScrollViewToImage:scrollStaticView];
    
    NSArray *activityItems = @[ imagetoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    if ([activityVC respondsToSelector:@selector(popoverPresentationController)])
    {
        // iOS 8+
        UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
        
        presentationController.sourceView =self.view; // if button or change to self.view.
    }
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}
- (IBAction)onShareDetail1:(id)sender {
   UIImage*imagetoshare= [self renderScrollViewToImage:scrollMainView];
   
    NSArray *activityItems = @[ imagetoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    if ([activityVC respondsToSelector:@selector(popoverPresentationController)])
    {
        // iOS 8+
        UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
        
        presentationController.sourceView =self.view; // if button or change to self.view.
    }
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}
- (IBAction)onsHarestatic4:(id)sender {
    [self onShareDetail:sender];
}
- (IBAction)onshare4:(id)sender {
    [self onShareDetail1:sender];
}
- (UIImage*) renderScrollViewToImage : (UIScrollView*)scroll
{
    UIImage* image = nil;
    
    UIGraphicsBeginImageContext(scroll.contentSize);
    {
        CGPoint savedContentOffset = scroll.contentOffset;
        CGRect savedFrame = scroll.frame;
        
        scroll.contentOffset = CGPointZero;
        scroll.frame = CGRectMake(0, 0, scroll.contentSize.width, scroll.contentSize.height);
        
        [scroll.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        scroll.contentOffset = savedContentOffset;
        scroll.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    if (image != nil) {
        [UIImagePNGRepresentation(image) writeToFile: @"/tmp/test.png" atomically: YES];
        system("open /tmp/test.png");
    }
    return image;
}
-(void)coreDataCodeWrite_bus:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Businesses" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"catId == %@",[[queArray objectAtIndex:0] objectForKey:@"Id"]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSError *error;
    
    /*if(([[dictDetails objectForKey:@"type"] isEqualToString:@"location"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"fixed"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"normal"]))
     {*/
    NSPredicate * parentIdPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id==%@",[dictDetails objectForKey:@"Id"]]];
    
    [fetchRequest setPredicate:parentIdPredicate];
    /* }*/
    
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Businesses *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    
    //    for(currentQUE.options in listOfQUEToBeDeleted){
    //
    //         [context1 deleteObject:currentQUE.options];
    //    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try {
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Businesses"
                                    inManagedObjectContext:context];
            
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"busId"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"name"]forKey:@"name"];
            [Que setValue:[dictDetails objectForKey:@"Id"]forKey:@"cat_id"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"address"]forKey:@"address"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"preview"]forKey:@"preview"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"details"]forKey:@"details"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"phone"]forKey:@"phone"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"website"]forKey:@"website"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"latitude"]forKey:@"latitude"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"longitude"]forKey:@"longitude"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"featured"]forKey:@"featured"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"star_rating"]forKey:@"star_rating"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"add_to_favorites"]forKey:@"add_to_favorites"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"share_listing"]forKey:@"share_listing"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"fixed_order"]forKey:@"fixed_order"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_call"]forKey:@"show_call"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_web"]forKey:@"show_web"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"website_text"]forKey:@"website_text"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"email_address"]forKey:@"email_address"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_email"]forKey:@"show_email"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"email_text"]forKey:@"email_text"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"disp_address"]forKey:@"disp_address"];
            
            
            NSMutableArray*muts=[[NSMutableArray alloc]init];
            @try {
                
                
                for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
                    
                    NSString*str1=str;
                    // str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    [muts addObject:str1];
                    if(str1.length>0)
                    {
                        
                    }
                }
            }
            @catch (NSException *exception) {
                NSString*str = [[queArray objectAtIndex:i] objectForKey:@"image"];
                [muts addObject:str];
            }
            
            
            [Que setValue:muts forKey:@"image"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"type"]forKey:@"type"];
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            /* if(str.length>0)
             {
             [self insertImagesToDownload:str];
             [self sdImageCaching:str];
             }*/
            [Que setValue:str forKey:@"thumb"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"weight"]forKey:@"weight"];
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                //            [self performSelectorInBackground:@selector(updateProgress:) withObject:[NSNumber numberWithFloat:i]];
                // [self stopCircleLoader4];
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    // [progressView removeFromSuperview];
    // [self coreDataCheck];
    
}
-(void)coreDataCodeRead_guided {
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Businesses" inManagedObjectContext:context];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
    int i;
    
    //    for (i=0; i<[dictCategory count]; i++) {
    //        //   NSString*categoryId=[[[[[queArray objectAtIndex:i]objectForKey:@"category"]objectForKey:@"text"]stringByReplacingOccurrencesOfString:@"\n" withString:@""]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //        // NSDictionary* dictID=[arrResponse objectAtIndex:i];
    //        cateID=[dictCategory objectForKey:@"id"];
    NSPredicate * parentIdPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"busId==%@",[[allArray objectAtIndex:indeX ]objectForKey:@"Id"]]];
      NSPredicate *compountPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[parentIdPredicate]];
   /* NSPredicate * parentIdPredicate1 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"type == 'guided tour'"]];
    NSPredicate *compountPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[parentIdPredicate, parentIdPredicate1]];*/

    [fetchRequest setPredicate:compountPredicate];
    //        }
    
    //fetchRequest.sortDescriptors = @[descriptor];
    //[fetchRequest setPredicate:compountPredicate];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    NSLog( @"error %@",error);
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
        [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
        [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
        [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
        [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
        [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
        [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
        [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
        [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
        [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
        [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
        [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
        [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
        [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
        
        // Answer *dof=manuf.options;
        
        
        //            if(dof.ans1!=nil){
        //                [tempManufacturerDictionary setObject:dof.ans1 forKey:@"ans1"];}
        //            if(dof.ans2!=nil){
        //                [tempManufacturerDictionary setObject:dof.ans2 forKey:@"ans2"];}
        //            if(dof.ans3!=nil){
        //                [tempManufacturerDictionary setObject:dof.ans3 forKey:@"ans3"];}
        //            if(dof.ans4!=nil){
        //                [tempManufacturerDictionary setObject:dof.ans4 forKey:@"ans4"];}
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array addObject:tempManufacturerDictionary];
    }
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp3 = [array sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array=[NSMutableArray arrayWithArray:arrtemp3];
    NSMutableDictionary*mutD=[[NSMutableDictionary alloc]init];
    [mutD setValue:array forKey:@"business"];
    dictresp=mutD;
    [self webServiceResponse:nil];
    
    // arraySmpl = [NSMutableArray arrayWithArray:array];
    // fixedArrayCount = [arraySmpl count];
    //[tblList reloadData];
}
-(NSMutableArray*)coreDataCodeRead_static{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Static" inManagedObjectContext:context];
      fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];

    NSPredicate * parentIdPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"catid='%@'",[dictItemDetails objectForKey:@"Id"]]];
   
    NSPredicate *compountPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[parentIdPredicate]];
     [fetchRequest setPredicate:compountPredicate];
  
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    
    NSLog( @"error %@",error);
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (Static *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.appid forKey:@"appid"];
        [tempManufacturerDictionary setObject:manuf.catid forKey:@"catid"];
        
        if(manuf.image)
        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
        [tempManufacturerDictionary setObject:manuf.content forKey:@"content"];
        [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
        [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
        [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
        [tempManufacturerDictionary setObject:manuf.staticId forKey:@"staticId"];
        [tempManufacturerDictionary setObject:manuf.status forKey:@"status"];
        [tempManufacturerDictionary setObject:manuf.title forKey:@"title"];
        
        [tempManufacturerDictionary setObject:manuf.email forKey:@"email"];
        [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
        [tempManufacturerDictionary setObject:manuf.favorites forKey:@"favorites"];
        [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
        [tempManufacturerDictionary setObject:manuf.share forKey:@"share"];
        [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
        [tempManufacturerDictionary setObject:manuf.show_phone forKey:@"show_phone"];
        [tempManufacturerDictionary setObject:manuf.show_url forKey:@"show_url"];
        [tempManufacturerDictionary setObject:manuf.urll forKey:@"url"];
        [tempManufacturerDictionary setObject:manuf.url_text forKey:@"url_text"];
        
        
      
        
        [array addObject:tempManufacturerDictionary];
    }
 
   
    
    return array;
    
   // arraySmpl = [NSMutableArray arrayWithArray:array];
   // fixedArrayCount = [arraySmpl count];
    //[tblList reloadData];
}

@end
