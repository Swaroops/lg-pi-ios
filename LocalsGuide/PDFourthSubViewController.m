

#import "PDRssListViewController.h"
#import "PDFourthSubViewController.h"
#import "PDMapViewController.h"
#import "PDDealsViewController.h"
#import "PDEventsViewController.h"
#import "PDDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "PDListViewController.h"
#import "Reachability.h"
#import "GMDCircleLoader.h"
#import "PDAppDelegate.h"
#import "Inapp.h"
#import "CustomIOSAlertView.h"
#import "MKStoreKit.h"
#import "NSString+MD5.h"


@interface PDFourthSubViewController ()<CustomIOSAlertViewDelegate>
{
    BOOL isLocationUpdated, isHotSpotAd;
    int banery;
    AsyncImageView *ad1Image;
    CustomIOSAlertView *customAlert;
    NSInteger currentIndex;
    
    UIView *loaderVC;
    UIActivityIndicatorView *spinnerVC;
}
@end

@implementation PDFourthSubViewController
NSArray *arrHere;
NSString *typeHere, *titleName, *adLink;
CLLocation *location_updated;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    if (IS_IPHONE5)
        banery = 470;
    else
        banery = 382;
    
    CGRect frame=tblContents.frame;
    frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-2;
    frame.origin.y=67;
    tblContents.frame = frame;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    titleLabel.text = titleName;
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable])
    if(![removAdStatus isEqualToString:@"purchased"])
        [self showAd];
    // Do any additional setup after loading the view from its nib.
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    searchBar.placeholder=@"search";
    searchBar.delegate = self;
    searchBar.showsCancelButton=YES;
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTintColor:[UIColor whiteColor]];
    
    loaderVC= [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    loaderVC.backgroundColor=[UIColor whiteColor];
    
    
    spinnerVC = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinnerVC.center = CGPointMake([[UIScreen mainScreen]bounds].size.width / 2.0, [[UIScreen mainScreen]bounds].size.height / 2.0);

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification123"
                                               object:nil];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)searchActionFourSubVC:(id)sender {
    
    tblContents.tableHeaderView =  nil;
    
    tblContents.tableHeaderView = searchBar;
    [searchBar becomeFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    [self.view addSubview:loaderVC];
    [loaderVC addSubview:spinnerVC];
    [spinnerVC startAnimating];
    
    if(![self reachable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Search not available in offline mode"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [loaderVC removeFromSuperview];
        
        return;
    }
    else
    {
        [self searchKeywrd];
        
    }
}
-(void)searchKeywrd
{
    NSString *keywrd;
    
    keywrd = searchBar.text;
    
    keywrd = [keywrd stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if(keywrd.length <3)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Search term should have a length of minimum 3 characters"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [loaderVC removeFromSuperview];
        
        return;
    }
    
    
    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    
    
    NSString *lat= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
    NSString *lon= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
    NSLog(@"lat : %@ , long : %@",lat,lon);
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@text=%@&id=%@&lat=%@&long=%@",SEARCH_URL,keywrd,str,lat,lon];
    
    NSLog(@"url searchVC = %@",wUrl);
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
        //  NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            searchArray=[[NSMutableArray alloc]init];
            searchArray=[json objectForKey:@"business"];
            
            if([searchArray count]<1)
            {
                [searchBar becomeFirstResponder];
                searchBar.text=@"";
                [loaderVC removeFromSuperview];
                
                tblContents.tableHeaderView =  nil;
                
                NSString *message;
                if([json objectForKey:@"message"])
                {
                    message = [json objectForKey:@"message"];
                    
                }
                else
                {
                    message=[json objectForKey:@"result"];
                }
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:[NSString stringWithFormat:@"%@",message]
                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [loaderVC removeFromSuperview];
            }
            else
            {
                //[searchListView reloadData];
                //[self.view addSubview:searchView];
                [[NSUserDefaults standardUserDefaults]setObject:data2 forKey:@"searchData"];
                
                [loaderVC removeFromSuperview];
                // [activityIndicator1 stopAnimating];
                
                PDListViewController *listViewController = [[PDListViewController alloc] init];
                // [listViewController fixedWebServiceResponsee:data];
                listViewController.isSomethingEnabled = YES;
                listViewController.isfromVC=YES;
                listViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:listViewController animated:YES];
                
                
                
                
            }
            [self.view endEditing:YES];
            
        });
        
    }];
    
    [dataTask resume];
    
    
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
    tblContents.tableHeaderView =  nil;
    searchBar.text = @"";
}


-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
       ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
        [self.view addSubview:ad1Image];
        UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        ad1Btn.frame = CGRectMake(0, banery, 320, 50);
        [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
       // [self.view addSubview:ad1Btn];
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_listing"] isEqualToString:@"FALSE"])) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
            adLink = [dict objectForKey:@"link_listing"];
        }
        else {
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"CatAdExist"] isEqualToString:@"YES"])
            {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"CatAd"];
                if (![[dict objectForKey:@"imageLink"] isEqualToString:@"NA"]) {
                    ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"imageLink"]];
                    adLink = [dict objectForKey:@"webLink"];
                    int scrollHeight;
                    if (IS_IPHONE5)
                        scrollHeight = 400;
                    else
                        scrollHeight = 311;
                    tblContents.frame = CGRectMake(0, 48, 320, scrollHeight);
                }
            }
            else {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
                if (![[dict objectForKey:@"ad_2"] isEqualToString:@"NA"]) {
                    
                    ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_2"]];
                    adLink = [dict objectForKey:@"link_2"];
                    int scrollHeight;
                    if (IS_IPHONE5)
                        scrollHeight = 400;
                    else
                        scrollHeight = 311;
                    tblContents.frame = CGRectMake(0, 48, 320, scrollHeight);
                }
            }
        }
    }
}
-(void)removeBanner
{
    tblContents.frame = CGRectMake(0, 48, 320,  self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height);
    tblContents.frame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height+2, 320,  self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-self.navigationController.navigationBar.frame.size.height-2);
    
    [bannerView_ removeFromSuperview];
    [ad1Image removeFromSuperview];
    
}
-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    int scrollHeight;
    if (IS_IPHONE5)
        scrollHeight = 400;
    else
        scrollHeight = 311;
    tblContents.frame = CGRectMake(0, 48, 320, scrollHeight);
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    isLocationUpdated = YES;
    location_updated = [locations lastObject];
    //NSLog(@"updated coordinate are %@",location_updated);
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getArray:(NSArray *) passArray
         ofType:(NSString *) passType
       withName:(NSString *) name
{
    arrHere = passArray;
    typeHere = passType;
    titleName = name;
}

-(IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
#pragma mark - UITableView delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrHere count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dict = [arrHere objectAtIndex:indexPath.row];
    cell.backgroundColor=[UIColor whiteColor];
    cell.contentView.backgroundColor=[UIColor whiteColor];
    UIImageView *imgCellBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    imgCellBg.image = [UIImage imageNamed:@"subCell.png"];
   // [cell.contentView addSubview:imgCellBg];
    int titleX;
    if (![[dict objectForKey:@"image"] isEqualToString:@"FALSE"]) {
#define IMAGE_VIEW_TAG 99
        UIImageView *img = [[AsyncImageView alloc] init];
        img.frame = CGRectMake(15, 5, 60, 60);
        
        
        NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        
        NSString *imgKey;
        if ([typeHere isEqualToString:@"fixed"])
           imgKey=[dict objectForKey:@"image"];
        else if([typeHere isEqualToString:@"location"])
            imgKey=[dict objectForKey:@"icon"];
        else
            imgKey=[dict objectForKey:@"image"];
        

        
            img.image=[imageCache imageFromDiskCacheForKey:imgKey];
            
            if (!img.image)
            {
                    [img sd_setImageWithURL:[NSURL URLWithString:imgKey] placeholderImage:nil options:SDWebImageRefreshCached];
                    img.imageURL = [NSURL URLWithString:imgKey];
            }
            
        
        
        img.tag = IMAGE_VIEW_TAG;
        [cell.contentView addSubview:img];
        titleX = 100;
    }
    else
        titleX = 20;
    
    NSInteger *points=[[dict objectForKey:@"points"] integerValue];
    
    NSLog(@" - %ld",points);
    
    NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
    
    BOOL forPuchase=NO;
    
    if(![unlimitedPurchase isEqualToString:@"purchased"])
        if([[dict objectForKey:@"paid"] isEqualToString:@"paid"]&&points>0)
        {
            
            
            
            if(![self checkCategoryPurchased:[dict objectForKey:@"Id"]])
            {
                
                
                
                forPuchase=YES;
                UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width-50, 10, 35, 35)];
                catPointsLbl.backgroundColor=[UIColor clearColor];
                catPointsLbl.font=[UIFont boldSystemFontOfSize:22];
                //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
                catPointsLbl.textAlignment=NSTextAlignmentCenter;
                catPointsLbl.textColor=[UIColor grayColor];
                catPointsLbl.text= [dict objectForKey:@"points"];
                [cell addSubview:catPointsLbl];
                
                catPointsLbl.layer.borderColor = [UIColor grayColor].CGColor;
                catPointsLbl.layer.borderWidth = 2.0;
                
                catPointsLbl.layer.masksToBounds = YES;
                catPointsLbl.layer.cornerRadius = 7.0;
                
                UILabel *catPointsLbl2=[[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width-50, 45, 35, 15)];
                catPointsLbl2.backgroundColor=[UIColor clearColor];
                
                catPointsLbl2.font=[UIFont systemFontOfSize:11];
                catPointsLbl2.textAlignment=NSTextAlignmentCenter;
                catPointsLbl2.textColor=[UIColor grayColor];
                
                catPointsLbl2.textColor=[UIColor grayColor];
                if([[dict objectForKey:@"points"] isEqualToString:@"1"])
                {
                    catPointsLbl2.text=@"Point";
                }
                else
                {
                    catPointsLbl2.text=@"Points";
                }
                [cell addSubview:catPointsLbl2];
                
//                catPointsLbl2.text=@"Points";
//                [cell addSubview:catPointsLbl2];
                
                
            }
        }
    
    if (!forPuchase) {
        UIImageView *imDetail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"subMore.png"]];
        imDetail.frame = CGRectMake(290, 20, 15, 28);
        [cell.contentView addSubview:imDetail];
        
    }

    
    
    UILabel *titlelabelh = [[UILabel alloc] init];
    titlelabelh.frame = CGRectMake(titleX, 2, 250 - titleX, 35);
    titlelabelh.numberOfLines = 2;
    [titlelabelh setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    titlelabelh.textColor = [UIColor blackColor];
    titlelabelh.text = [dict objectForKey:@"name"];
    
    [cell.contentView addSubview:titlelabelh];
    
    UILabel *lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(titleX, 45, 250 - titleX, 45)];
    lblDesc.numberOfLines = 3;
    lblDesc.backgroundColor = [UIColor clearColor];
    [lblDesc setFont:[UIFont fontWithName:@"Verdana" size:11]];
    if ([typeHere isEqualToString:@"fixed"])
        lblDesc.text = [dict objectForKey:@"address"];
    else if([typeHere isEqualToString:@"location"])
        lblDesc.text = [dict objectForKey:@"vicinity"];
    [cell.contentView addSubview:lblDesc];
    
   
    
    return cell;
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    
    [alertView close];
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"TestNotification123"])
        NSLog (@"Successfully received the test notification!");
    
    [self loaderStop];
}

#pragma mark - PurchaseAction Loader
UIView *overlaay;
-(void)loaderStartWithText :(NSString *)loaderText
{
    
    
    overlaay =[[UIView alloc]initWithFrame:self.view.window.rootViewController.view.bounds];
    overlaay.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    overlaay.tag = 1001;
    [overlaay setUserInteractionEnabled:YES];
    [GMDCircleLoader setOnView:overlaay withTitle:loaderText animated:YES];
    [self.view.window.rootViewController.view addSubview:overlaay];
    // [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self  selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
}


-(void)loaderStop
{
    
    
    [GMDCircleLoader hideFromView:overlaay animated:YES];
    [overlaay removeFromSuperview];
}
#pragma mark -Inapp- Purchase Notification Methods
- (void) inappSuccessNotification:(NSNotification *) notification
{
    
    [self loaderStop];
    
    
    
    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    dictionary=[inappStr objectAtIndex:0];
    
    
    
    
    
    
    
    
    
    NSString *notificationInap=[NSString stringWithFormat:@"%@",notification.object];
    
    if([notificationInap isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
    {
        NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self  removeBanner];
    }
    else if([notificationInap isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
    {
        
        
        
        NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [tblContents reloadData];
        
    }
    
    
}
- (void) inappFailureNotification:(NSNotification *) notification
{
    [self loaderStop];
    
    
}
- (void) inappReStoreSuccessNotification:(NSNotification *) notification
{
    
    [self loaderStop];
    
    
    
    
    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    dictionary=[inappStr objectAtIndex:0];
    
    
    
    
    NSString *notificationInap=[NSString stringWithFormat:@"%@",notification.object];
    
    if([notificationInap isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
    {
        NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self  removeBanner];
    }
    else if([notificationInap isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
    {
        
        
        
        NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [tblContents reloadData];
        
    }
    
    
    
}
- (void) inappReStoreFailureNotification:(NSNotification *) notification
{
    
    [self loaderStop];
    
    
    
}
#pragma mark -Inapp- Purchase Purchase
-(void)inappPurchasePopShow:(NSInteger)catIndex
{
    
    
    NSDictionary *dict=[arrHere objectAtIndex:catIndex];
    
    NSMutableArray *nonconsumableIds=[[NSMutableArray alloc]init];
    
    
    //  NSInteger points=[[dict objectForKey:@"points"] integerValue];
    
    NSNumber* myPoints=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
    if( myPoints==nil){
        myPoints=[NSNumber numberWithInt:0];
    }
    
    
    
    
    customAlert = [[CustomIOSAlertView alloc] init];
    
    
    UIView *viewInAlert = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 350)];
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSDictionary *dictionary=[inappStr objectAtIndex:0];
    
    
    
    
    int btnXpos=5;
    int btnYpos=5;
    int btnWidth  =240;
    int btnHeight =30;
    int btnYpospadding=2;
    
    // if(myPoints>=points)
    {
        UIView *pointView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, btnYpos, btnWidth, 70)];
        
        pointView.backgroundColor=[UIColor whiteColor];
        [viewInAlert addSubview:pointView];
        
        
        UILabel *pointLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, btnWidth, 40)];
        pointLbl.backgroundColor=[UIColor clearColor];
        pointLbl.font=[UIFont boldSystemFontOfSize:45];
        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
        pointLbl.textAlignment=NSTextAlignmentCenter;
        pointLbl.textColor=[UIColor blueColor];
        pointLbl.text=[NSString stringWithFormat:@"%@",myPoints];
        [pointView addSubview:pointLbl];
        
        UILabel *pointLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, btnWidth, 30)];
        pointLbl2.backgroundColor=[UIColor clearColor];
        
        pointLbl2.font=[UIFont systemFontOfSize:15];
        pointLbl2.textAlignment=NSTextAlignmentCenter;
        pointLbl2.textColor=[UIColor blueColor];
        pointLbl2.text=@"Points Available";
        [pointView addSubview:pointLbl2];
        
        btnYpos=btnYpos+70+btnYpospadding;
    }
    
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([[dictionary objectForKey:@"remove_ads"] isEqualToString:@"yes"]&&![removAdStatus isEqualToString:@"purchased"])
    {
        [nonconsumableIds addObject:[dictionary objectForKey:@"remove_ads_id_ios"]];
        [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
        
        
        UIButton *removeAdBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        
        removeAdBtn.backgroundColor=[UIColor colorWithRed:255/255.0 green:0 blue:0 alpha:1];
        [removeAdBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSString *msgStr=[NSString stringWithFormat:@"Remove Ads - $%@",[dictionary objectForKey:@"inapp_currency"]];
        
        
        [removeAdBtn setTitle:msgStr forState:UIControlStateNormal];
        removeAdBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        [viewInAlert addSubview:removeAdBtn];
        removeAdBtn.tag=0;
        removeAdBtn.titleLabel.font=[UIFont boldSystemFontOfSize:14];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
        
    }
    
    if([[dictionary objectForKey:@"one_category"] isEqualToString:@"yes"])
    {
        
        
        
        
        
        UIButton *oneCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        oneCatBtn.backgroundColor=[UIColor colorWithRed:204/255.0 green:102/255.0 blue:102/255.0 alpha:1];
        [oneCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *price=[NSString stringWithFormat:@"One Point - $%@",[dictionary objectForKey:@"one_category_price"]];
        [oneCatBtn setTitle:price forState:UIControlStateNormal];
        oneCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        oneCatBtn.tag=1;
        
        oneCatBtn.titleLabel.font=[UIFont boldSystemFontOfSize:14];
        
        
        [viewInAlert addSubview:oneCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
    }
    if([[dictionary objectForKey:@"three_category"] isEqualToString:@"yes"])
    {
        
        UIButton * threeCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        threeCatBtn.backgroundColor=[UIColor colorWithRed:255/255.0 green:204/255.0 blue:51/155.0 alpha:1];
        [threeCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSString *price=[NSString stringWithFormat:@"Three Points - $%@",[dictionary objectForKey:@"three_category_price"]];
        [threeCatBtn setTitle:price forState:UIControlStateNormal];
        threeCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        threeCatBtn.tag=3;
        threeCatBtn.titleLabel.font=[UIFont boldSystemFontOfSize:14];
        
        
        [viewInAlert addSubview:threeCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
    }
    
    
    if([[dictionary objectForKey:@"five_category"] isEqualToString:@"yes"])
    {
        
        
        UIButton * fiveCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        fiveCatBtn.backgroundColor=[UIColor colorWithRed:102/255.0 green:153/255.0 blue:204/255.0 alpha:1];
        [fiveCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSString *price=[NSString stringWithFormat:@"Five Points - $%@",[dictionary objectForKey:@"five_category_price"]];
        [fiveCatBtn setTitle:price forState:UIControlStateNormal];
        fiveCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        fiveCatBtn.tag=5;
        fiveCatBtn.titleLabel.font=[UIFont boldSystemFontOfSize:14];
        
        
        
        [viewInAlert addSubview:fiveCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
        
    }
    
    
    if([[dictionary objectForKey:@"unlimited_category"] isEqualToString:@"yes"])
    {
        btnHeight=btnHeight+20;
        
        UIView *fullAccessView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight)];
        fullAccessView.backgroundColor=[UIColor colorWithRed:251/255.0 green:175/255.0 blue:93/255.0 alpha:1];
        [viewInAlert addSubview:fullAccessView];
        
        UILabel *fullAccessLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, btnWidth, 25)];
        fullAccessLbl.backgroundColor=[UIColor clearColor];
        fullAccessLbl.font=[UIFont boldSystemFontOfSize:15];
        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
        fullAccessLbl.textAlignment=NSTextAlignmentCenter;
        fullAccessLbl.textColor=[UIColor whiteColor];
        NSString *price=[NSString stringWithFormat:@"Full Access - $%@",[dictionary objectForKey:@"unlimited_category_price"]];
        fullAccessLbl.text= price;
        [fullAccessView addSubview:fullAccessLbl];
        
        UILabel *fullAccessLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 25, btnWidth, 20)];
        fullAccessLbl2.backgroundColor=[UIColor clearColor];
        
        fullAccessLbl2.font=[UIFont systemFontOfSize:13];
        fullAccessLbl2.textAlignment=NSTextAlignmentCenter;
        fullAccessLbl2.textColor=[UIColor whiteColor];
        fullAccessLbl2.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppName"];;
        [fullAccessView addSubview:fullAccessLbl2];
        
        
        
        UIButton * unlimitedCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        unlimitedCatBtn.backgroundColor=[UIColor clearColor];
        [unlimitedCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        //[unlimitedCatBtn setTitle:@"Full Access - $9.99" forState:UIControlStateNormal];
        unlimitedCatBtn.frame=CGRectMake(0, 0, btnWidth, btnHeight);
        unlimitedCatBtn.tag=10;
        [fullAccessView addSubview:unlimitedCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
        [nonconsumableIds addObject:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
         [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
        
  //  }
    
    
    // if(myPoints>=points)
 //   {
        UIView *unlockView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight)];
        unlockView.backgroundColor=[UIColor colorWithRed:35/255.0 green:207/255.0 blue:52/255.0 alpha:1];
        [viewInAlert addSubview:unlockView];
        
        
        UILabel *unlockLbl=[[UILabel alloc]initWithFrame:CGRectMake(50, 5, btnWidth-70, 20)];
        unlockLbl.backgroundColor=[UIColor clearColor];
        unlockLbl.font=[UIFont systemFontOfSize:13];
        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
        unlockLbl.textAlignment=NSTextAlignmentLeft;
        unlockLbl.textColor=[UIColor whiteColor];
        unlockLbl.text= @"Unlock Category";
        [unlockView addSubview:unlockLbl];
        unlockLbl.backgroundColor=[UIColor clearColor];
        
        UILabel *unlockLbl2=[[UILabel alloc]initWithFrame:CGRectMake(20, 25, btnWidth-70, 20)];
        unlockLbl2.backgroundColor=[UIColor clearColor];
        unlockLbl2.backgroundColor=[UIColor clearColor];
        unlockLbl2.font=[UIFont boldSystemFontOfSize:15];
        unlockLbl2.textAlignment=NSTextAlignmentCenter;
        unlockLbl2.textColor=[UIColor whiteColor];
        unlockLbl2.text=[dict objectForKey:@"name"];
        [unlockView addSubview:unlockLbl2];
        
        UIView *pointsView=[[UIView alloc]initWithFrame:CGRectMake(btnWidth-48, 2, 46, 46)];
        pointsView.backgroundColor=[UIColor whiteColor];
        [unlockView addSubview:pointsView];
        
        
        UILabel *catPointsLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 46, 30)];
        catPointsLbl.backgroundColor=[UIColor clearColor];
        catPointsLbl.font=[UIFont boldSystemFontOfSize:22];
        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
        catPointsLbl.textAlignment=NSTextAlignmentCenter;
        catPointsLbl.textColor=[UIColor blueColor];
        catPointsLbl.text= [dict objectForKey:@"points"];
        [pointsView addSubview:catPointsLbl];
        
        UILabel *catPointsLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 46, 16)];
        catPointsLbl2.backgroundColor=[UIColor clearColor];
        
        catPointsLbl2.font=[UIFont systemFontOfSize:13];
        catPointsLbl2.textAlignment=NSTextAlignmentCenter;
        catPointsLbl2.textColor=[UIColor blueColor];
        catPointsLbl2.text=@"Points";
        [pointsView addSubview:catPointsLbl2];
        
        
        
        
        
        UIButton * unLockCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        unLockCatBtn.backgroundColor=[UIColor clearColor];
        [unLockCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        //  [unLockCatBtn setTitle:@"Unlock Category" forState:UIControlStateNormal];
        unLockCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight+30);
        unLockCatBtn.tag=11;
        
        currentIndex=catIndex;
        [viewInAlert addSubview:unLockCatBtn];
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
        
    }
    
    
    
    viewInAlert.frame=CGRectMake(0, 0, 250, btnYpos);
    
    
    
    
    
    
    
    
    
    
    [customAlert setContainerView:viewInAlert];
    
    [customAlert setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [customAlert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        //[alertView close];
    }];
    
    [customAlert setUseMotionEffects:true];
    
    // And launch the dialog
    [customAlert show];
    
    
    //  NSArray *myArray = [NSArray arrayWithArray:nonconsumableIds];
    // [alert show];
    // [[MKStoreKit sharedKit] startProductRequest:myArray];
    
}

-(void)PurchaseBtnAction:(id)sender
{
    
    
    NSInteger tag=  ((UIButton*)sender).tag;
    
    
    
    if(tag==0)
    {
        //RemoveAds
        
        if ([self reachable])
        {
            [self loaderStartWithText:@"Purchasing"];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappSuccessNotification:)
                                                         name:@"inappSuccessNotif"
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappFailureNotification:)
                                                         name:@"inappFailureNotif"
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappReStoreSuccessNotification:)
                                                         name:@"inappReStoreSuccessNotif"
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappReStoreFailureNotification:)
                                                         name:@"inappReStoreFailureNotif"
                                                       object:nil];
            
            NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
            NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
            dictionary=[inappStr objectAtIndex:0];
            
            
            
            
            
            
            
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    else if(tag==1)
    {
        //One category Point purchase
        if ([self reachable]) {
            
            
            
            NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
            if( unlockables==nil){
                unlockables=[NSNumber numberWithInt:0];
            }
            
            
            NSString *OnePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"OnePoint"];
            [self loaderStartWithText:@"Purchasing"];
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:OnePoint];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
    else if(tag==3)
    {
        if([self reachable])
        {
            NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
            if( unlockables==nil){
                unlockables=[NSNumber numberWithInt:0];
            }
            [self loaderStartWithText:@"Purchasing"];
            NSString *ThreePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"ThreePoint"];
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:ThreePoint];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
    else if(tag==5)
    {
        //Five point purchase
        
        if([self reachable])
        {
            NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
            if( unlockables==nil){
                unlockables=[NSNumber numberWithInt:0];
            }
            [self loaderStartWithText:@"Purchasing"];
            
            NSString *FivePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"FivePoint"];
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:FivePoint];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
    else if(tag==10)
    {
        //Unlimited category purchase
        if([self reachable])
        {
            [self loaderStartWithText:@"Purchasing"];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappSuccessNotification:)
                                                         name:@"inappSuccessNotif"
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappFailureNotification:)
                                                         name:@"inappFailureNotif"
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappReStoreSuccessNotification:)
                                                         name:@"inappReStoreSuccessNotif"
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(inappReStoreFailureNotification:)
                                                         name:@"inappReStoreFailureNotif"
                                                       object:nil];
            
            NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
            NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
            dictionary=[inappStr objectAtIndex:0];
            
            
            
            
            
            
            
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(unlimitedCategoryPurchaseSuccess:)
                                                         name:@"unlimitedCategoryPurchaseNotif"
                                                       object:nil];
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
            
        }else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
        
        
        
        
    }
    else if(tag==11)
    {
        //Unlock the selected category
        
        NSDictionary *dict=[arrHere objectAtIndex:currentIndex];
        
        
        //  NSNumber *points=[[dict objectForKey:@"points"] integerValue];
        
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *points = [f numberFromString:[dict objectForKey:@"points"]];
        
        
        NSNumber* myPoints=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( myPoints==nil){
            myPoints=[NSNumber numberWithInt:0];
        }
        
        if([myPoints intValue]>=[points intValue])
        {
            NSString *catIdToPurchase=[dict objectForKey:@"Id"];
            BOOL status=     [self coreDataWriteInappPurchased:catIdToPurchase];
            
            if(status)
            {
                //  NSInteger newPoint=myPoints-points;
                [[MKStoreKit sharedKit]consumeCredits:points identifiedByConsumableIdentifier:@"UnlockPoints"];
                
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Category unlocked successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
                
                [tblContents reloadData];
                
                
                
                
                
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Category unlocking failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
            
        }
        else{
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please purchase points to unlock this category" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    [customAlert close];
}
#pragma mark - InApp Purchase CoreData Methods
-(BOOL)checkCategoryPurchased:(NSString*)catid
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Inapp" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    NSString *currentAppid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@ AND categoryid=%@ AND categoryType=%@",currentAppid,catid,@"subcategory4"];
    
    
    
    [fetchRequest setPredicate:predicate];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
    
    NSArray *fetchedObjects = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    [fetchRequest setEntity:entity];
    
    
    
    
    
    
    NSString *purchaseStatus;
    
    
    //NSDate *lastSyncDate;
    @try {
        Inapp *appsObj=[fetchedObjects objectAtIndex:0];
        
        purchaseStatus=appsObj.paidStatus;
        
        
        
        
    }
    @catch (NSException * e) {
        
        purchaseStatus=@"No";
        
    }
    
    
    if([purchaseStatus isEqualToString:@"Yes"])
        return true;
    else
        return false;
    
    
}
-(BOOL)coreDataWriteInappPurchased:(NSString*)catid
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    
    @try {
        
        NSError *error;
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *Que = [NSEntityDescription
                                insertNewObjectForEntityForName:@"Inapp"
                                inManagedObjectContext:context];
        
        NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
        
        [Que setValue:appid forKey:@"appid"];
        
        [Que setValue:catid forKey:@"categoryid"];
        
        [Que setValue:@"subcategory4" forKey:@"categoryType"];
        
        [Que setValue:@"Yes" forKey:@"paidStatus"];
        
        if (![context save:&error]) {
            return false;
        }
        else
            return true;
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        return false;
        
        
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    NSDictionary *dict = [arrHere objectAtIndex:indexPath.row];
    
    NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
    
    if(![unlimitedPurchase isEqualToString:@"purchased"])
    {
        NSInteger points=[[dict objectForKey:@"points"] integerValue];
        if([[dict objectForKey:@"paid"] isEqualToString:@"paid"]&&points>0)
        {
            
            
            
            if(![self checkCategoryPurchased:[dict objectForKey:@"Id"]])
            {
                
                [self inappPurchasePopShow:indexPath.row];
                
                return;
                
            }
        }
        
    }
    
    if ([[dict objectForKey:@"type"] isEqualToString:@"fixed"] ) {
        PDListViewController *listViewController = [[PDListViewController alloc] init];
        [listViewController getDictionary:dict];
        [self.navigationController pushViewController:listViewController animated:YES];
        
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"location"]||[[dict objectForKey:@"type"] isEqualToString:@"static"]) {
        if (isLocationUpdated) {
            PDListViewController *listViewController = [[PDListViewController alloc] init];
            [listViewController getDictionary:dict];
            [self.navigationController pushViewController:listViewController animated:YES];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please wait" message:@"Please wait till location updated" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"rss"]) {
        PDRssListViewController *rssListViewController = [[PDRssListViewController alloc] initWithNibName:@"PDRssListViewController" bundle:nil];
        [rssListViewController getDictionary:dict];
        [self.navigationController pushViewController:rssListViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"url"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dict objectForKey:@"external_url"]]];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"static"])
    {
        PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
        [detailViewController getDictionary:dict];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"event"]) {
        
        PDEventsViewController *viewController = [[PDEventsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
        //        if (![[dict objectForKey:@"maxeventid"] isEqualToString:@"FALSE"]) {
        //            if (![[dict objectForKey:@"maxeventid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxEventId"]]) {
        //                viewController.tabBarItem.badgeValue = @"new";
        //            }
        // }
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"deal"]) {
        PDDealsViewController *viewController = [[PDDealsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
        //        if (![[dict objectForKey:@"maxdealid"] isEqualToString:@"FALSE"]){
        //            if (![[dict objectForKey:@"maxdealid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxDealId"]]) {
        //                viewController.tabBarItem.badgeValue = @"new";
        //            }
        //        }
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else
    {
        
        PDListViewController *listViewController = [[PDListViewController alloc] init];
        [listViewController getDictionary:dict];
        [self.navigationController pushViewController:listViewController animated:YES];
        
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
@end
