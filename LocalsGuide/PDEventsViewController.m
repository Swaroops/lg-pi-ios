

#import "PDEventsViewController.h"
#import "PDViewController.h"
#import "PDDealsViewController.h"
#import "PDMapViewController.h"
#import "PDEventDetailViewController.h"
#import "PDEventsCell.h"
#import "PDAppDelegate.h"
#import "Events.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "PDEventCatCell.h"


@interface PDEventsViewController () <CLLocationManagerDelegate>
{
    int banery;
    NSArray *arrayResponse;
    BOOL isHotSpotAd;
    
    UIRefreshControl *refreshControl;
    
    AsyncImageView *tblHeaderImage;
    NSString *bannerImgURL;
    
    UIButton *btnMenu;
    UIImageView *btnMenuImg;
    
    UIButton *buttonImg;
    UIImage *btnImage;
    UIImage *btnImageSel;
    
    BOOL isFavorite;
}
@end

@implementation PDEventsViewController
CLLocation *location_updated;
NSString *adLink;
NSDictionary *dictEve;
NSDictionary *dictDetailsEve;

@synthesize isFromFav;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)offlineAvailable
{
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}

-(BOOL)reachable_events {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view addSubview:vwSettings];
    self.view.backgroundColor=[UIColor whiteColor];
    tblEvents.backgroundColor=[UIColor whiteColor];
    vwSettings.frame = CGRectMake( self.view.frame.size.width+250, 0, 250, vwSettings.frame.size.height );//[[UIScreen mainScreen] bounds]
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
       evenCatArr=[[NSMutableArray alloc]init];
    
    
    veryFirst=0;
    
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readAllEventCategories:@"firstTime"];

    
    NSUInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
    if(currentIndex==0)
       // backBtn.hidden=YES;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefreshe:) forControlEvents:UIControlEventValueChanged];
    [tblEvents addSubview:refreshControl];
    
    
   
    
    self.navigationController.navigationBarHidden = YES;
    self.tabBarItem.badgeValue = 0;
    //code change by Ram for changing GAD y position
    if (IS_IPHONE5) {
        //banery = 470;
        banery=515;
    }
    else {
        //banery = 382;
        banery=427;
    }
    
    CGRect frame=tblEvents.frame;
    frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-2;
    tblEvents.frame = frame;
    
    NSLog(@"%f",self.navigationController.navigationBar.frame.size.height);
     NSLog(@"%f",self.view.frame.size.height);
    
    titleLabel.text = self.tabBarItem.title;
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    
    indiProgress.hidden = NO;
    
    
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if ([self reachable_events])
     {
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
     }
    else
    {
        CGRect frame=CGRectZero;
        frame=tblEvents.frame;
        frame.size.height=[UIScreen mainScreen].bounds.size.height-67;
        tblEvents.frame = frame;
        
        NSLog(@"frame :%@",NSStringFromCGRect(frame));
    }
    
    
    [tblEvents setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    //[tblEvents reloadData];
    
    
    //[backgroundView setHidden:YES];

    if(![self reachable_events])
    {
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"offlineresultArray"];
       // NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
       // NSDictionary* offData = [[NSDictionary alloc] initWithDictionary:retrievedDictionary];
        
        NSMutableArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        
        NSDictionary* offData= [retrievedDictionary objectAtIndex:1];
        
        
        NSDictionary * bannerdict= [[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"];
        
        NSLog(@"bannerdict : %@",bannerdict);
        
        
        NSLog(@"offData : %@",offData);
        
        NSString*detailid = [dictDetailsEve objectForKey:@"Id"];
        
        for(NSDictionary *dictDetailss in offData)
        {
            if([detailid isEqualToString:[NSString stringWithFormat:@"%@",[dictDetailss objectForKey:@"Id"]]])
            {
                
                if(![[dictDetailss objectForKey:@"fav_icon"] isEqualToString:@""])
                {
                    if([[dictDetailss objectForKey:@"fav_icon"] isEqualToString:@"heart"])
                    {
                        btnImage= [UIImage imageNamed:@"1.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"2.png"];
                        
                    }
                    
                    else
                    {
                        btnImage= [UIImage imageNamed:@"star.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                    }
                }
                else
                {
                    //test
                    // str =@"star";
                    //
                    NSString * str=[[NSUserDefaults standardUserDefaults]stringForKey:@"fav_icon"];
                    
                    if([str isEqualToString:@"star"])
                    {
                        btnImage= [UIImage imageNamed:@"star.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                        
                    }
                    else
                    {
                        btnImage= [UIImage imageNamed:@"1.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"2.png"];
                        
                        
                    }
                }
                
                
                
            }
            
        }
        
        
        
        
        
    }
    
   
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //[self.tabBarController.tabBar setHidden:YES];
    
    {
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
        }
        
        if ([myMutableArrayAgain count] == 0) {
            isFavorite = NO;
        }
        for (dictEve in myMutableArrayAgain) {
            if ([[dictEve objectForKey:@"Id"] isEqualToString:[dictEve objectForKey:@"Id"]]&&[[dictEve objectForKey:@"event_title"] isEqualToString:[dictEve objectForKey:@"event_title"]]) {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
        }
        if (isFavorite) {
            [buttonImg setImage:btnImageSel forState:UIControlStateNormal];
        }
        else {
            [buttonImg setImage:btnImage forState:UIControlStateNormal];
        }
    }
    
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    //NSLog(@"updated coordinate are %@",location_updated);
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    
    
  //  NSString   *advStatus=[[appDetailDic objectForKey:@"settings"] objectForKey:@"admob_status"];
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if ([self reachable_events])
    if(![removAdStatus isEqualToString:@"purchased"])
     [self showAd];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if ([self reachable_events])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showAd
{
   
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {

        if (isHotSpotAd) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            if (![[dict objectForKey:@"image_listing"] isEqualToString:@"NA"]) {
                AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];
                
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
                adLink = [dict objectForKey:@"link_listing"];
                int scrollHeight;
                if (IS_IPHONE5)
                    scrollHeight = 445;
                else
                    scrollHeight = 356;
                tblEvents.frame = CGRectMake(0, 48, 320, scrollHeight);
            }
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            if (![[dict objectForKey:@"ad_2"] isEqualToString:@"NA"]) {
                AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];
                
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_2"]];
                adLink = [dict objectForKey:@"link_2"];
                int scrollHeight;
                if (IS_IPHONE5)
                    scrollHeight = 445;
                else
                    scrollHeight = 356;
                tblEvents.frame = CGRectMake(0, 48, 320, scrollHeight);
            }
        }
    }
    
    CGRect frame2 = CGRectZero;
    
    frame2=tblEvents.frame;
    frame2.size.height=self.view.frame.size.height-67;
    tblEvents.frame = frame2;
    
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}
-(void)setAdbannerWithKey:(NSString *) key
{
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    int scrollHeight;
    if (IS_IPHONE5)
        scrollHeight = 465;//445;
    else
        scrollHeight = 356;
    tblEvents.frame = CGRectMake(0, 65, 320, scrollHeight);
}

- (void)testRefreshe:(UIRefreshControl *)refreshControlH
{
    if ([self reachable_events])
        [self startWebService];
    else
         [refreshControl endRefreshing];
    
    
}

-(void)getDictionaryeve:(NSDictionary *) dictHere
{
    dictDetailsEve = dictHere;
}


-(void)startWebService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    
   if ([classIds isEqualToString:@"&clid=(null)"])
   {
      classIds=@"&clid=";
   }
      [webService startParsing:[NSString stringWithFormat:@"%@%@%@",EVENTS_NEW,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"],classIds]];
   // [webService startParsing:[NSString stringWithFormat:@"%@%@&device=device_type",EVENTS_URL,]];
}
-(void)webServiceResponse:(NSData *) responseData
{
    
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    
    
    NSArray *classArr=[[NSArray alloc]init];
    
    classArr =[dict objectForKey:@"event_category"];
    
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj writeEventCategories:classArr];
    [noDataLbl removeFromSuperview];
    
    
    noDataLbl =[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 60)];
    noDataLbl.hidden=NO;
    if (veryFirst==1) {
        //Ram
        if ([evenCatArr count]>1) {
            noDataLbl.hidden=YES;
            veryFirst=0;
            [self readAllSelected];
            
            
        }
       // return;
    }
    
    
    [indiProgress stopAnimating];
    indiProgress.hidden=YES;
    [refreshControl endRefreshing];
    
    @try {
        
        NSString *resultStr=[NSString stringWithFormat:@"%@",[dict objectForKey:@"business"]];
        if([resultStr isEqualToString:@"No Events Found"])
        {
            
            NSLog(@"%@",evenCatArr);
          
            noDataLbl.backgroundColor=[UIColor clearColor];
            noDataLbl.text=[dict objectForKey:@"business"];
            noDataLbl.font=[UIFont systemFontOfSize:24];
            noDataLbl.textAlignment=NSTextAlignmentCenter;
            
            [backgroundView addSubview:noDataLbl];
            arrayResponse=[[NSArray alloc]init];
            [tblEvents reloadData];
            if (flagMenu)
                backgroundView.frame = CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height);
            return;
        }
        else
        {
            arrayResponse = [dict objectForKey:@"business"];
            if([arrayResponse count]>0)
                [self coreDataCodeWrite_events:arrayResponse];
            NSDictionary *dicLatest = [arrayResponse objectAtIndex:0];
            [[NSUserDefaults standardUserDefaults] setObject:[dicLatest objectForKey:@"Id"] forKey:@"maxEventId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [tblEvents reloadData];
            indiProgress.hidden = YES;
            if (flagMenu)
                backgroundView.frame = CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height);
            
        }
    }
    @catch (NSException *exception) {
        arrayResponse = [dict objectForKey:@"business"];
        if([arrayResponse count]>0)
            [self coreDataCodeWrite_events:arrayResponse];
        NSDictionary *dicLatest = [arrayResponse objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults] setObject:[dicLatest objectForKey:@"Id"] forKey:@"maxEventId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [tblEvents reloadData];
        indiProgress.hidden = YES;
        if (flagMenu)
            backgroundView.frame = CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    }
}
#pragma mark - alert retry
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self startWebService];
    }
}
-(IBAction)onMenu:(id)sender
{
 /*   [timm invalidate];
    timm=nil;
    timm=  [NSTimer scheduledTimerWithTimeInterval:APP_IDLE_DELAY
                                            target:self
                                          selector:@selector(onTimerfunc)
                                          userInfo:nil
                                           repeats:NO];*/
    
    
    if (!flagMenu) {
       // vwSettings.frame=CGRectMake(self.view.frame.size.width, 0, 250, vwSettings.frame.size.height);
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveEaseInOut
                         animations:^{
                             
                             vwSettings.frame = CGRectMake(self.view.frame.size.width-250, 0, 250, vwSettings.frame.size.height);
                             vwSettings.alpha = 1.0;
                             backgroundView.frame = CGRectMake(-250, 0, self.view.frame.size.width, self.view.frame.size.height);
                         }
                         completion:^(BOOL finished) {
                              // backgroundView.frame = CGRectMake(-250, 0, 320, self.view.frame.size.height);
                         }];
        
        if (veryFirst==1)
        {
            //Ram
           /* btnMenuImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 27, 25, 25)];
                          
                          UIImage *image = [UIImage imageNamed:@"menu (2).png"];
                          [btnMenuImg setImage:image];
                          
            
            btnMenuImg.frame=CGRectMake(10, 27, 25, 25);
            btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
            btnMenu.frame = CGRectMake(0, 0, 40, 60);
            //[btnMenu setBackgroundImage:[UIImage imageNamed:@"menu (2).png"] forState:UIControlStateNormal];
            [btnMenu addTarget:self action:@selector(onMenu:) forControlEvents:UIControlEventTouchUpInside];
            btnMenu.hidden=NO;
            [btnMenu addSubview:btnMenuImg];
            [self.view addSubview:btnMenu];*/
            
           
        }
        flagMenu = !flagMenu;
    }
    
    else {
        [self readAllSelected];
        
        //[btnMenu removeFromSuperview];
        //[btnMenuImg removeFromSuperview];
        //Ram
        btnMenu.hidden=NO;
        btnMenuImg.hidden=NO;
        vwSettings.frame=CGRectMake(self.view.frame.size.width-250, 0,250, vwSettings.frame.size.height);
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveEaseInOut
                         animations:^{
                             vwSettings.frame = CGRectMake(self.view.frame.size.width,0, 250, vwSettings.frame.size.height);
                             vwSettings.alpha = 0;
                             backgroundView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);                         }
                         completion:^(BOOL finished) {
                         }];
        flagMenu = !flagMenu;
    }
    
    
}
#pragma mark - Event List Methods
-(void)processComplete
{
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readAllEventCategories:@"secondTime"];
    if (flagMenu)
        backgroundView.frame = CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height);
}
-(void)eventCategoriesAccessCompleteForFirstTime:(NSArray*)resultArray
{
    [evenCatArr removeAllObjects];
    [evenCatArr addObjectsFromArray:resultArray];
    if ([evenCatArr count]<1) {
        veryFirst=1;
        classIds=@"&clid=";
        [self startWebService];
        
    }
    else{
           veryFirst=0;
        [self readAllSelected];
    }
    
   
    
}
-(void)eventCategoriesAccessComplete:(NSArray*)resultArray
{
    [evenCatArr removeAllObjects];
    [evenCatArr addObjectsFromArray:resultArray];
    
    
    
    [eventListTbl reloadData];
    if (flagMenu)
        backgroundView.frame = CGRectMake(-320, 0, self.view.frame.size.width, self.view.frame.size.height);
    
}
-(void)eventSelectedCategoriesAccessComplete:(NSArray*)resultArray
{
    
    classIds=@"";
    if ([resultArray count]==1)
    {
        NSDictionary *dict=[resultArray objectAtIndex:0];
        classIds=[dict objectForKey:@"id"];
        
        classIds=[NSString stringWithFormat:@"&clid=%@",classIds];
        
        if([self reachable_events]){
            [self startWebService];
        }
        else{
            [self coreDataCodeRead_events];
        }
        return;
    }
    else if ([resultArray count]<1)
    {
        classIds=@"&clid=";
    }
    else
    {
        BOOL classbool=YES;
        
        for (NSDictionary *dict in resultArray) {
            
            if (classbool) {
                NSDictionary *dict=[resultArray objectAtIndex:0];
                classIds=[dict objectForKey:@"id"];
                classbool=NO;
            }
            else
                classIds=[NSString stringWithFormat:@"%@,%@",classIds,[dict objectForKey:@"id"]];
            
        }
        classIds=[NSString stringWithFormat:@"&clid=%@",classIds];
    }
    if([self reachable_events]){
        //if(![classIds isEqualToString:@"&clid="]){
            [self startWebService];
       // }
    }
    else{
        [self coreDataCodeRead_events];
    }
    
}
- (void)eventSwitchAction:(id)sender
{
    UISwitch *tempSwitch=(UISwitch*)sender;
    NSString *switchTo;
    if (tempSwitch.on==YES)
        switchTo=@"1";
    else
        switchTo=@"0";
    
    
    
    int tag= (int)((UISwitch*)sender).tag;
    
    NSDictionary *dict=[evenCatArr objectAtIndex:tag];
    DBOperations *obj22=[[DBOperations alloc]init];
    obj22.delegate=self;
    [obj22 updateEventCategoryId:dict onOf:switchTo];
    
    
    if (flagMenu)
        backgroundView.frame = CGRectMake(-250, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    
}
-(void)eventCatUpdateComplete
{
    if (flagMenu)
        backgroundView.frame = CGRectMake(-250, 0, self.view.frame.size.width, self.view.frame.size.height);
    NSLog(@"event single row updated");
    
}
-(void)readAllSelected
{
    DBOperations *obj1=[[DBOperations alloc]init];
    obj1.delegate=self;
    [obj1 readSelectedEventCategories];
    
    if (flagMenu)
        backgroundView.frame = CGRectMake(-250, 0, self.view.frame.size.width, self.view.frame.size.height);
    
}
#pragma mark - UITableView delegate and datasource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    if(section==0){
//        
//        tblHeaderImage = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, kTableViewHeaderHeight)];
//        [tblHeaderImage setBackgroundColor:[UIColor clearColor]];
//        
//        
//        
//        NSDictionary * bannerdict= [[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"];
//        
//        NSArray *classArr=[[NSArray alloc]init];
//        classArr =[bannerdict objectForKey:@"maincategory"];
//        for(NSDictionary *avatar in classArr)
//        {
//            if([[avatar objectForKey:@"name"] isEqualToString:titleLabel.text])
//            {
//                bannerImgURL=[avatar objectForKey:@"header_image"];
//                
//                NSLog(@"bannerdict : %@",bannerImgURL);
//                
//            }
//        }
//        tblHeaderImage.imageURL=[NSURL URLWithString:bannerImgURL];
//        return tblHeaderImage;
//    }
//    else{
        return nil;
        
   // }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if(section==0){
//        
//        int height;
//        NSDictionary * bannerdict= [[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"];
//        
//        NSArray *classArr=[[NSArray alloc]init];
//        classArr =[bannerdict objectForKey:@"maincategory"];
//        for(NSDictionary *avatar in classArr)
//        {
//            if([[avatar objectForKey:@"name"] isEqualToString:titleLabel.text])
//            {
//                if ([[avatar objectForKey:@"header_image"] isEqualToString:@"NA"]) {
//                    height= 0;
//                    break;
//                }
//                else{
//                    height= kTableViewHeaderHeight;
//                    break;
//                }
//                
//            }
//        }
//        return height;
//        
//    }
//    else{
        return 0;
        
   // }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        
        
        return 0;
    }
    else{
        if (tableView.tag==0) {
            
            if([self reachable_events])
            return   [evenCatArr count];
            else return  1;
        }
        else{
            return [arrayResponse count];
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==0) {
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    NSDictionary *dict2 = [evenCatArr objectAtIndex:indexPath.row];
    PDEventCatCell *cell = (PDEventCatCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    // if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDEventCatCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    
        
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.responseTarget = self;
    UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectZero];
    onoff.tag=indexPath.row;
    
    if ([[dict2 objectForKey:@"status"] isEqualToString:@"1"])
        [onoff setOn:YES];
    else
        [onoff setOn:NO];
    
    [onoff addTarget: self action: @selector(eventSwitchAction:) forControlEvents:UIControlEventValueChanged];
    // Set the desired frame location of onoff here
    [cell addSubview: onoff];
    
    onoff.frame=CGRectMake(cell.frame.size.width-60, (cell.frame.size.height/2)-15, 51, 31);
    
    
    
    
    
    cell.lblCatName.text = [dict2 objectForKey:@"name"];
    
        if([self reachable_events])
        {
        
        }
        else
        {
           cell.lblCatName.text = @"NA";
        }
    
    
    return cell;
    
}
    else
    {
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    PDEventsCell *cell = (PDEventsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDEventsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSDictionary *dict = [arrayResponse objectAtIndex:indexPath.row];
    cell.lblName.text = [dict objectForKey:@"event_title"];
    cell.lblTime.text = [dict objectForKey:@"event_time"];
    cell.lblPlace.text = [dict objectForKey:@"event_address"];
    cell.lblDesc.text = [dict objectForKey:@"event_description"];
   // cell.imgEvent.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
 /*   if ([self reachable_events]) {
        cell.imgEvent.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
    }
    else{
      [cell.imgEvent sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]] placeholderImage:nil options:SDWebImageRefreshCached];
    }
    
    */
    
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    
    cell.imgEvent.image=[imageCache imageFromDiskCacheForKey:[dict objectForKey:@"thumb"]];
    
    if (!cell.imgEvent.image)
    {
//        if ([self reachable_events]) {
//            
//          
//            [cell.imgEvent sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
//              cell.imgEvent.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
//        }
        
        NSString *str = [dict objectForKey:@"thumb"];
        
        if (![self offlineAvailable])
        {
            
            [cell.imgEvent sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]] placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageRefreshCached];
            cell.imgEvent.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
            
        }
        
        else
            
        {
            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
            
            NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
            
            
            
            
            NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
            
            NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit);
            NSLog(@"secondBit folder :%@",secondBit2);
            
            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            NSLog(@"str url :%@",str);
            
            NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/event_images/thumb",secondBit2]];
            
            NSArray* str3 = [str componentsSeparatedByString:@"event_images/thumb/"];
            
            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit3);
            NSLog(@"secondBit folder :%@",secondBit3);
            
            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
            
            UIImage* image = [UIImage imageWithContentsOfFile:Path];
            
            cell.imgEvent.image=image;
            
            // [self loadImageFromURL1:str image:tblHeaderImage];
            
            // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
            
            
        }
    }
   
//     [cell.imgEvent sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"thumb"]] placeholderImage:nil options:SDWebImageRefreshCached];
//
//    if (!cell.imgEvent.image)
//    {
//        if ([self reachable_events]) {
//           cell.imgEvent.imageURL = [NSURL URLWithString:[dict objectForKey:@"thumb"]];
//        }
//    }
    
        
        cell.imgEvent.layer.cornerRadius = 6.0;
        cell.imgEvent.layer.masksToBounds = YES;
        
        buttonImg = (UIButton *)[cell.contentView.subviews objectAtIndex:0];
        
        buttonImg =[[UIButton alloc]init];
        
        btnImage = [UIImage imageNamed:@"1.png"];
        btnImageSel = [UIImage imageNamed:@"2.png"];
        
        
        NSString * str=[[NSUserDefaults standardUserDefaults]stringForKey:@"fav_icon"];
        
        NSLog(@"str fav_icon : %@",str);
        
       if([self reachable_events])
       {
        if(![[dictDetailsEve objectForKey:@"fav_icon"] isEqualToString:@""])
        {
            if([[dictDetailsEve objectForKey:@"fav_icon"] isEqualToString:@"heart"])
            {
                btnImage= [UIImage imageNamed:@"1.png"];
                
                btnImageSel= [UIImage imageNamed:@"2.png"];
                
            }
            
            else
            {
                btnImage= [UIImage imageNamed:@"star.png"];
                
                btnImageSel= [UIImage imageNamed:@"star_sel.png"];
            }
        }
        else
        {
            //test
            // str =@"star";
            //
            if([str isEqualToString:@"star"])
            {
                btnImage= [UIImage imageNamed:@"star.png"];
                
                btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                
            }
            else
            {
                btnImage= [UIImage imageNamed:@"1.png"];
                
                btnImageSel= [UIImage imageNamed:@"2.png"];
                
                
            }
        }

       }
        buttonImg = [UIButton buttonWithType:UIButtonTypeCustom];
        
        buttonImg.frame = CGRectMake(10, 45, 50, 50);
        
        // [buttonImg setTitleEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
        //top ,left , bottom ,right
        [buttonImg setImageEdgeInsets:UIEdgeInsetsMake(20, 5, 5, 20)];
        
        [[buttonImg imageView] setContentMode: UIViewContentModeScaleAspectFit];
        /////
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
        }
        
        if ([myMutableArrayAgain count] == 0) {
            isFavorite = NO;
        }
        for (dictEve in myMutableArrayAgain) {
            if ([[dict objectForKey:@"Id"] isEqualToString:[dictEve objectForKey:@"Id"]]&&[[dict objectForKey:@"event_title"] isEqualToString:[dictEve objectForKey:@"event_title"]]) {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
        }

        if (isFavorite) {
            [buttonImg setImage:btnImageSel forState:UIControlStateNormal];
        }
        else {
            [buttonImg setImage:btnImage forState:UIControlStateNormal];
        }

        /////
        
      //  [buttonImg setBackgroundImage:btnImage forState:UIControlStateNormal];
        [self->buttonImg addTarget:self action:@selector(onFavbtnList:) forControlEvents:UIControlEventTouchUpInside];
        
        buttonImg.userInteractionEnabled = YES;
        
        buttonImg.tag = indexPath.row;
        
        NSLog(@"buttonImg tag vc: %ld",(long)buttonImg.tag);
        
        [cell.contentView addSubview:buttonImg];
    
    return cell;
    }
}
-(void)onFavbtnList:(UIButton*)sender
{
    
    
    
    NSLog(@"sender tag : %ld",(long)sender.tag);
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
    }
    
    if ([myMutableArrayAgain count] == 0) {
        isFavorite = NO;
    }
    
     NSDictionary *dict = [arrayResponse objectAtIndex:sender.tag];
    
    for (dictEve in myMutableArrayAgain) {
        if ([[dict objectForKey:@"Id"] isEqualToString:[dictEve objectForKey:@"Id"]]&&[[dict objectForKey:@"event_title"] isEqualToString:[dictEve objectForKey:@"event_title"]]) {
            NSLog(@"these are same man!!!");
            isFavorite = YES;
            break;
        }
        else {
            isFavorite = NO;
        }
    }

    
    if (!isFavorite)
    {
        
        isFavorite = YES;

        
        UIImage *btnImage2 = [UIImage imageNamed:@"2.png"];
        NSMutableDictionary *dict3 = [arrayResponse objectAtIndex:sender.tag];
        [sender setImage:btnImageSel forState:UIControlStateNormal];
        PDEventDetailViewController *eVC = [[PDEventDetailViewController alloc]init];
        [eVC getEventDetails:dict3];
       // [eVC getArray:arraySmpl :sender.tag];
        [eVC onFavorite:sender];
    }
    
    else
    {
        isFavorite = NO;
        NSLog(@"buttonImg tag : %ld",(long)buttonImg.tag);
        
        UIImage *btnImage2 = [UIImage imageNamed:@"1.png"];
        NSMutableDictionary *dict3 = [arrayResponse objectAtIndex:sender.tag];
        [sender setImage:btnImage forState:UIControlStateNormal];
        PDEventDetailViewController *eVC = [[PDEventDetailViewController alloc]init];
        [eVC getEventDetails:dict3];
        // [eVC getArray:arraySmpl :sender.tag];
        //[eVC onFavorite:sender];
        [eVC onClick:sender];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if(tableView.tag==0)
    {
        return 50.0;
    
    }
      return 100.0;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    
    NSDictionary *dict = [arrayResponse objectAtIndex:indexPath.row];
    
    NSString *eventShowStat = [dict objectForKey:@"event_continue"];
    if ([eventShowStat isEqualToString:@"yes"]) {
        
        PDEventDetailViewController *eventDetailViewCpntroller = [[PDEventDetailViewController alloc] init];
        [eventDetailViewCpntroller getEventDetails:[arrayResponse objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:eventDetailViewCpntroller animated:YES];
        return;
    }
    
    
    
    NSString *dateString = [dict objectForKey:@"event_expire_date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    
    dateFromString = [dateFormatter dateFromString:dateString];
    
    
    
    NSDate *date = [NSDate date];
    
    NSComparisonResult result = [dateFromString compare:date];
    
    
    
    switch (result)
    {
        case NSOrderedAscending:
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Event has been expired" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [alert show];
            NSLog(@"earlier"); break;
            
        }
        case NSOrderedDescending:
        {
            PDEventDetailViewController *eventDetailViewCpntroller = [[PDEventDetailViewController alloc] init];
            [eventDetailViewCpntroller getEventDetails:[arrayResponse objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:eventDetailViewCpntroller animated:YES];
            NSLog(@"future"); break;
            
        }
        case NSOrderedSame:
        {
            PDEventDetailViewController *eventDetailViewCpntroller = [[PDEventDetailViewController alloc] init];
            [eventDetailViewCpntroller getEventDetails:[arrayResponse objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:eventDetailViewCpntroller animated:YES];
            NSLog(@"same");
            
            break;
            
        }
        default: NSLog(@"e"); break;
    }
    

    
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [cell setBackgroundColor:[UIColor whiteColor]];
//    if(eventListTbl)
//    {
    [cell setBackgroundColor:[UIColor clearColor]];
  //  }
}
#pragma mark - Button actions

-(IBAction)onBack:(id)sender
{
 /*   PDViewController *viewController = [[PDViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:NO];*/
    
    
  /*  [self.tabBarController setSelectedIndex:0];
    
          [self.navigationController popViewControllerAnimated:YES];
   */
    
    NSLog(@"title2 :%@",self.navigationItem.title);
    
    int i;
    
    for (i=0; i<[self.tabBarController.tabBar.items count]; i++)
    {
        
        NSLog(@"title2 :%@",[self.tabBarController.tabBar.items objectAtIndex:i].title );
        NSString *title =[self.tabBarController.tabBar.items objectAtIndex:i].title;
        if([title isEqualToString:@"Home"])
        {
            [self.tabBarController setSelectedIndex:i];
            break;
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
  //
}
-(void)coreDataCodeRead_events{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Events" inManagedObjectContext:context];
      fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
   

    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    NSLog( @"error %@",error);
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (Events *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.event_id forKey:@"Id"];
        
        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
        [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
        [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
        [tempManufacturerDictionary setObject:manuf.event_title forKey:@"event_title"];
        [tempManufacturerDictionary setObject:manuf.event_time forKey:@"event_time"];
        [tempManufacturerDictionary setObject:manuf.event_address forKey:@"event_address"];
        [tempManufacturerDictionary setObject:manuf.event_description forKey:@"event_description"];
        
        [tempManufacturerDictionary setObject:manuf.event_continue forKey:@"event_continue"];
        [tempManufacturerDictionary setObject:manuf.event_start_date forKey:@"event_start_date"];
        [tempManufacturerDictionary setObject:manuf.event_expire_date forKey:@"event_expire_date"];
        [tempManufacturerDictionary setObject:manuf.event_expiration forKey:@"event_expiration"];
       

        [array addObject:tempManufacturerDictionary];
    }
    NSLog(@"result %@",array);
    NSMutableDictionary*dict=[[NSMutableDictionary alloc]init];
    [dict setObject:array forKey:@"business"];
    arrayResponse = [dict objectForKey:@"business"];
    if([arrayResponse count]>0)
    {
    NSDictionary *dicLatest = [arrayResponse objectAtIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:[dicLatest objectForKey:@"Id"] forKey:@"maxEventId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [tblEvents reloadData];
    indiProgress.hidden = YES;
    
    
    
}

-(void)coreDataCodeWrite_events:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Events" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Events *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    
    //    for(currentQUE.options in listOfQUEToBeDeleted){
    //
    //         [context1 deleteObject:currentQUE.options];
    //    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try
    {
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Events"
                                    inManagedObjectContext:context];
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"event_id"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"featured"]forKey:@"featured"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_title"]forKey:@"event_title"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_time"]forKey:@"event_time"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_address"]forKey:@"event_address"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"event_description"]forKey:@"event_description"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_continue"]forKey:@"event_continue"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_start_date"]forKey:@"event_start_date"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_expire_date"]forKey:@"event_expire_date"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_expiration"]forKey:@"event_expiration"];

            
            
            
            
            NSMutableArray*muts=[[NSMutableArray alloc]init];
            @try {
                
                
                for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
                    
                    NSString*str1=str;
                    // str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    [muts addObject:str1];
                  /*  if(str1.length>0)
                    {
                        [self insertImagesToDownload:str1];
                        [self sdImageCaching:str1];
                    }*/
                    
                    
                }
            }
            @catch (NSException *exception) {
                
            }
            
            [Que setValue:muts forKey:@"image"];
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            /*if(str.length>0)
            {
                [self insertImagesToDownload:str];
                [self sdImageCaching:str];
            }*/
            
            [Que setValue:str forKey:@"thumb"];
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    
    NSLog(@"EVENTS WROTE");
}
@end
