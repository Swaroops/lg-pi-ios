//
//  PDGalleryDetailViewController.m
//  Visiting Ashland
//
//  Created by swaroop on 06/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "PDGalleryDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "PhotoViewController.h"


@interface PDGalleryDetailViewController ()
{
    NSDictionary*dictDetails;
     int banery;
    NSArray*mainArray;
    long int arrayIndex;
    AsyncImageView *img;
    UILabel *lblStaticContentView;
    CGFloat __previousScale,__scale;
    BOOL isHotSpotAd;
    NSString *adLink;
    PhotoViewController *previousView;
    PhotoViewController *nextView;
    
    
}
@end


@implementation PDGalleryDetailViewController
CLLocation *location_updated;
@synthesize scrollDetailGallery,headingLbl,currentImgLbl;
-(void)getDictionary:(NSDictionary *) dictHere
{
    dictDetails = dictHere;
}
-(void)getArray:(NSArray *) ArrayHere : (long int) index{
    mainArray=ArrayHere;
    arrayIndex=index;
    _imageArray =[[NSMutableArray alloc]init];
    for(NSDictionary *dict in mainArray)
    {
        [_imageArray addObject:[dict objectForKey:@"image"]];
    }
}
-(BOOL)reachable_gallery {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    //NSLog(@"updated coordinate are %@",location_updated);
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
   /* NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_gallery])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];*/
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    /*NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_gallery])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];*/
}
-(IBAction)closeImage:(id)sender
{NSLog(@"close");
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];
   [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_detail"] isEqualToString:@"FALSE"])) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            if (![[dict objectForKey:@"image_detail"] isEqualToString:@"NA"]) {
                AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 20, 0, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, 20, 0, 50);
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_detail"]];
                adLink = [dict objectForKey:@"link_detail"];
                
                /*if (IS_IPHONE5)
                    scrollHeight = 400;
                else
                    scrollHeight = 311;
                
                scrollView_.frame = CGRectMake(0, 68, 320, scrollHeight);*/
            }
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            if (![[dict objectForKey:@"ad_3"] isEqualToString:@"NA"]) {
                AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 20, 320, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, 20, 320, 50);
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_3"]];
                adLink = [dict objectForKey:@"link_3"];
                
               /* if (IS_IPHONE5)
                    scrollHeight = 400;
                else
                    scrollHeight = 311;
                scrollView_.frame = CGRectMake(0, 68, 320, scrollHeight);*/
            }
        }
    }
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}
-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, 20, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
   /* if (IS_IPHONE5)
        scrollHeight = 400;
    else
        scrollHeight = 311;
    scrollView_.frame = CGRectMake(0, 68, 320, scrollHeight);*/
}
- (void)viewDidLoad {
    
    self.tabBarController.tabBar.hidden=YES;
    if (IS_IPHONE5) {
        [[NSBundle mainBundle] loadNibNamed:@"PDGalleryDetailViewController" owner:self options:nil];
        banery = 470;
    }
    else {
        [[NSBundle mainBundle] loadNibNamed:@"PDGalleryDetailViewController4" owner:self options:nil];
        banery = 382;
    }
   // [self setupView];
    
 /*  UISwipeGestureRecognizer* leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onLeftSwipe)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
   UISwipeGestureRecognizer*   rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onRightSwipe)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:leftSwipe];
    [self.view addGestureRecognizer:rightSwipe];*/
    CGRect frm;
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    switch (orientation) {
        case 1:
        case 2:
            if (IS_IPHONE5) {
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 568);
            }
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 480);
            
            // your code for portrait...
            break;
            
        case 3:
        case 4:
            
            if (IS_IPHONE5)
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,568, 320);
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,480, 320);
            // your code for landscape...
            break;
        default:
            NSLog(@"other");
            // your code for face down or face up...
            break;
    }   
    
    
    
    
    
    
    NSLog(@"Did rotate");
    self.view.frame=frm;
    self.pageViewController.view.frame=frm;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    /*if([self reachable_gallery])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];*/
    
    
    //newcode
    
     dictDetails=[mainArray objectAtIndex:arrayIndex];
    self.pageViewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                             navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                           options:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:81.0f] forKey:UIPageViewControllerOptionInterPageSpacingKey]];
    
    
    PhotoViewController *imageViewController = [PhotoViewController new];
    imageViewController.imageURL = [_imageArray objectAtIndex:arrayIndex];
   NSUInteger currentIndex3 =arrayIndex;
   
    
    
    NSArray *viewControllers = [NSArray arrayWithObject:imageViewController];
    
    
    
    [self.pageViewController setViewControllers:viewControllers
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:noErr
                                     completion:nil];
    
    
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    [self addChildViewController:self.pageViewController];
    [self.start addSubview:self.pageViewController.view];
    
   // self.pageViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    
    [self.pageViewController didMoveToParentViewController:self];
    currentImgLbl.text = [NSString stringWithFormat:@"%lu %@ %lu",(unsigned long) currentIndex3+1, NSLocalizedString(@"/", @"/") , (unsigned long)[mainArray count]];
    //newcode
    [stopBtn bringSubviewToFront:self.view];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // UITouch *touch = [[event allTouches] anyObject];
    
    [previousView setUpPhoto];
   [nextView setUpPhoto];
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
  /*  CGRect frm=self.view.frame;
    
 
    _start.frame=frm;
    self.pageViewController.view.frame=frm;
    self.view.frame=frm;*/
    
  
    
}
//- (void)pageViewController:(UIPageViewController *)pageViewController
//        didFinishAnimating:(BOOL)finished
//   previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers
//       transitionCompleted:(BOOL)completed
//{
//    NSLog(@"finished");
//    
//}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    
    
    CGRect frm;
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    NSLog(@"%f",[[UIScreen mainScreen]bounds].size.height);
    
    float phoneHeight=[[UIScreen mainScreen]bounds].size.height;
    float phoneWidth=[[UIScreen mainScreen]bounds].size.width;
    switch (orientation) {
        case 1:
        case 2:
            if (phoneHeight>=568) {
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 568);
            }
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 480);
            
            // your code for portrait...
            break;
            
        case 3:
        case 4:
            
             if (phoneWidth>=568)
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,568, 320);
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,480, 320);
            // your code for landscape...
            break;
        default:
            NSLog(@"other");
            // your code for face down or face up...
            break;
    }

    

   
    
    NSLog(@"Did rotate");
    self.view.frame=frm;
    self.pageViewController.view.frame=frm;
    
    _start.frame=frm;
    [previousView setUpPhoto];
    [nextView setUpPhoto];
   
}
#pragma mark - UIPageViewController delegate methods


- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    for (PhotoViewController *phototViewCnt in pendingViewControllers) {
        [phototViewCnt setUpPhoto];
    }
    [previousView setUpPhoto];
    [nextView setUpPhoto];
}
/*- (UIInterfaceOrientation)pageViewControllerPreferredInterfaceOrientationForPresentation:(UIPageViewController *)pageViewController
{
    
}*/
/*- (UIInterfaceOrientationMask)pageViewControllerSupportedInterfaceOrientations:(UIPageViewController *)pageViewController
{
    return (UIInterfaceOrientationMaskAllButUpsideDown); 
}*/
- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerBeforeViewController:(UIViewController *)viewController
{
    [previousView setUpPhoto];
    [nextView setUpPhoto];
   
    

    
    PhotoViewController *contentVc1 = (PhotoViewController *)viewController;
     nextView=contentVc1;
     // [contentVc1.photoScroller displayImage:contentVc1.photoScroller.zoomView.image];
     //  [ contentVc1.photoScroller photZoomOut];
    
    [nextView setUpPhoto];
    NSUInteger currentIndex = [_imageArray indexOfObject:contentVc1.imageURL];
    //_vcIndex = currentIndex;
    arrayIndex=currentIndex;
    
    currentImgLbl.text = [NSString stringWithFormat:@"%lu %@ %lu",(unsigned long) currentIndex+1, NSLocalizedString(@"/", @"/") , (unsigned long)[_imageArray count]];

   
    if (currentIndex==0)
    {
        return nil;
    }
   // if(currentIndex>1)
    // nextView=(PhotoViewController *)[pvc.viewControllers objectAtIndex:currentIndex-2];
   

    
    PhotoViewController *imageViewController = [PhotoViewController new];
    NSString *imageItem = [_imageArray objectAtIndex:currentIndex-1];
    imageViewController.imageURL =imageItem;
    
    
  
        return imageViewController;
    
    
    
    
}
- (UIViewController *)pageViewController:(UIPageViewController *)pvc viewControllerAfterViewController:(UIViewController *)viewController
{
    [previousView setUpPhoto];
    [nextView setUpPhoto];
    
    PhotoViewController *contentVc = (PhotoViewController *)viewController;
    NSUInteger currentIndex5 = [_imageArray  indexOfObject:contentVc.imageURL];
    arrayIndex=currentIndex5;
    previousView=contentVc;
    
   // [contentVc.photo Scroller displayImage:contentVc.photoScroller.zoomView.image];
    // [contentVc.photoScroller photZoomOut];
  [previousView setUpPhoto];
    
    currentImgLbl.text = [NSString stringWithFormat:@"%lu %@ %lu", (unsigned long)currentIndex5+1, NSLocalizedString(@"/", @"/") , (unsigned long)_imageArray.count];
    
    
    if (currentIndex5==_imageArray.count-1)
    {
        return nil;
    }
   //  if ((currentIndex5<=_imageArray.count-2))
   // nextView=(PhotoViewController *)[pvc.viewControllers objectAtIndex:currentIndex5+2];
    
    
    
    PhotoViewController *imageViewController = [PhotoViewController new];
    NSString *imageItem = [_imageArray objectAtIndex:currentIndex5+1];
    imageViewController.imageURL = imageItem;
    
    
    return imageViewController;
    
}


- (IBAction)back_detailGallery:(id)sender {
    // [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)onLeftSwipe  {
    if(arrayIndex+1<[mainArray count]){
        arrayIndex++;
        dictDetails=[mainArray objectAtIndex:arrayIndex];
        [self setupView];
    }
}
-(IBAction)shareImage:(id)sender
{
          NSMutableArray *sharingItems = [NSMutableArray new];
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    UIImageView *testimgv=[[UIImageView alloc]init];
    
   
    
    testimgv.image=[imageCache imageFromDiskCacheForKey:[_imageArray objectAtIndex:arrayIndex]];
    
    if(testimgv.image)
    {
    [sharingItems addObject:testimgv.image];


    
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
    }
    
}
- (void)onRightSwipe  {
         
        if(arrayIndex!=0){
            arrayIndex--;
            dictDetails=[mainArray objectAtIndex:arrayIndex];
            [self setupView];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView{
    
    currentImgLbl.text=[NSString stringWithFormat:@"%ld/%ld",arrayIndex+1,[mainArray count]];
    
    [img removeFromSuperview];
    [lblStaticContentView removeFromSuperview];
    CGSize maximumLabelSize = CGSizeMake(280, FLT_MAX);
   img = [[AsyncImageView alloc] init];
    img.contentMode = UIViewContentModeScaleToFill;
    CALayer * l = [img layer];
    [l setMasksToBounds:YES];
    [l setBorderWidth:0.5];
    img.clipsToBounds = YES;
    CGSize expectedContentSize = [[dictDetails objectForKey:@"description"] sizeWithFont:[UIFont fontWithName:@"Verdana" size:13.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    lblStaticContentView = [[UILabel alloc] init];
    lblStaticContentView.numberOfLines = 100;
    lblStaticContentView.backgroundColor = [UIColor clearColor];
    [lblStaticContentView setFont:[UIFont fontWithName:@"Verdana" size:13.0]];
    lblStaticContentView.text = [dictDetails objectForKey:@"description"];
    [scrollDetailGallery addSubview:lblStaticContentView];
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    NSString *imgKey=[dictDetails objectForKey:@"image"];
    img.image=[imageCache imageFromDiskCacheForKey:imgKey];
    [scrollDetailGallery addSubview:img];
     
    
   if (!img.image)
    {
        img.image=[UIImage imageNamed:@"noimage.png"];
        float imgWidth=scrollDetailGallery.frame.size.width;
        
        img.frame = CGRectMake(0,0, imgWidth, imgWidth/2);
        if([self reachable_gallery])
        {
            
            [img sd_setImageWithURL:[NSURL URLWithString:imgKey] placeholderImage:nil options:SDWebImageRefreshCached];
            
                       img.imageURL = [NSURL URLWithString:imgKey];
          
            
            [img setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dictDetails objectForKey:@"image"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
               
                
                float imgWidth=scrollDetailGallery.frame.size.width;
                float heightImage = (imageX.size.height*imgWidth)/imageX.size.width;
                
                
                
                [img setImage:imageX];
                img.frame = CGRectMake(0,0, imgWidth, heightImage);
            
                
             
                lblStaticContentView.frame=CGRectMake(20, img.frame.origin.y + img.frame.size.height + 20, 280, expectedContentSize.height);
                __scale=1.0;
                UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoom:)];
                [pinchGesture setDelegate:nil];
                [scrollDetailGallery addGestureRecognizer:pinchGesture];
            
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                NSLog(@"fail with %@ and %@",[error localizedDescription],[request URL]);
                //        [vwLoading removeFromSuperview];
                UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                float imgWidth=scrollDetailGallery.frame.size.width;
                
                imgH.frame = CGRectMake(0,0, imgWidth, imgWidth/2);
                //[self setStaticWithDict:dictResult Image:imgH];
                 img.image=[UIImage imageNamed:@"noimage.png"];
            }
             ];
        }
        else
        {
            [scrollDetailGallery addSubview:img];
            img.image=[UIImage imageNamed:@"noimage.png"];
            
            {
                NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                
                NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                
                NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                
                
                
                
                NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                
                NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                
                NSLog(@"firstBit folder :%@",firstBit);
                NSLog(@"secondBit folder :%@",secondBit2);
                
                NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                
                NSLog(@"str url :%@",imgKey);
                
                NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/gallery",secondBit2]];
                
                NSArray* str3 = [imgKey componentsSeparatedByString:@"gallery_images/"];
                
                NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                
                NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                NSLog(@"firstBit folder :%@",firstBit3);
                NSLog(@"secondBit folder :%@",secondBit3);
                NSLog(@"decoded folder :%@",decoded);
                
                NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                
                UIImage* image = [UIImage imageWithContentsOfFile:Path];
                
                img.image=image;
                
                // [self loadImageFromURL1:str image:tblHeaderImage];
                
                // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                
                
            }
            
            float imgWidth=scrollDetailGallery.frame.size.width;
            
            img.frame = CGRectMake(0,0, imgWidth, imgWidth/2);
        }
    }
    else
    {
        
        float imgWidth=scrollDetailGallery.frame.size.width;
        float heightImage = (img.image.size.height*imgWidth)/img.image.size.width;
        img.frame = CGRectMake(0,0, imgWidth, heightImage);
        [scrollDetailGallery addSubview:img];
        lblStaticContentView.frame=CGRectMake(20, img.frame.origin.y + img.frame.size.height + 20, 280, expectedContentSize.height);
        __scale=1.0;
        UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoom:)];
        [pinchGesture setDelegate:nil];
        [scrollDetailGallery addGestureRecognizer:pinchGesture];
    }
    

    
    
        
    headingLbl.text=[dictDetails objectForKey:@"title"];
    
}
- (void)pinch:(UIPinchGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded
        || gesture.state == UIGestureRecognizerStateChanged) {
        NSLog(@"gesture.scale = %f", gesture.scale);
        
        CGFloat currentScale = img.frame.size.width / img.frame.size.width;
        CGFloat newScale = currentScale * gesture.scale;
        
        if (newScale < 1.0) {
            newScale = 1.0;
        }
        if (newScale > 1.5) {
            newScale = 1.5;
        }
        
        CGAffineTransform transform = CGAffineTransformMakeScale(newScale, newScale);
        img. transform = transform;
        lblStaticContentView.frame=CGRectMake(20, img.frame.origin.y + img.frame.size.height + 20, 280, lblStaticContentView.frame.size.height);
       // gesture.scale = 1;
        gesture.scale = newScale;
    }
}
-(void) zoom:(UIPinchGestureRecognizer *)gesture {
    
    NSLog(@"Scale: %f", [gesture scale]);
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        __previousScale = __scale;
    }
    
    CGFloat currentScale = MAX(MIN([gesture scale] * __scale, 1.5), 1.0);
    CGFloat scaleStep = currentScale / __previousScale;
    [img setTransform: CGAffineTransformScale(img.transform, scaleStep, scaleStep)];
    
    __previousScale = currentScale;
    
    if ([gesture state] == UIGestureRecognizerStateEnded ||
        [gesture state] == UIGestureRecognizerStateCancelled ||
        [gesture state] == UIGestureRecognizerStateFailed) {
        // Gesture can fail (or cancelled?) when the notification and the object is dragged simultaneously
        __scale = currentScale;
        NSLog(@"Final scale: %f", __scale);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
