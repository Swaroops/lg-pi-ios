
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface PDDetailViewController : UIViewController
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblDistanceStatic, *lblDistanceBusiness, *lblNameLabel, *lblStaticName2;
    IBOutlet UIButton *btDir;
    IBOutlet UIButton *btDirSt;
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnBackSt;
    IBOutlet UIButton *btPhone;
    IBOutlet UIButton *btWeb;
    IBOutlet UIButton *btPhoneStat;
    IBOutlet UIButton *btWebStat;
    IBOutlet UIWebView *webView;
    IBOutlet UIView *vwDirection;
    IBOutlet UIView *vwLoading;
    IBOutlet UIView *vwStatic, *vwOther;
    IBOutlet UIScrollView *scrollMainView, *scrollStaticView;
    GADBannerView *bannerView_;
    UIActivityIndicatorView *activityIndicator;
    IBOutlet UIButton *btnFavorites, *btnFavoritesStatic;
   
    IBOutlet UIButton *btnShareDetailStatic;
    
    IBOutlet UIButton *btnShareDetail;
    IBOutlet UIButton *shareStatic4;
    IBOutlet UIButton *share4;
    IBOutlet UIButton *btnAddr;
    
    IBOutlet UIView *appleMap;
    BOOL googleLocType;
}
//@property (nonatomic, strong) UISlider *seekbar;
//
//@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
//
//@property (nonatomic, strong) NSTimer *updateTimer;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property(nonatomic,strong)IBOutlet UILabel *lblNameLabel;
@property(nonatomic,strong)NSString *lblNameLabelStr;
@property (nonatomic, strong) UIWebView *playerWeb;
@property (nonatomic)BOOL isFromFavorite;
@property (nonatomic)BOOL isFromFav;

@property (nonatomic)BOOL isfromSearch;

@property (nonatomic)BOOL isOwnGuide;
@property (nonatomic)BOOL isFromIn;
@property (nonatomic)BOOL isFromFrom;

-(IBAction)onBack:(id)sender;
-(IBAction)onWebSite:(id)sender;
-(IBAction)onCall:(id)sender;
-(IBAction)onDirection:(id)sender;
-(IBAction)onDone:(id)sender;
-(IBAction)onFavorite:(id)sender;
-(IBAction)onClick:(id)sender;

-(void)setStaticDictFromFavorite:(NSDictionary *) dict;
-(void)getDictionary:(NSDictionary *) dict;
-(void)getArray:(NSArray *) Array :(long int) Index;


//@property (weak, nonatomic) IBOutlet UIButton *btnShareDetail;
//@property (weak, nonatomic) IBOutlet UIButton *btnShareDetailStatic;
@end
