//
//  Businesses.h
//  Visiting Ashland
//
//  Created by swaroop on 13/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Businesses : NSManagedObject

@property (nonatomic, retain) NSString * busId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * cat_id2;
@property (nonatomic, retain) NSString * cat_id3;
@property (nonatomic, retain) NSString * cat_id4;
@property (nonatomic, retain) NSString * cat_id5;
@property (nonatomic, retain) NSString * cat_id6;
@property (nonatomic, retain) NSString * cat_id7;
@property (nonatomic, retain) NSString * cat_id8;
@property (nonatomic, retain) NSString * cat_id9;
@property (nonatomic, retain) NSString * cat_id10;

@property (nonatomic, retain) NSString * address;
@property (nonatomic,retain) NSString  *appid;
@property (nonatomic, retain) NSString * preview;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSString * website_text;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * featured;
@property (nonatomic, retain) NSString * star_rating;
@property (nonatomic, retain) NSArray* image;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSString * weight;
@property (nonatomic, retain) NSString * add_to_favorites;
@property (nonatomic, retain) NSString * share_listing;
@property (nonatomic, retain) NSString * fixed_order;
@property (nonatomic, retain) NSString * show_call;
@property (nonatomic, retain) NSString * show_web;
@property (nonatomic, retain) NSString * email_address;
@property (nonatomic, retain) NSString * show_email;
@property (nonatomic, retain) NSString * email_text;
@property (nonatomic, retain) NSString * disp_address;
@property (nonatomic,retain) NSString  *guide_id;
@property (nonatomic,retain) NSString  *guide_name;


@end
