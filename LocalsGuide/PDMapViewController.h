//
//  PDMapViewController.h
//  LocalsGuide
//
//  Created by swaroop on 27/02/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PDMapViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate, GADBannerViewDelegate>
{
    IBOutlet MKMapView *listingMapView;
    GADBannerView *bannerView_;
    IBOutlet UIView *vwLoading;
    IBOutlet UIView *vwSettings;
    IBOutlet UIView *vwMap;
    IBOutlet UITableView *tblMenu;
    BOOL flagMenu;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(IBAction)onBack:(id)sender;
-(IBAction)flip: (id) sender;
-(IBAction)onMenu:(id)sender;
-(IBAction)onClose:(id)sender;
-(IBAction)myLocation;
@end
