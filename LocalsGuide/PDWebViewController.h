//
//  PDWebViewController.h
//  Visiting Ashland
//
//  Created by swaroop on 04/04/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDWebViewController : UIViewController <CLLocationManagerDelegate, GADBannerViewDelegate>
{
    GADBannerView *bannerView_;
    IBOutlet UILabel *nameLabel;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(void)getDictionary:(NSDictionary *)dictDetails;
@end
