

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PDThirdSubViewController : UIViewController <CLLocationManagerDelegate,GADBannerViewDelegate>
{
    IBOutlet UILabel *titleLabel;
    IBOutlet UITableView *tblContents;
    GADBannerView *bannerView_;
    
    UISearchBar *searchBar;
    NSMutableArray *searchArray;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(IBAction)onBack:(id)sender;
-(void)getArray:(NSArray *) passArray
         ofType:(NSString *) passType
       withName:(NSString *) name;
@end
