//
//  PDMapViewController.m
//  LocalsGuide
//
//  Created by swaroop on 27/02/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "JPSThumbnailAnnotation.h"
#import "PDMapCatCell.h"
#import "Reachability.h"

@interface PDMapViewController ()
{
    NSMutableArray *arrIds;
    NSMutableArray *arrSwitchFlags;
    NSMutableArray *arrTemp;
    BOOL isLocationUpdated, isHotSpotAd;
    BOOL flagOn;
    int banery;
    int count;
    int indexCount;
    BOOL once;
    
    NSMutableArray *arrMapDicts;
    
     NSDictionary * dictH;
}
@end

@implementation PDMapViewController
CLLocation *location_updated;
NSString *adLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(IBAction)backToApplist:(id)sender
{
    
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
    
}

-(BOOL)reachable_map {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBarHidden = YES;
    once=NO;
    if(![self reachable_map]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alertView.tag=5;
        [alertView show];
        once=YES;
        return;
    }
    indexCount = -1;
    self.navigationController.navigationBarHidden = YES;
    arrIds = [[NSMutableArray alloc] init];
    if (IS_IPHONE5)
        banery = 470;
    else
        banery = 382;
    vwMap.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
    [self.view addSubview:vwMap];
    arrSwitchFlags =  [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"settingsArray"]];
    flagMenu = NO;
    isLocationUpdated = NO;
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    count = 0;
    
    //vwSettings.frame = CGRectMake(-220, 20, 220, [[UIScreen mainScreen] bounds].size.height - 20);
    vwSettings.frame = CGRectMake(self.view.frame.size.width,0, 220, vwSettings.frame.size.height);
    vwSettings.alpha = 0.4;
    [self.view addSubview:vwSettings];
    
    [self startWebService];
    
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_map])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBarHidden = YES;
    
    if(![self reachable_map])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alertView.tag=5;
        if(once==NO)
        [alertView show];
        return;
    }
    
}


-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
        [self.view addSubview:ad1Image];
        UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        ad1Btn.frame = CGRectMake(0, banery, 320, 50);
        [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:ad1Btn];
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image"] isEqualToString:@"FALSE"])) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image"]];
            adLink = [dict objectForKey:@"link"];
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_3"]];
            adLink = [dict objectForKey:@"link_3"];
        }
    }
}

-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [vwMap addSubview:bannerView_];
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
     [self.tabBarController.tabBar setHidden:YES];
    location_updated = [locations lastObject];
    CLLocation *location = [locations lastObject];
    if (!isLocationUpdated) {
        CLLocationCoordinate2D pointLocation = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        MKCoordinateSpan spanRegion = MKCoordinateSpanMake(0.03, 0.03);
        MKCoordinateRegion region = MKCoordinateRegionMake(pointLocation, spanRegion);
        [listingMapView setRegion:region];
    }
    isLocationUpdated = YES;
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_map])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_map])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];
}

-(void)customGoto
{
    if ([arrIds count] > count) {
        [self startFixedService];
    }
    else {
        [self addAnnotationsInMapView];
    }
}
#pragma mark - Web service call
// Start Web service here
-(void)startWebService
{
    vwLoading.frame = self.view.frame;
    [vwMap addSubview:vwLoading];
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@&device=device_type",MAP_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}
// Get web service response here
-(void)webServiceResponse:(NSData *) responseData
{
    arrMapDicts = [[NSMutableArray alloc] init];
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSArray *arrayContents = [dict objectForKey:@"mapcategory"];
    [tblMenu reloadData];
    [vwLoading removeFromSuperview];
    for (NSDictionary *dict in arrayContents) {
        if ([[dict objectForKey:@"type"] isEqualToString:@"normal"] || [[dict objectForKey:@"type"] isEqualToString:@"fixed"]) {
            [arrIds addObject:dict];//type//fixed//location
        }
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"settingsArray"]) {
        NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"settingsArray"]];
        if ([myMutableArrayAgain count] == [arrIds count]) {
            BOOL flagNot = NO;
            for (int i = 0; i < [myMutableArrayAgain count]; i++) {
                NSDictionary *dict1 = [myMutableArrayAgain objectAtIndex:i];
                NSDictionary *dict2 = [arrIds objectAtIndex:i];
                if (![[dict1 objectForKey:@"name"] isEqualToString:[dict2 objectForKey:@"name"]]) {
                    flagNot = YES;
                    break;
                }
            }
            if (flagNot) {
                NSMutableArray *arrToSetDefault = [NSMutableArray new];
                for (NSDictionary *dict  in arrIds) {
                    NSMutableDictionary *dict3 = [NSMutableDictionary new];
                    [dict3 setObject:[dict objectForKey:@"name"] forKey:@"name"];
                    [dict3 setObject:@"No" forKey:@"enabled"];
                    [arrToSetDefault addObject:dict3];
                }
                [[NSUserDefaults standardUserDefaults] setObject:arrToSetDefault forKey:@"settingsArray"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
        }
        else {
            NSMutableArray *arrToSetDefault = [NSMutableArray new];
            for (NSDictionary *dict  in arrIds) {
                NSMutableDictionary *dict3 = [NSMutableDictionary new];
                [dict3 setObject:[dict objectForKey:@"name"] forKey:@"name"];
                [dict3 setObject:@"No" forKey:@"enabled"];
                [arrToSetDefault addObject:dict3];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrToSetDefault forKey:@"settingsArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    else {
        NSMutableArray *arrToSetDefault = [NSMutableArray new];
        for (NSDictionary *dict  in arrIds) {
            NSMutableDictionary *dict3 = [NSMutableDictionary new];
            [dict3 setObject:[dict objectForKey:@"name"] forKey:@"name"];
            [dict3 setObject:@"No" forKey:@"enabled"];
            [arrToSetDefault addObject:dict3];
        }
        [[NSUserDefaults standardUserDefaults] setObject:arrToSetDefault forKey:@"settingsArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    arrSwitchFlags =  [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"settingsArray"]];
    for (int k = 0; k < [arrSwitchFlags count]; k++) {
        [arrMapDicts addObject:@""];
    }
    NSLog(@"%@",arrMapDicts);
    [tblMenu reloadData];
    [self startFixedService];
}
#pragma mark - alert retry
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self startWebService];
    }
    
    if(alertView.tag==5)
    {
        
        if(buttonIndex == 0)
        {
            
            NSLog(@"title2 :%@",self.navigationItem.title);
            
            int i;
            
            for (i=0; i<[self.tabBarController.tabBar.items count]; i++)
            {
                
                NSLog(@"title2 :%@",[self.tabBarController.tabBar.items objectAtIndex:i].title );
                NSString *title =[self.tabBarController.tabBar.items objectAtIndex:i].title;
                if([title isEqualToString:@"Home"])
                {
                    [self.tabBarController setSelectedIndex:i];
                    break;
                }
                
            }
            
            [self.navigationController popViewControllerAnimated:YES];
            
            NSLog(@"cancel button presed");
        }
    
    }
}
-(void)startFixedService
{
    if ([arrSwitchFlags count] == 0) {
        return;
    }
    if ([arrIds count] <= count) {
        [self addAnnotationsInMapView];
        return;
    }
    NSDictionary *dictSettings = [arrSwitchFlags objectAtIndex:count];
    if (flagOn) {
        indexCount++;
        NSDictionary *dict = [arrIds objectAtIndex:count];
        WebService *webService = [[WebService alloc] init];
        webService.responseTarget = self;
        webService.respondToMethod = @selector(fixedWebServiceResponse:);
        NSString *str = [NSString stringWithFormat:@"%@%@&lat=%f&long=%f",LIST_URL,[dict objectForKey:@"Id"],location_updated.coordinate.latitude,location_updated.coordinate.longitude];
        [webService startParsing:str];
        [self.view addSubview:vwLoading];
    }
    else {
        indexCount++;
        if ([[dictSettings objectForKey:@"enabled"] isEqualToString:@"Yes"]) {
            NSDictionary *dict = [arrIds objectAtIndex:count];
            WebService *webService = [[WebService alloc] init];
            webService.responseTarget = self;
            webService.respondToMethod = @selector(fixedWebServiceResponse:);
            NSString *str = [NSString stringWithFormat:@"%@%@&lat=%f&long=%f",LIST_URL,[dict objectForKey:@"Id"],location_updated.coordinate.latitude,location_updated.coordinate.longitude];
            [webService startParsing:str];
            [self.view addSubview:vwLoading];
        }
        else {
            count++;
            [self customGoto];
        }
    }
}

-(void)startGoogleWebService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(googleWebServiceResponse:);
    NSDictionary *dict = [arrIds objectAtIndex:count];
    NSString *urlParsing = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&rankby=distance&types=%@&sensor=false&key=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,[dict objectForKey:@"googletype"], GOOGLE_API_KEY];
    [webService startParsing:urlParsing];
    vwLoading.frame = self.view.frame;
    [self.view addSubview:vwLoading];
}
-(void)fixedWebServiceResponse:(NSData *) responseData
{
    [vwLoading removeFromSuperview];
    NSError *e;
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
    NSArray *arrayResult;
    arrayResult =[dict objectForKey:@"business"];
    
    NSDictionary *dicMain = [arrIds objectAtIndex:count];
    NSMutableArray *arrReResult = [NSMutableArray array];
    for (NSDictionary *dictio in arrayResult) {
        NSMutableDictionary *mutDict = [NSMutableDictionary dictionaryWithDictionary:dictio];
        //[mutDict setObject:[dicMain objectForKey:@"name"] forKey:@"type"];
        if (![[dicMain objectForKey:@"listing_icon"] isEqualToString:@""])
            [mutDict setObject:[dicMain objectForKey:@"listing_icon"] forKey:@"listing_icon"];
        [arrReResult addObject:mutDict];
    }
    NSDictionary *dict1 = [arrIds objectAtIndex:count];
    if([[dict1 objectForKey:@"type"] isEqualToString:@"location"]&&isLocationUpdated)
    {
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrReResult];
        [self startGoogleWebService];
        arrTemp = [[NSMutableArray alloc] init];
        arrTemp = [NSMutableArray arrayWithArray:arrReResult];
    }
    else {
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrReResult];
        count++;
        if ([arrIds count] > count) {
            //if (2 > count) {
            [self startFixedService];
        }
        else {
            [arrTemp removeAllObjects];
            NSLog(@"arr counr = %d \n\n %@",[arrMapDicts count],arrMapDicts);
            [self addAnnotationsInMapView];
        }
    }
}
-(void)googleWebServiceResponse:(NSData *) responseData
{
    [vwLoading removeFromSuperview];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
    NSArray *arrayResult;
    arrayResult =[dict objectForKey:@"results"];
    
    
    NSDictionary *dicMain = [arrIds objectAtIndex:count];
    NSMutableArray *arrReResult = [NSMutableArray array];
    for (NSDictionary *dictio in arrayResult) {
        NSMutableDictionary *mutDict = [NSMutableDictionary dictionaryWithDictionary:dictio];
        //[mutDict setObject:[dicMain objectForKey:@"name"] forKey:@"type"];
        if (![[dicMain objectForKey:@"listing_icon"] isEqualToString:@""])
            [mutDict setObject:[dicMain objectForKey:@"listing_icon"] forKey:@"listing_icon"];
        [arrReResult addObject:mutDict];
    }
    if ([[arrMapDicts objectAtIndex:indexCount] isKindOfClass:[NSString class]]) {
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrReResult];
    }
    else {
        NSMutableArray *arrTe = [[arrMapDicts objectAtIndex:indexCount] mutableCopy];
        [arrTe addObjectsFromArray:arrReResult];
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrTe];
    }
    
    [arrTemp addObjectsFromArray:arrReResult];
    if ([arrIds count] > count) {
        //if (2 > count) {
        count ++;
        [self startFixedService];
    }
    else {
        [arrTemp removeAllObjects];
        NSLog(@"arr counr = %d \n\n %@",[arrMapDicts count],arrMapDicts);
        [self addAnnotationsInMapView];
    }
}

#pragma mark - Customizing map
// Method to add pin to map
-(void)addAnnotationsInMapView
{
    [listingMapView addAnnotations:[self generateAnnotations]];
}

- (NSArray *)generateAnnotations {
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<[arrMapDicts count]; i++) {
        NSDictionary *dictImL = [arrIds objectAtIndex:i];
        if (![[arrMapDicts objectAtIndex:i] isKindOfClass:[NSArray class]]) {
            continue;
        }
        NSArray *arr = [arrMapDicts objectAtIndex:i];
        for (NSDictionary *dict in arr) {
            JPSThumbnail *empire = [[JPSThumbnail alloc] init];
            //            UIImageView *imag = [[UIImageView alloc] init];
            //            if ([dict objectForKey:@"listing_icon"]) {
            //                [imag setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dict objectForKey:@"listing_icon"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            //                    empire.image = image;
            //                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            //                    NSLog(@"failed to download pin image %@",[error localizedDescription]);
            //                }
            //                 ];
            //            }
            //            else if ([dict objectForKey:@"icon"]){
            //                [imag setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dict objectForKey:@"icon"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            //                    empire.image = image;
            //                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            //                    NSLog(@"failed to download pin image %@",[error localizedDescription]);
            //                }
            //                 ];
            //            }
            empire.image = [UIImage imageNamed:[dictImL objectForKey:@"pin_name"]];
            empire.title = [dict objectForKey:@"name"];
            if ([dict objectForKey:@"address"])
                empire.subtitle = [dict objectForKey:@"address"];
            else
                empire.subtitle = [dict objectForKey:@"vicinity"];
            CLLocationCoordinate2D pointLocation;
            if(![dict objectForKey:@"geometry"]) {
                pointLocation = CLLocationCoordinate2DMake([[dict objectForKey:@"latitude"] doubleValue], [[dict objectForKey:@"longitude"] doubleValue]);
            }
            else{
                NSDictionary *dict1 = [dict objectForKey:@"geometry"];
                NSDictionary *locaDict = [dict1 objectForKey:@"location"];
                pointLocation = CLLocationCoordinate2DMake([[locaDict objectForKey:@"lat"] doubleValue], [[locaDict objectForKey:@"lng"] doubleValue]);
            }
            
            empire.coordinate = pointLocation;
            
            empire.disclosureBlock = ^{
                NSString *emTitle = empire.title;
                NSString *emSubTitle = empire.subtitle;
                float emLat = empire.coordinate.latitude;
                float emLng = empire.coordinate.longitude;
                
                for (int i = 0; i<[arrMapDicts count]; i++) {
                    if (![[arrMapDicts objectAtIndex:i] isKindOfClass:[NSArray class]]) {
                        continue;
                    }
                    NSArray *arr = [arrMapDicts objectAtIndex:i];
                    for (NSDictionary *dict in arr) {
                        NSString *dicSubTitle;
                        NSString *dicTitle = [dict objectForKey:@"name"];
                        float dicLat;
                        float dicLng;
                        if ([dict objectForKey:@"address"])
                            dicSubTitle = [dict objectForKey:@"address"];
                        else
                            dicSubTitle = [dict objectForKey:@"vicinity"];
                        
                        if(![dict objectForKey:@"geometry"]) {
                            dicLat = [[dict objectForKey:@"latitude"] floatValue];
                            dicLng = [[dict objectForKey:@"longitude"]floatValue];
                        }
                        else {
                            NSDictionary *dict1 = [dict objectForKey:@"geometry"];
                            NSDictionary *locaDict = [dict1 objectForKey:@"location"];
                            dicLat = [[locaDict objectForKey:@"lat"] floatValue];
                            dicLng = [[locaDict objectForKey:@"lng"] floatValue];
                        }
                        
                        if (([emTitle isEqualToString:dicTitle]&&[emSubTitle isEqualToString:dicSubTitle]&&(emLat == dicLat)&&(emLng == dicLng))) {
                            [self loadDetailWithDict:dict];
                        }
                    }
                }
            };
            [annotations addObject:[[JPSThumbnailAnnotation alloc] initWithThumbnail:empire]];
        }
    }
    return annotations;
}
-(void)loadDetailWithDict:(NSDictionary *) dictCaught
{
    NSLog(@"%@",dictCaught);
    PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
    [detailViewController getDictionary:dictCaught];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - UITableView delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSwitchFlags count];
}


// code change by Ram to hide tableview selection
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    NSDictionary *dict2 = [arrSwitchFlags objectAtIndex:indexPath.row];
    PDMapCatCell *cell = (PDMapCatCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PDMapCatCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.responseTarget = self;
    cell.respondToMethod = @selector(getAction:);
    cell.switchCat.tag = indexPath.row;
    cell.lblCatName.text = [dict2 objectForKey:@"name"];
    if (flagOn) {
        cell.switchCat.enabled = NO;
        [cell.switchCat setOn:YES];
    }
    else {
        cell.switchCat.enabled = YES;
        if ([[dict2 objectForKey:@"enabled"] isEqualToString:@"Yes"])
        {
            [cell.switchCat setOn:YES];
        }
        else
        {
            {
                
                NSString *lbltitleMap;
                
                lbltitleMap=[[NSUserDefaults standardUserDefaults] objectForKey:@"lbltitleMap"];
                
                
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"dict2 : %@",dict2);
                
                if([lbltitleMap isEqualToString:[dict2 objectForKey:@"name"]])
                {
                    
                    
                    [cell.switchCat setOn:YES animated:YES];
                    
                    [cell.switchCat setOn:YES];
                    
                    cell.respondToMethod = @selector(getAction:);
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        
                        
                        [self getAction:dictH];
                        
                        [cell.switchCat sendActionsForControlEvents: UIControlEventTouchUpInside];
                        
                       
                        
                        
                        
                        
                        NSLog(@"arrIds : %@",arrIds);
                        
                        
                    });
                    
                    
                   
                }
                else
                    
                {
                    
                    [cell.switchCat setOn:NO];
                    
                }
                
                
                
                
                
                
            }
            
            //[cell.switchCat setOn:NO];
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//}


#pragma mark - Button actions

-(void)getAction:(NSDictionary *) dictH
{
    int index = [[dictH objectForKey:@"index"] integerValue];
    indexCount = index;
    if ([[dictH objectForKey:@"switchValue"] isEqualToString:@"No"]) {
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:@""];
        
        
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrSwitchFlags objectAtIndex:index]];
        [dict setValue:[dictH objectForKey:@"switchValue"] forKey:@"enabled"];
        [arrSwitchFlags replaceObjectAtIndex:index withObject:dict];
        [[NSUserDefaults standardUserDefaults] setObject:arrSwitchFlags forKey:@"settingsArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        
        [self removeAllPinsButUserLocation];
        [self addAnnotationsInMapView];
    }
    else {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrSwitchFlags objectAtIndex:index]];
        [dict setValue:[dictH objectForKey:@"switchValue"] forKey:@"enabled"];
        [arrSwitchFlags replaceObjectAtIndex:index withObject:dict];
        [[NSUserDefaults standardUserDefaults] setObject:arrSwitchFlags forKey:@"settingsArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSDictionary *dictz = [arrIds objectAtIndex:indexCount];
        [self startFixedServiceWithId:[dictz objectForKey:@"Id"]];
    }
}
- (IBAction) flip: (id) sender {
    
    [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"nearby"];
    
    count = 0;
    indexCount = -1;
    UISwitch *onoff = (UISwitch *) sender;
    flagOn = !flagOn;
    if (![onoff isOn]) {
        [self removeAllPinsButUserLocation];
    }
    [tblMenu reloadData];
    for (int x = 0; x < [arrMapDicts count]; x++) {
        [arrMapDicts replaceObjectAtIndex:x withObject:@""];
    }
    [self removeAllPinsButUserLocation];
    [self startFixedService];
    
}
- (void)removeAllPinsButUserLocation
{
    id userLocation = [listingMapView userLocation];
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[listingMapView annotations]];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
    [listingMapView removeAnnotations:pins];
}
-(IBAction)onMenu:(id)sender
{
    if (!flagMenu) {
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveEaseInOut
                         animations:^{
//                             vwSettings.frame = CGRectMake(0, 67, 220, vwSettings.frame.size.height);
//                             vwSettings.alpha = 1.0;
//                             vwMap.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
                             
                             
                             vwSettings.frame = CGRectMake(self.view.frame.size.width-220, 0, 220, vwSettings.frame.size.height);
                             vwSettings.alpha = 1.0;
                             vwMap.frame = CGRectMake(-220, 0, self.view.frame.size.width, self.view.frame.size.height);
                             
                         }
                         completion:^(BOOL finished) {
                         }];
        flagMenu = !flagMenu;
    }
    
    else {
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveEaseInOut
                         animations:^{
//                             vwSettings.frame = CGRectMake(-220, 20, 220, vwSettings.frame.size.height);
//                             vwSettings.alpha = 0;
//                             vwMap.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
                             
                             vwSettings.frame = CGRectMake(self.view.frame.size.width,0, 220, vwSettings.frame.size.height);
                             vwSettings.alpha = 0;
                             vwMap.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                         
                         
                         }
                         completion:^(BOOL finished) {
                         }];
        flagMenu = !flagMenu;
    }
}

-(IBAction)onBack:(id)sender
{
//    PDViewController *viewController = [[PDViewController alloc] init];
//    [self.navigationController pushViewController:viewController animated:NO];
    
    
    //Ram
    
    /*[self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:NO];*/
    
   /* [self.tabBarController setSelectedIndex:0];
    [self.navigationController popViewControllerAnimated:YES];*/
    
    NSLog(@"title2 :%@",self.navigationItem.title);
    
    int i;
    
    for (i=0; i<[self.tabBarController.tabBar.items count]; i++)
    {
        
        NSLog(@"title2 :%@",[self.tabBarController.tabBar.items objectAtIndex:i].title );
        NSString *title =[self.tabBarController.tabBar.items objectAtIndex:i].title;
        if([title isEqualToString:@"Home"])
        {
            [self.tabBarController setSelectedIndex:i];
            break;
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)onClose:(id)sender
{
    
}
-(IBAction)myLocation
{
    CLLocationCoordinate2D pointLocation = CLLocationCoordinate2DMake(location_updated.coordinate.latitude, location_updated.coordinate.longitude);
    MKCoordinateSpan spanRegion = MKCoordinateSpanMake(0.03, 0.03);
    MKCoordinateRegion region = MKCoordinateRegionMake(pointLocation, spanRegion);
    [listingMapView setRegion:region animated:YES];
}

-(void)startFixedServiceWithId:(NSString *) cid
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(fixedWebServiceResponseInn:);
    NSString *str = [NSString stringWithFormat:@"%@%@&lat=%f&long=%f",LIST_URL,cid,location_updated.coordinate.latitude,location_updated.coordinate.longitude];
    [webService startParsing:str];
    vwLoading.frame = self.view.frame;
    [self.view addSubview:vwLoading];
}

-(void)startGoogleWebServiceInn
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(googleWebServiceResponseInn:);
    NSDictionary *dict = [arrIds objectAtIndex:indexCount];
    NSString *urlParsing = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&rankby=distance&types=%@&sensor=false&key=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,[dict objectForKey:@"googletype"], GOOGLE_API_KEY];
    [webService startParsing:urlParsing];
    vwLoading.frame = self.view.frame;
    [self.view addSubview:vwLoading];
}
-(void)fixedWebServiceResponseInn:(NSData *) responseData
{
    [vwLoading removeFromSuperview];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
    NSArray *arrayResult =[dict objectForKey:@"business"];
    
    NSDictionary *dicMain = [arrIds objectAtIndex:indexCount];
    NSMutableArray *arrReResult = [NSMutableArray array];
    for (NSDictionary *dictio in arrayResult) {
        NSMutableDictionary *mutDict = [NSMutableDictionary dictionaryWithDictionary:dictio];
        //[mutDict setObject:[dicMain objectForKey:@"name"] forKey:@"type"];
        if (![[dicMain objectForKey:@"listing_icon"] isEqualToString:@""])
            [mutDict setObject:[dicMain objectForKey:@"listing_icon"] forKey:@"listing_icon"];
        [arrReResult addObject:mutDict];
    }
    NSDictionary *dict1 = [arrIds objectAtIndex:indexCount];
    if([[dict1 objectForKey:@"type"] isEqualToString:@"location"]&&isLocationUpdated)
    {
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrReResult];
        [self startGoogleWebServiceInn];
        arrTemp = [[NSMutableArray alloc] init];
        arrTemp = [NSMutableArray arrayWithArray:arrReResult];
    }
    else {
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrReResult];
        [arrTemp removeAllObjects];
        NSLog(@"arr counr = %d \n\n %@",[arrMapDicts count],arrMapDicts);
        [self removeAllPinsButUserLocation];
        [self addAnnotationsInMapView];
        
    }
}
-(void)googleWebServiceResponseInn:(NSData *) responseData
{
    [vwLoading removeFromSuperview];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
    NSArray *arrayResult;
    arrayResult =[dict objectForKey:@"results"];
    
    
    NSDictionary *dicMain = [arrIds objectAtIndex:indexCount];
    NSMutableArray *arrReResult = [NSMutableArray array];
    for (NSDictionary *dictio in arrayResult) {
        NSMutableDictionary *mutDict = [NSMutableDictionary dictionaryWithDictionary:dictio];
        //[mutDict setObject:[dicMain objectForKey:@"name"] forKey:@"type"];
        if (![[dicMain objectForKey:@"listing_icon"] isEqualToString:@""])
            [mutDict setObject:[dicMain objectForKey:@"listing_icon"] forKey:@"listing_icon"];
        [arrReResult addObject:mutDict];
    }
    if ([[arrMapDicts objectAtIndex:indexCount] isKindOfClass:[NSString class]]) {
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrReResult];
    }
    else {
        NSMutableArray *arrTe = [[arrMapDicts objectAtIndex:indexCount] mutableCopy];
        [arrTe addObjectsFromArray:arrReResult];
        [arrMapDicts replaceObjectAtIndex:indexCount withObject:arrTe];
    }
    
    [self removeAllPinsButUserLocation];
    [self addAnnotationsInMapView];
}


#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)]) {
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}
@end
