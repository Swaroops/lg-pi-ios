

#import <Foundation/Foundation.h>

@interface WebService : NSObject

@property(nonatomic,strong) NSURLConnection * urlConnection;
@property(nonatomic,strong) NSMutableData* responseData;
@property(nonatomic,retain) NSString *responseString;
@property(nonatomic)BOOL  withOutRetry;
@property(nonatomic,strong) id responseTarget;
@property SEL respondToMethod;
@property SEL respondFailMethod;

-(void)startParsing: (NSString *) URL;
@end
