//
//  ACPStaticImagesAlternative.m
//  ACPDownload
//
//  Created by Palmero, Antonio on 1/19/15.
//  Copyright (c) 2015 Antonio Casero Palmero. All rights reserved.
//

#import "ACPStaticImagesAlternative.h"

@implementation ACPStaticImagesAlternative

@synthesize strokeColor,bounds;

- (void)updateFrame:(CGRect)frame {
    self.bounds = frame;
}

- (void)updateColor:(UIColor*)color {
    
    //sarath
    color=[UIColor grayColor];
    
    self.strokeColor = color;
}

- (void) drawStatusNone{
    
    
    // General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Color Declarations
    UIColor *fillColor = [UIColor colorWithRed:0.22 green:0.267 blue:0.384 alpha:1];
    CGFloat fillColorRGBA[4];
    [fillColor getRed:&fillColorRGBA[0] green:&fillColorRGBA[1] blue:&fillColorRGBA[2] alpha:&fillColorRGBA[3]];
    
    UIColor* strokeColor = [UIColor grayColor];
    UIColor *fillColor2 = [UIColor grayColor];
    UIColor *shadowColor2 = [UIColor grayColor];
    UIColor *shadowColor3 = [UIColor grayColor];
    
    // Gradient Declarations
    NSArray *gradientColors = @[(id)fillColor2.CGColor, (id)fillColor.CGColor];
    CGFloat gradientLocations[] = {0, 1};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
    
    // Shadow Declarations
    UIColor *innerShadow = shadowColor2;
    CGSize innerShadowOffset = CGSizeMake(0.1, 1.1);
    CGFloat innerShadowBlurRadius = 2;
    UIColor *shadow = shadowColor3;
    CGSize shadowOffset = CGSizeMake(0.1, 1.1);
    CGFloat shadowBlurRadius = 2;
    
    // Frames
    CGRect frame = CGRectMake(0, 0, 30, 38);
    CGRect frame2 = CGRectMake(170, 0, 30, 38);
    
    // Bezier Drawing
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame))];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetMinX(frame), CGRectGetMinY(frame) + 19)];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame) + 38)];
    [bezierPath addCurveToPoint:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame) + 25)
                  controlPoint1:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame) + 38)
                  controlPoint2:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame) + 29.88)];
    [bezierPath addCurveToPoint:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2) + 25)
                  controlPoint1:CGPointMake(CGRectGetMinX(frame) + 50.73, CGRectGetMinY(frame) + 25)
                  controlPoint2:CGPointMake(CGRectGetMinX(frame2) - 23.76, CGRectGetMinY(frame2) + 25)];
    [bezierPath addCurveToPoint:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2) + 38)
                  controlPoint1:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2) + 30.36)
                  controlPoint2:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2) + 38)];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetMinX(frame2) + 30, CGRectGetMinY(frame2) + 19)];
    [bezierPath addLineToPoint:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2))];
    [bezierPath addCurveToPoint:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2) + 13)
                  controlPoint1:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2))
                  controlPoint2:CGPointMake(CGRectGetMinX(frame2) + 10, CGRectGetMinY(frame2) + 7.35)];
    [bezierPath addCurveToPoint:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame) + 13)
                  controlPoint1:CGPointMake(CGRectGetMinX(frame2) - 25.01, CGRectGetMinY(frame2) + 13)
                  controlPoint2:CGPointMake(CGRectGetMinX(frame) + 50.92, CGRectGetMinY(frame) + 13)];
    [bezierPath addCurveToPoint:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame))
                  controlPoint1:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame) + 7.39)
                  controlPoint2:CGPointMake(CGRectGetMinX(frame) + 20, CGRectGetMinY(frame))];
    [bezierPath closePath];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
    CGContextBeginTransparencyLayer(context, NULL);
    [bezierPath addClip];
    CGRect bezierBounds = CGPathGetPathBoundingBox(bezierPath.CGPath);
    CGContextDrawLinearGradient(context, gradient,
                                CGPointMake(CGRectGetMidX(bezierBounds), CGRectGetMinY(bezierBounds)),
                                CGPointMake(CGRectGetMidX(bezierBounds), CGRectGetMaxY(bezierBounds)),
                                0);
    CGContextEndTransparencyLayer(context);
    
    // Bezier Inner Shadow
    CGRect bezierBorderRect = CGRectInset([bezierPath bounds], -innerShadowBlurRadius, -innerShadowBlurRadius);
    bezierBorderRect = CGRectOffset(bezierBorderRect, -innerShadowOffset.width, -innerShadowOffset.height);
    bezierBorderRect = CGRectInset(CGRectUnion(bezierBorderRect, [bezierPath bounds]), -1, -1);
    
    UIBezierPath *bezierNegativePath = [UIBezierPath bezierPathWithRect:bezierBorderRect];
    [bezierNegativePath appendPath: bezierPath];
    bezierNegativePath.usesEvenOddFillRule = YES;
    
    CGContextSaveGState(context);
    {
        CGFloat xOffset = innerShadowOffset.width + round(bezierBorderRect.size.width);
        CGFloat yOffset = innerShadowOffset.height;
        CGContextSetShadowWithColor(context,
                                    CGSizeMake(xOffset + copysign(0.1, xOffset), yOffset + copysign(0.1, yOffset)),
                                    innerShadowBlurRadius,
                                    innerShadow.CGColor);
        
        [bezierPath addClip];
        CGAffineTransform transform = CGAffineTransformMakeTranslation(-round(bezierBorderRect.size.width), 0);
        [bezierNegativePath applyTransform:transform];
        [[UIColor grayColor] setFill];
        [bezierNegativePath fill];
    }
    
    CGContextRestoreGState(context);
    CGContextRestoreGState(context);
    
    [self.strokeColor setStroke];
    bezierPath.lineWidth = 1;
    [bezierPath stroke];
    
    // Cleanup
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
    
    /*
    CGRect frameContainer;
    frameContainer.origin.x = CGRectGetMidX(self.bounds) - self.bounds.size.width / 3;
    frameContainer.origin.y = CGRectGetMidY(self.bounds) - self.bounds.size.height / 3;
    frameContainer.size.width = self.bounds.size.width / 1.5;
    frameContainer.size.height = self.bounds.size.height / 1.5;
    
    CGRect groupInFrame = CGRectMake(CGRectGetMinX(frameContainer) + floor(CGRectGetWidth(frameContainer) * 0.09375 + 0.5), CGRectGetMinY(frameContainer) + floor(CGRectGetHeight(frameContainer) * 0.02539 + 0.5), floor(CGRectGetWidth(frameContainer) * 0.90625 + 0.5) - floor(CGRectGetWidth(frameContainer) * 0.09375 + 0.5), floor(CGRectGetHeight(frameContainer) * 1.00000 + 0.5) - floor(CGRectGetHeight(frameContainer) * 0.02539 + 0.5));
    
    //Arrow
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.28199 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.49896 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.24671 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.52837 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.50026 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.73975 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.75381 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.52837 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.71853 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.49896 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.52521 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.66013 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.52521 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.00179 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.47531 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.00179 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.47531 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.66013 * CGRectGetHeight(groupInFrame))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.28199 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.49896 * CGRectGetHeight(groupInFrame))];
    [bezierPath closePath];
    bezierPath.miterLimit = 4;
    
    [self.strokeColor setFill];
    [bezierPath fill];
    
    
    //Cube
    UIBezierPath* bezier2Path = [UIBezierPath bezierPath];
    [bezier2Path moveToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.62499 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.20975 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.62499 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.25134 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.94928 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.25134 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.94928 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.95841 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.05125 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.95841 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.05125 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.25134 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.37553 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.25134 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.37553 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.20975 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.00135 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.20975 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.00135 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 1.00000 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.99917 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 1.00000 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.99917 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.20975 * CGRectGetHeight(groupInFrame))];
    [bezier2Path addLineToPoint: CGPointMake(CGRectGetMinX(groupInFrame) + 0.62499 * CGRectGetWidth(groupInFrame), CGRectGetMinY(groupInFrame) + 0.20975 * CGRectGetHeight(groupInFrame))];
    [bezier2Path closePath];
    bezier2Path.miterLimit = 4;
    
    [self.strokeColor setFill];
    [bezier2Path fill];
    
    */
    
    
}
- (void) drawStatusIndeterminate{
    //Nothing, just the layer animation.
}
- (void) drawStatusRunning{
    
    CGPoint center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    CGFloat radius = (MIN(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) / 2))-1;
    CGFloat startAngle = (CGFloat)(0);
    CGFloat endAngle = (CGFloat)(2*M_PI);
    
    
    //// The circle
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
    [[UIColor clearColor] setFill];
    [ovalPath fill];
    [self.strokeColor setStroke];
    ovalPath.lineWidth = 1.0;  //1.5
    [ovalPath stroke];
    
    CGRect page1 = self.bounds;
    page1.origin.x = CGRectGetMidX(self.bounds) - self.bounds.size.width / 8;
    page1.origin.y = CGRectGetMidY(self.bounds) - self.bounds.size.height / 8;
    page1.size.width = self.bounds.size.width / 4;
    page1.size.height = self.bounds.size.height /4;
    
    //// The stop in the middle
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: page1];
    [self.strokeColor setFill];
    [rectanglePath fill];
    
}
- (void) drawStatusComplete{
    
   /* CGRect frameContainer;
    frameContainer.origin.x = CGRectGetMidX(self.bounds) - self.bounds.size.width / 3;
    frameContainer.origin.y = CGRectGetMidY(self.bounds) - self.bounds.size.height / 3;
    frameContainer.size.width = self.bounds.size.width / 1.5;
    frameContainer.size.height = self.bounds.size.height / 1.5;
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
   [bezierPath moveToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.50000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.00000 * CGRectGetHeight(frameContainer))];
    //[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.00000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.50000 * CGRectGetHeight(frameContainer)) controlPoint1: CGPointMake(CGRectGetMinX(frameContainer) + 0.22500 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.00000 * CGRectGetHeight(frameContainer)) controlPoint2: CGPointMake(CGRectGetMinX(frameContainer) + 0.00000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.22500 * CGRectGetHeight(frameContainer))];
   // [bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.50000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 1.00000 * CGRectGetHeight(frameContainer)) controlPoint1: CGPointMake(CGRectGetMinX(frameContainer) + 0.00000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.77500 * CGRectGetHeight(frameContainer)) controlPoint2: CGPointMake(CGRectGetMinX(frameContainer) + 0.22500 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 1.00000 * CGRectGetHeight(frameContainer))];
   // [bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 1.00000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.50000 * CGRectGetHeight(frameContainer)) controlPoint1: CGPointMake(CGRectGetMinX(frameContainer) + 0.77500 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 1.00000 * CGRectGetHeight(frameContainer)) controlPoint2: CGPointMake(CGRectGetMinX(frameContainer) + 1.00000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.77500 * CGRectGetHeight(frameContainer))];
   // [bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.50000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.00000 * CGRectGetHeight(frameContainer)) controlPoint1: CGPointMake(CGRectGetMinX(frameContainer) + 1.00000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.22500 * CGRectGetHeight(frameContainer)) controlPoint2: CGPointMake(CGRectGetMinX(frameContainer) + 0.77500 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.00000 * CGRectGetHeight(frameContainer))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.50000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.00000 * CGRectGetHeight(frameContainer))];
    [bezierPath closePath];
    [bezierPath moveToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.40000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.72500 * CGRectGetHeight(frameContainer))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.40000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.27500 * CGRectGetHeight(frameContainer))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.70000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.50000 * CGRectGetHeight(frameContainer))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.40000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.72500 * CGRectGetHeight(frameContainer))];
    [bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frameContainer) + 0.40000 * CGRectGetWidth(frameContainer), CGRectGetMinY(frameContainer) + 0.72500 * CGRectGetHeight(frameContainer))];
    [bezierPath closePath];
    bezierPath.miterLimit = 4;
    
    bezierPath.usesEvenOddFillRule = YES;
    
    [self.strokeColor setFill];
    [bezierPath fill];
    */
    
}



@end
