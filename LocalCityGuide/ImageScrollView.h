#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ImageScrollView : UIScrollView

@property (strong, nonatomic) UIImageView *zoomView;
@property (nonatomic) BOOL isZoomed;
- (void)displayImage:(UIImage *)image;

-(void)setScrlFrm:(CGRect)frame;

-(void)photZoomOut;
@end

