//
//  PhotoViewController.h
//  Wallpaperz
//
//  Created by CODERLAB on 17.05.14.
//  Copyright (c) 2014 studio76. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ImageScrollView.h"
#import "NSString+MD5.h"
#import "Cache.h"

@interface PhotoViewController : UIViewController<UIScrollViewDelegate,UIAlertViewDelegate>
{
    CGSize _imageSize;
    CGPoint _pointToCenterAfterResize;
    CGFloat _scaleToRestoreAfterResize;
    BOOL isTapZoom;
}

//@property (strong, nonatomic)  ImageScrollView *scroller;
@property (strong, nonatomic)IBOutlet  UIScrollView *photoScroller;
@property (strong, nonatomic)IBOutlet  UIImageView *photoImgv;
@property (strong, nonatomic)  UIImage *image;
@property (strong, nonatomic)  NSString *imageURL;
@property (strong, nonatomic)  UIImageView *photimgv;


@property(strong, nonatomic)IBOutlet UIView *activityHolderView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndi;

@property (nonatomic) BOOL isZoomed;

- (void) loadImageFromURL:(NSString*)URL;
-(void)loadPhotoFromUrl:(NSString*)URL;
-(void)imageSetUp;
-(void)setUpPhoto;

@end

