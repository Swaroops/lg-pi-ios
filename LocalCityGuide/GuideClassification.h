//
//  GuideClassification.h
//  Visiting Ashland
//
//  Created by Anu on 09/05/16.
//  Copyright © 2016 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuideClassification : NSObject

@property (nonatomic, retain) NSString * classid;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * status;
@end
