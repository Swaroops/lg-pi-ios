//
//  DBOperations.h
//  Visiting Ashland
//
//  Created by Anu on 18/01/16.
//  Copyright (c) 2016 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDAppDelegate.h"

#import "Apps.h"
#import "Inapp.h"
#import "IMAGES_TO_CACHE.h"
#import "Static.h"
#import "Categories.h"
#import "Businesses.h"
#import "Gallery.h"
#import "Deals.h"
#import "Events.h"

@protocol DBOperationsDelegate <NSObject>
@required
- (void) processCompleted:(NSArray*)resultArray;
-(void) classificaionAccessComplete:(NSArray*)resultArray;
-(void) classificaionAccessCompleteSecond:(NSArray*)resultArray;
- (void) processFailed:(NSString*)status;
-(void)processComplete;

-(void)eventCatUpdateComplete;

-(void)eventCategoriesAccessComplete:(NSArray*)resultArray;
-(void)eventCategoriesAccessCompleteForFirstTime:(NSArray*)resultArray;
-(void)eventSelectedCategoriesAccessComplete:(NSArray*)resultArray;
@end
@interface DBOperations : NSObject
{
     id <DBOperationsDelegate> _delegate;
    NSArray *dataArray;
}
@property (nonatomic,strong) id delegate;
@property(nonatomic,strong)NSArray *dataArray;
-(void)writeClassificationId:(NSArray*)classIdentArr;
-(void)startOfflineDataLoading;
-(void)offlineDataSynchronize;

-(void)startRefeshHomePage;
-(void)readSelectedGuideClassification;
-(void)readGuideClassification:(NSString*)callType;
-(void)updateClassificationId:(NSDictionary*)obj onOf:(NSString*)onOffStatus;

-(void)updateEventCategoryId:(NSDictionary*)obj onOf:(NSString*)onOffStatus;
-(void)writeEventCategories:(NSArray*)classIdentArr;
-(void)readSelectedEventCategories;
-(void)readAllEventCategories:(NSString*)forThePurpose;

+(BOOL)isSingleApp;
+(NSString*)getMainAppid;


@end
