//
//  Apps.h
//  Visiting Ashland
//
//  Created by Anu on 15/10/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Apps : NSManagedObject
@property (nonatomic, retain) NSString * appid;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * settings;
@property (nonatomic, retain) NSString * footerList;
@property (nonatomic, retain) NSString * lastSync;
@property (nonatomic, retain) NSString * inappsettings;
@property (nonatomic, retain) NSString * appHeaderImg;

@end
