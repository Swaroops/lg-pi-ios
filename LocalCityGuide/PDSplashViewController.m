//
//  PDSplashViewController.m
//  LocalsGuide
//
//  Created by swaroop on 21/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import "PDSplashViewController.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"

@interface PDSplashViewController ()
{
    
}
@end

@implementation PDSplashViewController
@synthesize splashImagView4s,splashImgView,splashImgStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initWithSplashName:(NSString*)imgStr
{
    splashImgStr=imgStr;
    
    [self showImage];
}
-(void)showImage
{
    NSString *key=splashImgStr;
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:@"Splash"];
   
    if (IS_IPHONE5){
        
        CGRect frame=[[UIScreen mainScreen] bounds];
        splashImgView.frame=frame;
       // splashImgView.frame=CGRectMake(0, 0, 320,575 );//568
        splashImgView.backgroundColor=[UIColor clearColor];
        
        UIImage *splImg=[imageCache imageFromDiskCacheForKey:splashImgStr];
        
        
        if (!splImg) {
            
            // [splashImgv sd_setImageWithURL:[NSURL URLWithString:splashImgUrl] placeholderImage:nil options:SDWebImageRefreshCached];
             [splashImgView sd_setImageWithURL:[NSURL URLWithString:splashImgStr] placeholderImage:[UIImage imageNamed:@"Default.png"] options:SDWebImageRefreshCached];
            
            [splashImgView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:splashImgStr]] placeholderImage:[UIImage imageNamed:@"Default.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                
                [imageCache storeImage:imageX forKey:key completion:^{  }];
                // splashImgv.image=imageX;
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                
            }];
        }else{
            splashImgView.image=splImg;
            
        }
    }
    else{
       
        splashImagView4s.image=[imageCache imageFromDiskCacheForKey:splashImgStr];
        if (!splashImagView4s.image) {
            
            // [splashImgv sd_setImageWithURL:[NSURL URLWithString:splashImgUrl] placeholderImage:nil options:SDWebImageRefreshCached];
             [splashImagView4s sd_setImageWithURL:[NSURL URLWithString:splashImgStr] placeholderImage:[UIImage imageNamed:@"Default.png"] options:SDWebImageRefreshCached];
            
            [splashImagView4s setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:splashImgStr]] placeholderImage:[UIImage imageNamed:@"Default.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                
                [imageCache storeImage:imageX forKey:key completion:^{  }];
                // splashImgv.image=imageX;
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                
            }];
        }
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMyStatus:) name:@"loadSplash" object:nil];
    if (IS_IPHONE5){
        [[NSBundle mainBundle] loadNibNamed:@"PDSplashViewController" owner:self options:nil];
        
        
        
       
    }
    else{
        [[NSBundle mainBundle] loadNibNamed:@"PDSplashViewController4" owner:self options:nil];
        
    }
    // Do any additional setup after loading the view from its nib.
}
//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
//    
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//    
//    return UIStatusBarStyleLightContent;//UIStatusBarStyleDefault
//}
-(void)showMyStatus : (NSNotification *)aNotification{
   /* if (![[[[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"] objectAtIndex:0]objectForKey:@"splash"]isEqualToString:@"NA"]) {
        
    
    if (IS_IPHONE5){
       
        [splashImgView sd_setImageWithURL:[NSURL URLWithString:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"] objectAtIndex:0]objectForKey:@"splash"]] placeholderImage:nil options:SDWebImageRefreshCached];
//        splashImgView.imageURL=[NSURL URLWithString:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"] objectAtIndex:0]objectForKey:@"splash"]]
    }
    else{
        
        [splashImagView4s sd_setImageWithURL:[NSURL URLWithString:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"] objectAtIndex:0]objectForKey:@"splash"]] placeholderImage:nil options:SDWebImageRefreshCached];
    }
    }
    else{
        [splashImgView setImage:[UIImage imageNamed:@"5.0"]];
        [splashImagView4s setImage:[UIImage imageNamed:@"3.0"]];
    }*/
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
