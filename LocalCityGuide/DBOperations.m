//
//  DBOperations.m
//  Visiting Ashland
//
//  Created by Anu on 18/01/16.
//  Copyright (c) 2016 iDeveloper. All rights reserved.
//

#import "DBOperations.h"

#import "UIImageView+WebCache.h"
#import "JMImageCache.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "GuideClassification.h"
#import "EventCategory.h"
#import "Reachability.h"
#import "SDImageCache.h"
#import "SDWebImagePrefetcher.h"

#import "SSZipArchive.h"

@implementation DBOperations;
@synthesize delegate;
@synthesize dataArray;

+(BOOL)isSingleApp
{
    return NO;
    //return YES;
    //pass No value if not single
}

+(NSString*)getMainAppid
{
    //return @"22";
    return @"94";
}
-(void)startOfflineDataLoading
{
    dataArray=[[NSArray alloc]init];
    [self offlineDataSynchronize];
}
-(void)startRefeshHomePage
{
     dataArray=[[NSArray alloc]init];
    [self startRefreshWebService];
}
- (void)taskComplete
{
    [[self delegate] processCompleted:dataArray];
    
}
-(void)offlineDataSynchronize
{
    if ([self reachable]) {
        
        NSMutableDictionary *appDetailDic= [self coreDataReadAppDetail];
        NSString   *offlineSyncStatus=[[appDetailDic objectForKey:@"settings"] objectForKey:@"offline_guide"];
        
        //if([offlineSyncStatus isEqualToString:@"yes"])
        if ([self reachable])
        {
            NSDate *lastSyncDate=[appDetailDic objectForKey:@"lastSync"];
            
            /////////
            NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            
            NSString*stt = [[NSUserDefaults standardUserDefaults]objectForKey:ida];
            
            NSLog(@"ida :%@ ,stt :%@",ida,stt);
            
           if([stt isEqualToString:@"noo"])
           {
               [[NSUserDefaults standardUserDefaults]setObject:@"yess" forKey:ida];
                   //App id is using for the image folder
                   NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                   
                   
                   SDImageCache *imageCache =[SDImageCache sharedImageCache];
                   imageCache = [imageCache initWithNamespace:imageCacheFolder];
                   [imageCache clearMemory];
                   // [imageCache clearDisk];
                   
                   
                   
                   [imageCache clearDiskOnCompletion:^{
                       
                       
                       [self AllQuestWebService];
                       
                   }];
               
               
              /* NSString *stringURL = @"http://insightto.com/Crater%20Lake%20Park_images.zip";
               NSURL  *url = [NSURL URLWithString:stringURL];
               NSData *urlData = [NSData dataWithContentsOfURL:url];*/
               
               
               
           /*   NSString *zipPath = [[NSBundle mainBundle] pathForResource:zipFileName ofType:@"zip"];
               NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
               [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];*/
               
               // the URL to save
           /*    NSURL *yourURL = [NSURL URLWithString:@"http://insightto.com/Crater%20Lake%20Park_images.zip"];
               // turn it into a request and use NSData to load its content
               NSURLRequest *request = [NSURLRequest requestWithURL:yourURL];
               NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
               
               // find Documents directory and append your local filename
               NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
               documentsURL = [documentsURL URLByAppendingPathComponent:@"localFile.zip"];
               
               // and finally save the file
               [data writeToURL:documentsURL atomically:YES];
               
               NSString *filename = @"localFile.zip";
               NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
               NSString *documentsDirectory = [paths objectAtIndex:0];
               NSString * zipPath = [documentsDirectory stringByAppendingPathComponent:filename];
               
               NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
               [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];*/
               
           
           }
            
            //////

            
           else if(lastSyncDate==nil)
            {
                
                 [self coreDataCodeRead];
                
                
                //App id is using for the image folder
              /*  NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                
                
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:imageCacheFolder];
                [imageCache clearMemory];
               // [imageCache clearDisk];
                
               
                
                [imageCache clearDiskOnCompletion:^{
                    
                   
                    [self AllQuestWebService];
                    
                }];*/
            }
            else
            {
                NSDate *currentDate = [NSDate date];
                float refreshInterval = [[[appDetailDic objectForKey:@"settings"] objectForKey:@"refresh_interval"]floatValue] ;
                NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:lastSyncDate];
                double secondsInAnHour = 3600;
                NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
                
                if (hoursBetweenDates >= refreshInterval){
                    
                 /*   SDImageCache *imageCache =[SDImageCache sharedImageCache];
                    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                    imageCache = [imageCache initWithNamespace:imageCacheFolder];
                    [imageCache clearMemory];
                    [imageCache clearDisk];
                  
                    [imageCache clearDiskOnCompletion:^{
                        
                     
                        [self AllQuestWebService];
                        
                    }];*/
                    
                    [self coreDataCodeRead];
                    
                    NSLog(@"dataArray :%@",dataArray);
                    
                }
                else
                {
                    [self coreDataCodeRead];
                    //[self startRefreshWebService];
                    
                    NSLog(@"dataArray :%@",dataArray);
                    
                }
                
            }
        }
        else
        {
            [self startWebService];
            //offline status No - delete images and text data
            
        }
        
    }
    else
        [self coreDataCodeRead];
    
}
-(void)AllQuestWebService
{
   
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(AllQuestResponse:);
    
    NSString* ismulti=@"yes";
    if ([DBOperations isSingleApp]) {
        ismulti=@"no";
    }
    else if([[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"])
        ismulti=  [[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"];
    
    [webService startParsing:[NSString stringWithFormat:@"%@%@&multi=%@",OFFLINE_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"],ismulti]];
    //&device=hdpi
   
}


-(void)AllQuestResponse:(NSData *) responseData
{
    NSArray* arrResponse1;
    NSArray* arrResponse2;
    NSArray* arrResponse3;
    NSArray* arrResponse4;
    NSArray* arrResponse5;
    NSArray* arrResponse6;
        
    NSError *err;
    NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&err];
    
    NSArray *totalSize = [dict1 objectForKey:@"total_file_size"];
    
    [[NSUserDefaults standardUserDefaults]setObject:totalSize forKey:[NSString stringWithFormat:@"totalSize%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
    
    NSLog(@"totalSize :%@",totalSize);

    
    arrResponse1 = [dict1 objectForKey:@"categories"];
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_async(queue, ^{
    
        if([arrResponse1 count]>0)
            [self coreDataCodeWrite:@"subcategory" arr:arrResponse1];
   // });

    
    
    arrResponse2 = [dict1 objectForKey:@"businesses"];
    
//    dispatch_queue_t queue2 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_async(queue2, ^{
    
        if([arrResponse2 count]>0)
            [self coreDataCodeWrite_bus:arrResponse2];
   // });
    
    
    arrResponse3 = [dict1 objectForKey:@"gallery"];
    if([arrResponse3 count]>0)
        [self coreDataCodeWrite_gallery:arrResponse3];
    
    arrResponse4 = [dict1 objectForKey:@"deals"];
    //if([arrResponse4 count]>0)
    if(!([arrResponse4 count] <= 0))
        [self coreDataCodeWrite_deals:arrResponse4];
    
    arrResponse5 = [dict1 objectForKey:@"events"];
    //if([arrResponse5 count]>0)
    if(!([arrResponse5 count] <= 1))
        [self coreDataCodeWrite_events:arrResponse5];
    
    arrResponse6 = [dict1 objectForKey:@"static"];
    if([arrResponse6 count]>0)
        [self coreDataCodeWrite_statics:arrResponse6];
    
    NSDate *lastSyncDate = [NSDate date];
    [[NSUserDefaults standardUserDefaults]setObject:lastSyncDate forKey:@"lastSynced"];
    
    [self coreDataWriteAppsDetail:lastSyncDate];
    [self coreDataCodeRead];
    
    //[self readListOfImagesToDownload];
    
}
-(void)startWebService
{
    
    
    
    
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@",HOM_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}

// Get web service response here
-(void)webServiceResponse:(NSData *) responseData
{

    
    
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];

    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"HomeResponse"];
    [[NSUserDefaults standardUserDefaults] synchronize];
   NSArray *arrayContents = [dict objectForKey:@"maincategory"];
//    queArray=[[NSMutableArray alloc]init];
  //  queArray=[dict objectForKey:@"maincategory"] ;
    NSMutableDictionary *appDetailDic= [self coreDataReadAppDetail];
    NSString   *offlineSyncStatus=[[appDetailDic objectForKey:@"settings"] objectForKey:@"offline_guide"];
    
    if([offlineSyncStatus isEqualToString:@"yes"])
    {
        if([arrayContents count]>0)
            [self coreDataCodeWrite:@"subcategory" arr:arrayContents];
    }
    
    dataArray=arrayContents;
    [self taskComplete];

    
  
}

-(void)startRefreshWebService
{

    
    
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(startRefreshWebServiceResponse:);
    NSString* ismulti=@"yes";
    if ([DBOperations isSingleApp]) {
       ismulti=@"no";
    }
    else if([[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"])
      ismulti=  [[NSUserDefaults standardUserDefaults]objectForKey:@"ismulti"];
    
    [webService startParsing:[NSString stringWithFormat:@"%@%@&app_type=city&device=xxhdpi&multi=%@",HOM_WEBSERVICE_REFRESH,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"],ismulti]];
    
  
}
-(void)startRefreshWebServiceResponse:(NSData *) responseData
{
   
    
    
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
 
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"HomeResponse"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSArray *arrayContents = [dict objectForKey:@"maincategory"];
  //  queArray=[[NSMutableArray alloc]init];
   // queArray=[dict objectForKey:@"maincategory"] ;
    NSMutableDictionary *appDetailDic= [self coreDataReadAppDetail];
    NSString   *offlineSyncStatus=[[appDetailDic objectForKey:@"settings"] objectForKey:@"offline_guide"];
    
    if([offlineSyncStatus isEqualToString:@"yes"])
    {
        if([arrayContents count]>0)
            [self coreDataCodeWrite:@"sub" arr:arrayContents];
        else
        {
            [self coreDataCodeRead];
            return;
        }
    }
   
    dataArray=arrayContents;
   
    
  
    
         [self taskComplete];
        
    

    //  [vwLoading removeFromSuperview];
    

}
#pragma mark - Event GuidClassificationOperation
-(void)updateEventCategoryId:(NSDictionary*)obj onOf:(NSString*)onOffStatus
{
    
    
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"EventCategory" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventcatid==%@",[obj objectForKey:@"id"]];
    [fetchRequest setPredicate:predicate];
    
    
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    
    for(EventCategory *object in results)
    {
        NSManagedObject* favoritsGrabbed = object;
        //   NSString *classid=object.classid;
        
        //   [favoritsGrabbed setValue:[obj objectForKey:@"name"] forKey:@"name"];
        [favoritsGrabbed setValue:onOffStatus forKey:@"status"];
        
        if (![readContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        else{
            NSLog(@"saved");
            
        }
        
        
        
    }
    [[self delegate]eventCatUpdateComplete];
}

-(void)writeEventCategories:(NSArray*)classIdentArr
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"EventCategory" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *classIdArr=[[NSMutableArray alloc]init];
    
    [classIdArr addObjectsFromArray:classIdentArr];
    
    for(EventCategory *object in results)
    {
        NSString *appidFromDb=object.eventcatid;
        BOOL rowFoundInDb=false;
        
        for (i=0; i<[classIdArr count]; i++)
        {
            
            NSString *appidFromServer=[[classIdArr objectAtIndex:i] objectForKey:@"id"];
            
            if([appidFromDb isEqualToString:appidFromServer])
            {
                //Update the DB
                NSManagedObject* favoritsGrabbed = object;
                
                
                [favoritsGrabbed setValue:[[classIdArr objectAtIndex:i]objectForKey:@"category"]forKey:@"category"];
                
                
                
                
                if (![readContext save:&error]) {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
                else{
                    
                    
                    
                    rowFoundInDb=true;
                    [classIdArr removeObjectAtIndex:i];
                    
                    break; //
                    
                }
                
                //Update the DB
            }
            
        }
        
        
        if(rowFoundInDb==false)
        {
            //Delete row in Db which is not in server
            
            [readContext deleteObject:object];
        }
        
    }
    
    
    for(int i =0;i<[classIdArr count];i++)
    {
        //Insert into DB
        NSManagedObjectContext *insertContext = [appDelegate managedObjectContext];
        NSManagedObject *Que = [NSEntityDescription
                                insertNewObjectForEntityForName:@"EventCategory"
                                inManagedObjectContext:insertContext];
        
        
        NSString *STRR=[NSString stringWithFormat:@"%@",[[classIdArr objectAtIndex:i] objectForKey:@"id"] ];
        [Que setValue:STRR forKey:@"eventcatid"];
        
        
        
        
        [Que setValue:[[classIdArr objectAtIndex:i]objectForKey:@"category"]forKey:@"category"];
        [Que setValue:@"1" forKey:@"status"];
        
        
        if (![insertContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        
    }
    
    [[self delegate]processComplete];
}
-(void)readSelectedEventCategories
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status==1"];
    [fetchRequest setPredicate:predicate];
    
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"EventCategory" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (EventCategory *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.eventcatid forKey:@"id"];
        [tempManufacturerDictionary setObject:manuf.category forKey:@"category"];
        [tempManufacturerDictionary setObject:manuf.status forKey:@"status"];
        
        [array addObject:tempManufacturerDictionary];
    }
    
    NSArray  *classIdArr=[[NSArray alloc]init];
    classIdArr=array;
    
    
    
    [[self delegate]eventSelectedCategoriesAccessComplete:classIdArr];
    
    
}

-(void)readAllEventCategories:(NSString*)forThePurpose
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"EventCategory" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (EventCategory *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.eventcatid forKey:@"id"];
        [tempManufacturerDictionary setObject:manuf.category forKey:@"name"];
        [tempManufacturerDictionary setObject:manuf.status forKey:@"status"];
        
        [array addObject:tempManufacturerDictionary];
    }
    
    NSArray  *classIdArr=[[NSArray alloc]init];
    classIdArr=array;
    
    if ([forThePurpose isEqualToString:@"firstTime"]) {
        [[self delegate]eventCategoriesAccessCompleteForFirstTime:classIdArr];
    }
    else
        [[self delegate]eventCategoriesAccessComplete:classIdArr];
    
    
}

#pragma mark - GuidClassificationOperation
-(void)updateClassificationId:(NSDictionary*)obj onOf:(NSString*)onOffStatus
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GuideClassification" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    
    NSArray *fck =[[NSUserDefaults standardUserDefaults]objectForKey:@"classifIdsArr"];
    NSMutableArray *muttaa = [[NSMutableArray alloc]initWithArray:fck];
    
     if([[obj objectForKey:@"status"] isEqualToString:@"1"])
     {
         [muttaa addObject:[obj objectForKey:@"classid"]];
     }
         
         
    for(int i=0;i<[fck count];i++)
    {
    if([[fck objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@",[obj objectForKey:@"classid"]]])
    {
       if([[obj objectForKey:@"status"] isEqualToString:@"0"])
        [muttaa removeObjectAtIndex:i];
    }
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:muttaa forKey:@"classifIdsArr"];
    
    NSString *strng1;
    strng1 = [muttaa componentsJoinedByString:@","];
    NSLog(@"strngclass :%@",strng1);
    
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"classid==%@",[obj objectForKey:@"classid"]];//[obj objectForKey:@"classid"]  //strng1
    [fetchRequest setPredicate:predicate];
    
    
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    NSArray *srr=[[NSArray alloc]init];
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"classifIdsArr"])
       srr= [[NSUserDefaults standardUserDefaults]objectForKey:@"classifIdsArr"];
    
    
    for(GuideClassification *object in results)
    {
        NSManagedObject* favoritsGrabbed = object;
     //   NSString *classid=object.classid;
       
     //   [favoritsGrabbed setValue:[obj objectForKey:@"name"] forKey:@"name"];
        [favoritsGrabbed setValue:onOffStatus forKey:@"status"];
        
        if (![readContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        else{
            NSLog(@"saved");
            
        }
        
        
      
    }
    [[self delegate]processComplete];
}

-(void)writeClassificationId:(NSArray*)classIdentArr
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GuideClassification" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *classIdArr=[[NSMutableArray alloc]init];
    
   [classIdArr addObjectsFromArray:classIdentArr];
    
    for(GuideClassification *object in results)
    {
       NSString *appidFromDb=object.classid;
        BOOL rowFoundInDb=false;
        
        for (i=0; i<[classIdArr count]; i++)
        {
            
            NSString *appidFromServer=[[classIdArr objectAtIndex:i] objectForKey:@"id"];
            
            if([appidFromDb isEqualToString:appidFromServer])
            {
                //Update the DB
                NSManagedObject* favoritsGrabbed = object;
                
               
                [favoritsGrabbed setValue:[[classIdArr objectAtIndex:i]objectForKey:@"classification"]forKey:@"name"];
                
                
                
                
                if (![readContext save:&error]) {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
                else{
                    
                    
                    
                    rowFoundInDb=true;
                    [classIdArr removeObjectAtIndex:i];
                    
                    break; //
                    
                }
                
                //Update the DB
            }
            
        }
        
        
        if(rowFoundInDb==false)
        {
            //Delete row in Db which is not in server
            
            [readContext deleteObject:object];
        }
        
    }
    
    
    for(int i =0;i<[classIdArr count];i++)
    {
        //Insert into DB
        NSManagedObjectContext *insertContext = [appDelegate managedObjectContext];
        NSManagedObject *Que = [NSEntityDescription
                                insertNewObjectForEntityForName:@"GuideClassification"
                                inManagedObjectContext:insertContext];
        
        
        NSString *STRR=[NSString stringWithFormat:@"%@",[[classIdArr objectAtIndex:i] objectForKey:@"id"] ];
        [Que setValue:STRR forKey:@"classid"];
        
    
        
        
        [Que setValue:[[classIdArr objectAtIndex:i]objectForKey:@"classification"]forKey:@"name"];
        [Que setValue:@"0" forKey:@"status"];
        
        
        if (![insertContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        
    }
}
-(void)readSelectedGuideClassification
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status==1"];
   [fetchRequest setPredicate:predicate];
    
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GuideClassification" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (GuideClassification *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.classid forKey:@"classid"];
        [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
        [tempManufacturerDictionary setObject:manuf.status forKey:@"status"];
        
        [array addObject:tempManufacturerDictionary];
    }
    
    NSArray  *classIdArr=[[NSArray alloc]init];
    classIdArr=array;
    
    
    
    [[self delegate]classificaionAccessComplete:classIdArr];
    
    
}

-(void)readGuideClassification:(NSString*)callType
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
      
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GuideClassification" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (GuideClassification *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        [tempManufacturerDictionary setObject:manuf.classid forKey:@"classid"];
        [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
        [tempManufacturerDictionary setObject:manuf.status forKey:@"status"];
        
        [array addObject:tempManufacturerDictionary];
    }
    
   NSArray  *classIdArr=[[NSArray alloc]init];
    classIdArr=array;
    
    
    if ([callType isEqualToString:@"first"])
        [[self delegate]classificaionAccessComplete:classIdArr];
    else
        [[self delegate]classificaionAccessCompleteSecond:classIdArr];
    
    
}
#pragma mark - App Detail operations

-(void)coreDataWriteAppsDetail:(NSDate*)lastSynchDate{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *updateContext= [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:updateContext];
    [fetchRequest setEntity:entity];
    
    @try {
        NSError *error;
        NSArray *results = [updateContext executeFetchRequest:fetchRequest error:&error];
        NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
        [favoritsGrabbed setValue:lastSynchDate forKey:@"lastSync"];
    
        if (![updateContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        else{
            [self coreDataReadAppDetail];
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"error:%@",exception);
    }
    
}
-(NSMutableDictionary*)coreDataReadAppDetail
{
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setPredicate:predicate];
    
    
    NSArray *fetchedObjects = [readContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest setEntity:entity];
    
    NSMutableDictionary *dicObject;
    
    @try {
        Apps *appsObj=[fetchedObjects objectAtIndex:0];
         dicObject=[[NSMutableDictionary alloc]init];
        
       // if([appsObj.lastSync length] != 0)
        {
            [dicObject setObject:appsObj.settings forKey:@"settings"];
            
            [dicObject setObject:[appsObj.settings valueForKey:@"app_home_header_img"] forKey:@"app_home_header_img"];
            
            NSLog(@"app_home_header_img :%@",[dicObject objectForKey:@"app_home_header_img"]);
            
            //if (appsObj.lastSync != nil)
            
           // [dicObject setObject:appsObj.lastSync forKey:@"lastSync"];
            
            NSLog(@"appsObj.lastSync :%@",appsObj.lastSync);
            
           
            
        }
            
//        else
//        {
//            [dicObject setValue:nil forKey:@"lastSync"];
//           // [dicObject setObject:@"" forKey:@"lastSync"];
//        }
    
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    return dicObject;
}





-(void)coreDataCodeWrite:(NSString*)subKeyStr arr:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Categories" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Categories *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
     @try {
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
   
        
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Categories"
                                    inManagedObjectContext:context];
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"catId"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"name"] forKey:@"name"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"type"]forKey:@"type"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"googletype"] forKey:@"googletype"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"external_url"] forKey:@"external_url"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"paid"]  forKey:@"paid"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"points"]  forKey:@"points"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"rss_url"] forKey:@"rss_url"];
            
            // [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"fav_icon"]forKey:@"fav_icon"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"ad"] forKey:@"ad"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"listing_icon"]forKey:@"listing_icon"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"sub_exists"]forKey:@"sub_exists"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"ad"]forKey:@"ad"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"link"]forKey:@"link"];
            
         
            
            //[[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"offlineresultArray"];
          //  [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"image"]forKey:@"image"];
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"image"];
            
            // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if(str.length>0)
            {
                [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",str], nil]];
                
                [self insertImagesToDownload:str];
                // [self sdImageCaching:str];
            }
            [Que setValue:str forKey:@"image"];
            
            subKeyStr=[[queArray objectAtIndex:i] objectForKey:@"subcategory"];
            [Que setValue:subKeyStr forKey:@"subcategory"];
            
        NSString *headerImg =[[queArray objectAtIndex:i] objectForKey:@"header_image"];
            if(!([headerImg isEqualToString:@"NA"]))
            {
                if(!(headerImg==nil))
             headerImg= [headerImg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
             [self insertImagesToDownload:headerImg];
                
                [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"header_image"]forKey:@"header_image"];
            }
            
            if([[[queArray objectAtIndex:i] objectForKey:@"sub_exists"]isEqualToString:@"yes"]){
    NSArray*subtemp=[[queArray objectAtIndex:i] objectForKey:@"subcategory"];//subKeyStr
                for (int q=0; q<[subtemp count ]; q++)
                {
                    
                    
                    NSString*str=[[subtemp objectAtIndex:q]objectForKey:@"listing_icon"];
                    str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    [self insertImagesToDownload:str];

                   // [self sdImageCaching:str];
                    
                    NSString*str1=[[subtemp objectAtIndex:q]objectForKey:@"image"];
                    str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    [self insertImagesToDownload:str1];

                    //[self sdImageCaching:str1];
                    
                    if([[[subtemp objectAtIndex:q] objectForKey:@"sub_exists"]isEqualToString:@"yes"]){
                    NSArray*subtemp1=[[subtemp objectAtIndex:q] objectForKey:@"subcategory"];//@"subcategory" //subKeyStr
                        
                        for (int f=0; f<[subtemp1 count ]; f++) {
                            
                            NSString*str1=[[subtemp1 objectAtIndex:f]objectForKey:@"listing_icon"];
                            str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            
                            [self insertImagesToDownload:str1];

                           // [self sdImageCaching:str1];
                            
                            
                            NSString*str2=[[subtemp1 objectAtIndex:f]objectForKey:@"image"];
                            str2= [str2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            [self insertImagesToDownload:str2];

                           // [self sdImageCaching:str2];
                            
                        }
                    }
                }
            }
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
         
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    
    
  /*  NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
    dict2= [dict mutableCopy];
    
    
    [dict setObject:dict2 forKey: @"test"];
    
    // ... and later ...
    
    id something = [dict objectForKey:@"Some Key"];
    NSLog(@"something :%@",something);
    
    
    
    NSMutableArray *ar = [[NSMutableArray alloc]init];
    ar=[dict objectForKey:@"test"];*/

   // [queArray
    NSMutableArray *myArray = [[NSMutableArray alloc]init];
    [myArray insertObject:@"Test" atIndex:0];
     [myArray insertObject:queArray atIndex:1];
     
   
    
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
      mutableDict= [myArray objectAtIndex:1];
    
    
    NSLog(@"mutableDict :%@",mutableDict);
    
    // NSDictionary *jsont =[mutableArray valueForKey:@"ALL ITEMS"];
    
    
     NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:myArray];
     [[NSUserDefaults standardUserDefaults]setObject:data2 forKey:@"offlineresultArray"];
    
    //[[NSUserDefaults standardUserDefaults] setObject:queArray forKey:@"offlineresultArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
}
-(void)coreDataCodeRead{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Categories" inManagedObjectContext:context];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];

    int i;
    //fav_icon
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    for (Categories *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        //Ram
        [tempManufacturerDictionary setObject:[manuf getAppid] forKey:@"appid"];
        [tempManufacturerDictionary setObject:[manuf getCatID] forKey:@"Id"];
        [tempManufacturerDictionary setObject:[manuf getImage] forKey:@"image"];
        [tempManufacturerDictionary setObject:[manuf getPaid] forKey:@"paid"];
        [tempManufacturerDictionary setObject:[manuf getPoints] forKey:@"points"];
        [tempManufacturerDictionary setObject:[manuf getName] forKey:@"name"];
        [tempManufacturerDictionary setObject:[manuf getType] forKey:@"type"];
        [tempManufacturerDictionary setObject:[manuf getRssUrl] forKey:@"rss_url"];
        
        [tempManufacturerDictionary setObject:[manuf getAd] forKey:@"ad"];
        
        [tempManufacturerDictionary setObject:[manuf getLink] forKey:@"link"];
        
       // [tempManufacturerDictionary setObject:[manuf getfav] forKey:@"fav_icon"];
        
        [tempManufacturerDictionary setObject:[manuf getListingIcon] forKey:@"listing_icon"];
        [tempManufacturerDictionary setObject:[manuf getSubExists] forKey:@"sub_exists"];
        [tempManufacturerDictionary setObject:[manuf getExternalUrl] forKey:@"external_url"];
        [tempManufacturerDictionary setObject:[manuf getAd] forKey:@"ad"];
        [tempManufacturerDictionary setObject:[manuf getGoogleType] forKey:@"googletype"];
        
        
        [tempManufacturerDictionary setObject:[manuf getSubcategory] forKey:@"subcategory"];
         [tempManufacturerDictionary setObject:[manuf getheader_image] forKey:@"header_image"];
        
        [array addObject:tempManufacturerDictionary];
    }
    
    
    dataArray=array;
    [self taskComplete];
    
       

}

-(void)coreDataCodeWrite_bus:(NSArray*)queArray
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Businesses" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"catId == %@",[[queArray objectAtIndex:0] objectForKey:@"Id"]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Businesses *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    NSLog(@"queArray : %@",queArray);
    
    //[[NSUserDefaults standardUserDefaults]setObject:queArray forKey:@"queArraybus"];
    
    @try {
        for (i=0; i<[queArray count]; i++) {
            
             
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Businesses"
                                    inManagedObjectContext:context];
            
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"busId"];
            NSString *str123 = [[queArray objectAtIndex:i] objectForKey:@"name"];
            if(!(str123==(id) [NSNull null] || [str123 length]==0 || [str123 isEqualToString:@""]))
                
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"name"]forKey:@"name"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id"]forKey:@"cat_id"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id2"]forKey:@"cat_id2"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id3"]forKey:@"cat_id3"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id4"]forKey:@"cat_id4"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id5"]forKey:@"cat_id5"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id6"]forKey:@"cat_id6"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id7"]forKey:@"cat_id7"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id8"]forKey:@"cat_id8"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id9"]forKey:@"cat_id9"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id10"]forKey:@"cat_id10"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"address"]forKey:@"address"];
            
            NSString *strpreview =[[queArray objectAtIndex:i] objectForKey:@"preview"];
            NSString *strdetails =[[queArray objectAtIndex:i] objectForKey:@"details"];
            NSString *strphone =[[queArray objectAtIndex:i] objectForKey:@"phone"];
            
            if((!(strpreview == (id)[NSNull null] || strpreview.length == 0)))
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"preview"]forKey:@"preview"];
            
            if((!(strdetails == (id)[NSNull null] || strdetails.length == 0)))
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"details"]forKey:@"details"];
            
            if((!(strphone == (id)[NSNull null] || strphone.length == 0)))
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"phone"]forKey:@"phone"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"website"]forKey:@"website"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"latitude"]forKey:@"latitude"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"longitude"]forKey:@"longitude"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"featured"]forKey:@"featured"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"star_rating"]forKey:@"star_rating"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"add_to_favorites"]forKey:@"add_to_favorites"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"share_listing"]forKey:@"share_listing"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"fixed_order"]forKey:@"fixed_order"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_call"]forKey:@"show_call"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_web"]forKey:@"show_web"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"website_text"]forKey:@"website_text"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"email_address"]forKey:@"email_address"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_email"]forKey:@"show_email"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"email_text"]forKey:@"email_text"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"disp_address"]forKey:@"disp_address"];
            
            if([[queArray objectAtIndex:i] objectForKey:@"guide_id"])
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"guide_id"]forKey:@"guide_id"];
            
            if([[queArray objectAtIndex:i] objectForKey:@"guide_name"])
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"guide_name"]forKey:@"guide_name"];
            
            NSMutableArray*muts=[[NSMutableArray alloc]init];
            for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
                
                NSString*str1=str;
                // str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [muts addObject:str1];
                if(str1.length>0)
                {
                    [self insertImagesToDownload:str1];

                   // [self sdImageCaching:str1];
                }
                
                
            }
            [Que setValue:muts forKey:@"image"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"type"]forKey:@"type"];
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if(str.length>0)
            {
                [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",str], nil]];
                
                [self insertImagesToDownload:str];

               // [self sdImageCaching:str];
            }
            [Que setValue:str forKey:@"thumb"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"weight"]forKey:@"weight"];
            
            
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{

            }
           // NSLog(@"queArray catid : %@",[[queArray objectAtIndex:i] objectForKey:@"cat_id"]);
        }
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }

    
}
-(void)coreDataCodeWrite_gallery:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Gallery" inManagedObjectContext:context1];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Gallery *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try {
        
        
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Gallery"
                                    inManagedObjectContext:context];
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"id"]forKey:@"gall_id"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"title"]forKey:@"title"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"description"]forKey:@"gall_description"];
            NSString*str1=[[queArray objectAtIndex:i] objectForKey:@"image"];
            if(str1.length>0)
            {
                [self insertImagesToDownload:str1];

               // [self sdImageCaching:str1];
            }
            
            [Que setValue:str1 forKey:@"image"];
            
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            //str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if(str.length>0)
            {
                [self insertImagesToDownload:str];

              //  [self sdImageCaching:str];
            }
            [Que setValue:str forKey:@"thumb"];
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    
    
}
-(void)coreDataCodeWrite_deals:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Deals" inManagedObjectContext:context1];
    
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Deals *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try {
        
        
        
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Deals"
                                    inManagedObjectContext:context];
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"deal_id"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"featured"]forKey:@"featured"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_title"]forKey:@"deal_title"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_subtitle"]forKey:@"deal_subtitle"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_description"]forKey:@"deal_description"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_phone"]forKey:@"deal_phone"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"deal_web"]forKey:@"deal_web"];
            
            
            
            
            
            NSMutableArray*muts=[[NSMutableArray alloc]init];
            @try {
                
                
                for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
                    
                    NSString*str1=str;
                    [muts addObject:str1];
                    if(str1.length>0)
                    {
                        [self insertImagesToDownload:str1];

                       // [self sdImageCaching:str1];
                    }
                    
                    
                }
            }
            @catch (NSException *exception) {
                
            }
            
            [Que setValue:muts forKey:@"image"];
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            if(str.length>0)
            {
                [self insertImagesToDownload:str];

              //  [self sdImageCaching:str];
            }
            
            [Que setValue:str forKey:@"thumb"];
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                
            }
            
        }
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
}
-(void)coreDataCodeWrite_events:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Events" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Events *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }

    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try
    {
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Events"
                                    inManagedObjectContext:context];
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"event_id"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"featured"]forKey:@"featured"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_title"]forKey:@"event_title"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_time"]forKey:@"event_time"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_address"]forKey:@"event_address"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"event_description"]forKey:@"event_description"];
            
            
            
            
            NSMutableArray*muts=[[NSMutableArray alloc]init];
//            @try {
//                
//                for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
//                    
//                    NSString*str1=str;
//                    [muts addObject:str1];
//                    if(str1.length>0)
//                    {
//                        [self insertImagesToDownload:str1];
//
//                       // [self sdImageCaching:str1];
//                    }
//                    
//                    
//                }
//            }
//            @catch (NSException *exception) {
//                
//            }
//            
            [Que setValue:muts forKey:@"image"];
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            if(str.length>0)
            {
                [self insertImagesToDownload:str];

               // [self sdImageCaching:str];
            }
            
            [Que setValue:str forKey:@"thumb"];
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    
    NSLog(@"EVENTS WROTE");
}

-(void)coreDataCodeWrite_statics:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Static" inManagedObjectContext:context1];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Static *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try
    {
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Static"
                                    inManagedObjectContext:context];
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            
            NSString *strii ;
            
            
            if([queArray isKindOfClass:[NSDictionary class]])
            {
                strii = [queArray objectAtIndex:i];
                
            }
            else
            {
             strii = [NSString stringWithFormat:@"%@",queArray];
            }
            
            if([strii isEqual: [NSNull null]])
            {
                NSLog(@"strii is nil");
            }
            
            if(strii)
            {
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"cat_id"]forKey:@"catid"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"title"] forKey:@"title"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"content" ] forKey:@"content"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"address"] forKey:@"address"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"latitude"] forKey:@"latitude"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"longitude"] forKey:@"longitude"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"Id"] forKey:@"staticId"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"status"] forKey:@"status"];

            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"email"]forKey:@"email"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"email_text"] forKey:@"email_text"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"favorites" ] forKey:@"favorites"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"phone"] forKey:@"phone"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"share"] forKey:@"share"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"show_email"] forKey:@"show_email"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"show_phone"] forKey:@"show_phone"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"show_url"] forKey:@"show_url"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"url"] forKey:@"urll"];
            [Que setValue:[[queArray objectAtIndex:i]objectForKey:@"url_text"] forKey:@"url_text"];
            NSMutableArray*muts=[[NSMutableArray alloc]init];
            
                
                
                for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
                    
                    NSString*str1=str;
                    [muts addObject:str1];
                    if(str1.length>0)
                    {
                        [self insertImagesToDownload:str1];

                       // [self sdImageCaching:str1];
                    }
                    
                    
                }
                
            [Que setValue:muts forKey:@"image"];
            
            }
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    
    
}


-(void)readListOfImagesToDownload
{
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:readContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
     
        @try {
            
        
        for(IMAGES_TO_CACHE *object in results)
        {
            
            NSString *imageUrl=object.imageurl;
            
                NSURL *urlimg=[NSURL URLWithString:imageUrl];
            
                UIImageView*imagevw=[[UIImageView alloc]init];
                imagevw.image=[imageCache imageFromDiskCacheForKey:imageUrl];
                
                
                if (!imagevw.image)
                {
                    
                   
                        
                        NSData *data = [NSData dataWithContentsOfURL:urlimg];
                        imagevw.image = [UIImage imageWithData:data];
                      //  [imageCache storeImage:imagevw.image forKey:imageUrl completion:^{  }];
                        
                    
                    
                }
                else
                {
                    
                    
                    
                    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
                    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                    NSEntityDescription *entity = [NSEntityDescription
                                                   entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:context1];
                    [fetchRequest setEntity:entity];
                    
                    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"imageurl=%@",imageUrl];
                    NSError *error;
                    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
                    IMAGES_TO_CACHE *currentQUE;
                    for(currentQUE in listOfQUEToBeDeleted)
                    {
                        
                        [context1 deleteObject:currentQUE];
                        
                    
                        
                    }
                    
                    if (![context1 save:&error]) {
                        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                    }
                }
                
            }
            
            
            
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        
    }
    @catch (NSException * e)
        
        {
            
            NSLog(@"Exception: %@", e);
            
            
        }
                   
    });
    
    
    
   
        
    
    }

-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

-(void)downloadZip
{
    dispatch_queue_t queue = dispatch_get_global_queue(0,0);
    dispatch_async(queue, ^{
        
        
        NSURL *yourURL = [NSURL URLWithString:@"http://insightto.com/Crater%20Lake%20Park_images.zip"];
        // turn it into a request and use NSData to load its content
        NSURLRequest *request = [NSURLRequest requestWithURL:yourURL];
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        // find Documents directory and append your local filename
        NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        documentsURL = [documentsURL URLByAppendingPathComponent:@"localFile.zip"];
        
        // and finally save the file
        [data writeToURL:documentsURL atomically:YES];
        
        NSString *filename = @"localFile.zip";
        NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString * zipPath = [documentsDirectory stringByAppendingPathComponent:filename];
        
        NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
        
        
        
        // list contents of Documents Directory just to check
        //    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        
        NSArray *contents = [[NSFileManager defaultManager]contentsOfDirectoryAtURL:documentsURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
        
        NSLog(@"description for zip:%@", [contents description]);
        
        
        NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        
        NSString *imageUrl;// =object.imageurl;
        
        UIImageView*imagevw=[[UIImageView alloc]init];
        
        [imageCache storeImage:imagevw.image forKey:imageUrl completion:^{  }];
        
    });
    
}

-(void)insertImagesToDownload:(NSString*)imgstr
{
    
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        // Some long running task you want on another thread
    
        PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
        
        
        
        @try{
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"IMAGES_TO_CACHE"
                                    inManagedObjectContext:context];
            
            
            
            
            [Que setValue:imgstr forKey:@"imageurl"];
            [Que setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"] forKey:@"appid"];
            
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                //            [self performSelectorInBackground:@selector(updateProgress:) withObject:[NSNumber numberWithFloat:i]];
                // [self stopCircleLoader4];
                
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            if (completion) {
//                                completion();
//                            }
//                        });

            }
            
            
        }
        @catch (NSException * e)
        {
            NSLog(@"Exception: %@", e);
            
            
        }

        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (completion) {
//                completion();
//            }
//        });
//    });

    
    
    
}
-(void)sdImageCaching:(NSString*)imageUrlKey;
{
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    NSURL *urlimg=[NSURL URLWithString:imageUrlKey];
    UIImageView*imagevw=[[UIImageView alloc]init];
    
    
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
    dispatch_async(queue, ^{
    
    
    imagevw.image=[imageCache imageFromDiskCacheForKey:imageUrlKey];
    if (!imagevw.image)
    {
        
        [imagevw sd_setImageWithURL:urlimg placeholderImage:nil options:SDWebImageRefreshCached];
        
    }
    else
    {
        
        
        
        PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription
                                       entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:context1];
        [fetchRequest setEntity:entity];
        
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"imageurl=%@",imageUrlKey];
        NSError *error;
        NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
        IMAGES_TO_CACHE *currentQUE;
        for(currentQUE in listOfQUEToBeDeleted)
        {
            [context1 deleteObject:currentQUE];
        }
        
        if (![context1 save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
    }
    
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            
            
        });
    });

}


- (void) loadImageFromURL:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        im.image = [UIImage imageNamed:@"img_def"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                im.image = image;
            });
        });
    }
}
- (void) loadImageFromURL_bus:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        im.image = [UIImage imageNamed:@"img_def"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
 
        });
    }
}
@end
