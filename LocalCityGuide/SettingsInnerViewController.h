//
//  SettingsInnerViewController.h
//  Visiting Ashland
//
//  Created by swaroop on 17/05/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsInnerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UITableView *tblViewa;
@property (strong, nonatomic) IBOutlet UITabBar *tabbAr;
@property (strong, nonatomic) IBOutlet UIView *loaderView;
@property(nonatomic) BOOL isCellClicked;
@end
