//
//  SettingsInnerViewController.m
//  Visiting Ashland
//
//  Created by swaroop on 17/05/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import "SettingsInnerViewController.h"
#import "AsyncImageView.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "JMImageCache.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "PDInappHomeViewController.h"
#import "PDGalleryViewController.h"
#import "Reachability.h"
#import "Constants.h"
#import "SettingsViewController.h"
#import "PDDetailViewController.h"
#import "PDViewController.h"
#import "PDFavoriteViewController.h"

@interface SettingsInnerViewController ()
{
   NSString *bannerImgURL;
    NSMutableArray *array;
}
@end

@implementation SettingsInnerViewController
NSArray *arrayContents;
NSArray *arrayFooter;
NSArray *arraySettings;

-(void)viewWillAppear:(BOOL)animated
{
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"forloader123"])
    {
      [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:@"forloader123"];
    
      [self.view addSubview:_loaderView];
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        [[NSBundle mainBundle] loadNibNamed:@"SettingsInnerViewController4" owner:self options:nil];
    }
    else if(IS_IPHONE_4_OR_LESS)
    {
        [[NSBundle mainBundle] loadNibNamed:@"SettingsInnerViewController4" owner:self options:nil];
    }

    else
    {
      [[NSBundle mainBundle] loadNibNamed:@"SettingsInnerViewController" owner:self options:nil];
    }
    
    self.titleLbl.text=@"LocalsGuide";
    
    [self settingsWS];
    [self footerWS];
    [self webServiceInner];
    
    
   /* [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableCellStartGuide:)
                                                 name:@"tableCellStartGuide"
                                               object:nil];*/
    
}

-(void) tableCellStartGuide:(NSNotification *) notification
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:1 inSection:0] ;
    [self tableView:_tblViewa didSelectRowAtIndexPath:myIP];
}

-(void)settingsWS
{
    NSString *wUrl=[NSString stringWithFormat:@"%@94",SETTINGS_WEBSERVICE_SUB];
    //SETTINGS_WEBSERVICE_SUB
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self settingsWS];
            return ;
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
        
        arraySettings=[[NSMutableArray alloc]init];
        arraySettings=[json objectForKey:@"settings"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"arraySettings :%@",arraySettings);
            
            bannerImgURL = [[arraySettings objectAtIndex:0] valueForKey:@"app_home_header_img"];
            [[NSUserDefaults standardUserDefaults]setObject:bannerImgURL forKey:@"app_home_header_imgInner"];
        });
    }];
    
    [dataTask resume];
    
}
-(void)footerWS
{
    NSString *wUrl=[NSString stringWithFormat:@"%@94",FOOTER_WEBSERVICE];
    //SETTINGS_WEBSERVICE_SUB
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self footerWS];
            return ;
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
        
        arrayFooter=[[NSMutableArray alloc]init];
        arrayFooter=[json objectForKey:@"footer"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            UITabBarItem *item1;UITabBarItem *item2;UITabBarItem *item3;UITabBarItem *item4;
            UIImageView *img=[[UIImageView alloc]init];
            
            
            
            switch ([arrayFooter count]) {
                case 0:
                {
                    
                    
                    item1 =
                    [[UITabBarItem alloc] initWithTitle:@""
                                                  image:[UIImage imageNamed:@"view"]
                                                    tag:1];

                    array = [array initWithObjects:item1, nil];
                    break;
                }
                case 1:
                {
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:0]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView = img.image;
                    

                    
                    item1 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:0]valueForKey:@"name"]
                                                  image:imageFromImageView
                                                    tag:1];

                    array = [[NSMutableArray alloc]initWithObjects:item1, nil];
                    break;
                }

                case 2:
                {
                     [self loadImageFromURL1:[[arrayFooter objectAtIndex:0]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView = img.image;
                    
                    
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:1]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView2 = img.image;
                    
                    item1 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:0]valueForKey:@"name"]
                                                  image:imageFromImageView
                                                    tag:1];
                    item2 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:1]valueForKey:@"name"]
                                                  image:imageFromImageView2
                                                    tag:2];

                    array = [[NSMutableArray alloc]initWithObjects:item1,item2, nil];
                    
                    
                    break;
                }
                case 3:
                {
                    
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:0]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView = img.image;
                    
                    
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:1]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView2 = img.image;
                    
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:2]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView3 = img.image;
                    
                    
                    item1 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:0]valueForKey:@"name"]
                                                  image:imageFromImageView
                                                    tag:1];
                    item2 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:1]valueForKey:@"name"]
                                                  image:imageFromImageView2
                                                    tag:2];
                    item3 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:2]valueForKey:@"name"]
                                                  image:imageFromImageView3
                                                    tag:3];

                    array = [[NSMutableArray alloc]initWithObjects:item1,item2,item3, nil];
                    break;
                    
                }
                case 4:
                   
                {
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:0]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView = img.image;
                    
                    
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:1]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView2 = img.image;
                    
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:2]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView3 = img.image;
                    
                    [self loadImageFromURL1:[[arrayFooter objectAtIndex:3]valueForKey:@"footer_icon"] image:img];
                    UIImage *imageFromImageView4 = img.image;
                    
                    
                    item1 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:0]valueForKey:@"name"]
                                                  image:imageFromImageView
                                                    tag:1];
                    item2 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:1]valueForKey:@"name"]
                                                  image:imageFromImageView2
                                                    tag:2];
                    item3 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:2]valueForKey:@"name"]
                                                  image:imageFromImageView3
                                                    tag:3];
                    
                    item4 =
                    [[UITabBarItem alloc] initWithTitle:[[arrayFooter objectAtIndex:3]valueForKey:@"name"]
                                                  image:imageFromImageView4
                                                    tag:4];
                    
                    array = [[NSMutableArray alloc]initWithObjects:item1,item2,item3,item4, nil];
                    
                    break;
                }

                default:
                    break;
            }
            
            
            [self.tabbAr setItems:array];
          self.tabbAr.selectedItem = item1;
            
            [self.tabbAr setTintColor:[UIColor colorWithRed:251/255.0f green:173/255.0f blue:24/255.0f alpha:1.0]];
            [self.tabbAr setTranslucent:YES];
            
        });
    }];
    
    [dataTask resume];

}

-(void)webServiceInner
{
    
    NSString *wUrl=[NSString stringWithFormat:@"%@94&device=hdpi",HOM_WEBSERVICE];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self webServiceInner];
            return ;
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
        
        arrayContents=[[NSMutableArray alloc]init];
        arrayContents=[json objectForKey:@"maincategory"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [_tblViewa reloadData];
            [_loaderView removeFromSuperview];
        });
    }];
    
    [dataTask resume];
    
    
}

- (IBAction)back:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
   // [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark - UITableView delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        
        return 0;
    }
    else{
        
        return [arrayContents count];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section==0){
        
        bannerImgURL = [[NSUserDefaults standardUserDefaults] valueForKey:@"app_home_header_imgInner"];
        return ([bannerImgURL isEqualToString:@"NA"]) ? 0 : kTableViewHeaderHeight;
    }
    else{
        return 0;
        
    }
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
   
        if(section==0){
            
            AsyncImageView *tblHeaderImage = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, kTableViewHeaderHeight)];
            [tblHeaderImage setBackgroundColor:[UIColor clearColor]];
            
            tblHeaderImage.imageURL=[NSURL URLWithString:bannerImgURL];
            
            
            
            [tblHeaderImage sd_setImageWithURL:[NSURL URLWithString:bannerImgURL] placeholderImage:nil options:SDWebImageRefreshCached];
            
            
            
            NSString *headerImgKey=[NSString stringWithFormat:@"header%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            NSString *headerImgName=[[NSUserDefaults standardUserDefaults]objectForKey:headerImgKey];
            
            
            SDImageCache *imageCache =[SDImageCache sharedImageCache];
            imageCache = [imageCache initWithNamespace:@"header"];
            
            
            
            tblHeaderImage.image=[imageCache imageFromDiskCacheForKey:bannerImgURL];
            
            
            if(!tblHeaderImage.image)
            {
                
                [self loadImageFromURL1:bannerImgURL image:tblHeaderImage];
                
            }
            
            
            return tblHeaderImage;
        }
        else{
            return nil;
            
        }
    
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = [arrayContents objectAtIndex:indexPath.row];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(75,10,250,40)];
    [lbl setFont:[UIFont fontWithName:@"Verdana" size:18]];
    lbl.text = [dict objectForKey:@"name"];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textAlignment = NSTextAlignmentLeft;
    [cell.contentView addSubview:lbl];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 50, 50)];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
          
                
                [imageview sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"placeholder_new.png"] options:SDWebImageRefreshCached];
            
            [cell.contentView addSubview:imageview];
        });
    });
    
    
    
    [cell setNeedsDisplay];
    [cell setNeedsLayout];
    
    
    NSInteger *points=[[dict objectForKey:@"points"] integerValue];
    
    NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
    
   
        {
            
            
            
    
    int titleX;
    if (![[dict objectForKey:@"image"] isEqualToString:@"FALSE"]) {
        
        
        
        titleX = 75;
    }
    else
        titleX = 20;
    
    
    lbl.frame = CGRectMake(titleX,10,250,40);//(titleX,10,250 - titleX,40)  (75,10,250,40)
    
        }
    
    return cell;
        
}



// code change by Ram to hide tabbar
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"arrayContents count :%lu",(unsigned long)[arrayContents count]);
    
    //if (_isCellClicked)return;
    
    
   // [self dismissViewControllerAnimated:NO completion:nil];
    
    NSDictionary *dict = [arrayContents objectAtIndex:indexPath.row];
    
    NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
    
    if(![unlimitedPurchase isEqualToString:@"purchased"])
    {
        NSInteger points=[[dict objectForKey:@"points"] integerValue];
        
    }
    _isCellClicked=YES;
    if (![[dict objectForKey:@"ad"] isEqualToString:@"FALSE"]) {/*
        NSMutableDictionary *dicto = [[NSMutableDictionary alloc] init];
        [dicto setObject:[dict objectForKey:@"ad"] forKey:@"imageLink"];
        [dicto setObject:[dict objectForKey:@"link"] forKey:@"webLink"];
        [[NSUserDefaults standardUserDefaults] setObject:dicto forKey:@"CatAd"];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"CatAdExist"];
   */ }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"CatAdExist"];
    }
    
    if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
        PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
        [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:[dict objectForKey:@"type"] withName:[dict objectForKey:@"name"]];
        [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
        subCategoryViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:subCategoryViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"url"]){
        if([self reachable]){
            
            
            
            NSString *webString =[dict objectForKey:@"external_url"];
            
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:webString]];
            _isCellClicked=NO;
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"static"])
    {
        PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
        [detailViewController getDictionary:dict];
        detailViewController.hidesBottomBarWhenPushed = YES;
        detailViewController.isFromIn=YES;
        [self presentViewController:detailViewController animated:NO completion:nil];
       // [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dict objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dict objectForKey:@"type"] isEqualToString:@"normal"]){
        PDListViewController *listViewController = [[PDListViewController alloc] init];
        [listViewController getDictionary:dict];
        listViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:listViewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"event"]) {
        
        PDEventsViewController *viewController = [[PDEventsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
        
        [viewController getDictionaryeve:dict];
        
       
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"deal"]) {
        PDDealsViewController *viewController = [[PDDealsViewController alloc] init];
        viewController.title = [dict objectForKey:@"name"];
        
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    else if ([[dict objectForKey:@"type"] isEqualToString:@"rss"]) {
        if([self reachable]){
            PDRssListViewController *rssListViewController = [[PDRssListViewController alloc] initWithNibName:@"PDRssListViewController" bundle:nil];
            [rssListViewController getDictionary:dict];
            rssListViewController.isFromHome = YES;
            rssListViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:rssListViewController animated:YES];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    else if ([[dict objectForKey:@"type"] isEqualToString:@"gallery"]) {
        PDGalleryViewController *galleryViewController = [[PDGalleryViewController alloc] initWithNibName:@"PDGalleryViewController" bundle:nil];
        [galleryViewController getDictionary:dict];
        galleryViewController.hidesBottomBarWhenPushed = YES;
        //galleryViewController.isFromHome = YES;
        galleryViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:galleryViewController animated:YES];
    }
    else {
        if ([[dict objectForKey:@"type"] isEqualToString:@"fixed"] || [[dict objectForKey:@"type"] isEqualToString:@"location"]) {
            if ([[dict objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
                PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
                [subCategoryViewController getSubId:[dict objectForKey:@"Id"] ofType:@"fixed" withName:[dict objectForKey:@"name"]];
                [subCategoryViewController getSubArray:[dict objectForKey:@"subcategory"]];
                subCategoryViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:subCategoryViewController animated:YES];
            }
            else {
                PDListViewController *listViewController = [[PDListViewController alloc] init];
                [listViewController getDictionary:dict];
                listViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:listViewController animated:YES];
            }
        }
    }
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
    
}

// Check internet connection
-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

#pragma mark - Image caching

- (void) loadImageFromURL1:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        im.image = [UIImage imageNamed:@"img_def"];

        NSData *data = [NSData dataWithContentsOfURL:imageURL];
        [FTWCache setObject:data forKey:key];
        UIImage *image = [UIImage imageWithData:data];
        
        im.image = image;
        
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
