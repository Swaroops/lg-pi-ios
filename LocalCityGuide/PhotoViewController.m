//
//  PhotoViewController.m
//  Wallpaperz
//
//  Created by CODERLAB on 17.05.14.
//  Copyright (c) 2014 studio76. All rights reserved.
//

#import "PhotoViewController.h"
#import "LoadingHUD.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "SDImageCache.h"
#import "FTWCache.h"

@interface PhotoViewController ()

@end

@implementation PhotoViewController
@synthesize photoScroller,photoImgv,activityIndi,activityHolderView;
- (void)viewDidLoad
{
    
    if (IS_IPHONE5) {
       [[NSBundle mainBundle] loadNibNamed:@"PhotViewController" owner:self options:nil];    }
    else {
       [[NSBundle mainBundle] loadNibNamed:@"PhotViewController4" owner:self options:nil];
    }
    [self setUpPhoto];
    //_scroller = [ImageScrollView new];
   // _scroller.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
  //  self.view = _scroller;
    
  //  [_scroller.zoomView removeFromSuperview];
    
    /* _scroller.zoomView = [[UIImageView alloc] init];
    [_scroller addSubview:_scroller.zoomView];
    _scroller.backgroundColor=[UIColor greenColor];
    _scroller.zoomView.backgroundColor=[UIColor yellowColor];
    _scroller.zoomView = nil;
    _scroller.zoomScale = 1.0;*/
    
   // [self loadImageFromURL:_imageURL];
    
  //  photoScroller = [[UIScrollView alloc]init];
   
  //  [self.view addSubview:photoScroller];
   // photoImgv = [[UIImageView alloc]init];
//    [photoScroller addSubview:photoImgv];
    photoScroller.zoomScale=1;
    
    photoScroller.bouncesZoom = YES;
    photoScroller.decelerationRate = UIScrollViewDecelerationRateFast;
  // photoScroller.backgroundColor=[UIColor redColor];
    photoScroller.frame=self.view.frame;
    photoScroller.contentSize=CGSizeMake([[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    
      [self loadImageFromURL:_imageURL];
    
    photoScroller.delegate=self;
    
    
    [super viewDidLoad];

    
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [photoScroller addGestureRecognizer:doubleTapRecognizer];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void) viewWillDisappear:(BOOL)animated{
}

- (void) viewWillAppear {
//     [self setUpPhoto];
    
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];
}

- (void) loadImageFromURL:(NSString*)URL {
    NSURL *imageURL = [NSURL URLWithString:URL];
    NSString *key = [URL MD5Hash];
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    UIImageView *testimgv=[[UIImageView alloc]init];
    testimgv.image=[imageCache imageFromDiskCacheForKey:URL];
    
    if (!testimgv.image)
    {

    
    NSData *data = [FTWCache objectForKey:key];
    
    activityHolderView.hidden=NO;
    activityIndi.hidden=NO;
    [activityIndi startAnimating];
    
     [activityHolderView.layer setCornerRadius:7.0f];
    
    if (data) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        _image = [UIImage imageWithData:data];
        photoImgv.image=_image;
      //  [self setImageRect:_image containersize:photoScroller.contentSize];
         [self displayImage:_image];
        [imageCache storeImage:_image forKey:URL completion:^{  }];
        
        activityIndi.hidden=YES;
        [activityIndi stopAnimating];
        activityHolderView.hidden=YES;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self setUpPhoto];
    } else {
        
        if (![self offlineAvailable])
        {
       
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            
            [FTWCache setObject:data forKey:key];
            
            _image = [UIImage imageWithData:data];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            dispatch_sync(dispatch_get_main_queue(), ^{
                photoImgv.image=_image;
                [imageCache storeImage:_image forKey:URL completion:^{  }];
                
            //    [self setImageRect:_image containersize:photoScroller.contentSize];

                [self displayImage:_image];
                 //[[LoadingHUD loadingHUD] hideActivityIndicator];
                activityIndi.hidden=YES;
                [activityIndi stopAnimating];
                activityHolderView.hidden=YES;
                            [self setUpPhoto];
            });
            //[[LoadingHUD loadingHUD] hideActivityIndicator];
           
            
        });
        }
        else
        {
            //not Reachable
            {
                NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                
                NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                
                NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                
                
                
                
                NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                
                NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                
                NSLog(@"firstBit folder :%@",firstBit);
                NSLog(@"secondBit folder :%@",secondBit2);
                
                NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                
                NSLog(@"str url :%@",URL);
                
                NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/gallery",secondBit2]];
                
                NSArray* str3 = [URL componentsSeparatedByString:@"gallery_images/"];
                
                NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                
                NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                NSLog(@"firstBit folder :%@",firstBit3);
                NSLog(@"secondBit folder :%@",secondBit3);
                NSLog(@"decoded folder :%@",decoded);
                
                NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                
                UIImage* image = [UIImage imageWithContentsOfFile:Path];
                
                testimgv.image=image;
                
                activityIndi.hidden=YES;
                [activityIndi stopAnimating];
                activityHolderView.hidden=YES;
                photoImgv.image=testimgv.image;
                [self displayImage:testimgv.image];
                // [self loadImageFromURL1:str image:tblHeaderImage];
                
                // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                
                
            }
            
           
        }
        
        
        
    }
        
        
    }
    else
    {
        activityIndi.hidden=YES;
        [activityIndi stopAnimating];
        activityHolderView.hidden=YES;
        photoImgv.image=testimgv.image;
        [self displayImage:testimgv.image];
    }
    
 /*   _isZoomed=NO;
    photoScroller.contentSize=CGSizeMake([[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    [self setImageRect:photoImgv.image containersize:photoScroller.contentSize];
    [self centerScrollViewContents];*/
    
    
   [self setUpPhoto];
}



-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(void)setImageRect:(UIImage*)img containersize:(CGSize)contSize
{
    float imgx,imgy,imgHt,ImgWd;
    
    CGSize imgsiz=img.size;
    
    float aspectratio=imgsiz.width/imgsiz.height;
    aspectratio=imgsiz.width==0||imgsiz.height==0?0:aspectratio;
    if (imgsiz.width>imgsiz.height) {
        ImgWd=contSize.width;
        imgHt=ImgWd/aspectratio;
        if (imgHt>contSize.height) {
            imgHt=contSize.height;
            ImgWd=imgHt*aspectratio;
        }
        
        imgx=0;
        imgy=(contSize.height-imgHt)/2;
        
    }
    else {
        imgHt=contSize.height;
        ImgWd=imgHt*aspectratio;
        if (ImgWd>contSize.width) {
            ImgWd=contSize.width;
            imgHt=ImgWd/aspectratio;
        }
        
        imgy=0;
        imgx=(contSize.width-ImgWd)/2;
        
    }
    
    ImgWd=ImgWd==0?[[UIScreen mainScreen]bounds].size.width:ImgWd;
    imgHt=imgHt==0?[[UIScreen mainScreen]bounds].size.height:imgHt;
    CGRect tst= CGRectMake(imgx,imgy,ImgWd,imgHt);
    
    photoImgv.frame=tst;
   
    
    float widd,htt;
    
    if(contSize.width<[[UIScreen mainScreen]bounds].size.width)
        widd=[[UIScreen mainScreen]bounds].size.width/2;
    else
        widd=contSize.width/2;
    
    
    if(contSize.height<[[UIScreen mainScreen]bounds].size.height)
        htt=[[UIScreen mainScreen]bounds].size.height/2;
    else
        htt=contSize.height/2;
    
   photoImgv.center=CGPointMake(widd, htt);
    
  
}
- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = photoScroller.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, photoScroller.minimumZoomScale);
    [photoScroller setZoomScale:newZoomScale animated:YES];
}

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer
{
  
    
    CGPoint touchPoint = [recognizer locationInView: photoScroller];
    if( _isZoomed )
    {
       
        _isZoomed = NO;
        [photoScroller setZoomScale:photoScroller.minimumZoomScale animated:YES];
    }
    else {
       
        
  if(touchPoint.x>photoImgv.frame.origin.x&&touchPoint.x<(photoImgv.frame.size.width+photoImgv.frame.origin.x)&&touchPoint.y>photoImgv.frame.origin.y&&touchPoint.y<(photoImgv.frame.origin.y+photoImgv.frame.size.height) )
  { _isZoomed = YES;
    
      [photoScroller setZoomScale:photoScroller.maximumZoomScale animated:YES];
 
        CGPoint pointInView =[recognizer locationInView: photoImgv];
        
        // 2
        CGFloat newZoomScale = photoScroller.zoomScale *photoScroller.maximumZoomScale;
        newZoomScale = MIN(newZoomScale, photoScroller.maximumZoomScale);
        
        // 3
        CGSize scrollViewSize = photoScroller.bounds.size;
        
        CGFloat w = scrollViewSize.width / newZoomScale;
        CGFloat h = scrollViewSize.height / newZoomScale;
        CGFloat x = pointInView.x - (w / 2.0f);
        CGFloat y = pointInView.y - (h / 2.0f);
        
        CGRect rectToZoomTo = CGRectMake(x, y, w, h);
       // photoScroller.contentOffset=CGPointMake(x+w/2, y+h/2);
        // 4
       [photoScroller zoomToRect:rectToZoomTo animated:NO];
    
  }}
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    //   [self layoutSubviews];
    if (touch.tapCount == 2) {
        
        if( _isZoomed )
        {
            _isZoomed = NO;
            [photoScroller setZoomScale:photoScroller.minimumZoomScale animated:YES];
        }
        else {
            
            _isZoomed = YES;
            
            [photoScroller setZoomScale:photoScroller.maximumZoomScale animated:YES];
            
            
        }
    }
    
    
}
- (void)centerScrollViewContents {
    CGSize boundsSize = photoScroller.bounds.size;
    CGRect contentsFrame = photoImgv.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    photoImgv.frame = contentsFrame;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so you need to re-center the contents
    [self centerScrollViewContents];
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{

    NSLog(@"ZOOOOM");
    
   
    
    [self setImageRect:photoImgv.image containersize:photoScroller.contentSize];
    return photoImgv;
    
    
    BOOL sizeChanging = !CGSizeEqualToSize(photoImgv.frame.size, photoScroller.frame.size);
    
    if (sizeChanging) {
        [self prepareToResize];
    }
    
    
    
    if (sizeChanging) {
        [self recoverFromResizing];
    }
}

- (void)displayImage:(UIImage *)image
{
   
    [self configureForImageSize:image.size];
    
    
}

- (void)configureForImageSize:(CGSize)imageSize
{
    _imageSize = imageSize;
    [self setMaxMinZoomScalesForCurrentBounds];
    
//    photoScroller.zoomScale = photoScroller.minimumZoomScale;
}

- (void)setMaxMinZoomScalesForCurrentBounds
{
    CGSize boundsSize = photoScroller.bounds.size;
    
    
    
    
    
    CGFloat xScale = boundsSize.width  / _imageSize.width;
    CGFloat yScale = boundsSize.height / _imageSize.height;
    
    BOOL imagePortrait = _imageSize.height > _imageSize.width;
    BOOL phonePortrait = boundsSize.height > boundsSize.width;
    CGFloat minScale = imagePortrait == phonePortrait ? xScale : MIN(xScale, yScale);
    
    CGFloat maxScale = 3.0 / [[UIScreen mainScreen] scale];
    
    if (minScale > maxScale) {
        minScale = maxScale;
    }
    
    photoScroller.maximumZoomScale =3;// maxScale;
    photoScroller.minimumZoomScale = 1;
}
-(BOOL)reachable_gallery {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

-(BOOL)offlineAvailable
{
  if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
  {
      return NO;
  }
 else
    return YES;
}

#pragma mark - Rotation support
-(void)setUpPhoto
{
    _isZoomed=NO;
    
    
    
    CGRect frm;
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    NSLog(@"%f",[[UIScreen mainScreen]bounds].size.height);
    
    float phoneHeight=[[UIScreen mainScreen]bounds].size.height;
    float phoneWidth=[[UIScreen mainScreen]bounds].size.width;
    switch (orientation) {
        case 1:
        case 2:
            if (phoneHeight>=568) {
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 568);
            }
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 480);
            
            // your code for portrait...
            break;
            
        case 3:
        case 4:
            
            if (phoneWidth>=568)
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,568, 320);
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,480, 320);
            // your code for landscape...
            break;
        default:
            NSLog(@"other");
            // your code for face down or face up...
            break;
    }
    
     self.view.frame=frm;
    photoScroller.contentSize=CGSizeMake(frm.size.width, frm.size.height);
    photoScroller.frame=frm;
    photoImgv.frame=frm;
    [self configureForImageSize:photoImgv.image.size];
    [self recoverFromResizing];
    [self setImageRect:photoImgv.image containersize:photoScroller.contentSize];
    [self centerScrollViewContents];
   
    
    [photoScroller layoutSubviews];
    [photoImgv layoutSubviews];
  //  photoImgv.contentMode=UIViewContentModeScaleAspectFit;
    /*
    [self setImageRect:photoImgv.image containersize:photoScroller.contentSize];
    [self centerScrollViewContents];
    [self configureForImageSize:photoImgv.image.size];
    [self recoverFromResizing];
    */
    
    
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
// photoImgv.contentMode=uiconts
    /*
    [self recoverFromResizing];
   
   
    photoImgv.center=CGPointMake([[UIScreen mainScreen]bounds].size.height/2,[[UIScreen mainScreen]bounds].size.width/2);
    */

}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
   
    CGRect frm;
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    NSLog(@"%f",[[UIScreen mainScreen]bounds].size.height);
    
    float phoneHeight=[[UIScreen mainScreen]bounds].size.height;
    float phoneWidth=[[UIScreen mainScreen]bounds].size.width;
    switch (orientation) {
        case 1:
        case 2:
            if (phoneHeight>=568) {
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 568);
            }
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,320, 480);
            
            // your code for portrait...
            break;
            
        case 3:
        case 4:
            
            if (phoneWidth>=568)
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,568, 320);
            else
                frm=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y,480, 320);
            // your code for landscape...
            break;
        default:
            NSLog(@"other");
            // your code for face down or face up...
            break;
    }
    
    
   if ( [(NSString*)[UIDevice currentDevice].model isEqualToString:@"iPad"] )
    {
        // The device is an iPad running iOS 3.2 or later.
        [self setUpPhoto];
    }
    else
    {
        // The device is an iPhone or iPod touch.
         self.view.superview.frame=frm;
        [self setUpPhoto];
    }
    
    
    
}
- (void)prepareToResize
{
    CGPoint boundsCenter = CGPointMake(CGRectGetMidX(photoScroller.bounds), CGRectGetMidY(photoScroller.bounds));
    _pointToCenterAfterResize = [photoScroller convertPoint:boundsCenter toView:photoImgv];
    
    _scaleToRestoreAfterResize = photoScroller.zoomScale;
    
    if (_scaleToRestoreAfterResize <= photoScroller.minimumZoomScale + FLT_EPSILON)
        _scaleToRestoreAfterResize = 0;
}

- (void)recoverFromResizing
{
    [self setMaxMinZoomScalesForCurrentBounds];
    
    CGFloat maxZoomScale = MAX(photoScroller.minimumZoomScale, _scaleToRestoreAfterResize);
    photoScroller.zoomScale = MIN(photoScroller.maximumZoomScale, maxZoomScale);
    
    CGPoint boundsCenter = [photoScroller convertPoint:_pointToCenterAfterResize fromView:photoImgv];
    
    CGPoint offset = CGPointMake(boundsCenter.x - photoScroller.bounds.size.width / 2.0,
                                 boundsCenter.y - photoScroller.bounds.size.height / 2.0);
    
    CGPoint maxOffset = [self maximumContentOffset];
    CGPoint minOffset = [self minimumContentOffset];
    
    CGFloat realMaxOffset = MIN(maxOffset.x, offset.x);
    offset.x = MAX(minOffset.x, realMaxOffset);
    
    realMaxOffset = MIN(maxOffset.y, offset.y);
    offset.y = MAX(minOffset.y, realMaxOffset);
    
    photoScroller.contentOffset = offset;
}

- (CGPoint)maximumContentOffset
{
    CGSize contentSize = photoScroller.contentSize;
    CGSize boundsSize = photoScroller.bounds.size;
    return CGPointMake(contentSize.width - boundsSize.width, contentSize.height - boundsSize.height);
}

- (CGPoint)minimumContentOffset
{
    return CGPointZero;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAllButUpsideDown);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


@end
