//
//  PDBuyViewController.m
//  Visiting Ashland
//
//  Created by Anu on 18/11/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "PDBuyViewController.h"
#import "NSString+MD5.h"
#import "PDAppDelegate.h"
#import "Inapp.h"
#import "MKStoreKit.h"
#import "GMDCircleLoader.h"
#import "Reachability.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface PDBuyViewController ()

@end

@implementation PDBuyViewController
-(IBAction)onBack:(id)sender
{
//    PDViewController *viewController = [[PDViewController alloc] init];
//    [self.navigationController pushViewController:viewController animated:NO];
    
    [self.tabBarController setSelectedIndex:0];
    
     [self.tabBarController.tabBar setHidden:NO];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.tabBarController.delegate = self;
    
     [self.tabBarController.tabBar setHidden:YES];

    // Do any additional setup after loading the view from its nib.
    
    if (IS_IPHONE5) {
        [[NSBundle mainBundle] loadNibNamed:@"PDBuyViewController" owner:self options:nil];
       
    }
    else {
        [[NSBundle mainBundle] loadNibNamed:@"PDBuyViewController4" owner:self options:nil];
     
    }
    
    [self setupInapView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification123"
                                               object:nil];

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"TestNotification123"])
        NSLog (@"Successfully received the test notification!");
    
    [self loaderStop];
}

#pragma mark - PurchaseAction Loader
UIView *overlaay;
-(void)loaderStartWithText :(NSString *)loaderText
{
    
    
    overlaay =[[UIView alloc]initWithFrame:self.view.window.rootViewController.view.bounds];
    overlaay.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    overlaay.tag = 1001;
    [overlaay setUserInteractionEnabled:YES];
    [GMDCircleLoader setOnView:overlaay withTitle:loaderText animated:YES];
    [self.view.window.rootViewController.view addSubview:overlaay];
    // [NSTimer scheduledTimerWithTimeInterval: 5.0 target: self  selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
}

/*-(void) callAfterSixtySecond:(NSTimer*) t
 {
 NSLog(@"red");
 
 [GMDCircleLoader hideFromView:overlaay animated:YES];
 [overlaay removeFromSuperview];
 }
 */

-(void)loaderStop
{
    
    
    [GMDCircleLoader hideFromView:overlaay animated:YES];
    [overlaay removeFromSuperview];
}

#pragma mark -Inapp- Purchase Notification Methods
- (void) inappSuccessNotification:(NSNotification *) notification
{
    
   [self loaderStop];
    NSLog (@"Successfully received the test notification %@ ",notification);
    
    
    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    dictionary=[inappStr objectAtIndex:0];
   
    
    
    
    
    
    
    
    
    NSString *notificationInap=[NSString stringWithFormat:@"%@",notification.object];
    
    if([notificationInap isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
    {
        NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else if([notificationInap isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
    {
        NSLog (@"unlimitedcategry %@ ",notification);
        
        NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self setupInapView];
    }
    
    
}
- (void) inappFailureNotification:(NSNotification *) notification
{
   [self loaderStop];
    NSLog (@"Successfully received the test notification %@ ",notification.object);
}
- (void) inappReStoreSuccessNotification:(NSNotification *) obj
{
    NSArray *inappids=obj.object;
   [self loaderStop];
    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[inappStr objectAtIndex:0];
    NSString *currentAppid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    

    
    
    
    for (int i=0;i<[inappids count];i++)
    {
        NSString *pid=[inappids objectAtIndex:i];
        if([pid isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
        {
            NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
            [[NSUserDefaults standardUserDefaults]synchronize];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Restore Purchase" message:@"Remove Ads restored successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [self setupInapView];
        }
        else if([pid isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
        {
            NSLog (@"unlimitedcategry %@ ",inappids);
            
            NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self setupInapView];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Restore Purchase" message:@"Full access restored successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
    }
    
    
    NSLog (@"Successfully received the test notification %@ ",inappids);
    
   
    //temporary code
    
    
    
  
    
    
    
    
    
}
- (void) inappReStoreFailureNotification:(NSNotification *) notification
{
    
    [self loaderStop];
    NSLog (@"Successfully received the test notification %@ ",notification);
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Restore Purchase" message:@"Purchase restoring failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{

[self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBarHidden = YES;

}

-(void)setupInapView
{
    
    [self.tabBarController.tabBar setHidden:YES];
   
    NSInteger myPoints=[[NSUserDefaults standardUserDefaults]integerForKey:@"myPoints"];
    
    
    
    
  
    [viewInAlert removeFromSuperview];
    
    viewInAlert = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 350)];
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSDictionary *dictionary=[inappStr objectAtIndex:0];
    
    
    
    
    
    int btnXpos=5;
    int btnYpos=5;
    int btnWidth  =300-10;
    int btnHeight =60;
    int btnYpospadding=2;
    
    /* if(myPoints>=points)
    {
        UIView *pointView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, btnYpos, btnWidth, 70)];
        
        pointView.backgroundColor=[UIColor whiteColor];
        [viewInAlert addSubview:pointView];
        
        
        UILabel *pointLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, btnWidth, 40)];
        pointLbl.backgroundColor=[UIColor clearColor];
        pointLbl.font=[UIFont systemFontOfSize:45];
        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
        pointLbl.textAlignment=NSTextAlignmentCenter;
        pointLbl.textColor=[UIColor blueColor];
        pointLbl.text=[NSString stringWithFormat:@"%ld",(long)myPoints];
        [pointView addSubview:pointLbl];
        
        UILabel *pointLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, btnWidth, 30)];
        pointLbl2.backgroundColor=[UIColor clearColor];
        
        pointLbl2.font=[UIFont systemFontOfSize:15];
        pointLbl2.textAlignment=NSTextAlignmentCenter;
        pointLbl2.textColor=[UIColor blueColor];
        pointLbl2.text=@"Points Available";
        [pointView addSubview:pointLbl2];
        
        btnYpos=btnYpos+70+btnYpospadding;
    }
    */
    
    NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
    
    
    
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    
    if([[dictionary objectForKey:@"remove_ads"] isEqualToString:@"yes"]&&![removAdStatus isEqualToString:@"purchased"])
    {
        
        
        UIButton *removeAdBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        
       // removeAdBtn.backgroundColor=[UIColor colorWithRed:255/255.0 green:0 blue:0 alpha:1];
        
        removeAdBtn.backgroundColor=[UIColor colorWithRed:200/255.0 green:215/255.0 blue:117/255.0 alpha:1];
        [removeAdBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSString *msgStr=[NSString stringWithFormat:@"Remove Ads                              $.99"];
        [removeAdBtn setTitle:msgStr forState:UIControlStateNormal];
        removeAdBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        [viewInAlert addSubview:removeAdBtn];
        removeAdBtn.tag=0;
        removeAdBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        
        [removeAdBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
        removeAdBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        removeAdBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        removeAdBtn.layer.cornerRadius=5.0;
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
        
    }
    
    if([[dictionary objectForKey:@"one_category"] isEqualToString:@"yes"])
    {
        
        
        UIButton *oneCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
       // oneCatBtn.backgroundColor=[UIColor colorWithRed:204/255.0 green:102/255.0 blue:102/255.0 alpha:1];
        
        oneCatBtn.backgroundColor=UIColorFromRGB(0xededed);
        [oneCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *price=[NSString stringWithFormat:@"One Point                                   $%@",[dictionary objectForKey:@"one_category_price"]];
        [oneCatBtn setTitle:price forState:UIControlStateNormal];
        oneCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        oneCatBtn.tag=1;
        
        oneCatBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        
        [oneCatBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
        oneCatBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        oneCatBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        oneCatBtn.layer.cornerRadius=5.0;
        
        [viewInAlert addSubview:oneCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
    }
    if([[dictionary objectForKey:@"three_category"] isEqualToString:@"yes"])
    {
        
        UIButton * threeCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        //threeCatBtn.backgroundColor=[UIColor colorWithRed:255/255.0 green:204/255.0 blue:51/155.0 alpha:1];
        
        threeCatBtn.backgroundColor=UIColorFromRGB(0xededed);
        [threeCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSString *price=[NSString stringWithFormat:@"Three Points                              $%@",[dictionary objectForKey:@"three_category_price"]];
        [threeCatBtn setTitle:price forState:UIControlStateNormal];
        threeCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        threeCatBtn.tag=3;
        threeCatBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        
        [threeCatBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
        threeCatBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        threeCatBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        threeCatBtn.layer.cornerRadius=5.0;
        
        [viewInAlert addSubview:threeCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
    }
    
    
    if([[dictionary objectForKey:@"five_category"] isEqualToString:@"yes"])
    {
        
        
        UIButton * fiveCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        //fiveCatBtn.backgroundColor=[UIColor colorWithRed:102/255.0 green:153/255.0 blue:204/255.0 alpha:1];
        
        fiveCatBtn.backgroundColor=UIColorFromRGB(0xededed);
        [fiveCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSString *price=[NSString stringWithFormat:@"Five Points                                $%@",[dictionary objectForKey:@"five_category_price"]];
        [fiveCatBtn setTitle:price forState:UIControlStateNormal];
        fiveCatBtn.frame=CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight);
        fiveCatBtn.tag=5;
        fiveCatBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        
        [fiveCatBtn setTitleColor:UIColorFromRGB(0x535353) forState:UIControlStateNormal];
        fiveCatBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        fiveCatBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        fiveCatBtn.layer.cornerRadius=5.0;
        
        [viewInAlert addSubview:fiveCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
        
    }
    
    if(![unlimitedPurchase isEqualToString:@"purchased"])
    if([[dictionary objectForKey:@"unlimited_category"] isEqualToString:@"yes"])
    {
        btnHeight=btnHeight+20;
        
        UIView *fullAccessView=[[UIView alloc]initWithFrame:CGRectMake(btnXpos, btnYpos, btnWidth, btnHeight)];
        //fullAccessView.backgroundColor=[UIColor colorWithRed:251/255.0 green:175/255.0 blue:93/255.0 alpha:1];
        fullAccessView.backgroundColor=[UIColor colorWithRed:200/255.0 green:215/255.0 blue:117/255.0 alpha:1];
        fullAccessView.layer.cornerRadius=5.0;
        [viewInAlert addSubview:fullAccessView];
        
        UILabel *fullAccessLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 15, btnWidth, 25)];
        fullAccessLbl.backgroundColor=[UIColor clearColor];
        fullAccessLbl.font=[UIFont systemFontOfSize:15];
        //pointLbl.font=[UIFont fontWithName:@"Ariel" size:40];
        fullAccessLbl.textAlignment=NSTextAlignmentCenter;
        fullAccessLbl.textColor=UIColorFromRGB(0xededed);
        NSString *price=[NSString stringWithFormat:@"   Full Access                            $%@",[dictionary objectForKey:@"unlimited_category_price"]];
        fullAccessLbl.text= price;
        fullAccessLbl.textColor=UIColorFromRGB(0x535353);
        fullAccessLbl.textAlignment=NSTextAlignmentLeft;
        [fullAccessView addSubview:fullAccessLbl];
        
        UILabel *fullAccessLbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 25+15, btnWidth, 20)];
        fullAccessLbl2.backgroundColor=[UIColor clearColor];
        
        fullAccessLbl2.font=[UIFont systemFontOfSize:13];
        fullAccessLbl2.textAlignment=NSTextAlignmentLeft;
        fullAccessLbl2.textColor=UIColorFromRGB(0x535353);
        fullAccessLbl2.text=[NSString stringWithFormat:@"   %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppName"]];
        [fullAccessView addSubview:fullAccessLbl2];
        
       
        
        
        UIButton * unlimitedCatBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        unlimitedCatBtn.backgroundColor=[UIColor clearColor];
        [unlimitedCatBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        //[unlimitedCatBtn setTitle:@"Full Access - $9.99" forState:UIControlStateNormal];
        unlimitedCatBtn.frame=CGRectMake(0, 0, btnWidth, btnHeight);
        unlimitedCatBtn.tag=10;
        [fullAccessView addSubview:unlimitedCatBtn];
        
        btnYpos=btnYpos+btnHeight+btnYpospadding;
        
         [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
        
    }
    
    

        
        
        UIButton * restoreBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        //restoreBtn.backgroundColor=[UIColor colorWithRed:102/255.0 green:132/255.0 blue:113/255.0 alpha:1];
    
        restoreBtn.backgroundColor=UIColorFromRGB(0x28bbf2);
        [restoreBtn addTarget:self action:@selector(PurchaseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
      //  NSString *price=[NSString stringWithFormat:@"Five Points - $%@",[dictionary objectForKey:@"five_category_price"]];
        [restoreBtn setTitle:@"Restore" forState:UIControlStateNormal];
        restoreBtn.frame=CGRectMake(btnXpos+75, btnYpos+30, btnWidth-150, 40);
        restoreBtn.tag=12;
        restoreBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        
    restoreBtn.layer.cornerRadius=5.0;
    
        [viewInAlert addSubview:restoreBtn];
    
      btnYpos=btnYpos+40+btnYpospadding+10;
        
    
        
        
    
    
    // if(myPoints>=points)
    
    
    
    
    viewInAlert.frame=CGRectMake((self.view.frame.size.width/2)-150, 75, 300, btnYpos);
    
    
    
    
    [self.view addSubview:viewInAlert];
    
    
    
    
    
   
    
    
    
    // [alert show];
    
    
}





-(void)PurchaseBtnAction:(id)sender
{
    NSInteger tag=  ((UIButton*)sender).tag;
    
    
    
    if(tag==0)
    {
        //RemoveAds
        if ([self reachable])
        {
            [self loaderStartWithText:@"Purchasing"];
        
       
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappSuccessNotification:)
                                                     name:@"inappSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappFailureNotification:)
                                                     name:@"inappFailureNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreSuccessNotification:)
                                                     name:@"inappReStoreSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreFailureNotification:)
                                                     name:@"inappReStoreFailureNotif"
                                                   object:nil];
        
        
        
        
        
        
        NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        dictionary=[inappStr objectAtIndex:0];
        
        
        
        
        
        
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];

        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if(tag==1)
    {
        //One category Point purchase
        
        if ([self reachable])
        {
            [self loaderStartWithText:@"Purchasing"];

        NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( unlockables==nil){
            unlockables=[NSNumber numberWithInt:0];
        }
        
            NSString *OnePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"OnePoint"];
            
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:OnePoint];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    else if(tag==3)
    {
        if ([self reachable])
        {
            [self loaderStartWithText:@"Purchasing"];
        NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( unlockables==nil){
            unlockables=[NSNumber numberWithInt:0];
        }
        
            
            NSString *ThreePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"ThreePoint"];
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:ThreePoint];
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }

        
        
    }
    else if(tag==5)
    {
        //Five point purchase
        
        if ([self reachable])
        {
            [self loaderStartWithText:@"Purchasing"];
        NSNumber* unlockables=[[MKStoreKit sharedKit]availableCreditsForConsumable:@"UnlockPoints"];
        if( unlockables==nil){
            unlockables=[NSNumber numberWithInt:0];
        }
        
            NSString *FivePoint = [[[MKStoreKit configs]objectForKey:@"ConsumableInappIds"]objectForKey:@"FivePoint"];
            [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:FivePoint];

        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if(tag==10)
    {
        //Unlimited category purchase
        
        if ([self reachable])
        {
            [self loaderStartWithText:@"Purchasing"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappSuccessNotification:)
                                                     name:@"inappSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappFailureNotification:)
                                                     name:@"inappFailureNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreSuccessNotification:)
                                                     name:@"inappReStoreSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreFailureNotification:)
                                                     name:@"inappReStoreFailureNotif"
                                                   object:nil];
        
        NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        dictionary=[inappStr objectAtIndex:0];
        
        
        
        
        
        
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(unlimitedCategoryPurchaseSuccess:)
                                                     name:@"unlimitedCategoryPurchaseNotif"
                                                   object:nil];
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
        
        }
        else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Could not connect to server"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }

        
        
        
        
    }
    else if(tag==12)
    {
        
        NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
        NSMutableDictionary *dictionary=[inappStr objectAtIndex:0];
        
        NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        NSString *unlimitedPurchase=[[NSUserDefaults standardUserDefaults]objectForKey:unlimicatKey];
        NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:@"remove_ads"];
        
        BOOL restoreflag=false;
        
        if([[dictionary objectForKey:@"remove_ads"] isEqualToString:@"yes"]&&![removAdStatus isEqualToString:@"purchased"])
        {
            restoreflag=true;
        }
        
        if(![unlimitedPurchase isEqualToString:@"purchased"])
        if([[dictionary objectForKey:@"unlimited_category"] isEqualToString:@"yes"])
            {
                restoreflag=true;
            }
        
        
        if(restoreflag)
        {
       
      
       
        
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappSuccessNotification:)
                                                     name:@"inappSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappFailureNotification:)
                                                     name:@"inappFailureNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreSuccessNotification:)
                                                     name:@"inappReStoreSuccessNotif"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(inappReStoreFailureNotification:)
                                                     name:@"inappReStoreFailureNotif"
                                                   object:nil];

            if ([self reachable])
            {
                [self loaderStartWithText:@"Purchasing"];
                [[MKStoreKit sharedKit] restorePurchases];
            }
            else
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Could not connect to server"
                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        
        
       
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Restore Purchase" message:@"No items to restore" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
      
        
    }
    
}



-(BOOL)checkCategoryPurchased:(NSString*)catid
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Inapp" inManagedObjectContext:readContext];
    [fetchRequest setEntity:entity];
    
    NSString *currentAppid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@ AND categoryid=%@",currentAppid,catid];
    NSLog(@"appid=%@, catid =%@",currentAppid,catid);
    
    [fetchRequest setPredicate:predicate];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
    
    NSArray *fetchedObjects = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    [fetchRequest setEntity:entity];
    
    
    NSLog( @"error %@",error);
    
    
    NSString *purchaseStatus;
    
    
    //NSDate *lastSyncDate;
    @try {
        Inapp *appsObj=[fetchedObjects objectAtIndex:0];
        
        purchaseStatus=appsObj.paidStatus;
        
        
        
        
    }
    @catch (NSException * e) {
        
        purchaseStatus=@"No";
        
    }
    
    
    if([purchaseStatus isEqualToString:@"Yes"])
        return true;
    else
        return false;
    
    
}
-(BOOL)coreDataWriteInappPurchased:(NSString*)catid
{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    
    @try {
        
        NSError *error;
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *Que = [NSEntityDescription
                                insertNewObjectForEntityForName:@"Inapp"
                                inManagedObjectContext:context];
        
        NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
        
        [Que setValue:appid forKey:@"appid"];
        
        [Que setValue:catid forKey:@"categoryid"];
        
        [Que setValue:@"Yes" forKey:@"paidStatus"];
        
        if (![context save:&error]) {
            return false;
        }
        else
            return true;
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        return false;
        
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
