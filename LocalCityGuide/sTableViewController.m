//
//  sTableViewController.m
//  Visiting Ashland
//
//  Created by swaroop on 23/06/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import "sTableViewController.h"
#import "SettingsInnerViewController.h"
#import "AsyncImageView.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "JMImageCache.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "PDInappHomeViewController.h"
#import "PDGalleryViewController.h"
#import "Reachability.h"
#import "Constants.h"
#import "SettingsViewController.h"
#import "PDDetailViewController.h"
#import "PDViewController.h"
#import "PDFavoriteViewController.h"


@interface sTableViewController ()

@end

@implementation sTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        [[NSBundle mainBundle] loadNibNamed:@"sTableViewController4" owner:self options:nil];
    }
    else if(IS_IPHONE_4_OR_LESS)
    {
        [[NSBundle mainBundle] loadNibNamed:@"sTableViewController4" owner:self options:nil];
    }
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView delegate and datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
        return 60;
    
   
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   
    
        return 1;
    
    
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    if(section==0)
        
    {
        
            //rgb(241, 241, 241)
            UILabel *lblBelowBanner=[[UILabel alloc]initWithFrame:CGRectMake(0,0, tableView.bounds.size.width, 30)];
            lblBelowBanner.backgroundColor= Rgb2UIColor(241, 241, 241);
            lblBelowBanner.text=@"  Guide Type";
            lblBelowBanner.textAlignment=NSTextAlignmentLeft;
            lblBelowBanner.textColor=[UIColor darkGrayColor];
            lblBelowBanner.font=[UIFont systemFontOfSize:12];
            return lblBelowBanner;
            
        
       
        
        
    }
    
    else{
        return nil;
        
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return 30;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"celll"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    
    
    NSDictionary *dict;
    
        
        
        UIImageView *cellimage = [[UIImageView alloc]init];
        cellimage.frame=CGRectMake(5, 5, 50, 50);
        // cellimage.image =[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dict objectForKey:@"image"]]]];
        [cellimage setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholder:nil];
        [cell addSubview:cellimage];
        
        UILabel *titlelabelh = [[UILabel alloc] init];
        titlelabelh.frame = CGRectMake(cellimage.frame.origin.x+60, 10, 250 , 55);
        titlelabelh.numberOfLines = 0;
        [titlelabelh setFont:[UIFont systemFontOfSize:16]];
        titlelabelh.textColor = [UIColor darkGrayColor];
        titlelabelh.backgroundColor = [UIColor clearColor];
        titlelabelh.text =[dict objectForKey:@"app_name"];
        [titlelabelh sizeToFit];
        
        float i = titlelabelh.intrinsicContentSize.width;
        NSLog(@"titlelabelh width :%f",i);
        /* if(i>150)
         {
         CGRect widthTrunc;
         widthTrunc=CGRectZero;
         widthTrunc=titlelabelh.frame;
         widthTrunc.size.width=150;
         titlelabelh.frame=widthTrunc;
         }*/
        [cell addSubview:titlelabelh];
        
        UILabel *sizeLbl = [[UILabel alloc] init];
        sizeLbl.frame = CGRectMake(titlelabelh.frame.origin.x+5, titlelabelh.frame.origin.y+titlelabelh.frame.size.height+2, 60 , 15);
        sizeLbl.numberOfLines = 0;
        [sizeLbl setFont:[UIFont systemFontOfSize:12]];
        sizeLbl.textColor = [UIColor darkGrayColor];
        sizeLbl.backgroundColor = [UIColor clearColor];
      //  sizeLbl.text = [valueForKey:@"FolderSize"];//[muttarrSize objectAtIndex:indexPath.row];//reversedArray
        [sizeLbl sizeToFit];
        [cell addSubview:sizeLbl];
        
        UIButton *btnDel = [[UIButton alloc]init];
      //  btnDel.frame=CGRectMake(tblStorage.frame.size.width-70, titlelabelh.frame.origin.y+5, 60, 25);//titlelabelh.frame.origin.x+titlelabelh.frame.size.width+20
        [btnDel setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDel.titleLabel.font= [UIFont systemFontOfSize:12];
        [btnDel setBackgroundColor:[UIColor colorWithRed:(194/255.0) green:(0/255.0) blue:(10/255.0) alpha:1.0]];
        btnDel.layer.cornerRadius=5.0;
        [btnDel addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        btnDel.tag = indexPath.row;
        
        [cell addSubview:btnDel];
        
        return cell;
    
    
   
}

- (IBAction)onBackk:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];

}

@end
