//
//  SettingsViewController.m
//  Visiting Ashland
//
//  Created by Anu on 28/04/16.
//  Copyright © 2016 iDeveloper. All rights reserved.
//

#import "SettingsViewController.h"
#import "CustomIOSAlertView.h"
#import "DBOperations.h"
#import <QuartzCore/QuartzCore.h>
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "PDAppDelegate.h"
#import "SettingsInnerViewController.h"
#import "UIImageView+JMImageCache.h"
#import "UIImageView+AFNetworking.h"
#import "sTableViewController.h"

#import "ACPDownloadView.h"
#import "ACPStaticImagesAlternative.h"
#import "ACPIndeterminateGoogleLayer.h"
#import "SSZipArchive.h"
#import "PDLoader.h"
#import "Constants.h"


@interface SettingsViewController ()<CustomIOSAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>
{
    CustomIOSAlertView *customAlert;
    UIRefreshControl *refreshControl;
    
    NSString *cachePath;
     NSString *cachePath1;
     NSString *cachePath2;
     NSString *cachePath3;
     NSString *cachePath4;
     NSString *cachePath5;
     NSString *cachePath6;
    
    UIView *loaderFI;
    
    UITableView *tblStorage;
    NSMutableArray *muttarr;
    NSMutableArray *muttarrSize;
    
    NSMutableArray* reversedArray;
    
    NSMutableArray *arrayForDetails;
    
    NSMutableArray* myMutableArrayAgain;
    
    UIView *viewInAlert;
    UIButton *btnBackStorage;
    UIButton *reloadBtn;
    
    UICircle* m_testView;
}
@property (weak, nonatomic) IBOutlet ACPDownloadView *downloadView;
@property (nonatomic, retain) NSMutableData *dataToDownload;
@property (nonatomic) float downloadSize;
@end

@implementation SettingsViewController
NSArray *arraystatic;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        [[NSBundle mainBundle] loadNibNamed:@"SettingsViewController4" owner:self options:nil];
    }
    else if(IS_IPHONE_4_OR_LESS)
    {
        [[NSBundle mainBundle] loadNibNamed:@"SettingsViewController4" owner:self options:nil];
    }

    
    
  NSString  *distance=[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"];
    if ([distance isEqualToString:@"M"]) {
        distUnitSegment.selectedSegmentIndex=1;
        
    }
    else
        distUnitSegment.selectedSegmentIndex=0;
    
    NSString *nearby= [[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"];
    
    if([nearby isEqualToString:@"on"])
    {
        nearBySwitch.on=YES;
    }
    else
    {
        nearBySwitch.on=NO;
       // [self nearyByValueChanged:nearBySwitch];
    }
    
    NSLog(@"HOME > %@", NSHomeDirectory());
    
    
   // NSFileManager *fileManagerr = [NSFileManager defaultManager];
   

    int f=0;
    if(IS_IPHONE5 ||IS_IPHONE_4_OR_LESS)
    {
        f=80;
    }
    else
    {
        f=50;
    }
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        NSLog(@"IPADD");
        f=20;
        
        
        
//        CGRect framei2=CGRectZero;
//        framei2 = cacheLabel.frame;
//        framei2.origin.y = cacheLabel.frame.origin.y-20;
//        cacheLabel.frame=framei2;
//        
//        
//        CGRect framei=CGRectZero;
//        framei = deleteBtn.frame;
//        framei.origin.y = deleteBtn.frame.origin.y-40;
//        deleteBtn.frame=framei;
        
    }
    
//    CGRect frm=CGRectZero;
//    frm = imgTree.frame;
//    frm.origin.y = deleteBtn.frame.origin.y+deleteBtn.frame.size.height+f;
//    frm.size.height=43;frm.size.width=90;
//    imgTree.frame=frm;
//    
//    CGRect frm2=CGRectZero;
//    frm2 = versionLbl.frame;
//    frm2.origin.y = imgTree.frame.origin.y+imgTree.frame.size.height;
//    versionLbl.frame=frm2;
    
    versionLbl.text=[NSString stringWithFormat:@"v.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    versionLbl.textAlignment=NSTextAlignmentCenter;
    
    
    
     //customise the segmented container
    if(_segmentView.tag==13)
    {
     [_segmentView setDividerImage:[self imageWithColor:[UIColor clearColor]] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
     
     self.segmentView.layer.cornerRadius = 15.0;
     self.segmentView.layer.borderColor = [UIColor colorWithRed:(242/255.0) green:(155/255.0) blue:(20/255.0) alpha:1.0].CGColor; //242, 155, 20  //230 82 34
     self.segmentView.layer.borderWidth = 2.0f;
     self.segmentView.layer.masksToBounds = YES;
     
     UIFont *font = [UIFont boldSystemFontOfSize:14.0f];
     NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
     forKey:NSFontAttributeName];
     [_segmentView setTitleTextAttributes:attributes
     forState:UIControlStateNormal];
     
     [_segmentView setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateNormal];
     
     [_segmentView setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
     
    
     
     float width = 0.0;
     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
     {
     if(IS_IPHONE5)
     {
     NSLog(@"is iphone 5");
     }
     
     width=(([UIScreen mainScreen].bounds.size.width)/4);
     NSLog(@"float width :%f",width);
     
     
     // _segmentView.apportionsSegmentWidthsByContent=YES;
     NSLog(@"segmentView frame :%@",NSStringFromCGRect(_segmentView.frame));
//     [_segmentView setWidth:width forSegmentAtIndex:0];
//     [_segmentView setWidth:width forSegmentAtIndex:1];
//     [_segmentView setWidth:width forSegmentAtIndex:2];
     [_segmentView setWidth:55 forSegmentAtIndex:4];
//     
//     [_segmentView setContentOffset:CGSizeMake(15,0) forSegmentAtIndex:0];
//     [_segmentView setContentOffset:CGSizeMake(10,0) forSegmentAtIndex:1];
//     [_segmentView setContentOffset:CGSizeMake(-15,0) forSegmentAtIndex:2];
//     [_segmentView setContentOffset:CGSizeMake(-25,0) forSegmentAtIndex:3];
     
     
     _segmentView.apportionsSegmentWidthsByContent=YES;
     
   //  [_segmentView setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(230/255.0) green:(82/255.0) blue:(34/255.0) alpha:1.0]} forState:UIControlStateSelected];
     
     // [_segmentView setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
     
     self.segmentView.layer.borderWidth = 1.0f;
     self.segmentView.layer.cornerRadius = 10.0;
     
     NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
     [UIFont boldSystemFontOfSize:10], NSFontAttributeName,
     [UIColor whiteColor], NSForegroundColorAttributeName, nil];
     
     //[_segmentView setTitleTextAttributes:attributes forState:UIControlStateNormal];
     }
    
    }
    
    NSInteger x=0;
    x =  [[NSUserDefaults standardUserDefaults]integerForKey:@"numberofAppsToShow"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSObject * object = [prefs objectForKey:@"numberofAppsToShow"];
    
    
    
        if(object != nil){
            //object is there
            
            NSString * trimed = [[NSString stringWithFormat:@"%ld",(long)x] substringToIndex:1];
            
            int value = [trimed intValue]-1;
            
           [_segmentView setSelectedSegmentIndex:value];
        }

    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"];
    if([str isEqualToString:@"K"])
    {
    lblSlider.text=@" 0.5km     1km     2km     3km     4km     5km    6km     7km     8km     9km     10km";
    }
    else
    {
    lblSlider.text=@" 0.5mi      1mi      2mi      3mi      4mi      5mi     6mi      7mi      8mi      9mi      10mi";
    }
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
   myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
    }
    
    
    
   /////////
    
//    UIButton *capBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 250, 25, 25)];
//    [capBtn addTarget:self
//                       action:@selector(sizeCapDeleteAction)
//             forControlEvents:UIControlEventTouchUpInside];
//    capBtn.backgroundColor=[UIColor redColor];
   // [self.view addSubview:capBtn];

}
-(void)viewWillAppear:(BOOL)animated
{
    [loaderFI removeFromSuperview];
    
    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"];
    if([str isEqualToString:@"on"])
    {
      [geoFeedSwitch setOn:NO];
    }
    else
    {
        [geoFeedSwitch setOn:YES];
    }
}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


-(NSString *)sizeOfFile:(NSString *)filePath
{

    NSString *cacheSize;
    
//    NSLog(@"HOME > %@", NSHomeDirectory());
//
//
//    NSFileManager *fileManagerr = [NSFileManager defaultManager];
//    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    // if([fileManagerr fileExistsAtPath:[cachePath stringByAppendingPathComponent:@"com.pidigi.lgdev"]])
//    {
//        
//        
//        NSError *error;
//        
//        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachePath error:&error];
//        for (NSString *strName in dirContents) {
//            
//            //  if([strName isEqualToString:@"com.pidigi.lgdev"])
//            {
//                NSString *shit = [NSByteCountFormatter stringFromByteCount:dirContents countStyle:NSByteCountFormatterCountStyleFile];
//                NSLog(@"shit:%@", shit);
//                
//                //  cachePath = [NSString stringWithFormat:@"%@/com.pidigi.lgdev/fsCachedData",cachePath];
//                
//                cachePath = [NSString stringWithFormat:@"%@/com.hackemist.SDWebImageCache.Splash",cachePath];
//                
//                //com.hackemist.SDWebImageCache.Splash
//                NSLog(@"cachePath :%@",cachePath);
//                
//                
//                
//                cacheSize =  [self sizeOfFolder:cachePath];
//                
//                NSLog(@"cacheSize :%@",cacheSize);
//            }
//            
//        }
//        
//        
//        
//        
//    }
    
    return cacheSize;

    

}
-(NSString *)sizeOfFiless:(NSString *)filePath
{
//    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
//    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] integerValue];
//    NSString *fileSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize countStyle:NSByteCountFormatterCountStyleFile];
    
    
    NSString *fileSizeStr;
   
    NSString *cacheSize;
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *URL = [documentsDirectory stringByAppendingPathComponent:@"XML/Extras/Approval.xml"];
    
    NSLog(@"URL:%@",URL);
    NSError *attributesError = nil;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:URL error:&attributesError];
    
   // int fileSize = [fileAttributes fileSize];
    
//    NSLog(@"fileSize : %d",fileSize);
//    
//    NSInteger sizeofarray = [[NSUserDefaults standardUserDefaults]objectForKey:@"arraysize"];
//    
//    NSLog(@"sizeofarray :%ld",(long)sizeofarray);
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    
    for (NSString *filename in fileArray)
    {
        
        
        fileSizeStr = [NSByteCountFormatter stringFromByteCount:filename countStyle:NSByteCountFormatterCountStyleFile];

        
    }

    
    NSInteger si;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *bundleURL = [[NSBundle mainBundle] bundleURL];
    NSArray *contents = [fileManager contentsOfDirectoryAtURL:bundleURL
                                   includingPropertiesForKeys:@[]
                                                      options:NSDirectoryEnumerationSkipsHiddenFiles
                                                        error:nil];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pathExtension == 'png'"];
    for (NSURL *fileURL in [contents filteredArrayUsingPredicate:predicate]) {
        
         si = [NSByteCountFormatter stringFromByteCount:fileURL countStyle:NSByteCountFormatterCountStyleFile];
    }
    
    NSLog(@"si :%ld",(long)si);
    
    
    
    
    
    //get full pathname of bundle directory
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    
    //get paths of all of the contained subdirectories
    NSArray *bundleArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:bundlePath error:nil];
    
    //to access each object in array
    NSEnumerator *filesEnumerator = [bundleArray objectEnumerator];
    
    NSString *fileName;
    
    unsigned long long int fileSize = 0;
    NSError *error = nil;
    
    //return next object from enumerator
    while (fileName = [filesEnumerator nextObject])
    {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[bundlePath stringByAppendingPathComponent:fileName] error:&error];
        
        fileSize += [fileDictionary fileSize];
    }
    //converts a byte count value into a textual representation that is formatted with the appropriate byte modifier (KB, MB, GB and so on)
    NSString *folderSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize countStyle:NSByteCountFormatterCountStyleMemory];
    NSLog(@"App size (bundle size): %@ \n\n\n",folderSizeStr);
    
    
     NSInteger big = [NSByteCountFormatter stringFromByteCount:bundleURL countStyle:NSByteCountFormatterCountStyleFile];
    NSLog(@"big :%ld",(long)big);
    
    
    
    
    
    
    
    
    
    NSString *string;
    
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
//        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
        
        string = [NSByteCountFormatter stringFromByteCount:file countStyle:NSByteCountFormatterCountStyleFile];
    }

    NSLog(@"string :%@",string);
    
    
   // NSError* error;
    
    NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:NSTemporaryDirectory() error: &error];
    NSNumber *size = [fileDictionary objectForKey:NSFileSize];
    
    NSLog(@"size :%@",size);
    
    NSInteger a= [fileSizeStr integerValue]-[size integerValue];
    a=a+13;
    NSLog(@"a:%ld",(long)a);
    fileSizeStr=[NSString stringWithFormat:@"%ld Mb",(long)a];
    
    
    NSError *errorr = nil;
    NSDictionary *attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:documentsDirectory error:&errorr];
    if (attribs) {
        NSString *string = [NSByteCountFormatter stringFromByteCount:[attribs fileSize] countStyle:NSByteCountFormatterCountStyleFile];
        NSLog(@"string:%@", string);
    }
    
    
    
    NSString *folderPath;
    
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:mainPath error:&error];
    for (NSString *strName in dirContents) {
        
        NSString *shit = [NSByteCountFormatter stringFromByteCount:dirContents countStyle:NSByteCountFormatterCountStyleFile];
        NSLog(@"shit:%@", shit);

        
    }
    
    
    
//    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    [appDelegate calculate];
    
    NSLog(@"HOME > %@", NSHomeDirectory());
    
    
    NSFileManager *fileManagerr = [NSFileManager defaultManager];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
   // if([fileManagerr fileExistsAtPath:[cachePath stringByAppendingPathComponent:@"com.pidigi.lgdev"]])
    {
        
        
      
        
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachePath error:&error];
        for (NSString *strName in dirContents) {
            
          //  if([strName isEqualToString:@"com.pidigi.lgdev"])
            {
            NSString *shit = [NSByteCountFormatter stringFromByteCount:dirContents countStyle:NSByteCountFormatterCountStyleFile];
            NSLog(@"shit:%@", shit);
                
              //  cachePath = [NSString stringWithFormat:@"%@/com.pidigi.lgdev/fsCachedData",cachePath];
                
                cachePath = [NSString stringWithFormat:@"%@/com.hackemist.SDWebImageCache.Splash",cachePath];
                
                //com.hackemist.SDWebImageCache.Splash
                NSLog(@"cachePath :%@",cachePath);
                

                
            cacheSize =  [self sizeOfFolder:cachePath];
            
                NSLog(@"cacheSize :%@",cacheSize);
            }
            
        }
        

        
       
    }
    
    return cacheSize;
    
   // [self getFreeSpace];
    
    //return fileSizeStr;
    
}

-(NSString *)sizeOfFolder:(NSString *)folderPath
{
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *contentsEnumurator = [contents objectEnumerator];
    
    NSString *file;
    unsigned long long int folderSize = 0;
    
    while (file = [contentsEnumurator nextObject]) {
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:file] error:nil];
        folderSize += [[fileAttributes objectForKey:NSFileSize] intValue];
    }
    
    //This line will give you formatted size from bytes ....
    NSString *folderSizeStr = [NSByteCountFormatter stringFromByteCount:folderSize countStyle:NSByteCountFormatterCountStyleFile];
    
    NSLog(@"folderSizeStr folder :%@",folderSizeStr);
    
    return folderSizeStr;
}

- (long long)getFreeSpace
{
    long long freeSpace = 0.0f;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemFreeSizeInBytes = [dictionary objectForKey: NSFileSystemFreeSize];
        freeSpace = [fileSystemFreeSizeInBytes longLongValue];
    } else {
        //Handle error
    }
    
    NSLog(@"freeSpace : %lld",freeSpace);
    return freeSpace;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)testRefresh:(UIRefreshControl *)refreshControlH
{
    
    [refreshControl endRefreshing];
    /*DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readGuideClassification:@"second"];*/
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) classificaionAccessComplete:(NSArray*)resultArray
{
    NSArray *sortedArray=[[NSArray alloc]init];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    sortedArray=[resultArray sortedArrayUsingDescriptors:@[sort]];
    
    classIdArr=[[NSMutableArray alloc]initWithArray:sortedArray];
    
    //classIdArr=[[NSMutableArray alloc]initWithArray:resultArray];
    [self showPopUp];
}

-(void) classificaionAccessCompleteSecond:(NSArray*)resultArray
{
    NSArray *sortedArray=[[NSArray alloc]init];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    sortedArray=[resultArray sortedArrayUsingDescriptors:@[sort]];
    
    classIdArr=[[NSMutableArray alloc]initWithArray:sortedArray];
    
   //classIdArr=[[NSMutableArray alloc]initWithArray:resultArray];
    [guideSelectTable reloadData];
}
-(void)showPopUp
{
    customAlert = [[CustomIOSAlertView alloc] init];
    
    float popUpWidth=self.view.frame.size.width-(self.view.frame.size.width*10/100);
    float popUpHeight=self.view.frame.size.height-(self.view.frame.size.height*40/100);
    
   // UIView *viewInAlert = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight)];
   
    //viewInAlert.backgroundColor=[UIColor whiteColor];
    
    
    
    
    
    
    viewInAlert.layer.cornerRadius = 7;
    viewInAlert.clipsToBounds = YES;
    
    
    
    
    [customAlert setContainerView:viewInAlert];
    viewInAlert.frame=CGRectMake(0, distUnitSegment.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    [customAlert setDelegate:self];
    
    customAlert.buttonTitles=[[NSArray alloc]initWithObjects:@"OK",nil];
    

    CATransition *transition = [CATransition animation];
    transition.duration = 1.0;
    transition.type = kCATransitionFromLeft; //choose your animation
    [viewInAlert.layer addAnimation:transition forKey:nil];
    [self.view addSubview:viewInAlert];
    
    btnBackStorage = [[UIButton alloc]initWithFrame:bckBtn.frame];
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        CGRect frame;
        frame=CGRectZero;
        frame=btnBackStorage.frame;
        frame.origin.y=3;
        
        btnBackStorage.frame=frame;
        
    }
    
    
    btnBackStorage.backgroundColor=[UIColor clearColor];
    [btnBackStorage addTarget:self
                       action:@selector(myaction)
             forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBackStorage];
    
    
    
 
    // You may use a Block, rather than a delegate.
  /*  [customAlert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
       // [alertView close];
    }];*/
    
    [customAlert setUseMotionEffects:true];
    
    // And launch the dialog
    //[customAlert show];
    
    
   
    previos=-1;
    [guideSelectTable flashScrollIndicators];
    
    
    
  
    
    guideSelectTable=[[UITableView alloc]initWithFrame:viewInAlert.frame style:UITableViewStylePlain];
   // guideSelectTable.frame=CGRectMake(-12, 0, viewInAlert.frame.size.width+12, viewInAlert.frame.size.height);
    
    
  //  guideSelectTable.frame=CGRectMake(0, distUnitSegment.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height -distUnitSegment.frame.origin.y);
    
    guideSelectTable.frame=CGRectMake(0, bckBtn.frame.origin.y+bckBtn.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(bckBtn.frame.origin.y+bckBtn.frame.size.height+3));
    
    [self.view addSubview:guideSelectTable];
    
    
   // [viewInAlert addSubview:guideSelectTable];
    guideSelectTable.dataSource=self;
    guideSelectTable.delegate=self;
 //   guideSelectTable.backgroundColor=[UIColor redColor];
   // viewInAlert.backgroundColor=[UIColor greenColor];
    
    
    moreUpArrow=[[UIImageView alloc]init];
    moreUpArrow.image=[UIImage imageNamed:@"arrow-up.png"];
    [viewInAlert addSubview:moreUpArrow];
    moreUpArrow.hidden=YES;
      moreUpArrow.frame=CGRectMake((viewInAlert.frame.size.width/2)-10, 0, 20, 12.25);
    
    moreDownArrow=[[UIImageView alloc]init];
    moreDownArrow.image=[UIImage imageNamed:@"arrow-down.png"];
    [viewInAlert addSubview:moreDownArrow];
    moreDownArrow.hidden=YES;
    moreDownArrow.frame=CGRectMake((viewInAlert.frame.size.width/2)-10, viewInAlert.frame.size.height-15, 20, 12.25);
    
    
    
    [guideSelectTable reloadData];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    [guideSelectTable addSubview:refreshControl];
    

}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
       if(customAlert.tag==999)
       {
         [alertView close];
       }
       else{
           
           [alertView close];
     [self dismissViewControllerAnimated:NO completion:nil];
       }
    
}
#pragma mark - UITableView delegate and datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==911)
    {
        return 60;
    }
    return 55;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag==911)
    {
        return 1;
    }

    else
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==911)
    {
        if(!([muttarr count]==0))
        return [muttarr count]; //[muttarr count]
    }
    
    return [classIdArr count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if(section==0)
//    {
//
//        
//    }
    
    if(section==0)
        
    {
        if(tableView.tag==911)
        {
           
            //rgb(241, 241, 241)
        UILabel *lblBelowBanner=[[UILabel alloc]initWithFrame:CGRectMake(0,0, tableView.bounds.size.width, 30)];
        lblBelowBanner.backgroundColor= Rgb2UIColor(241, 241, 241);
        lblBelowBanner.text=@"  Offline Guides";
        lblBelowBanner.textAlignment=NSTextAlignmentLeft;
        lblBelowBanner.textColor=[UIColor darkGrayColor];
        lblBelowBanner.font=[UIFont systemFontOfSize:12];
        return lblBelowBanner;
        
        }
        else
        {
            UILabel *lblBelowBanner=[[UILabel alloc]initWithFrame:CGRectMake(0,0, tableView.bounds.size.width, 30)];
            lblBelowBanner.backgroundColor=Rgb2UIColor(241, 241, 241);
            lblBelowBanner.text=@"  Guide Type";
            lblBelowBanner.textAlignment=NSTextAlignmentLeft;
            lblBelowBanner.textColor=[UIColor darkGrayColor];
            lblBelowBanner.font=[UIFont systemFontOfSize:12];
            return lblBelowBanner;
        
        }
        
    }
    
    else{
        return nil;
        
    }
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return 30;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"celll"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }

    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if(tableView.tag==911)
    {
//        if([arrayForDetails count] ==0)
//        {
//            UILabel *errorLbl = [[UILabel alloc] init];
//            errorLbl.frame = CGRectMake(tblStorage.frame.origin.x+60,tblStorage.frame.origin.y+60,250,100);
//            errorLbl.numberOfLines = 0;
//            [errorLbl setFont:[UIFont systemFontOfSize:16]];
//            errorLbl.textColor = [UIColor darkGrayColor];
//            errorLbl.backgroundColor = [UIColor clearColor];
//            errorLbl.text =[NSString stringWithFormat:@"%@",cacheLabel.text];
//            [errorLbl sizeToFit];
//            //[cell addSubview:errorLbl];
//            
//            return cell;
//        }
       // NSDictionary *dict=[arrayForDetails objectAtIndex:indexPath.row];
        NSDictionary *dict=[muttarr objectAtIndex:indexPath.row];
        
       
        
        UIImageView *cellimage = [[UIImageView alloc]init];
        cellimage.frame=CGRectMake(5, 5, 50, 50);
       // cellimage.image =[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dict objectForKey:@"image"]]]];
      [cellimage setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholder:nil];
        [cell addSubview:cellimage];
        
        UILabel *titlelabelh = [[UILabel alloc] init];
        titlelabelh.frame = CGRectMake(cellimage.frame.origin.x+60, 10, 250 , 55);
        titlelabelh.numberOfLines = 0;
        [titlelabelh setFont:[UIFont systemFontOfSize:16]];
        titlelabelh.textColor = [UIColor darkGrayColor];
        titlelabelh.backgroundColor = [UIColor clearColor];
        titlelabelh.text =[dict objectForKey:@"app_name"];
        [titlelabelh sizeToFit];
        
       float i = titlelabelh.intrinsicContentSize.width;
        NSLog(@"titlelabelh width :%f",i);
        /* if(i>150)
         {
             CGRect widthTrunc;
             widthTrunc=CGRectZero;
             widthTrunc=titlelabelh.frame;
             widthTrunc.size.width=150;
             titlelabelh.frame=widthTrunc;
         }*/
        [cell addSubview:titlelabelh];
        
        UILabel *sizeLbl = [[UILabel alloc] init];
        sizeLbl.frame = CGRectMake(titlelabelh.frame.origin.x+5, titlelabelh.frame.origin.y+titlelabelh.frame.size.height+2, 60 , 15);
        sizeLbl.numberOfLines = 0;
        [sizeLbl setFont:[UIFont systemFontOfSize:12]];
        sizeLbl.textColor = [UIColor darkGrayColor];
        sizeLbl.backgroundColor = [UIColor clearColor];
        sizeLbl.text = [[myMutableArrayAgain objectAtIndex:indexPath.row]valueForKey:@"FolderSize"];//[muttarrSize objectAtIndex:indexPath.row];//reversedArray
        [sizeLbl sizeToFit];
        [cell addSubview:sizeLbl];
        
        UIButton *btnDel = [[UIButton alloc]init];
        btnDel.frame=CGRectMake(tblStorage.frame.size.width-80, titlelabelh.frame.origin.y+5, 60, 25);//titlelabelh.frame.origin.x+titlelabelh.frame.size.width+20
        [btnDel setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDel.titleLabel.font= [UIFont systemFontOfSize:12];
        [btnDel setBackgroundColor:[UIColor colorWithRed:(194/255.0) green:(0/255.0) blue:(10/255.0) alpha:1.0]];
        btnDel.layer.cornerRadius=5.0;
        [btnDel addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        btnDel.tag = indexPath.row;
        
        [cell addSubview:btnDel];
        
        
        reloadBtn = [[UIButton alloc]init];
        reloadBtn.frame=CGRectMake(tblStorage.frame.size.width-(70+45), titlelabelh.frame.origin.y+5, 30, 30);
        //titlelabelh.frame.origin.x+titlelabelh.frame.size.width+20
        //[reloadBtn setTitle:@"" forState:UIControlStateNormal];
        //[reloadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //reloadBtn.titleLabel.font= [UIFont systemFontOfSize:12];
        [reloadBtn setBackgroundColor:[UIColor clearColor]];
        [reloadBtn setBackgroundImage:[UIImage imageNamed:@"reloadBttn.png"] forState:UIControlStateNormal];
        reloadBtn.layer.cornerRadius=5.0;
        reloadBtn.tag = indexPath.row;
        [reloadBtn addTarget:self action:@selector(reloadd:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell addSubview:reloadBtn];
        
        
        return cell;
    }
    
    NSDictionary *dict=[classIdArr objectAtIndex:indexPath.row];
    
    UILabel *titlelabelh = [[UILabel alloc] init];
    titlelabelh.frame = CGRectMake(40, 2, 250 , 55);
    titlelabelh.numberOfLines = 2;
    [titlelabelh setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    titlelabelh.textColor = [UIColor darkGrayColor];
    titlelabelh.backgroundColor = [UIColor clearColor];
    titlelabelh.text =[dict objectForKey:@"name"];
   
   // moreArrow.hidden=YES;
    NSLog(@"%ld",(long)indexPath.row);
    if ([classIdArr count]>6) {
          moreUpArrow.hidden=NO;
        moreDownArrow.hidden=NO;
  
        if(indexPath.row>=([classIdArr count]-1)||(indexPath.row==8))
        { moreDownArrow.hidden=YES;
            
        }
        if(indexPath.row==0||indexPath.row==6)
        {
            moreUpArrow.hidden=YES;
        }
    }
    
    [cell addSubview:titlelabelh];
    
    
    UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectMake(guideSelectTable.frame.size.width-66, 12, 51, 31)];
    [onoff addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
     onoff.tag=indexPath.row;
    if([[dict objectForKey:@"status"] isEqualToString:@"0"])//@"1"
    {
        onoff.on=YES;
    }
    else
         onoff.on=NO;
    
    
    [cell addSubview: onoff];
    
    
    
    return cell;
}
-(IBAction)segmentValueChanged:(id)sender
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self];

    
    if (distUnitSegment.selectedSegmentIndex == 0) {
        [[NSUserDefaults standardUserDefaults]setObject:@"K" forKey:@"showDistanceIn"];
        lblSlider.text=@" 0.5km     1km     2km     3km     4km     5km    6km     7km     8km     9km     10km";
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"M" forKey:@"showDistanceIn"];
        lblSlider.text=@" 0.5mi      1mi      2mi      3mi      4mi      5mi     6mi      7mi      8mi      9mi      10mi";
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
}

-(IBAction)segmentViewValueChangedNumber:(id)sender
{
    if(_segmentView.selectedSegmentIndex==0)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:10 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==1)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:20 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==2)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:30 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==3)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:40 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==4)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:50 forKey:@"numberofAppsToShow"];
    }
}
-(IBAction)nearyByValueChanged:(id)sender
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self];

    
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readGuideClassification:@"first"];
    
    
   /* if ([nearBySwitch isOn]) {
        NSLog(@"switchOn");
        
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"nearbyisOFF"];
        
          [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
          [[NSUserDefaults standardUserDefaults]synchronize];
        
        [geoFeedSwitch setOn:NO];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"nearbyisOFF"];
        
        [geoFeedSwitch setOn:YES];
        
        NSLog(@"switchOff");
        
              [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"nearby"];
              [[NSUserDefaults standardUserDefaults]synchronize];
        
        DBOperations *obj=[[DBOperations alloc]init];
        obj.delegate=self;
        [obj readGuideClassification:@"first"];
    }*/
}

- (IBAction)geoFeedValeChanged:(id)sender {
    
    
    
    if ([geoFeedSwitch isOn]) {
       // NSLog(@"switchOn");
        
        [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"nearby"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [nearBySwitch setOn:NO];
        [nearBySwitch sendActionsForControlEvents:UIControlEventValueChanged];
    }
    else
    {
        [nearBySwitch setOn:YES];
        [nearBySwitch sendActionsForControlEvents:UIControlEventValueChanged];
        
      //  NSLog(@"switchOff");
        
        [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }

    
}

-(void)flip:(id)sender
{
   // [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"nearby"];
    
    UISwitch *tempSwitch=(UISwitch*)sender;
    NSString *switchTo;
    if (!tempSwitch.on==YES)//tempSwitch.on==YES
        switchTo=@"1";
    else
        switchTo=@"0";
        
    
    
    int tag= (int)((UISwitch*)sender).tag;
    
    NSDictionary *obj=[classIdArr objectAtIndex:tag];
    DBOperations *dbObj=[[DBOperations alloc]init];
    dbObj.delegate=self;
    [dbObj updateClassificationId:obj onOf:switchTo];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self];
    
    
}
-(void)processComplete
{
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readGuideClassification:@"second"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}


-(IBAction)back:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableView" object:self];
    
  //  PDInappHomeViewController *viewController = [[PDInappHomeViewController alloc ]init];
   // UINavigationController *navig1 = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self dismissViewControllerAnimated:NO completion:nil];
  
}

+ (void)clearTmpDirectory
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}
- (IBAction)btnClearCache:(id)sender
{
    [deleteBtn setTitle:@"Deleting" forState:UIControlStateNormal];
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    for (NSString *filename in fileArray)  {
        [fileMgr removeItemAtPath:[mainPath stringByAppendingPathComponent:filename] error:NULL];
    }

    
    NSLog(@"cachePath :%@",cachePath);
    
    cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@",[self sizeOfFolder:cachePath]];
    
    NSString *strg = [self sizeOfFile:nil];
    if (strg == (id)[NSNull null] || strg.length == 0 )
        {
          cacheLabel.text =[NSString stringWithFormat:@"Disk Cache : 0.0 Mb"];
        }
    else{
        
        cacheLabel.text =[NSString stringWithFormat:@"Disk Cache : %@", [self sizeOfFolder:cachePath]];
    
    }
    
    [customAlert close];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)manageStorage:(id)sender {
    
    [deleteBtn setTitle:@"Delete" forState:UIControlStateNormal];
    
    muttarr = [[NSMutableArray alloc]init];
    muttarrSize = [[NSMutableArray alloc]init];
    reversedArray = [[NSMutableArray alloc]init];
    arrayCacheP = [[NSMutableArray alloc]init];

    
    NSMutableDictionary *plistDict;
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
            
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
            }
            
        }
    }
    
    for(int i=0;i<[myMutableArrayAgain count];i++)
    {
   
      //  NSString *appsiz = [self sizeOFDownloadsFolderR:[[myMutableArrayAgain objectAtIndex:i]valueForKey:@"id"]];
        
       // NSLog(@"appsiz :%@",appsiz);
        
       // [reversedArray insertObject:[NSString stringWithFormat:@"%@ Mb",appsiz] atIndex:i];
        
        
        NSString *string = [NSString stringWithFormat:@"totalSize%@",[[myMutableArrayAgain objectAtIndex:i]valueForKey:@"id"]];
        
        NSLog(@"string :%@",string);
        
    NSString *sizeOfApp = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"totalSize%@",[[myMutableArrayAgain objectAtIndex:i]valueForKey:@"id"]]];
    
        NSLog(@"sizeOfApp :%@",sizeOfApp);
        
        if(!(sizeOfApp==nil))
        {
       // [reversedArray insertObject:sizeOfApp atIndex:i];
        }
        else
        {
          // [reversedArray insertObject:@"Error" atIndex:i];
        }
    }
//    NSLog(@"reversedArray :%@",reversedArray);
//    
//    NSLog(@"muttarrSize :%@",muttarrSize);
//    
//    double sum = 0;
//    for (NSNumber * n in arrayCacheP) {
//        sum += [n doubleValue];
//    }
//    
//    NSLog(@"sum :%f",sum);
//    
//    //cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@",[self sizeOfFolder:cachePath]];
//    NSString * sizeStr =[NSString stringWithFormat:@"%.2f",sum];
//    
//    NSString *size = [NSByteCountFormatter stringFromByteCount:sizeStr countStyle:NSByteCountFormatterCountStyleFile];
//    
//    cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@ Mb",sizeStr];
//    [cacheLabel sizeToFit];
//    
    
    
    [self popupManageStorage];
    
    
}

-(NSString*)sizeOFUrl:(NSString*)appid
{
    NSString *string =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",appid]]];
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL *urlforData = [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithURL: urlforData];
    
    [dataTask resume];
    
    return string;
}
-(NSString*)sizeOFDownloadsFolderR:(NSString*)appid
{
    // NSLog(@"HOME > %@", NSHomeDirectory());
    
    NSMutableArray *arrayCachePp;
    NSString *cachePathp;
    
    
    NSString* strCachePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    {
        
        
        NSError *error;
        NSMutableArray *arrayCachePathsp = [[NSMutableArray alloc] init];
        arrayCachePp = [[NSMutableArray alloc]init];
        
        int s =0;
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:strCachePath error:&error];
        for (NSString *strName in dirContents) {
            
            //  if([strName isEqualToString:@"com.pidigi.lgdev"])
            
            NSString *string =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",appid]]];
            
            {
                NSArray* foo = [string componentsSeparatedByString:@".zip"];
                
                NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                
                
                
                
                NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                
                NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                
                NSLog(@"firstBit folder :%@",firstBit);
                NSLog(@"secondBit folder :%@",secondBit2);
                
                
                
                
                
                if ([strName containsString:secondBit2]) {
                    
                    
                    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                    documentsURL = [documentsURL URLByAppendingPathComponent:secondBit2];
                    
                    
                    cachePathp = [NSString stringWithFormat:@"%@",documentsURL];
                    NSLog(@"cachePath :%@",cachePathp);
                    
                    
                    
                    
                    [arrayCachePathsp insertObject:cachePathp atIndex:s];
                    // NSString *test = [arrayCachePaths objectAtIndex:s];
                    NSLog(@"%@", arrayCachePathsp);
                    
                    NSString *str = [self sizeOfFolder:cachePathp];
                    
                    
                    
                    NSString *code = [str substringFromIndex: [str length] - 2];
                    NSLog(@"code :%@",code);
                    
                    if([code isEqualToString:@"MB"]||[code isEqualToString:@"GB"])
                    {
                        [arrayCachePp insertObject:str atIndex:s];
                    }
                    else
                    {
                        [arrayCachePp insertObject:@"0.1" atIndex:s];
                    }
                    s++;
                    
                }
                
            }
        }
    }
    
    
    NSLog(@"arrayCacheP :%@",arrayCachePp);
    
    
    
    double sum = 0;
    for (NSNumber * n in arrayCachePp) {
        sum += [n doubleValue];
    }
    
    NSLog(@"sum :%f",sum);
    
    //cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@",[self sizeOfFolder:cachePath]];
    NSString * sizeStr =[NSString stringWithFormat:@"%.2f",sum];
    
    NSString *size = [NSByteCountFormatter stringFromByteCount:sizeStr countStyle:NSByteCountFormatterCountStyleFile];
    
    
    
    return sizeStr;
    
    
}
-(NSString*)sizeOfAppFolder:(NSString*)appid
{
   
    NSLog(@"HOME > %@", NSHomeDirectory());
        
        NSMutableArray *arrayCacheP;
        NSString *cachePath;
        
        // NSFileManager *fileManagerr = [NSFileManager defaultManager];
        NSString* strCachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        // if([fileManagerr fileExistsAtPath:[cachePath stringByAppendingPathComponent:@"com.pidigi.lgdev"]])
        {
            
            
            NSError *error;
            NSMutableArray *arrayCachePaths = [[NSMutableArray alloc] init];
            arrayCacheP = [[NSMutableArray alloc]init];
            
            int s =0;
            NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:strCachePath error:&error];
            for (NSString *strName in dirContents) {
                
                //  if([strName isEqualToString:@"com.pidigi.lgdev"])
                {
                    if ([strName containsString:[NSString stringWithFormat:@"MyFolder%@",appid]]) {
                        
                        
                        cachePath = [NSString stringWithFormat:@"%@/%@/com.hackemist.SDWebImageCache.MyFolder%@",strCachePath,strName,appid];
                        NSLog(@"cachePath :%@",cachePath);
                        
                        
                        
                        
                        [arrayCachePaths insertObject:cachePath atIndex:s];
                        // NSString *test = [arrayCachePaths objectAtIndex:s];
                        NSLog(@"%@", arrayCachePaths);
                        
                        NSString *str = [self sizeOfFolder:cachePath];
                        
                        NSString *code = [str substringFromIndex: [str length] - 2];
                        NSLog(@"code :%@",code);
                        
                        if([code isEqualToString:@"MB"]||[code isEqualToString:@"GB"])
                        {
                            [arrayCacheP insertObject:str atIndex:s];
                        }
                        else
                        {
                            [arrayCacheP insertObject:str atIndex:s];
                        }
                        s++;
                        
                    }
                    
                }
            }
        }
        
        
        NSLog(@"arrayCachePsettings :%@",arrayCacheP);
        
        
        
        double sum = 0;
        for (NSNumber * n in arrayCacheP) {
            sum += [n doubleValue];
        }
        
        NSLog(@"sum :%f",sum);
        
        //cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@",[self sizeOfFolder:cachePath]];
        NSString * sizeStr =[NSString stringWithFormat:@"%.0f",sum];
        
        NSString *size = [NSByteCountFormatter stringFromByteCount:sizeStr countStyle:NSByteCountFormatterCountStyleFile];
        
        
        
        return sizeStr;
        

}

-(void)popupManageStorage
{

    
        customAlert = [[CustomIOSAlertView alloc] init];
        
        float popUpWidth=self.view.frame.size.width-(self.view.frame.size.width*10/100);
        float popUpHeight=self.view.frame.size.height-(self.view.frame.size.height*40/100);
        
       viewInAlert = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight)];
        
        //viewInAlert.backgroundColor=[UIColor whiteColor];
        
        
    
        viewInAlert.layer.cornerRadius = 7;
        viewInAlert.clipsToBounds = YES;
        
        
        
        
        [customAlert setContainerView:viewInAlert];
        viewInAlert.frame=CGRectMake(0, distUnitSegment.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        
        [customAlert setDelegate:self];
        
        customAlert.buttonTitles=[[NSArray alloc]initWithObjects:@"OK",nil];
        
        
    
        
        [customAlert setUseMotionEffects:true];
    
    CGRect frame;
    frame = CGRectZero;
    frame= deleteBtn.frame;
    frame.origin.x= (viewInAlert.frame.size.width/2)-(deleteBtn.frame.size.width/2);
    frame.origin.y= (viewInAlert.frame.size.height/2)-(deleteBtn.frame.size.height/2);
    deleteBtn.frame=frame;
    
    
    CGRect frame2;
    frame2 = CGRectZero;
    frame2= cacheLabel.frame;
    frame2.origin.x= (viewInAlert.frame.size.width/2)-(cacheLabel.frame.size.width/2);
    frame2.origin.y= (viewInAlert.frame.size.height/2)-(cacheLabel.frame.size.height+30);
    cacheLabel.frame=frame2;
    
    
    CGRect frame3;
    frame3 = CGRectZero;
    frame3= lblCap.frame;
    frame3.origin.x= (viewInAlert.frame.size.width/2)-(lblCap.frame.size.width/2);
    frame3.origin.y= (viewInAlert.frame.size.height/2)-(lblCap.frame.size.height+cacheLabel.frame.size.height+30);
    lblCap.frame=frame3;
    
    viewInAlert.backgroundColor=[UIColor whiteColor];
    
   // [viewInAlert addSubview:lblCap];
    
    //[viewInAlert addSubview:cacheLabel];
    
    //[viewInAlert addSubview:deleteBtn];
    
    customAlert.tag=999;
    
       // [customAlert show];
    
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0;
    transition.type = kCATransitionFromLeft; //choose your animation
    [viewInAlert.layer addAnimation:transition forKey:nil];
    [self.view addSubview:viewInAlert];
    
    btnBackStorage = [[UIButton alloc]initWithFrame:bckBtn.frame];
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        CGRect frame;
        frame=CGRectZero;
        frame=btnBackStorage.frame;
        frame.origin.y=3;
        
        btnBackStorage.frame=frame;
        
    }

    
    btnBackStorage.backgroundColor=[UIColor clearColor];
    [btnBackStorage addTarget:self
                 action:@selector(myaction)
       forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBackStorage];
    
    
   NSMutableArray *arr = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    NSLog(@"arr :%@",arr);
    
    
    
    arrayForDetails = [[NSMutableArray alloc]init];
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
          
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
                muttarr =[myMutableArrayAgain copy];
                
            }
        }}

    
    
  /*  for(int i=0;i<[muttarr count];i++)//(int i=0;i<[muttarr count];i++)
    {
      for(int j=0;j<[arr count];j++)//(int j=0;j<[arr count];j++)
     {
         if([muttarr count]==1)
         {
         
             NSArray *array = [muttarr valueForKey:@"id"];
             NSString *string = [array firstObject];
             
             if([string isEqualToString:[[arr objectAtIndex:j] valueForKey:@"id"]])
             {
                 
                 [arrayForDetails insertObject:[arr objectAtIndex:j] atIndex:0];
                 
             }
         }
         else
       if([[[muttarr objectAtIndex:i]valueForKey:@"id"] isEqualToString:[[arr objectAtIndex:j] valueForKey:@"id"]])
        {
          
            [arrayForDetails insertObject:[arr objectAtIndex:j] atIndex:0];
            
        }
     }
    
    }
    */
    
    NSLog(@"arrayForDetails :%@",arrayForDetails);
    
    tblStorage = [[UITableView alloc]initWithFrame:viewInAlert.frame style:UITableViewStylePlain];
    
    tblStorage.tag=911;
    tblStorage.delegate=self;
    tblStorage.dataSource=self;
    
   // tblStorage.backgroundColor=[UIColor redColor];

    tblStorage.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    tblStorage.frame=CGRectMake(0, bckBtn.frame.origin.y+bckBtn.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(bckBtn.frame.origin.y+bckBtn.frame.size.height+3));
    
    [self.view addSubview:tblStorage];
    
    
   // [tblStorage reloadData];
        
        
    
}

-(void)myaction
{
    [viewInAlert removeFromSuperview];
    [btnBackStorage removeFromSuperview];
    [tblStorage removeFromSuperview];
    
    [guideSelectTable removeFromSuperview];
    
}
- (IBAction)knowCommu:(id)sender {
    
    SettingsInnerViewController *abc =[[SettingsInnerViewController alloc]init];
    
   [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"forloader123"];
    
     [self presentViewController:abc animated:NO completion:nil];
    
//[[NSNotificationCenter defaultCenter] postNotificationName:@"forTableviewCellSelect" object:self];
    
   // [self dismissViewControllerAnimated:NO completion:nil];

}
- (IBAction)startGuide:(id)sender {
    
    loaderFI= [[UIView alloc]initWithFrame:self.view.frame];
    loaderFI.backgroundColor=[UIColor whiteColor];
    
    CGRect frame=CGRectZero;
    frame=loaderFI.frame;
    //frame.origin.y=settingsBtn.frame.origin.y+settingsBtn.frame.size.height+2;
    loaderFI.frame=frame;
    
    
    UIActivityIndicatorView *spinnerFI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinnerFI.center = CGPointMake([[UIScreen mainScreen]bounds].size.width / 2.0, ([[UIScreen mainScreen]bounds].size.height / 2.0)-frame.origin.y);
    [spinnerFI startAnimating];

    [loaderFI addSubview:spinnerFI];
    
    [self.view addSubview:loaderFI];
    
    [self staticWS];
    
    
   /* SettingsInnerViewController *abc =[[SettingsInnerViewController alloc]init];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"forloader123"];
    
    [self presentViewController:abc animated:NO completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"tableCellStartGuide" object:self];*/
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"forTableviewCellSelect" object:self];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"startOunGuide" object:self];
//    
//    [self dismissViewControllerAnimated:NO completion:nil];
    
}
-(void)staticWS
{
   // http://insightto.com/webservice/staticpage.php?id=1770

    
    {
        NSString *wUrl=[NSString stringWithFormat:@"http://insightto.com/webservice/staticpage.php?id=1770"];
        //SETTINGS_WEBSERVICE_SUB
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(data==nil)
            {
                [self staticWS];
                return ;
            }
            
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"URL :%@", wUrl);
            NSLog(@"json :%@", json);
            
            
            arraystatic=[[NSMutableArray alloc]init];
            arraystatic=[json objectForKey:@"static"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
                PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
                //NSDictionary *dict= [arraystatic objectAtIndex:0];[NSDictionary dictionaryWithObjects:records
           // forKeys:[records valueForKey:@"intField"]];
               
                
               // [[json objectForKey:@"static"]insertObject:@"type" atIndex:1];
                //[[json objectForKey:@"static"] setObject:@"static" forKey:@"type"];
                
                
                 NSMutableArray *mut = [[NSMutableArray alloc]initWithArray:arraystatic];
                
                [mut insertObject:@"Type" atIndex:0];
                
//                NSMutableArray *mutt =[[NSMutableArray alloc]init];
//                mutt = [[mut objectAtIndex:1]copy];
//                NSLog(@"mutt :%@",mutt);
              //  [mut setObject:@"type" forKey:@"preview"];
                
                NSMutableDictionary *combinedAttributes;
                
                if([[mut objectAtIndex:1] isKindOfClass:[NSDictionary class]])
                {
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                    [dictionary setObject:@"static" forKey:@"type"];
                    NSLog(@"dictionary :%@",dictionary);
                    
                    combinedAttributes = [[mut objectAtIndex:1] mutableCopy];
                    [combinedAttributes addEntriesFromDictionary:dictionary];
                    
                   
                    NSLog(@"combinedAttributes :%@",combinedAttributes);
                    
                    //[[mut objectAtIndex:1]setObject:@"static" forKey:@"type"];
                   
                }
                else
                {
                    [[mut objectAtIndex:1] addObject:@"type"];
                }
        
                
                
              //  [mutt setValue:@"static" forKey:@"type"];
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                dic = [combinedAttributes copy];
                
                [detailViewController getDictionary:dic];
                detailViewController.hidesBottomBarWhenPushed = YES;
                detailViewController.isFromFrom=YES;
                [self presentViewController:detailViewController animated:NO completion:nil];
            
            });
        }];
        
        [dataTask resume];
        
    }
    
}
- (IBAction)review:(id)sender {
    
    NSString *theUrl = @"https://itunes.apple.com/us/app/localsguide/id1094964995?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];

    
}
- (IBAction)facebook:(id)sender {
    
    NSURL *facebookURL = [NSURL URLWithString:@"https://www.facebook.com/localsguideapp/?ref=bookmarks"];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/625769634120952"]];
    }
    else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/625769634120952"]];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://profile/625769634120952"]]) {
            
           
        }
        else
        {
             [[UIApplication sharedApplication] openURL:facebookURL];
        
        }
    }

    
}

-(void)deleteBtnAction:(UIButton*)sender {
    
    NSMutableDictionary *plistDict;
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
    
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
            }
            
            
            if (!myMutableArrayAgain) myMutableArrayAgain = [[NSMutableArray alloc] init];
            
            
            
            
            
        }
    }

    
    //[tblStorage setEditing:YES animated:YES];
    
     NSLog(@"deleteBtn tag : %ld",(long)sender.tag);
    
   // [deleteBtn setTitle:@"Deleting" forState:UIControlStateNormal];
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    
    
    
   /* for (NSString *filename in fileArray)  {
        
        if ([filename containsString:[NSString stringWithFormat:@"MyFolder%@",[[arrayForDetails objectAtIndex:sender.tag]valueForKey:@"id"]]])
        {
           
         // reversedArray
          [fileMgr removeItemAtPath:[mainPath stringByAppendingPathComponent:filename] error:NULL];
            
            
            
            
        }
        
    }
    */
    
     NSLog(@"check for app id: %@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]);
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"urlZip%@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]]];
    
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]]];
    
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"100Percent%@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]]];
    
    
     [myMutableArrayAgain removeObjectAtIndex:sender.tag];
    // [reversedArray removeObjectAtIndex:sender.tag];
    
   
    
   // [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"urlZip%@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]]];
    
    
    [plistDict setObject:myMutableArrayAgain forKey:@"downloadsArray"];
    [plistDict writeToFile:finalPath atomically:YES];
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:sender.tag inSection:1];
    
   
    
   // deleteRowsAtIndexPaths:withRowAnimation
   // if (!muttarr)
        muttarr = [[NSMutableArray alloc] init];
    
//    if ([muttarr isKindOfClass: [NSMutableArray class]])
//    {
//        [muttarr removeObjectAtIndex:path];
//    }
//    else
    {
        muttarr = [myMutableArrayAgain copy];
    
    }
    
    
    [tblStorage reloadData];
    
 //  [tblStorage deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
    
}

- (IBAction)toSTableVC:(id)sender {
    
   // sTableViewController *sTB = [[sTableViewController alloc]init];
    
    //[self presentViewController:sTB animated:YES completion:nil];
}

-(void)reloadd:(UIButton*)sender
{
    NSLog(@"sender tag reload :%ld",(long)sender.tag);
    
    
    
    m_testView = [[UICircle alloc] initWithFrame:CGRectMake(tblStorage.frame.size.width, reloadBtn.frame.origin.y+10, 120, 40)];
    m_testView.percent = 0;
    m_testView.backgroundColor=[UIColor whiteColor];
    
    
    //[viewInAlert addSubview:m_testView];
    
    
    UITableViewCell* cell = (UITableViewCell*)[sender superview];
    NSIndexPath* indexPath = [tblStorage indexPathForCell:cell];
    
    [cell.contentView addSubview:m_testView];
   // [tblStorage addSubview:m_testView];
   // reloadBtn.hidden=YES;
    
    
     
     CGPoint touchPoint = [sender convertPoint:CGPointZero toView:tblStorage]; // maintable --> replace your tableview name
     NSIndexPath *clickedButtonIndexPath = [tblStorage indexPathForRowAtPoint:touchPoint];
     
     NSLog(@"index path.section ==%ld",(long)clickedButtonIndexPath.section);
     
     NSLog(@"index path.row ==%ld",(long)clickedButtonIndexPath.row);
    
    [[NSUserDefaults standardUserDefaults]setInteger:sender.tag
                                              forKey:@"index-path.row"];
    //[tblStorage cellForRowAtIndexPath:clickedButtonIndexPath].accessoryView = m_testView;
    UITableViewCell* cell1 = [tblStorage cellForRowAtIndexPath:clickedButtonIndexPath];
    cell1.accessoryView = m_testView;
    
    tblStorage.userInteractionEnabled=false;
    btnBackStorage.enabled=NO;
    bckBtn.enabled=NO;
    
    [self test];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    completionHandler(NSURLSessionResponseAllow);
    
    
    _downloadSize=[response expectedContentLength];
    _dataToDownload=[[NSMutableData alloc]init];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    [_dataToDownload appendData:data];
    // progressBar.progress=[ _dataToDownload length ]/_downloadSize;
    
    NSLog(@"_dataToDownload STORAGE :%lu  ,_downloadedSize STORAGE :%f",(unsigned long)[_dataToDownload length ],_downloadSize); //[_dataToDownload length ]/_downloadSize
    
    
    float percenta = (float)_downloadSize;
    
    float sizeOFFoler=(float)[_dataToDownload length];
    
    NSLog(@"sizeFolder STORAGE :%f",sizeOFFoler);
    
    float sizee = ((float)sizeOFFoler/(float)percenta)*100;//6.0f / 100.0f);
    
    NSLog(@"sizee STORAGE:%f",sizee);
    
    NSLog(@"m_testView.percent STORAGE:%f",m_testView.percent);
    
    if(sizee >99)
    {
      m_testView.hidden=YES;
        
        btnBackStorage.enabled=YES;
        bckBtn.enabled=YES;
        tblStorage.userInteractionEnabled=true;
        
       

    }
    if (sizee < 100) {
        
        if (m_testView.percent < 1)
        {
            m_testView.percent = (float)sizee;
            [m_testView setNeedsDisplay];
        }
        else
        {
            m_testView.percent = (int)sizee;
            [m_testView setNeedsDisplay];
        }
        
    }
    
    
}
-(void)test
{
    NSInteger indexpathe =[[NSUserDefaults standardUserDefaults]integerForKey:@"index-path.row"];
    
    NSLog(@"indexpathe :%ld",(long)indexpathe);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@%@&multi=yes",OFFLINE_WEBSERVICE,[[myMutableArrayAgain objectAtIndex:indexpathe]valueForKey:@"id"]];
    
    // NSString *wUrl=[NSString stringWithFormat:@"%@1&multi=yes",OFFLINE_WEBSERVICE];
    NSLog(@"url = %@",wUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self test];
            return ;
        }
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
        // NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *totalSize = [json objectForKey:@"total_file_size"];
            
            NSString *urlZip = [json objectForKey:@"app_image_folder"];
            
            [[NSUserDefaults standardUserDefaults]setObject:urlZip forKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            [self downloadZip];
            
            NSLog(@"totalSize :%@",totalSize);
            
            [[NSUserDefaults standardUserDefaults]setObject:totalSize forKey:@"TS"];
            
          
        });
        
    }];
    
    [dataTask resume];
    
    
    
}

-(void)downloadZip
{
    //    dispatch_queue_t queue = dispatch_get_global_queue(0,0);
    //    dispatch_async(queue, ^{
    
    NSString *url1 = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
    
    // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSLog(@"Downloading Started");
    
    
    
    
    //  NSURL *url = [NSURL URLWithString:url1];
    NSURL *url = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        NSInteger httpStatus = [((NSHTTPURLResponse *)response) statusCode];
        NSLog(@"responsecode:%ld", (long)httpStatus);
        
        
        if (error||httpStatus == 404) {
            
            NSLog(@"Download Error:%@",error.description);
            
            
            btnBackStorage.enabled=YES;
            bckBtn.enabled=YES;
            tblStorage.userInteractionEnabled=true;
            
            m_testView.hidden=YES;
            
//            UITableViewCell* cell = (UITableViewCell*)[sender superview];
//            NSIndexPath* indexPath = [tblStorage indexPathForCell:cell];
//            
//            [cell.contentView addSubview:m_testView];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"System error occured"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            return ;
        }
        if (data) {
            
            
            //                NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            //                [[NSUserDefaults standardUserDefaults]setObject:@"yess" forKey:ida];
            
            
            // find Documents directory and append your local filename
            NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
            documentsURL = [documentsURL URLByAppendingPathComponent:@"localFile.zip"];
            
            // and finally save the file
            [data writeToURL:documentsURL atomically:YES];
            
            
            NSString *filename = @"localFile.zip";
            NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString * zipPath = [documentsDirectory stringByAppendingPathComponent:filename];
            
            NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
            
            
            NSLog(@"File is saved  ");
            
            btnBackStorage.enabled=YES;
            bckBtn.enabled=YES;
            tblStorage.userInteractionEnabled=true;
            
        }
        
        
        
        
        
        
        // list contents of Documents Directory just to check
        NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        
        NSArray *contents = [[NSFileManager defaultManager]contentsOfDirectoryAtURL:documentsURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
        
        NSLog(@"description for zip:%@", [contents description]);
        
        
        
        //753_514
        
        /*   NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
         SDImageCache *imageCache =[SDImageCache sharedImageCache];
         imageCache = [imageCache initWithNamespace:imageCacheFolder];
         
         NSString *imageUrl ;//=destinationPath;
         
         UIImageView*imagevw=[[UIImageView alloc]init];
         
         
         [imageCache storeImage:imagevw.image forKey:imageUrl completion:^{  }];*/
        
    }];
    
    
    ///
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL *urlforData = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithURL: urlforData];
    
    [dataTask resume];
    
    ///
    
    
    
    
    // });
    
    
}

-(void)sizeCapDeleteAction
{
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    
     NSString *str = [mainPath stringByAppendingPathComponent:@"JMCache"];
    
    NSError* error;
    NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:mainPath error: &error];
    NSNumber *size = [fileDictionary objectForKey:NSFileSize];
    
   uint64_t fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:mainPath error:nil] fileSize];
    
    NSLog(@"size of file :%@",size);NSLog(@"size of file :%llu",fileSize);
    
    unsigned long long int fileSizeee = 0;
    
    fileSizeee = [self folderSize:mainPath];
    
    NSLog(@"fileSizeee :%llu",fileSizeee);
    
    for (NSString *filename in fileArray)  {
        
       // [fileMgr removeItemAtPath:[mainPath stringByAppendingPathComponent:filename] error:NULL];
    }
    


}

- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}

@end
