//
//  Static.m
//  Visiting Ashland
//
//  Created by Anu on 11/01/16.
//  Copyright (c) 2016 iDeveloper. All rights reserved.
//

#import "Static.h"

@implementation Static

@dynamic    appid;
@dynamic    catid;
@dynamic    address;
@dynamic    content;
@dynamic    image;
@dynamic    latitude;
@dynamic    longitude;
@dynamic    staticId;
@dynamic    status;
@dynamic    title;

@dynamic  email;
@dynamic  email_text;
@dynamic  favorites;
@dynamic  phone;
@dynamic  share;
@dynamic  show_email;
@dynamic  show_phone;
@dynamic  show_url;
@dynamic  urll;
@dynamic  url_text;

@end
