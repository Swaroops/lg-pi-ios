
#import <Foundation/Foundation.h>

#import "ImageScrollView.h"


@interface ImageScrollView () <UIScrollViewDelegate>
{
    
    CGSize _imageSize;
    CGPoint _pointToCenterAfterResize;
    CGFloat _scaleToRestoreAfterResize;
   
    
}

@end

@implementation ImageScrollView

@synthesize zoomView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
        
    }
    
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
  
    CGSize ont=self.contentSize;
    

    
    
  
    
    
    float imgx,imgy,imgHt,ImgWd;
    
    CGSize imgsiz=_imageSize;
    
    float aspectratio=imgsiz.width/imgsiz.height;
     aspectratio=imgsiz.width==0||imgsiz.height==0?0:aspectratio;
    if (imgsiz.width>imgsiz.height) {
        ImgWd=ont.width;
        imgHt=ImgWd/aspectratio;
        if (imgHt>ont.height) {
            imgHt=ont.height;
            ImgWd=imgHt*aspectratio;
        }
        
        imgx=0;
        imgy=(ont.height-imgHt)/2;
        
    }
    else {
        imgHt=ont.height;
        ImgWd=imgHt*aspectratio;
        if (ImgWd>ont.width) {
            ImgWd=ont.width;
            imgHt=ImgWd/aspectratio;
        }
        
        imgy=0;
        imgx=(ont.width-ImgWd)/2;
        
    }
    
    ImgWd=ImgWd==0?[[UIScreen mainScreen]bounds].size.width:ImgWd;
    imgHt=imgHt==0?[[UIScreen mainScreen]bounds].size.height:imgHt;
    CGRect tst= CGRectMake(imgx,imgy,ImgWd,imgHt);
    
    self.zoomView.frame=tst;
    float widd,htt;
    
    if(self.contentSize.width<[[UIScreen mainScreen]bounds].size.width)
        widd=[[UIScreen mainScreen]bounds].size.width/2;
    else
        widd=self.contentSize.width/2;
    
    
    if(self.contentSize.height<[[UIScreen mainScreen]bounds].size.height)
        htt=[[UIScreen mainScreen]bounds].size.height/2;
    else
        htt=self.contentSize.height/2;
    
    self.zoomView.center=CGPointMake(widd, htt);
    self.contentSize=ont;
    
     
    
}

- (void)setFrame:(CGRect)frame
{
    BOOL sizeChanging = !CGSizeEqualToSize(frame.size, self.frame.size);
    
    if (sizeChanging) {
        [self prepareToResize];
    }
    
    [super setFrame:frame];
    
    if (sizeChanging) {
        [self recoverFromResizing];
    }
}
-(void)photZoomOut
{
    if( _isZoomed )
    {
        _isZoomed = NO;
        [self setZoomScale:self.minimumZoomScale animated:NO];
         [self layoutSubviews];
        
    }
   
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
 //   [self layoutSubviews];
    if (touch.tapCount == 2) {
        
        if( _isZoomed )
        {
            _isZoomed = NO;
            [self setZoomScale:self.minimumZoomScale animated:YES];
        }
        else {
            
            _isZoomed = YES;
            
            [self setZoomScale:self.maximumZoomScale animated:YES];
            
            
        }
    }
    
    
}


#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    float contentSizWid=self.zoomView.frame.size.width;
    float contenSizeHr=self.zoomView.frame.size.height;
    self.contentSize=CGSizeMake(contentSizWid,contenSizeHr);
        
    NSLog(@"ZOOOOM");
    
    return self.zoomView;
}

- (void)displayImage:(UIImage *)image
{
     self.contentSize=CGSizeMake(image.size.width,image.size.height);
    [self configureForImageSize:image.size];
}

- (void)configureForImageSize:(CGSize)imageSize
{
   _imageSize = imageSize;
  

      self.zoomScale = self.minimumZoomScale;
}

- (void)setMaxMinZoomScalesForCurrentBounds
{
    CGSize boundsSize = self.bounds.size;
    
    
   
    
    
    CGFloat xScale = boundsSize.width  / _imageSize.width;
    CGFloat yScale = boundsSize.height / _imageSize.height;
    
    BOOL imagePortrait = _imageSize.height > _imageSize.width;
    BOOL phonePortrait = boundsSize.height > boundsSize.width;
    CGFloat minScale = imagePortrait == phonePortrait ? xScale : MIN(xScale, yScale);
    
    CGFloat maxScale = 3.0 / [[UIScreen mainScreen] scale];
    
    if (minScale > maxScale) {
        minScale = maxScale;
    }
    
    self.maximumZoomScale = maxScale;
    self.minimumZoomScale = 0.25;//minScale;
}

#pragma mark - Rotation support

- (void)prepareToResize
{
    CGPoint boundsCenter = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    _pointToCenterAfterResize = [self convertPoint:boundsCenter toView:self.zoomView];
    
    _scaleToRestoreAfterResize = self.zoomScale;
    
    if (_scaleToRestoreAfterResize <= self.minimumZoomScale + FLT_EPSILON)
        _scaleToRestoreAfterResize = 0;
}

- (void)recoverFromResizing
{
    [self setMaxMinZoomScalesForCurrentBounds];
    
    CGFloat maxZoomScale = MAX(self.minimumZoomScale, _scaleToRestoreAfterResize);
    self.zoomScale = MIN(self.maximumZoomScale, maxZoomScale);
    
    CGPoint boundsCenter = [self convertPoint:_pointToCenterAfterResize fromView:self.zoomView];
    
    CGPoint offset = CGPointMake(boundsCenter.x - self.bounds.size.width / 2.0,
                                 boundsCenter.y - self.bounds.size.height / 2.0);
    
    CGPoint maxOffset = [self maximumContentOffset];
    CGPoint minOffset = [self minimumContentOffset];
    
    CGFloat realMaxOffset = MIN(maxOffset.x, offset.x);
    offset.x = MAX(minOffset.x, realMaxOffset);
    
    realMaxOffset = MIN(maxOffset.y, offset.y);
    offset.y = MAX(minOffset.y, realMaxOffset);
    
    self.contentOffset = offset;
}

- (CGPoint)maximumContentOffset
{
    CGSize contentSize = self.contentSize;
    CGSize boundsSize = self.bounds.size;
    return CGPointMake(contentSize.width - boundsSize.width, contentSize.height - boundsSize.height);
}

- (CGPoint)minimumContentOffset
{
    return CGPointZero;
}


@end



