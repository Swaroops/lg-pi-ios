//
//  Inapp.h
//  Visiting Ashland
//
//  Created by Anu on 09/11/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Inapp : NSManagedObject
@property (nonatomic, retain) NSString * appid;
@property (nonatomic, retain) NSString * catid;
@property (nonatomic, retain) NSString * categoryType;
@property (nonatomic, retain) NSString * paidStatus;
@end
