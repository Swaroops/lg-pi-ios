//
//  PDIntermediatePageViewController.h
//  Visiting Ashland
//
//  Created by swaroop on 22/06/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDIntermediatePageViewController : UIViewController
{
    IBOutlet AsyncImageView * splashImgv;
    IBOutlet UIView *vwLoading;
    IBOutlet UIActivityIndicatorView *activityIndicator1;
    IBOutlet UIButton *downloadButton;
    IBOutlet UIButton *onlineButton;
    IBOutlet UIButton *bckBtn;
    
    IBOutlet UIImageView *guideImage;
    
    IBOutlet UIButton *btnpaid;
    
}

@property(nonatomic)BOOL *fromSearchToInter;

@end
