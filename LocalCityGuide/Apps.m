//
//  Apps.m
//  Visiting Ashland
//
//  Created by Anu on 15/10/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "Apps.h"

@implementation Apps
@dynamic appid;
@dynamic image;
@dynamic name;
@dynamic settings;
@dynamic footerList;
@dynamic lastSync;
@dynamic inappsettings;
@dynamic appHeaderImg;

@end
