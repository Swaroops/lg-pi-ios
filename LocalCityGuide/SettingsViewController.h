//
//  SettingsViewController.h
//  Visiting Ashland
//
//  Created by Anu on 28/04/16.
//  Copyright © 2016 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
{
    IBOutlet UISwitch *nearBySwitch;
    
    IBOutlet UISwitch *offlineSwitch;
    
    IBOutlet UISegmentedControl *distUnitSegment;
    
    NSMutableArray *classIdArr;
    
    UITableView *guideSelectTable;
    int previos;
    UIImageView *moreUpArrow;
    UIImageView *moreDownArrow;
    
    NSMutableArray *arrayCacheP;
    
    IBOutlet UILabel *versionLbl;
    
    IBOutlet UIImageView *imgTree;
    
    IBOutlet UIButton *deleteBtn;
    
    IBOutlet UILabel *cacheLabel;
    
    IBOutlet UILabel *lblCap;
    
    IBOutlet UILabel *lblSlider;
    
    IBOutlet UISwitch *geoFeedSwitch;
    
    IBOutlet UIButton *bckBtn;
}

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentView;
@end
